import 'dart:convert';
import 'package:booket_flutter/api/api.dart';
import 'package:booket_flutter/localization/localization_delegate.dart';
import 'package:booket_flutter/blocs/router_bloc.dart';
import 'package:booket_flutter/blocs/authentication_bloc.dart';
import 'package:booket_flutter/blocs/database_bloc.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

import 'package:flutter/material.dart';
// import 'package:package_info/package_info.dart';

class MainStateContainer extends StatefulWidget {
  final Widget child;
  final dynamic config;
  final dynamic initialError;
  final dynamic me;
  final dynamic cart;
  final String hostName;
  final RouterBloc mainRouter;
  final ThemeData sharedTheme;
  final int randomShit;
  final String token;
  final Locale locale;
  final int chatTabNumber;
  final int readerPageNumber;
  final List<dynamic> booksList;
  final List<dynamic> degreesList;
  final List<dynamic> universitiesList;
  final List<dynamic> studyFieldsList;
  final List<dynamic> citiesList;
  final AuthenticationBloc authenticationBloc;
  final dynamic connectionsMap;
  final bool mainTabBarDragable;
  final DemoLocalizationsDelegate localizationDelegate;
  final int themeCode;
  final bool categoriezedLoader;
  final bool brandizedLoader;
  final bool gestureEnable;
  final String selectionType;
  MainStateContainer(
      {Key key,
      @required this.child,
      this.me,
      this.cart,
      this.hostName,
      this.sharedTheme,
      this.degreesList,
      this.universitiesList,
      this.studyFieldsList,
      this.citiesList,
      this.authenticationBloc,
      this.token,
      this.localizationDelegate,
      this.locale,
      this.chatTabNumber,
      this.booksList,
      this.connectionsMap,
      this.mainRouter,
      this.initialError,
      this.mainTabBarDragable,
      this.config,
      // this.prefrencesBloc,
      this.themeCode,
      this.randomShit,
      this.categoriezedLoader,
      this.brandizedLoader,
      this.gestureEnable,
      this.selectionType,
      this.readerPageNumber});

  @override
  StateContainerState createState() => StateContainerState(
        UniqueKey(),
        this.me,
        this.cart,
        this.child,
        this.hostName,
        this.degreesList,
        this.universitiesList,
        this.studyFieldsList,
        this.citiesList,
        this.sharedTheme,
        this.authenticationBloc,
        this.token,
        this.localizationDelegate,
        this.locale,
        this.chatTabNumber,
        this.booksList,
        this.connectionsMap,
        this.mainRouter,
        this.initialError,
        this.mainTabBarDragable,
        // this.prefrencesBloc,
        this.randomShit,
        this.themeCode,
      );

  static StateContainerState of(BuildContext context) {
    // bayad in be home page am montaghel she
    if (context != null)
      return (context.dependOnInheritedWidgetOfExactType<InheritedContainer>()
              as InheritedContainer)
          .data;
    // return (context.dependOnInheritedWidgetOfExactType()
    //         as InheritedContainer)
    //     .data;
    else
      return null;
  }
}

class StateContainerState extends State<MainStateContainer> {
  dynamic me;
  Widget child;
  AuthenticationBloc authenticationBloc;
  String hostName;
  int chatTabNumber = 0;
  String connectionStatusString;
  dynamic config;
  String selectionType = "highlight";
  // SideNav sideNav;
  RouterBloc mainRouter;
  // ProductsBloc productsBloc;
  dynamic initialError;
  ThemeData sharedTheme;
  dynamic connectionsMap = {};
  GlobalKey<ScaffoldState> scaffoldKey;
  // PrefrencesBloc prefrencesBloc;

  // DatabaseBloc databaseBloc;
  // BasketRepo basketRepo = BasketRepo();
  DemoLocalizationsDelegate localizationDelegate;
  List<dynamic> booksList;
  List<dynamic> degreesList;
  List<dynamic> universitiesList;
  List<dynamic> studyFieldsList;
  List<dynamic> citiesList;
  dynamic basket;
  dynamic cart;
  Locale locale;
  bool reloadingOrders = false;
  bool reloadingReviews = false;
  int readerPageNumber = 0;
  String token;
  bool updateFormLoader = false;
  bool mainTabBarDragable = true;
  int themeCode;
  bool categorizedLoader = true;
  bool gestureEnable = true;
  dynamic deviceInfo;
  bool brandizedLoader = true;
  int randomShit;
  bool processImage = false;
  bool processImages = false;
  dynamic sim_dynamic_data = {};
  bool processGuides = false;
  bool mahram = kIsWeb;

  // SimData simData;
  String deviceId;

  String gPlayLink =
      "https://play.google.com/store/apps/details?id=com.amestris.jerse.android";
  String appstoreLink = "https://apps.apple.com/app/id1502997572";
  String windowsLink = "https://jersestore.com/apps/jerse-windows-msi.msi";
  String macLink = "https://jersestore.com/apps/jerse-macos-dmg.dmg";
  String linuxLink = "https://jersestore.com/apps/jerse-linux-appImage";

  String lastPathBeforeAuth = "";

  // PackageInfo packageInfo;

  StateContainerState(
      Key key,
      this.me,
      this.cart,
      this.child,
      this.hostName,
      this.degreesList,
      this.universitiesList,
      this.studyFieldsList,
      this.citiesList,
      this.sharedTheme,
      this.authenticationBloc,
      this.token,
      this.localizationDelegate,
      this.locale,
      this.chatTabNumber,
      this.booksList,
      this.connectionsMap,
      this.mainRouter,
      this.initialError,
      this.mainTabBarDragable,
      // this.prefrencesBloc,
      this.randomShit,
      this.themeCode);

  void setCategorizedLoader(bool sit) {
    setState(() {
      categorizedLoader = sit;
    });
  }

  void setSelectionType(String s) {
    setState(() {
      selectionType = s;
    });
  }

  void setGestureEnable(s) {
    setState(() {
      gestureEnable = s;
    });
  }

  void setReaderPage(s) {
    setState(() {
      readerPageNumber = s;
    });
  }

  void setLastPath(String s) {
    setState(() {
      lastPathBeforeAuth = s;
    });
  }

  void setWifiConnectionStatus(s) {
    String string = s;
    setState(() {
      connectionStatusString = string;
    });
  }

  void setProcessImage(bool s) {
    var c = s;
    setState(() {
      processImage = c;
    });
  }

  // void setPackageInfo(PackageInfo pi) {
  //   var i = pi;
  //   setState(() {
  //     packageInfo = i;
  //   });
  // }

  void setDeviceId(String id) {
    var i = id;
    setState(() {
      deviceId = i;
    });
  }

  void setProcessImages(bool s) {
    var c = s;
    setState(() {
      processImages = c;
    });
  }

  void setProcessGuides(bool s) {
    var c = s;
    setState(() {
      processGuides = c;
    });
  }

  void setReloadingOrders(bool b) {
    var reload = b;
    setState(() {
      reloadingOrders = reload;
    });
  }

  void setReloadingReviews(bool b) {
    var reload = b;
    setState(() {
      reloadingOrders = reload;
    });
  }

  void setBrandizedLoader(bool sit) {
    var b = sit;
    setState(() {
      brandizedLoader = b;
    });
  }

  // void setProductsBloc(ProductsBloc pb) {
  //   var b = pb;
  //   setState(() {
  //     productsBloc = b;
  //   });
  // }

  void setUpdateUserLoader(bool x) {
    setState(() {
      updateFormLoader = x;
    });
  }

  // void setDeviceInfo(dynamic d) {
  //   Api.setHeader("platform", d);
  //   setState(() {
  //     deviceInfo = d;
  //   });
  // }

  void updateUserInfo(dynamic usr) {
    var user = usr;
    // must check bad gateway and    401 error just before calling this func  // and Connection Err :/

    setState(() {
      me = user;
    });
  }

  void updateUserInfoSooski(String key, var data) {
    var updateinfo = data;
    setState(() {
      me[key] = updateinfo;
    });
  }

  void setMahramity(bool b) {
    setState(() {
      mahram = b;
    });
  }

  void setSimData(dynamic simDt, dynamic summary) {
    // Api.setHeader("sim_data", summary); // should  not be done
    setState(() {
      // simData = simDt;
      sim_dynamic_data = summary;
    });
  }

  void updateUserInfoSooskiDeep(String key, String key2, var data) {
    var updateinfo = data;
    setState(() {
      me[key][key2] = updateinfo;
    });
  }

  void updateAddresses(List<dynamic> addresses) {
    setState(() {
      me["addresses"] = addresses;
    });
  }

  void resetBasket(dynamic basket) {
    basket["map"] = {};
    var z = 0;
    for (var order in basket["orders"]) {
      final id = order["product_id"] * (1 + order["card_id"]);
      order["id"] = id;
      basket["map"][id.toString()] = order;
    }
    setState(() {
      me["basket"] = basket;
    });
  }

  // Future<dynamic> addToBasket(int productId, int cardId, int sign) async {
  //   // add to basket single size , irad dare ... and free size ezafe she
  //   final res = await basketRepo.add(hostName, {
  //     "productId": productId.toString(),
  //     "cardId": cardId.toString(),
  //     "sign": sign.toString()
  //   });
  //   resetBasket(res["basket"]);
  //   return res;
  // }

  // Future<dynamic> removeFromBasket(int cartId) async {
  //   dynamic map = me["basket"]["map"];
  //   List orders = me["basket"]["orders"];
  //   var o = map[cartId.toString()];
  //   // dynamic orders =
  //   dynamic toBeDeletedOrder = map[cartId.toString()];
  //   map.remove(cartId.toString());

  //   var index = -1;
  //   for (var i = 0; i < orders.length; i++) {
  //     final order = orders[i];
  //     if ((order["product_id"] == o["product_id"]) &&
  //         (order["card_id"] == o["card_id"])) {
  //       index = i;
  //       break;
  //     }
  //   }
  //   orders.removeAt(index);
  //   setState(() {
  //     // me["basket"]["orders"] = orders;
  //     me["basket"]["map"] = map;
  //     me["basket"]["totalValue"] = (me["basket"]["totalValue"] -
  //         (toBeDeletedOrder["price"] * toBeDeletedOrder["count"]));
  //     me["basket"]["orders"] = orders;
  //     me["basket"]["count"] = me["basket"]["count"] - toBeDeletedOrder["price"];
  //   });
  //   return await basketRepo
  //       .remove(hostName, {"order_id": o["order_id"].toString()});
  // }

  void setMainDragableTabbar(bool situation) {
    print("sit done ! \n");
    setState(() {
      mainTabBarDragable = situation;
    });
  }

  void setInitialError(dynamic err) {
    var error = err;
    setState(() {
      initialError = error;
    });
  }

  void setChatTabNumber(pageNum) {
    var x = pageNum;
    setState(() {
      chatTabNumber = x;
    });
  }

  void setScaffoldKey(key) {
    var x = key;
    setState(() {
      scaffoldKey = x;
    });
  }

  void setToken(tk) {
    Api.setCookie(tk);
    var t = tk;
    setState(() {
      token = t;
    });
  }

  void setCart(cartReq) {
    var c = cartReq;
    setState(() {
      cart = c;
    });
  }

  // Future<void> updateLocalization(Locale locale) async {
  //   localizationDelegate = new DemoLocalizationsDelegate();
  //   setState(() {
  //     localizationDelegate = localizationDelegate;
  //   });
  //   await setLocale(locale);
  // }

  void setLibraryList(games) {
    var g = games;
    setState(() {
      booksList = g;
    });
  }

  void setDegreesList(degrees) {
    var d = degrees;
    setState(() {
      degreesList = d;
    });
  }

  void setUniversitiesList(universities) {
    var u = universities;
    setState(() {
      universitiesList = u;
    });
  }

  void setStudyFieldsList(studyFields) {
    var s = studyFields;
    setState(() {
      studyFieldsList = s;
    });
  }

  void setCitiesList(cities) {
    var c = cities;
    setState(() {
      citiesList = c;
    });
  }

  // Future<void> setConfig(newConf) async {
  //   var conf = newConf;
  //   if ((newConf["fa"] != null) && !kIsWeb) {
  //     setState(() {
  //       mahram = true;
  //     });
  //     Api.setDicJson(newConf["fa"]);
  //     setLocale(new Locale("fa", "FA"));
  //   } else if (kIsWeb) {
  //     setState(() {
  //       mahram = true;
  //     });
  //     // setLocale(new Locale("fa", "FA"));
  //   }
  //   setState(() {
  //     config = conf;
  //   });
  //   // await Future.delayed(Duration(milliseconds: 300));
  //   return;
  // }

  Future<void> updateHostName(String name) async {
    if (name != hostName) {
      print(hostName);
      setState(() {
        hostName = name;
      });
      await this.authenticationBloc.databaseBloc.setItem("HOST", name);
    }
    return;
  }

  // Future<void> setLocale(Locale localex) async {
  //   if (localex.languageCode != locale.languageCode) {
  //     final Locale x = localex;
  //     setState(() {
  //       locale = x;
  //     });
  //     prefrencesBloc.setLanguage(localex.languageCode);
  //     await this
  //         .authenticationBloc
  //         .databaseBloc
  //         .setItem("LANG", localex.languageCode.toString());
  //   }
  //   return;
  // }

  // Future<void> setTheme(int number) async {
  //   if (number != themeCode) {
  //     var x = number;
  //     setState(() {
  //       themeCode = x;
  //     });
  //     prefrencesBloc.setTheme(x);
  //     await this.authenticationBloc.databaseBloc.setItem("THEME", x.toString());
  //   }
  //   return;
  // }

  void setMainRouter(rt) {
    setState(() {
      mainRouter = rt;
    });
  }

  void removeUser() {
    setState(() {
      me = null;
    });
  }

  // scket dassan

  @override
  Widget build(BuildContext context) {
    return InheritedContainer(
      child: widget.child,
      data: this,
    );
  }
}

class InheritedContainer extends InheritedWidget {
  final Widget child;
  final StateContainerState data;

  InheritedContainer({Key key, @required this.data, @required this.child})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}
