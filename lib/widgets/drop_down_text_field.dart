import 'package:after_layout/after_layout.dart';
import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:dropdown_plus/dropdown_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DropDownTextFieldWidget extends StatefulWidget {
  final String text;
  final String label;
  final List<dynamic> contentList;
  final ValueChanged<dynamic> onChanged;

  const DropDownTextFieldWidget({
    Key key,
    this.text,
    this.label,
    this.contentList,
    this.onChanged,
  }) : super(key: key);
  @override
  DropDownTextFieldWidgetState createState() => DropDownTextFieldWidgetState();
}

class DropDownTextFieldWidgetState extends State<DropDownTextFieldWidget> {
  final AccountsRepo accountsRepo = new AccountsRepo();

  // TextEditingController controller;
  ValueNotifier<dynamic> controller;

  @override
  void initState() {
    super.initState();

    controller = ValueNotifier({"title": widget.text.toString()});
  }

  @override
  void dispose() {
    controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
        const SizedBox(height: 3),
        Container(
          // color: Colors.blue,
          height: 40.0,
          child: DropdownFormField<dynamic>(
            // emptyText: "salam",
            // emptyActionText: "salamm",
            controller: controller,
            onEmptyActionPressed: () async {},
            decoration: InputDecoration(
              isDense: true,
              contentPadding: EdgeInsets.all(7.0),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(7.0),
              ),
              suffixIcon: Icon(Icons.arrow_drop_down),
              // labelText: "Access"
            ),
            onSaved: (dynamic str) {
              // widget.onChanged(str);
            },
            onChanged: (dynamic str) {
              widget.onChanged(str);
            },
            validator: (dynamic str) {},
            displayItemFn: (dynamic item) => Text(
              (item ?? {})['title'] ?? '',
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 14),
            ),
            findFn: (dynamic str) async => widget.contentList,
            selectedFn: (dynamic item1, dynamic item2) {
              if (item1 != null && item2 != null) {
                return item1['title'] == item2['title'];
              }
              return false;
            },
            filterFn: (dynamic item, str) =>
                item['title'].toLowerCase().indexOf(str.toLowerCase()) >= 0,
            dropdownItemFn: (dynamic item, int position, bool focused,
                    bool selected, Function() onTap) =>
                ListTile(
              title: Text(
                item['title'],
                style: TextStyle(fontSize: 14),
              ),
              // subtitle: Text(
              //   item['desc'] ?? '',
              // ),
              tileColor:
                  focused ? Color.fromARGB(20, 0, 0, 0) : Colors.transparent,
              onTap: onTap,
            ),
          ),
        ),
      ],
    );
  }
}
