import 'dart:async';

import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/blocs/grid_card_bloc.dart';
import 'package:booket_flutter/blocs/tabs_bloc.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/models/route_argument.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:booket_flutter/tabs/library.dart';
import 'package:booket_flutter/tabs/profile.dart';
import 'package:booket_flutter/tabs/vitrine.dart';
import 'package:booket_flutter/widgets/books.dart';
import 'package:booket_flutter/widgets/refresh_tab.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:lottie/lottie.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

class TabsPage extends StatefulWidget {
  final int pageIndex;
  const TabsPage({Key key, this.pageIndex}) : super(key: key);
  @override
  _TabsPageState createState() => _TabsPageState(this.pageIndex);
}

class _TabsPageState extends State<TabsPage> with TickerProviderStateMixin {
  TabController _tabController;
  AnimationController _controller;
  ScrollController parentCont, childCont;
  String paginateUrl;
  int pageIndex = 0;
  bool open;
  TabsBloc tabsBloc;
  bool expandedSearch = false;
  bool logoOpacity = true;
  AccountsRepo accountsRepo = new AccountsRepo();
  bool loader = false;
  bool goTop = false;
  String lastQuery;
  bool oneTime = true;
  int paginateIndex = 1;
  List<dynamic> searchList;
  GridCardBloc gridCardBloc = new GridCardBloc(0);
  var delay;
  _TabsPageState(this.pageIndex);

  @override
  void initState() {
    this.childCont = ScrollController(initialScrollOffset: 0.0);
    this.parentCont = ScrollController(initialScrollOffset: 0.0);
    this.childCont.addListener(_scrollListener);
    this.open = false;
    tabsBloc = new TabsBloc(this.pageIndex);
    _tabController =
        TabController(length: 3, initialIndex: this.pageIndex, vsync: this);
    _tabController.addListener(() {
      if (_tabController.indexIsChanging)
        ;
      else if (_tabController.index != _tabController.previousIndex)
        tabsBloc.setIndex(_tabController.index);
    });

    _controller = AnimationController(vsync: this);
//     _controller.addListener(() {
//       print(_controller.value);
//       //  if the full duration of the animation is 8 secs then 0.5 is 4 secs
//       if (_controller.value > 0.5) {
// // When it gets there hold it there.
//         _controller.value = 0.5;
//       }
//     });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    return Container(
      color: Theme.of(context).accentColor,
      child: SafeArea(
        top: true,
        bottom: false,
        child: MediaQuery.removePadding(
          removeTop: true,
          context: context,
          child: AutoRefresh(
            gridCardBloc: gridCardBloc,
            child: Scaffold(
              floatingActionButton: goTop
                  ? FloatingActionButton(
                      backgroundColor: Theme.of(context).accentColor,
                      onPressed: () {
                        parentCont.animateTo(0.0,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.easeInCubic);
                        childCont.animateTo(0.0,
                            duration: Duration(milliseconds: 200),
                            curve: Curves.easeInCubic);
                        setState(() {
                          goTop = true;
                        });
                      },
                      child: Icon(Icons.arrow_upward,
                          color: Colors.white, size: 25.0))
                  : Container(),
              resizeToAvoidBottomInset: false,
              bottomNavigationBar: bottomNavigationBar(context),
              body: Stack(
                fit: StackFit.expand,
                children: [
                  Positioned(
                      left: 0,
                      right: 0,
                      top: 0,
                      height: 60.0,
                      child: Container(
                        color: Theme.of(context).accentColor,
                      )),
                  Positioned.fill(
                    top: 60.0,
                    child: TabBarView(
                      dragStartBehavior: DragStartBehavior.start,
                      controller: _tabController,
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        Profile(),
                        Library(),
                        Vitrine(),
                      ],
                    ),
                  ),
                  Positioned(
                      top: 15,
                      left: 15,
                      right: 15,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          AnimatedContainer(
                            duration: const Duration(milliseconds: 250),
                            child: Center(
                              child: AnimatedOpacity(
                                duration: const Duration(milliseconds: 100),
                                opacity: logoOpacity ? 1.0 : 0.0,
                                child: StreamBuilder<int>(
                                    stream: tabsBloc.getTabsStream(),
                                    builder: (context, snapshot) {
                                      if (snapshot.data != null) {
                                        return Text(setTitle(snapshot.data),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.w200));
                                      } else {
                                        return Container();
                                      }
                                    }),
                              ),
                            ),
                          ),
                          AnimatedContainer(
                            duration: const Duration(milliseconds: 250),
                            child: Center(
                                child: AnimatedOpacity(
                                    duration: const Duration(milliseconds: 100),
                                    opacity: logoOpacity ? 1.0 : 0.0,
                                    child: PlatformSvgWidget.asset(
                                        'assets/images/booket_logo.svg',
                                        color: Colors.white,
                                        height: 18))),
                          ),
                        ],
                      )),
                  // buildFloatingSearchBar(context)
                  Container(
                    // margin: EdgeInsets.only(top: 4),
                    child: buildFloatingSearchBar(context),
                  ),
                  // AnimatedPositioned(
                  //   duration: Duration(milliseconds: 300),
                  //   child: buildFloatingSearchBar(context),
                  //   left: 0,
                  //   right: 0,
                  //   top: 0,
                  //   height: open ? MediaQuery.of(context).size.height : 60.0,
                  // ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget bottomNavigationBar(context) {
    final iw = MainStateContainer.of(context);
    return SizedBox(
      child: new TabBar(
        controller: _tabController,
        onTap: (i) {
          // tabsBloc.setIndex(i);
          setState(() {
            pageIndex = i;
          });
        },
        tabs: [
          Container(
            // color: Colors.red,
            width: 50.0,
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.only(right: 8.0),
                  height: 60,
                  child: Tab(
                    icon: Icon(Octicons.person, size: 33),
                  ),
                ),
                pageIndex != 0 &&
                        iw.cart != null &&
                        iw.cart["orders"].length > 0
                    ? Positioned(
                        bottom: 2,
                        left: 0,
                        child: Stack(
                          children: [
                            Icon(
                              MaterialIcons.work,
                              size: 24,
                              color: Colors.orange,
                            ),
                            Positioned.fill(
                              // bottom: 4,
                              // left: 0,
                              top: 2.0,
                              right: 0.5,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    DemoLocalizations.of(context).transNumber(
                                        iw.cart["orders"].length.toString()),
                                    style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.white,
                                        fontWeight: FontWeight.normal),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    : Container(height: 0),
              ],
            ),
          ),
          SizedBox(
            height: 60,
            child: Tab(
              icon: Icon(
                Ionicons.ios_book,
                size: 32.0,
              ),
            ),
          ),
          SizedBox(
            height: 60,
            child: Tab(
              icon: Icon(MaterialCommunityIcons.store, size: 32.0),
            ),
          ),
        ],
        labelColor: Theme.of(context).accentColor,
        unselectedLabelColor: Colors.black,
        indicatorSize: TabBarIndicatorSize.label,
        indicatorPadding: EdgeInsets.fromLTRB(2, 0, 2, 10),
        indicatorColor: Theme.of(context).accentColor,
        indicatorWeight: 5.0,
      ),
    );
  }

  Widget buildFloatingSearchBar(context) {
    final isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;

    return FloatingSearchBar(
      margins: EdgeInsets.only(top: 9.8),
      elevation: 2,
      // accentColor: Colors.blue,
      automaticallyImplyBackButton: false,
      backgroundColor: Colors.white60,
      backdropColor: Colors.black26.withOpacity(0.7),
      iconColor: Theme.of(context).primaryColorDark,
      scrollPadding: const EdgeInsets.only(top: 20, bottom: 20),
      borderRadius: BorderRadius.circular(20),
      hint: DemoLocalizations.of(context).trans("Search"),
      hintStyle: TextStyle(fontSize: 14),
      height: 38.0,
      controller: FloatingSearchBarController(),
      transitionCurve: Curves.easeInOut,
      axisAlignment: isPortrait ? 0.0 : 0.0,
      openAxisAlignment: 0.0,
      openWidth: MediaQuery.of(context).size.width - 50,
      // width: isPortrait ? 200 : 500,
      width: MediaQuery.of(context).size.width / 2,
      debounceDelay: const Duration(milliseconds: 500),
      closeOnBackdropTap: true,
      clearQueryOnClose: true,
      onQueryChanged: (query) {
        // print(query);
        if (query == "")
          setState(() {
            searchList = null;
          });
        else {
          // delay = Future.delayed(Duration(milliseconds: 500)).then((_) {
          if (query != "") {
            print(query.toString());
            setState(() {
              searchList = [];
            });
            search(query.toString());
          } else {
            print("query : " + query);
          }
          // });
        }

        // setState(() {
        //   search(query);
        // });
      },
      onFocusChanged: (f) {
        setState(() {
          this.logoOpacity = !this.logoOpacity;
          open = f;
        });
      },
      actions: [
        FloatingSearchBarAction.searchToClear(
          size: 25,
          showIfClosed: true,
          color: Theme.of(context).primaryColorDark,
        ),
      ],
      transition: CircularFloatingSearchBarTransition(),
      physics: const BouncingScrollPhysics(),
      builder: (context, transition) {
        final pageWidth = MediaQuery.of(context).size.width;
        int count = pageWidth < 350.0
            ? 1
            : pageWidth < 550.0
                ? 2
                : pageWidth < 900.0
                    ? 3
                    : pageWidth < 1000.0
                        ? 4
                        : pageWidth < 1300.0
                            ? 5
                            : 6;
        return searchList != null
            ? ClipRRect(
                borderRadius: BorderRadius.circular(7),
                child: Material(
                  color: Colors.white,
                  elevation: 4.0,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                          // height: 50,
                          color: Colors.white,
                          child: searchList.length == 0
                              ? ClipRRect(
                                  borderRadius: BorderRadius.circular(7),
                                  child: Material(
                                      color: Colors.white,
                                      elevation: 4.0,
                                      child: Container(
                                          height: 50,
                                          color: Colors.white,
                                          child: Center(
                                            child: CircularProgressIndicator(),
                                          ))),
                                )
                              : searchList[0] != "empty"
                                  ? Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          height: MediaQuery.of(context)
                                              .size
                                              .height,
                                          child: LimitedBox(
                                            maxWidth: pageWidth > 700.0
                                                ? 680.0
                                                : pageWidth - 60,
                                            child: AnimationLimiter(
                                              child: ListView.builder(
                                                padding: EdgeInsets.all(15),
                                                controller: childCont,
                                                itemExtent:
                                                    MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            (count + 1) +
                                                        30,
                                                itemCount: searchList.length,
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  return Column(
                                                    children: [
                                                      Expanded(
                                                        child: Container(
                                                          padding:
                                                              EdgeInsets.only(
                                                                  bottom: 15.0),
                                                          child:
                                                              RepaintBoundary(
                                                                  key: Key('str' +
                                                                      index
                                                                          .toString()),
                                                                  child: AnimationConfiguration
                                                                      .staggeredList(
                                                                    position:
                                                                        index,
                                                                    duration: const Duration(
                                                                        milliseconds:
                                                                            500),
                                                                    delay: const Duration(
                                                                        milliseconds:
                                                                            100),
                                                                    child:
                                                                        SlideAnimation(
                                                                      verticalOffset:
                                                                          50.0,
                                                                      child:
                                                                          FadeInAnimation(
                                                                        child:
                                                                            Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.spaceAround,
                                                                          crossAxisAlignment:
                                                                              CrossAxisAlignment.center,
                                                                          children: [
                                                                            Container(
                                                                              padding: const EdgeInsets.only(left: 10.0),
                                                                              width: MediaQuery.of(context).size.width / (count + 1) - 20,
                                                                              child: GestureDetector(
                                                                                onTapDown: (s) {
                                                                                  setState(() {
                                                                                    // selectedBooksList.removeAt(0);
                                                                                    Navigator.of(context).pushNamed('/books/' + searchList[index]["data"]["token"], arguments: RouteArgument(id: 12, argumentsList: [true, "salam"]));
                                                                                  });
                                                                                },
                                                                                child: new Books(searchList[index]["data"]["front_cover"]["medium"], 1.0, null, count),
                                                                              ),
                                                                            ),
                                                                            // Container(width: 5.0), // spacer
                                                                            Expanded(
                                                                                child: Container(
                                                                              child: Column(
                                                                                mainAxisAlignment: MainAxisAlignment.end,
                                                                                children: [
                                                                                  // Spacer(),
                                                                                  Container(
                                                                                    padding: EdgeInsets.only(left: 10.0, top: 10.0),
                                                                                    child: Column(children: [
                                                                                      Row(
                                                                                        children: [
                                                                                          Flexible(
                                                                                              child: GestureDetector(
                                                                                            onTapDown: (s) {
                                                                                              setState(() {
                                                                                                // selectedBooksList.removeAt(0);
                                                                                                Navigator.of(context).pushNamed('/books/' + searchList[index]["data"]["token"], arguments: RouteArgument(id: 12, argumentsList: [true, "salam"]));
                                                                                              });
                                                                                            },
                                                                                            child: Text(
                                                                                              searchList[index]["data"]["title"].toString(),
                                                                                              maxLines: 2,
                                                                                              overflow: TextOverflow.ellipsis,
                                                                                              textAlign: TextAlign.right,
                                                                                              style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w600),
                                                                                            ),
                                                                                          )),
                                                                                        ],
                                                                                      ),
                                                                                      Container(
                                                                                        height: 10.0,
                                                                                      ),
                                                                                      Container(
                                                                                        padding: EdgeInsets.only(left: 15.0),
                                                                                        child: Row(
                                                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                                                          children: List.generate(searchList[index]["data"]["authors"].length, (i) {
                                                                                            return Container(
                                                                                                child: i == 0
                                                                                                    ? Text(
                                                                                                        searchList[index]["data"]["authors"][i]["title"].toString(),
                                                                                                        maxLines: 1,
                                                                                                        overflow: TextOverflow.ellipsis,
                                                                                                        softWrap: false,
                                                                                                        style: TextStyle(fontSize: 12.0),
                                                                                                      )
                                                                                                    : Text(
                                                                                                        i < 1
                                                                                                            ? ", " + searchList[index]["data"]["authors"][i]["title"].toString()
                                                                                                            : i == 1
                                                                                                                ? ", ..."
                                                                                                                : "",
                                                                                                        maxLines: 1,
                                                                                                        overflow: TextOverflow.ellipsis,
                                                                                                        softWrap: false,
                                                                                                        style: TextStyle(fontSize: 12.0),
                                                                                                      ));
                                                                                          }),
                                                                                        ),
                                                                                      ),
                                                                                    ]),
                                                                                  ),
                                                                                  Spacer(),
                                                                                ],
                                                                              ),
                                                                            )),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  )),
                                                        ),
                                                      ),
                                                      Container(
                                                        margin: const EdgeInsets
                                                            .symmetric(
                                                          horizontal: 8.0,
                                                        ),
                                                        padding:
                                                            EdgeInsets.only(
                                                                bottom: 100),
                                                        width: double.infinity,
                                                        height: 1.0,
                                                        color: Colors
                                                            .grey.shade400,
                                                      ),
                                                      Container(
                                                        height: 12,
                                                      ),
                                                    ],
                                                  );
                                                },
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  : ClipRRect(
                                      borderRadius: BorderRadius.circular(7),
                                      child: Material(
                                          color: Colors.white,
                                          elevation: 4.0,
                                          child: Column(
                                            children: [
                                              Container(
                                                height: 150.0,
                                                child: Lottie.asset(
                                                  "assets/images/Search_Not_Found.json",
                                                  repeat: false,
                                                  width: 150,
                                                  // animate: true,
                                                  // controller: _controller,
                                                  //     onLoaded: (comp) {
                                                  //   _controller
                                                  //     ..duration = comp.duration
                                                  //     ..lowerBound
                                                  //     ..forward();
                                                  // }
                                                ),
                                              ),
                                              Container(
                                                  padding: EdgeInsets.only(
                                                      bottom: 30.0),
                                                  child: Text(DemoLocalizations
                                                          .of(context)
                                                      .trans(
                                                          "Search_Not_Found"))),
                                            ],
                                          )))),
                      !oneTime
                          ? Center(
                              child: Container(
                                  margin: EdgeInsets.only(bottom: 30.0),
                                  height: 30.0,
                                  width: 30.0,
                                  child: CircularProgressIndicator()),
                            )
                          : Container(),
                    ],
                  ),
                ),
              )
            : Container();
      },
    );
  }

  Future<void> search(query) async {
    final iw = MainStateContainer.of(context);
    print(query);
    setState(() {
      lastQuery = query;
    });
    await accountsRepo
        .search(
      iw.hostName,
      query,
    )
        .then((searchRes) {
      if ((lastQuery == query) && (searchList != null)) {
        setState(() {
          if (searchRes["data"].isEmpty) {
            searchList.add("empty");
          } else {
            searchList = searchRes["data"];
            paginateUrl = searchRes["path"];
          }
        });
      } else {}
    });
  }

  Future<void> paginateRequest() {
    // final iw = MainStateContainer.of(context);
    accountsRepo
        .getSearchPaginate(this.paginateUrl, (paginateIndex + 1).toString())
        .then((value) {
      setState(() {
        searchList.addAll(value["data"]);
        this.paginateUrl = value["path"];
        paginateIndex++;
        oneTime = true;
      });
    });
  }

  void _scrollListener() {
    ScrollPosition sp;
    sp = this.childCont.positions.elementAt(0);
    if (sp != null && this.childCont.positions != null) {
      if (sp.pixels <= (150) && !sp.outOfRange) {
        if (goTop) {
          if (mounted) {
            setState(() {
              goTop = false;
            });
            parentCont.animateTo(0.0,
                duration: Duration(milliseconds: 200),
                curve: Curves.easeInOutCubic);
            // }
          }
        }
      } else if (sp.pixels > (150) && !sp.outOfRange) {
        if (!goTop) {
          if (mounted) {
            setState(() {
              goTop = true;
            });
            parentCont.animateTo(225.0,
                duration: Duration(milliseconds: 200),
                curve: Curves.easeInOutCubic);
          }
        } else if (sp.pixels + 200 >= sp.maxScrollExtent &&
            !sp.outOfRange &&
            oneTime) {
          setState(() {
            oneTime = false;
            paginateRequest();
          });
          print("reach the bottom");
        }
      }
    } else {
      print(sp.toString());
    }
  }

  String setTitle(int index) {
    if (index == 0)
      return DemoLocalizations.of(context).trans("Profile");
    else if (index == 1)
      return DemoLocalizations.of(context).trans("Library");
    else
      return DemoLocalizations.of(context).trans("Home");
  }
}
