import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:booket_flutter/widgets/expireDate.dart';
// import 'package:ok_image/ok_image.dart';
import 'package:transparent_image/transparent_image.dart';
// import 'package:flutter_image/network.dart';

class Books extends StatefulWidget {
  // Books({Key key});
  final String address;
  final double padding;
  final int count;
  final String _expireDate;

  Books(this.address, this.padding, this._expireDate, this.count);
  // : super(key: key);

  @override
  BooksState createState() =>
      BooksState(this.address, this.padding, this._expireDate, this.count);
}

class BooksState extends State<Books> {
  @override
  String address;
  double padding;
  int count;
  String _expireDate;

  BooksState(this.address, this.padding, this._expireDate, this.count);

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    final height = (MediaQuery.of(context).size.width / count) * 8 / 5;
    final width = MediaQuery.of(context).size.width / count;

    final _heightExp = height * 1 / 8;
    final _widthExp = width / 2;

    return Padding(
      padding: EdgeInsets.all(padding),
      child: Center(
        child: SizedBox(
          // height: height,
          // width: width,
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: height,
                width: width,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Container(color: Theme.of(context).dividerColor)),
              ),
              CircularProgressIndicator(),
              Container(
                height: height,
                width: width,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    //   child: OKImage(
                    //     // url: "https://ws1.sinaimg.cn/large/844036b9ly1fxfo76hzd4j20zk0nc48i.jpg",
                    //     url: iw.hostName + address,
                    // //      loadingWidget: CircularProgressIndicator(),
                    //     width: 200,
                    //     height: 200,
                    //     timeout: Duration(seconds: 20),
                    //     // fit: fit,
                    //     // onLoadStateChanged: _onLoadStateChange,
                    //   ),
                    child: FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      imageErrorBuilder: (context, url, err) =>
                          Container(child: Text("")),
                      image: iw.hostName + address,
                      fit: BoxFit.fill,
                    )),
              ),
              Positioned(
                top: 10,
                left: 10.0,
                height: _heightExp,
                // right: 10.0,
                width: _widthExp,
                child: Container(
                    decoration: new BoxDecoration(color: Colors.transparent),
                    child: _expireDate != null
                        ? new ExpireDate(_expireDate, _heightExp, _widthExp)
                        : Container()),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
