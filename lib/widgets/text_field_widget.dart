import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldWidget extends StatefulWidget {
  final int maxLines;
  final int maxLength;
  final String label;
  final String text;
  final bool enabled;
  final bool filled;
  final ValueChanged<String> onChanged;

  const TextFieldWidget({
    Key key,
    this.maxLines = 1,
    this.label,
    this.text,
    this.enabled,
    this.filled,
    this.onChanged,
    this.maxLength,
  }) : super(key: key);

  @override
  _TextFieldWidgetState createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  TextEditingController controller;

  @override
  void initState() {
    super.initState();

    controller = TextEditingController(text: widget.text);
  }

  @override
  void dispose() {
    controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            widget.label,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          ),
          const SizedBox(height: 3),
          Container(
            height:
                widget.maxLines == 1 ? 40.0 : widget.maxLines.toDouble() * 50,
            // width: 100.0,
            child: TextField(
              maxLengthEnforcement:
                  MaxLengthEnforcement.truncateAfterCompositionEnds,
              maxLength: widget.maxLength,
              style: TextStyle(fontSize: 15.0),
              // keyboardType: TextInputType.multiline,
              textAlignVertical: TextAlignVertical.top,
              controller: controller,
              enabled: widget.enabled,
              decoration: InputDecoration(
                filled: widget.filled,
                counterText: "",
                isDense: true,
                contentPadding: EdgeInsets.all(7),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(7),
                ),
              ),
              maxLines: widget.maxLines,
              onChanged: widget.onChanged,
            ),
          ),
        ],
      );
}
