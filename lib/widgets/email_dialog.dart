import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:image_picker/image_picker.dart';

class EmailDialog extends StatefulWidget {
  EmailDialog({Key key}) : super(key: key);

  @override
  EmailDialogState createState() => EmailDialogState();
}

class EmailDialogState extends State<EmailDialog> {
  bool sendResult = false;
  List<String> attachments = [];

  // final _recipientController = TextEditingController(
  //   text: 'example@example.com',
  // );

  final _subjectController = TextEditingController(text: '');

  final _bodyController = TextEditingController(
    text: '',
  );

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  EmailDialogState();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      child: Container(
        height: 450,
        child: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(0.0, 20.0, 0, 20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  child: PlatformSvgWidget.asset('assets/images/booket.svg',
                      color: Colors.black, height: 80.0)),
              Container(
                margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 5.0),
                child: Center(
                  child: Text(
                    DemoLocalizations.of(context).trans("Subject"),
                    style: TextStyle(
                        color: Theme.of(context).primaryColorDark,
                        fontSize: 15.0,
                        fontWeight: FontWeight.normal),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Container(
                height: 35.0,
                margin: EdgeInsets.fromLTRB(30.0, 0, 30.0, 0),
                child: TextField(
                  controller: _subjectController,
                  // onChanged: (s) {
                  //   // iw.authenticationBloc.updatePhoneNumber(s);
                  // },
                  keyboardType: TextInputType.text,
                  textAlign: TextAlign.center,
                  autofocus: false,
                  style: TextStyle(
                      color: Theme.of(context).primaryColorDark,
                      fontSize: 15.0,
                      fontWeight: FontWeight.normal),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding:
                        const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(7.0),
                        borderSide: BorderSide(
                            color: Theme.of(context)
                                .primaryColorDark
                                .withOpacity(0.3))),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(7.0),
                        borderSide: BorderSide(
                            color: Theme.of(context).primaryColorDark)),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                child: Center(
                  child: Text(
                    DemoLocalizations.of(context).trans("Body"),
                    style: TextStyle(
                        color: Theme.of(context).primaryColorDark,
                        fontSize: 15.0,
                        fontWeight: FontWeight.normal),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Container(
                height: 110.0,
                margin: EdgeInsets.fromLTRB(30.0, 0, 30.0, 10),
                child: TextField(
                  controller: _bodyController,
                  // onChanged: (s) {
                  //   // iw.authenticationBloc.updatePhoneNumber(s);
                  // },
                  maxLines: null,
                  expands: true,
                  textAlignVertical: TextAlignVertical.top,
                  keyboardType: TextInputType.text,
                  autofocus: false,
                  style: TextStyle(
                      color: Theme.of(context).primaryColorDark,
                      fontSize: 14.0,
                      fontWeight: FontWeight.normal),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    contentPadding:
                        const EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(7.0),
                        borderSide: BorderSide(
                            color: Theme.of(context)
                                .primaryColorDark
                                .withOpacity(0.3))),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(7.0),
                        borderSide: BorderSide(
                            color: Theme.of(context).primaryColorDark)),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(25.0, 0.0, 15.0, 0.0),
                child: Column(
                  children: <Widget>[
                    for (var i = 0; i < attachments.length; i++)
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              attachments[i],
                              softWrap: false,
                              overflow: TextOverflow.fade,
                            ),
                          ),
                          IconButton(
                            icon: Icon(Icons.remove_circle),
                            onPressed: () => {_removeAttachment(i)},
                          )
                        ],
                      ),
                    GestureDetector(
                      onTap: _openImagePicker,
                      child: Row(
                        children: [
                          Container(
                            alignment: Alignment.centerRight,
                            padding: EdgeInsets.fromLTRB(0.0, 10.0, 10.0, 10.0),
                            child: Icon(Icons.attach_file),
                            //   onPressed: _openImagePicker,
                            // ),
                          ),
                          Text(
                            "انتخاب فایل",
                            style: TextStyle(
                                fontSize: 15,
                                color: Theme.of(context).primaryColorDark),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: RaisedButton(
                  onPressed: () {
                    send();
                  },
                  child: Container(
                    width: 190.0,
                    child: !this.sendResult
                        ? Text(
                            DemoLocalizations.of(context).trans("Send"),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 13.5,
                                fontWeight: FontWeight.normal),
                          )
                        : Container(
                            height: 25.0,
                            child: Image.asset(
                                "assets/images/loading_book_white.gif")),
                  ),
                  color: Theme.of(context).primaryColorDark,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7.0),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> send() async {
    if (!this.sendResult) {
      setState(() {
        this.sendResult = true;
      });
      final Email email = Email(
        body: _bodyController.text,
        subject: _subjectController.text,
        recipients: ["zpedram77@gmail.com"],
        attachmentPaths: attachments,
      );
      await Future.delayed(Duration(seconds: 2));

      String platformResponse;

      try {
        await FlutterEmailSender.send(email);
        platformResponse = 'success';
        this.sendResult = false;
      } catch (error) {
        platformResponse = error.toString();
        this.sendResult = true;
      }

      if (!mounted) return;

      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(platformResponse),
      ));
    }
  }

  void _openImagePicker() async {
    final picker = ImagePicker();
    final pick = await picker.getImage(source: ImageSource.gallery);
    if (pick != null) {
      setState(() {
        attachments.add(pick.path.toString());
      });
    }
  }

  void _removeAttachment(int index) {
    setState(() {
      attachments.removeAt(index);
    });
  }
}
