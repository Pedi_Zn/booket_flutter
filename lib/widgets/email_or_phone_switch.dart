import 'package:booket_flutter/localization/localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmailOrPhoneSwitch extends StatefulWidget {
  final Function swich;
  final bool loginType; // false = Email
  final String title;
  EmailOrPhoneSwitch({Key key, this.swich, this.loginType, this.title})
      : super(key: key);

  @override
  EmailOrPhoneSwitchState createState() =>
      EmailOrPhoneSwitchState(this.swich, this.loginType, this.title);
}

class EmailOrPhoneSwitchState extends State<EmailOrPhoneSwitch> {
  @override
  bool loginType;
  Function swich;
  String title;
  EmailOrPhoneSwitchState(this.swich, this.loginType, this.title);

  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
              color: Theme.of(context).dividerColor,
              height: 65.0,
              padding: EdgeInsets.only(bottom: 12.0, top: 12.0),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        width: 320.0,
                        padding: EdgeInsets.fromLTRB(0.0, 0.0, 40.0, 0.0),
                        child: Text(
                          title,
                          style: TextStyle(
                              fontSize: 20.0, fontWeight: FontWeight.bold),
                        )),
                    /*Container(
                  width: 220.0,
                  child: Stack(
                    children: [
                      AnimatedPositioned(
                        child: Card(
                          color: Colors.grey[300],
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(7.0),
                          ),
                        ),
                        left: !loginType ? 15.0 : 105.0,
                        curve: Curves.fastOutSlowIn,
                        duration: Duration(milliseconds: 200),
                        height: 40.0,
                        width: 90.0,
                      ),
                      Positioned(
                        child: GestureDetector(
                          onTapDown: (details) => {
                            if (loginType)
                              setState(() {
                                loginType = false;
                              }),
                            this.swich(loginType)
                          },
                          child: Center(
                            child: Text("ایمیل", style: TextStyle(color: Theme.of(context).accentColor)),
                          ),
                        ),
                        left: 30.0,
                        top: 0.0,
                        bottom: 3.0,
                        width: 60.0,
                      ),
                      Positioned(
                        right: 40.0,
                        top: 0.0,
                        bottom: 3.0,
                        width: 60.0,
                        child: GestureDetector(
                          onTapDown: (details) => {
                            if (!loginType)
                              setState(() {
                                loginType = true;
                              }),
                            this.swich(loginType)
                          },
                          child: Center(
                            child: Text("موبایل", style: TextStyle(color: Theme.of(context).accentColor)),
                          )
                        ),
                      )
                    ],
                  )
                ), */
                  ])),
        ],
      ),
    );
  }
}
