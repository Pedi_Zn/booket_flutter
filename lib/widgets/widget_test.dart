import 'package:flutter/material.dart';
import 'package:gesture_x_detector/gesture_x_detector.dart';

class XGestureExample extends StatefulWidget {
  @override
  _XGestureExampleState createState() => _XGestureExampleState();
}

class _XGestureExampleState extends State<XGestureExample> {
  String lastEventName = 'Tap on screen';
  double left = 20;
  double top = 20;
  bool moving = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Material(
        child: Center(
          child: Stack(
            children: [
              Positioned(
                left: left,
                top: top,
                width: 200,
                height: 40,
                child: Text(
                  lastEventName,
                  style: TextStyle(fontSize: 30),
                ),
              ),
            ],
          ),
        ),
      ),
      onDoubleTap: () {
        print("Double");
      },
      onTapUp: (e) {
        print("TapUp  " + e.toString());
      },
      onPanStart: (e) {
        setState(() {
          moving = true;
        });
        // print("PanStartGlobal  " + e.globalPosition.toString());
        // // print("PanStartKind  " + e.kind.toString());
        // print("PanStartLocal  " + e.localPosition.toString());
        // print("PanStartTime  " + e.sourceTimeStamp.toString());
      },
      onPanCancel: () {
        setState(() {
          moving = false;
        });
        if (moving) print("PanCancel  ");
      },
      onPanUpdate: (e) {
        if (moving)
          setState(() {
            left = e.globalPosition.dx;
            top = e.globalPosition.dy;
          });
        print("left  " + left.toString() + " / top : " + top.toString());
        // // print("PanUpdateKind  " + e.kind.toString());
        // print("PanUpdateLocal  " + e.localPosition.toString());
        // print("PanUpdateTime  " + e.sourceTimeStamp.toString());
        // print("PanUpdate  " + e.toString());
      },
      onPanEnd: (e) {
        if (moving) {
          setState(() {
            moving = false;
          });
          print("PanEnd  " + e.toString());
        }
      },
      // doubleTapTimeConsider: 300,
      // longPressTimeConsider: 350,
      // onTap: onTap,
      // onDoubleTap: onDoubleTap,
      // onLongPress: onLongPress,
      // onLongPressEnd: onLongPressEnd,
      // onMoveStart: onMoveStart,
      // onMoveEnd: onMoveEnd,
      // onMoveUpdate: onMoveUpdate,
      // onScaleStart: onScaleStart,
      // onScaleUpdate: onScaleUpdate,
      // onScaleEnd: onScaleEnd,
      // bypassTapEventOnDoubleTap: true,
      // bypassMoveEventAfterLongPress: false,
    );
  }

  void onLongPressEnd() {
    setLastEventName('onLongPressEnd');
    print('onLongPressEnd');
  }

  void onScaleEnd() {
    setLastEventName('onScaleEnd');
    print('onScaleEnd');
  }

  void onScaleUpdate(ScaleEvent event) {
    setLastEventName('onScaleUpdate');
    print(
        'onScaleUpdate - changedFocusPoint:  ${event.focalPoint} ; scale: ${event.scale} ;Rotation: ${event.rotationAngle}');
  }

  void onScaleStart(initialFocusPoint) {
    setLastEventName('onScaleStart');
    print('onScaleStart - initialFocusPoint: $initialFocusPoint');
  }

  void onMoveUpdate(MoveEvent event) {
    setLastEventName('onMoveUpdate');
    print('onMoveUpdate - pos: ${event.localPos} delta: ${event.delta}');
  }

  void onMoveEnd(localPos) {
    setLastEventName('onMoveEnd');
    print('onMoveEnd - pos: $localPos');
  }

  void onMoveStart(localPos) {
    setLastEventName('onMoveStart');
    print('onMoveStart - pos: $localPos');
  }

  void onLongPress(TapEvent event) {
    setLastEventName('onLongPress');
    print('onLongPress - pos: ${event.localPos}');
  }

  void onDoubleTap(event) {
    setLastEventName('onDoubleTap');
    print('onDoubleTap - pos: ' + event.localPos.toString());
  }

  void onTap(event) {
    setLastEventName('onTap');
    print('onTap - pos: ' + event.localPos.toString());
  }

  void setLastEventName(String eventName) {
    setState(() {
      lastEventName = eventName;
    });
  }
}
