import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:booket_flutter/widgets/expireDate.dart';
import 'package:transparent_image/transparent_image.dart';

class Books2 extends StatefulWidget {
  // Books({Key key});
  final String address;
  final double padding;
  final int count;
  final int _expireDate;

  Books2(this.address, this.padding, this._expireDate, this.count);
  // : super(key: key);

  @override
  BooksState createState() =>
      BooksState(this.address, this.padding, this._expireDate, this.count);
}

class BooksState extends State<Books2> {
  @override
  String address;
  double padding;
  int count;
  int _expireDate;

  BooksState(this.address, this.padding, this._expireDate, this.count);

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    final height = (MediaQuery.of(context).size.width / count) * 8 / 5;
    final width = MediaQuery.of(context).size.width / count - 22.5;

    final _heightExp = height * 1 / 7;
    final _widthExp = width * 3 / 5;

    return Padding(
      padding: EdgeInsets.all(padding),
      child: Center(
        child: SizedBox(
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: height,
                width: width,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Container(color: Theme.of(context).dividerColor)),
              ),
              CircularProgressIndicator(),
              Container(
                height: height,
                width: width,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      imageErrorBuilder: (context, url, err) =>
                          Container(child: Text("Err")),
                      image: iw.hostName + address,
                      fit: BoxFit.fill,
                    )),
              ),
              Positioned(
                top: 10,
                left: 10.0,
                height: _heightExp,
                // right: 10.0,
                width: _widthExp,
                child: Container(
                    decoration: new BoxDecoration(color: Colors.transparent),
                    child: _expireDate != null
                        ? new ExpireDate(
                            DemoLocalizations.of(context).transNumber("14"),
                            _heightExp,
                            _widthExp)
                        : Container()),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
