import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:booket_flutter/widgets/expireDate.dart';

class SocialMediaCard extends StatefulWidget {
  // Books({Key key});
  final String address;
  final String title;

  SocialMediaCard(this.address, this.title);
  // : super(key: key);

  @override
  SocialMediaCardState createState() =>
      SocialMediaCardState(this.address, this.title);
}

class SocialMediaCardState extends State<SocialMediaCard> {
  @override
  String address;
  String title;

  SocialMediaCardState(this.address, this.title);

  @override
  Widget build(BuildContext context) {
    double cardSize = (MediaQuery.of(context).size.width - 90) / 2;

    return Container(
        decoration: BoxDecoration(
          // color: Theme.of(context).dividerColor,
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(14),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            PlatformSvgWidget.asset(address, height: 130.0),
            Text(
              title,
              style: TextStyle(
                fontWeight: FontWeight.w900,
                fontSize: 17.0,
              ),
            )
          ],
        ));
  }
}
