import 'package:booket_flutter/blocs/grid_card_bloc.dart';
import 'package:flutter/material.dart';

class AutoRefresh extends StatefulWidget {
  final GridCardBloc gridCardBloc;
  final Widget child;

  AutoRefresh({
    Key key,
    this.gridCardBloc,
    @required this.child,
  }) : super(key: key);

  @override
  _AutoRefreshState createState() => _AutoRefreshState();
}

class _AutoRefreshState extends State<AutoRefresh> {
  int keyValue;
  ValueKey key;
  var stream;

  @override
  void initState() {
    super.initState();

    keyValue = 0;
    key = ValueKey(keyValue);

    this.stream = this.widget.gridCardBloc.getTabsStream().listen((value) {
      _recursiveBuild();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: key,
      child: widget.child,
    );
  }

  void _recursiveBuild() {
    setState(() {
      keyValue = keyValue + 1;
      key = ValueKey(keyValue);
    });
  }

  @override
  void dispose() {
    this.stream.cancel();
    super.dispose();
  }
}
