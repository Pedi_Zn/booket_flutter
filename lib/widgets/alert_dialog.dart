import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomDialog extends StatefulWidget {
  final String title;

  CustomDialog({Key key, this.title}) : super(key: key);

  @override
  CustomDialogState createState() => CustomDialogState(this.title);
}

class CustomDialogState extends State<CustomDialog> {
  bool logoutResult = false;
  String title;
  final AccountsRepo accountsRepo = new AccountsRepo();

  CustomDialogState(this.title);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      child: Container(
        height: 320,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                child: PlatformSvgWidget.asset('assets/images/booket.svg',
                    color: Colors.black, height: 80.0)),
            Container(
              margin: EdgeInsets.fromLTRB(23.0, 20.0, 23.0, 25.0),
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 14.6,
                  color: Colors.black,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              child: RaisedButton(
                onPressed: () {
                  logoutPressed(context);
                },
                child: Container(
                  width: 190.0,
                  child: !this.logoutResult
                      ? Text(
                          DemoLocalizations.of(context).trans("Logout"),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 13.5,
                              fontWeight: FontWeight.normal),
                        )
                      : Container(
                          height: 25.0,
                          child: Image.asset(
                              "assets/images/loading_book_white.gif")),
                ),
                color: Theme.of(context).primaryColorDark,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(7.0),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> logoutPressed(context) async {
    final iw = MainStateContainer.of(context);
    if (!this.logoutResult) {
      setState(() {
        this.logoutResult = true;
      });
      await Future.delayed(Duration(seconds: 2));
      accountsRepo.logout();
      iw.removeUser();
      iw.setToken(null);
      await iw.authenticationBloc.removeTokenFromStorage();
      logoutResult = false;
      if (iw.lastPathBeforeAuth != "") {
        // Navigator.of(context).pop();
      } else {
        Navigator.of(context).popAndPushNamed("/Profile");
      }
    }
  }
}
