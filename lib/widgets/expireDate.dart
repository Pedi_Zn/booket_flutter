import 'package:booket_flutter/localization/localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ExpireDate extends StatelessWidget {
  final String days;
  final double _height;
  final double _width;

  ExpireDate(this.days, this._width, this._height);

  @override
  Widget build(BuildContext context) {
    final iconFont = _width < 40.0
        ? 15.0
        : (_width < 200.0
            ? 18.0
            : (_width < 300.0
                ? 19.0
                : (_width < 400.0
                    ? 20.0
                    : (_width < 500.0
                        ? 25.0
                        : (_width < 800.0 ? 28.0 : 30.0)))));
    final textFont = _width < 40.0
        ? 14.5
        : (_width < 200.0
            ? 15.0
            : (_width < 300.0
                ? 24.0
                : (_width < 400.0
                    ? 28.0
                    : (_width < 500.0
                        ? 30.0
                        : (_width < 800.0 ? 32.0 : 35.0)))));

    return Container(
      height: _height,
      width: _width,
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(14),
      ),
      child: Padding(
        padding: const EdgeInsets.only(right: 8.0, left: 8.0),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.access_time,
                size: iconFont,
                color: Colors.white.withOpacity(0.9),
              ),
              Text(
                days,
                style: TextStyle(
                  color: Colors.white.withOpacity(0.9),
                  fontSize: textFont,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.w500,
                  decorationThickness: 1.0,
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 6.0),
                child: Text(
                  DemoLocalizations.of(context).trans("Day"),
                  // textAlign: TextAlign.,
                  style: TextStyle(
                    color: Colors.white.withOpacity(0.9),
                    fontSize: textFont,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal,
                    decorationThickness: 1.0,
                  ),
                ),
              ),
            ]),
      ),
    );
  }
}
