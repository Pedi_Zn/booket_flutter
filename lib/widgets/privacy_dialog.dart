import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/api/api.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/models/route_argument.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PrivacyDialog extends StatefulWidget {
  final String title;
  final String phoneNumber;
  final String name;
  final String password;

  PrivacyDialog(
      {Key key, this.title, this.phoneNumber, this.name, this.password})
      : super(key: key);

  @override
  PrivacyDialogState createState() => PrivacyDialogState(
      this.title, this.phoneNumber, this.name, this.password);
}

class PrivacyDialogState extends State<PrivacyDialog> {
  bool loader = false;
  String title;
  String phoneNumber;
  String name;
  String password;
  final AccountsRepo accountsRepo = new AccountsRepo();

  PrivacyDialogState(this.title, this.phoneNumber, this.name, this.password);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(7.0)),
      child: Container(
        height: 350,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                child: PlatformSvgWidget.asset('assets/images/booket.svg',
                    color: Colors.black, height: 80.0)),
            Container(
              margin: EdgeInsets.fromLTRB(23.0, 20.0, 23.0, 25.0),
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 14.6,
                  color: Colors.black,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            RaisedButton(
              onPressed: () {
                Navigator.of(context).popAndPushNamed("/Terms");
              },
              child: Container(
                  width: 190.0,
                  child: Text(
                    "مشاهده قوانین و مقررات",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 13.5,
                        fontWeight: FontWeight.normal),
                  )),
              color: Theme.of(context).accentColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(7.0),
              ),
            ),
            RaisedButton(
              onPressed: () {
                submitPressed(context);
              },
              child: Container(
                width: 190.0,
                child: !this.loader
                    ? Text(
                        DemoLocalizations.of(context).trans("Submit"),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.5,
                            fontWeight: FontWeight.normal),
                      )
                    : Container(
                        height: 25.0,
                        child: Image.asset(
                            "assets/images/loading_book_white.gif")),
              ),
              color: Theme.of(context).primaryColorDark,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(7.0),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> submitPressed(context) async {
    setState(() {
      loader = true;
    });
    final iw = MainStateContainer.of(context);
    Api.changeHeader();
    await accountsRepo.sendPhoneVerification(iw.hostName,
        {"phone": "98" + int.parse(phoneNumber).toString()}).then((value) {
      setState(() {
        loader = false;
      });
      Api.returnHeader();
      Navigator.of(context).popAndPushNamed("/Pincode",
          arguments: new RouteArgument(argumentsList: [
            {
              "fullName": name,
              "phone": phoneNumber,
              "password": password,
              "trackingCode": value["tracking_code"],
              "type": "Register"
            }
          ], id: "Register"));
    });
  }
}
