import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GridOrListSwitch extends StatefulWidget {
  final Function swich;
  final bool grid;
  GridOrListSwitch({Key key, this.swich, this.grid}) : super(key: key);

  @override
  GridOrListSwitchState createState() =>
      GridOrListSwitchState(this.swich, this.grid);
}

class GridOrListSwitchState extends State<GridOrListSwitch> {
  @override
  bool grid;
  Function swich;
  GridOrListSwitchState(this.swich, this.grid);
  Widget build(BuildContext context) {
    return Container(
        color: Color.fromRGBO(0, 0, 0, 0.03),
        height: 50.0,
        padding: EdgeInsets.only(bottom: 5.0, top: 5.0),
        child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          Container(
              width: 140.0,
              child: Stack(
                children: [
                  AnimatedPositioned(
                    child: Card(
                      color: Colors.grey[300],
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6.0),
                      ),
                    ),
                    left: !grid ? 18.0 : 73.0,
                    curve: Curves.fastOutSlowIn,
                    duration: Duration(milliseconds: 200),
                    height: 40.0,
                    width: 45.0,
                  ),
                  Positioned(
                    child: GestureDetector(
                      onTapDown: (details) => {
                        if (grid)
                          setState(() {
                            grid = false;
                          }),
                        this.swich(grid)
                      },
                      child: Center(
                        child: Icon(Icons.view_stream, size: 30.0,
                        color: !grid
                            ? Theme.of(context).accentColor
                            : Theme.of(context).textTheme.bodyText1.color,
                        )
                      ),
                    ),
                    left: 10.0,
                    top: 0.0,
                    bottom: 0.0,
                    width: 60.0,
                  ),
                  Positioned(
                      right: 15.0,
                      top: 0.0,
                      bottom: 0.0,
                      width: 60.0,
                      child: GestureDetector(
                        onTapDown: (details) => {
                          if (!grid)
                            setState(() {
                              grid = true;
                            }),
                          this.swich(grid)
                        },
                        child: Center(
                          child: Icon(Icons.view_module, size: 30.0,
                          color: grid
                              ? Theme.of(context).accentColor
                              : Theme.of(context).textTheme.bodyText1.color,
                          )
                        ),
                      ))
                ],
              )),
          // Container(width: 30.0),
        ]));
  }
}
