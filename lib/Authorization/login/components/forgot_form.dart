import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';

class ForgotForm extends StatefulWidget {
  const ForgotForm({
    Key key,
    @required this.isLogin,
    @required this.animationDuration,
    @required this.size,
    @required this.defaultLoginSize,
  }) : super(key: key);

  final bool isLogin;
  final Duration animationDuration;
  final Size size;
  final double defaultLoginSize;

  @override
  _ForgotFormState createState() => _ForgotFormState();
}

class _ForgotFormState extends State<ForgotForm> {
  bool loginResult = false;
  TextEditingController phoneController = TextEditingController();
  // TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final GlobalKey scaffoldKey = new GlobalKey<ScaffoldState>();

  final AccountsRepo accountsRepo = new AccountsRepo();

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    return AnimatedOpacity(
      opacity: widget.isLogin ? 0.0 : 1.0,
      duration: widget.animationDuration * 5,
      child: Visibility(
        visible: !widget.isLogin,
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            width: widget.size.width,
            height: widget.defaultLoginSize,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: 20),
                  Lottie.asset("assets/images/forgot_password.json",
                      height: 180),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      width: widget.size.width * 0.8,
                      height: 50.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Theme.of(context).accentColor.withAlpha(50)),
                      child: TextField(
                        onChanged: (s) {
                          iw.authenticationBloc.updatePhoneNumber(s);
                        },
                        controller: phoneController,
                        keyboardType: TextInputType.number,
                        cursorColor: Theme.of(context).accentColor,
                        decoration: InputDecoration(
                            icon: Icon(MaterialIcons.phone_iphone,
                                color: Theme.of(context).accentColor),
                            hintText:
                                DemoLocalizations.of(context).trans("Phone"),
                            border: InputBorder.none),
                      )),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      width: widget.size.width * 0.8,
                      height: 50.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Theme.of(context).accentColor.withAlpha(50)),
                      child: TextField(
                        onChanged: (s) {
                          iw.authenticationBloc.updatePassword(s);
                        },
                        controller: passwordController,
                        obscureText: true,
                        cursorColor: Theme.of(context).accentColor,
                        decoration: InputDecoration(
                            icon: Icon(Ionicons.md_lock,
                                color: Theme.of(context).accentColor),
                            hintText: DemoLocalizations.of(context)
                                .trans("NewPassword"),
                            border: InputBorder.none),
                      )),
                  SizedBox(height: 10),
                  Container(
                    width: widget.size.width * 0.8,
                    child: RaisedButton(
                      onPressed: () {
                        forgotPressed(context);
                      },
                      child: Container(
                          height: 50.0,
                          child: Center(
                            child: !loginResult
                                ? Text(
                                    DemoLocalizations.of(context)
                                        .trans("Submit"),
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.normal),
                                    textAlign: TextAlign.center,
                                  )
                                : Container(
                                    width: 50.0,
                                    child: Image.asset(
                                        "assets/images/loading_book_white.gif")),
                          )),
                      color: Theme.of(context).accentColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(7.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> forgotPressed(context) async {
    final iw = MainStateContainer.of(context);
    if (!loginResult) {
      // 2 number refer the index of Home page
      setState(() {
        loginResult = true;
      });
      var response = await iw.authenticationBloc.login(iw.hostName, iw.locale);
      if (response.containsKey("message")) {
        Scaffold.of(context).showSnackBar(new SnackBar(
          content: Text("${response["message"]}",
              style: TextStyle(color: Colors.white, fontFamily: 'IranSans')),
          backgroundColor: Theme.of(context).accentColor,
        ));
        setState(() {
          loginResult = false;
        });
      } else {
        if (mounted) {
          iw.authenticationBloc.updateToken(response["auth"], 3);
          await Future.delayed(Duration(milliseconds: 10));
          iw.setToken(response["auth"]);
          dynamic me = response[
              "user"]; // = await iw.authenticationBloc.getProfile(iw.hostName);
          if ((me != null) && (!me.containsKey("title"))) {
            setState(() {
              loginResult = false;
            });
            Scaffold.of(scaffoldKey.currentContext).showSnackBar(new SnackBar(
              content: Text(
                "${me["message"]}",
                style: TextStyle(color: Colors.white),
              ),
              backgroundColor: Theme.of(context).accentColor,
            ));
          } else if ((me != null) && (me.containsKey("title"))) {
            setState(() {
              loginResult = false;
            });
            iw.updateUserInfo(me);
            var nextDestination = "/Profile";

            await accountsRepo
                .getProfile(
              iw.hostName,
              response["auth"],
            )
                .then((profile) {
              iw.updateUserInfo(profile);
              // print(iw.me.toString());
              // print(iw.me["hasBook"]);
              if (iw.me["hasbook"] != null) {
                Navigator.of(context).pushNamed('/Library');
              } else {
                Navigator.of(context).pushNamed('/Vitrine');
              }
            });
            await accountsRepo
                .getBasket(
              iw.hostName,
              response["auth"],
            )
                .then((basketReq) {
              setState(() {
                iw.setCart(basketReq);
              });
            });
            // Navigator.of(context)
            //     .popUntil((route) => !Navigator.of(context).canPop());

            // Navigator.of(context).pushNamed(nextDestination);

            iw.setLastPath("");
          }
        }
      }
    }
  }
}
