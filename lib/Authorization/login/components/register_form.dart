import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/widgets/privacy_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({
    Key key,
    @required this.isLogin,
    @required this.animationDuration,
    @required this.size,
    @required this.defaultLoginSize,
  }) : super(key: key);

  final bool isLogin;
  final Duration animationDuration;
  final Size size;
  final double defaultLoginSize;

  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  bool loginResult = false;
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: widget.isLogin ? 0.0 : 1.0,
      duration: widget.animationDuration * 5,
      child: Visibility(
        visible: !widget.isLogin,
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            width: widget.size.width,
            height: widget.defaultLoginSize,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: 20),
                  Lottie.asset("assets/images/register.json", height: 180),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      width: widget.size.width * 0.8,
                      height: 50.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Theme.of(context).accentColor.withAlpha(50)),
                      child: TextField(
                        controller: nameController,
                        cursorColor: Theme.of(context).accentColor,
                        decoration: InputDecoration(
                            icon: Icon(Ionicons.md_person,
                                color: Theme.of(context).accentColor),
                            hintText:
                                DemoLocalizations.of(context).trans("FullName"),
                            border: InputBorder.none),
                      )),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      width: widget.size.width * 0.8,
                      height: 50.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Theme.of(context).accentColor.withAlpha(50)),
                      child: TextField(
                        controller: phoneController,
                        cursorColor: Theme.of(context).accentColor,
                        decoration: InputDecoration(
                            icon: Icon(MaterialIcons.phone_iphone,
                                color: Theme.of(context).accentColor),
                            hintText:
                                DemoLocalizations.of(context).trans("Phone"),
                            border: InputBorder.none),
                      )),
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                      width: widget.size.width * 0.8,
                      height: 50.0,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Theme.of(context).accentColor.withAlpha(50)),
                      child: TextField(
                        obscureText: true,
                        controller: passwordController,
                        cursorColor: Theme.of(context).accentColor,
                        decoration: InputDecoration(
                            icon: Icon(Ionicons.md_lock,
                                color: Theme.of(context).accentColor),
                            hintText:
                                DemoLocalizations.of(context).trans("Password"),
                            border: InputBorder.none),
                      )),
                  SizedBox(height: 10),
                  Container(
                    width: widget.size.width * 0.8,
                    child: RaisedButton(
                      onPressed: () {
                        acceptPrivacy(context);
                      },
                      child: Container(
                          height: 50.0,
                          child: Center(
                            child: !loginResult
                                ? Text(
                                    DemoLocalizations.of(context)
                                        .trans("Register"),
                                    style: TextStyle(
                                        color: Theme.of(context).primaryColor,
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.normal),
                                    textAlign: TextAlign.center,
                                  )
                                : Container(
                                    width: 50.0,
                                    child: Image.asset(
                                        "assets/images/loading_book_white.gif")),
                          )),
                      color: Theme.of(context).accentColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(7.0),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> acceptPrivacy(context) async {
    final iw = MainStateContainer.of(context);
    if (phoneController.text != "" &&
        nameController.text != "" &&
        passwordController.text != "") {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return PrivacyDialog(
              title: DemoLocalizations.of(context).trans("PrivacyText"),
              phoneNumber: phoneController.text,
              name: nameController.text,
              password: passwordController.text,
            );
          });
    } else {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: Text("لطفا فیلدها را تکمیل کنید",
            style: TextStyle(color: Colors.white, fontFamily: 'IranSans')),
        backgroundColor: Theme.of(context).accentColor,
      ));
    }
  }
}
