import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/widgets/drop_down_text_field.dart';
import 'package:booket_flutter/widgets/text_field_widget.dart';
import 'package:flutter/material.dart';

class GraduateContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    return Form(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            DropDownTextFieldWidget(
              label: DemoLocalizations.of(context).trans("Degree"),
              contentList: iw.degreesList,
              text: iw.me["user"]["degree"] != null
                  ? iw.me["user"]["degree"]["title"]
                  : "",
              onChanged: (s) {
                iw.authenticationBloc.updateDegree(s);
              },
            ),
            const SizedBox(height: 5),
            DropDownTextFieldWidget(
              label: DemoLocalizations.of(context).trans("University"),
              contentList: iw.universitiesList,
              text: iw.me["user"]["university"] != null
                  ? iw.me["user"]["university"]["title"]
                  : "",
              onChanged: (s) {
                iw.authenticationBloc.updateUniversity(s);
              },
            ),
            const SizedBox(height: 5),
            DropDownTextFieldWidget(
              label: DemoLocalizations.of(context).trans("Field"),
              contentList: iw.studyFieldsList,
              text: iw.me["user"]["study_field"] != null
                  ? iw.me["user"]["study_field"]["title"]
                  : "",
              onChanged: (s) {
                iw.authenticationBloc.updateStudyField(s);
              },
            ),
          ],
        ),
      ),
    );
  }
}
