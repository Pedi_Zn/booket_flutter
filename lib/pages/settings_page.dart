/**
 * Author: Damodar Lohani
 * profile: https://github.com/lohanidamodar
  */

import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
// import 'package:flutter_ui_challenges/core/presentation/res/assets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../main_state_container.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  bool _dark;

  @override
  void initState() {
    super.initState();
    _dark = false;
  }

  Brightness _getBrightness() {
    return _dark ? Brightness.dark : Brightness.light;
  }

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    return Theme(
      data: ThemeData(
        brightness: _getBrightness(),
        fontFamily: 'IranSans',
      ),
      child: Scaffold(
        backgroundColor: _dark ? null : Colors.grey.shade200,
        appBar: AppBar(
          elevation: 0,
          brightness: _getBrightness(),
          iconTheme: IconThemeData(color: _dark ? Colors.white : Colors.black),
          backgroundColor: Colors.transparent,
          title: Text(
            DemoLocalizations.of(context).trans("Settings"),
            style: TextStyle(
                color: _dark ? Colors.white : Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.w800),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(FontAwesomeIcons.moon),
              onPressed: () {
                setState(() {
                  _dark = !_dark;
                });
              },
            )
          ],
        ),
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            SingleChildScrollView(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Card(
                    elevation: 8.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0)),
                    color: Theme.of(context).accentColor,
                    child: ListTile(
                      onTap: () {
                        Navigator.of(context).pushNamed('/EditProfile');
                      },
                      title: Text(
                        DemoLocalizations.of(context).trans("EditProfile"),
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      // leading: CircleAvatar(
                      //   backgroundImage: NetworkImage(avatars[0]),
                      // ),
                      trailing: Icon(
                        Icons.edit,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(height: 10.0),
                  Card(
                    elevation: 4.0,
                    margin: const EdgeInsets.fromLTRB(32.0, 8.0, 32.0, 16.0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(7.0)),
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          leading: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              PlatformSvgWidget.asset(
                                  'assets/images/booket_logo.svg',
                                  color: Theme.of(context).accentColor,
                                  height: 13.0),
                            ],
                          ),
                          title: Text(
                              DemoLocalizations.of(context).trans("AboutUs"),
                              style: TextStyle(fontSize: 14.0)),
                          trailing: Icon(Icons.keyboard_arrow_left),
                          onTap: () {
                            Navigator.of(context).pushNamed('/AboutUs');
                          },
                        ),
                        _buildDivider(),
                        ListTile(
                          leading: Icon(
                            Icons.call,
                            color: Theme.of(context).accentColor,
                          ),
                          title: Text(
                              DemoLocalizations.of(context).trans("ContactUs"),
                              style: TextStyle(fontSize: 14.0)),
                          trailing: Icon(Icons.keyboard_arrow_left),
                          onTap: () {
                            Navigator.of(context).pushNamed('/ContactUs');
                          },
                        ),
                        _buildDivider(),
                        ListTile(
                          leading: Icon(
                            Icons.support_agent_outlined,
                            color: Theme.of(context).accentColor,
                          ),
                          title: Text(
                              DemoLocalizations.of(context).trans("Support"),
                              style: TextStyle(fontSize: 14.0)),
                          trailing: Icon(Icons.keyboard_arrow_left),
                          onTap: () {
                            Navigator.of(context).pushNamed('/Support');
                          },
                        ),
                        _buildDivider(),
                        ListTile(
                          leading: Icon(
                            MaterialCommunityIcons.script_text_outline,
                            color: Theme.of(context).accentColor,
                          ),
                          title: Text(
                              DemoLocalizations.of(context).trans("Terms"),
                              style: TextStyle(fontSize: 14.0)),
                          trailing: Icon(Icons.keyboard_arrow_left),
                          onTap: () {
                            Navigator.of(context).pushNamed('/Terms');
                          },
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 20.0),
                  Text(
                    DemoLocalizations.of(context)
                        .trans("Notification_Settings"),
                    style: TextStyle(
                      color: _dark
                          ? Theme.of(context).primaryColor
                          : Theme.of(context).primaryColorDark,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SwitchListTile(
                    activeColor: Theme.of(context).accentColor,
                    contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    value: true,
                    title: Text(
                        DemoLocalizations.of(context)
                            .trans("Received_Notification"),
                        style: TextStyle(fontSize: 14.0)),
                    onChanged: (val) {},
                  ),
                  SwitchListTile(
                    activeColor: Colors.purple,
                    contentPadding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    value: false,
                    title: Text(
                        DemoLocalizations.of(context).trans("Received_Emails"),
                        style: TextStyle(fontSize: 14.0)),
                    onChanged: (val) {},
                  ),
                  const SizedBox(height: 15.0),
                  Text(
                    DemoLocalizations.of(context).trans("Language_Settings"),
                    style: TextStyle(
                      color: _dark
                          ? Theme.of(context).primaryColor
                          : Theme.of(context).primaryColorDark,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  ListTile(
                    leading: Text(
                        DemoLocalizations.of(context).trans("Change_Language"),
                        style: TextStyle(fontSize: 14.0)),
                    trailing: Container(
                        width: 100.0,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                                setLanguage(DemoLocalizations.of(context)
                                    .locale
                                    .languageCode
                                    .toString()),
                                style: TextStyle(
                                  fontSize: 13.0,
                                  color: _dark
                                      ? Theme.of(context).primaryColor
                                      : Colors.black38,
                                )),
                            Container(
                              width: 10.0,
                            ),
                            Icon(Icons.keyboard_arrow_left),
                          ],
                        )),
                    onTap: () {
                      showLang(context);
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Container _buildDivider() {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      width: double.infinity,
      height: 1.0,
      color: Colors.grey.shade400,
    );
  }

  String setLanguage(lang) {
    if (lang == "fa") return "فارسی";
    if (lang == "en") return "English";
  }

  bool checkCurrentLanguage(lang) {
    if (lang.toString() ==
        DemoLocalizations.of(context).locale.languageCode.toString())
      return true;
    else
      return false;
  }

  Future<void> showLang(context) async {
    final iw = MainStateContainer.of(context);
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return Container(
              // padding: EdgeInsets.fromLTRB(0, 10.0, 0, 0.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    leading: ClipOval(
                        child: PlatformSvgWidget.asset('assets/images/iran.svg',
                            height: 40)),
                    title: Text("فارسی"),
                    trailing: Container(
                      height: 30,
                      width: 30,
                      child: checkCurrentLanguage("fa")
                          ? Icon(
                              MaterialCommunityIcons.check,
                              color: Theme.of(context).accentColor,
                            )
                          : Container(),
                    ),
                    onTap: () {
                      // Navigator.of(context).pushNamed('/ContactUs');
                    },
                  ),
                  _buildDivider(),
                  ListTile(
                    leading: ClipOval(
                        child: PlatformSvgWidget.asset('assets/images/uk.svg',
                            height: 40)),
                    title: Text("English"),
                    trailing: Container(
                      height: 30,
                      width: 30,
                      child: checkCurrentLanguage("en")
                          ? Icon(
                              MaterialCommunityIcons.check,
                              color: Theme.of(context).accentColor,
                            )
                          : Container(),
                    ),
                    onTap: () {
                      // Navigator.of(context).pushNamed('/Support');
                    },
                  ),
                ],
              ),
            );
          });
        });
  }
}
