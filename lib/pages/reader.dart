import 'package:after_layout/after_layout.dart';
import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/blocs/reader_bloc.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/models/route_argument.dart';
import 'package:booket_flutter/pages/ebook_page.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:flutter_card_swipper/flutter_card_swiper.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:material_color_picker_wns/material_color_picker_wns.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';
import 'package:photo_view/photo_view.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:zoomer/zoomer.dart';

typedef DoubleClickAnimationListener = void Function();

class Reader extends StatefulWidget {
  final RouteArgument routeArgument;
  const Reader(this.routeArgument, {Key key}) : super(key: key);

  @override
  ReaderState createState() => new ReaderState();
}

typedef void ReaderMenuCallback(String val);

class ReaderState extends State<Reader>
    with TickerProviderStateMixin, AfterLayoutMixin {
  String token;
  String title;
  AnimationController _doubleClickAnimationController;
  Animation<double> _doubleClickAnimation;
  DoubleClickAnimationListener _doubleClickAnimationListener;
  List<double> doubleTapScales = <double>[1.0, 2.0];
  bool gestureEnable = true;
  final ReaderMenuCallback sendToEbookPage;
  bool oneTime = true;
  List<dynamic> searchList;
  ScrollController parentCont, childCont;
  bool open;
  dynamic book;
  bool isBookmarked = false;
  dynamic ebookPagesSent = {};
  BuildContext lastContext;
  bool textMode = false;
  double _value = 1.0;

  RangeValues values = RangeValues(1, 100);

  final _textFieldWidgetKey = GlobalKey();
  double _textFieldWidgetHeight = 0.0;
  Size _textFieldWidgetSize;

  final _listViewWidgetKey = GlobalKey();
  double _listViewWidgetHeight = 0.0;
  Size _listViewWidgetSize;
  bool bookmarkOrAnnotation = true; // true= bookmark false= Annotation

  Color firstHighlightColor = Colors.amberAccent;
  Color secondHighlightColor = Colors.greenAccent;
  Color thirdHighlightColor = Colors.redAccent;
  int selectedColorIndex = 0;

  bool listMode = false;

  void _setTextFieldSize() {
    setState(() {
      // final _width = _size.width;
      _textFieldWidgetHeight = _textFieldWidgetKey.currentContext.size.height;
    });
  }

  void _setListViewSize() {
    setState(() {
      _listViewWidgetHeight = _listViewWidgetKey.currentContext.size.height;
    });
  }

  final AccountsRepo accountsRepo = new AccountsRepo();

  bool expandedSearch = false;
  double logoOpacity = 1;
  double _width = 200;
  bool showMenuDelayed = false;
  FocusNode searchFocus;
  double _currentSliderValue = 30;
  double _numberOfPages = 100;
  bool showMenu = true;
  TabController _tabController;
  final imageList = [];
  final annotaionList = [];
  final ebookPages = [];

  ReaderBloc readerBloc = new ReaderBloc("highlight", 1);
  SwiperPagination pagination;
  SwiperControl control;
  TabController tabController;

  int openToolbarIndex = 0;
  // SwiperC
  dynamic marginsList = [];
  ReaderState({this.sendToEbookPage, this.token});

  var lastPageGoing = null;
  void goToPage(context, int tagetIndex, halfWork) {
    final iw = MainStateContainer.of(context);
    lastPageGoing = tagetIndex;
    Future.delayed(Duration(milliseconds: 200)).then((value) => {
          if ((lastPageGoing == tagetIndex) || halfWork)
            {
              iw.setReaderPage(tagetIndex),
              getBookPage(context, tagetIndex),
              setState(() {
                setState(() {
                  _value = double.parse(tagetIndex.toString());
                });
              }),
              tabController.animateTo(tagetIndex),

              // readerBloc.setCommand({"color": selectColor().withOpacity(0.3)}),
            }
        });
  }

  @override
  void initState() {
    super.initState();
    this.token = this
        .widget
        .routeArgument
        .argumentsList
        .elementAt(0)["bookToken"]
        .toString();
    this.title = this
        .widget
        .routeArgument
        .argumentsList
        .elementAt(0)["bookTitle"]
        .toString();
    _doubleClickAnimationController = AnimationController(
        duration: const Duration(milliseconds: 150), vsync: this);
    this.pagination = new SwiperPagination();
    this.control = new SwiperControl();
    this.childCont = ScrollController(initialScrollOffset: 0.0);
    this.parentCont = ScrollController(initialScrollOffset: 0.0);
    // this.childCont.addListener(_scrollListener);
    this.open = false;
    _tabController = TabController(length: 6, initialIndex: 0, vsync: this);

    tabController = TabController(length: 0, vsync: this);
  }

  @override
  void dispose() {
    _doubleClickAnimationController.dispose();
    // clearGestureDetailsCache();
    //cancelToken?.cancel();
    super.dispose();
  }

  ZoomerController _zoomerController = ZoomerController(initialScale: 1.0);
  String _zoomDetails = "Zoom";

  @override
  Widget build(BuildContext context) {
    // if (lastContext == null) {
    //   setState(() {
    //     lastContext = context;
    //   });
    //   tabController.addListener(_handleTabSelection);
    // }
    _zoomerController.onZoomUpdate(() {
      setState(() {
        _zoomDetails = "Scale = " + _zoomerController.scale.toStringAsFixed(2);
        _zoomDetails +=
            "\nRotation = " + _zoomerController.rotation.toStringAsFixed(2);
        _zoomDetails += "\nOffset = (" +
            _zoomerController.offset.dx.toStringAsFixed(2) +
            "," +
            _zoomerController.offset.dy.toStringAsFixed(2) +
            ")";
      });
    });
    final iw = MainStateContainer.of(context);
    final pageSize = MediaQuery.of(context).size;
    final pageHeight = pageSize.height - 0.0;
    var index = -1;

    return Container(
      color: Theme.of(context).accentColor,
      child: SafeArea(
        top: true,
        bottom: false,
        child: MediaQuery.removePadding(
          removeTop: true,
          context: context,
          child: Scaffold(
            body: Stack(
              children: [
                Positioned.fill(
                  top: 60,
                  left: 0,
                  right: 0,
                  child: Stack(
                    children: [
                      Positioned.fill(
                        child: TabBarView(
                          controller: tabController,
                          // showTabIndicator: false,
                          // tabIndicatorColor: Colors.lightBlue,
                          // tabIndicatorSelectedColor:
                          //     Color.fromARGB(255, 0, 0, 255),
                          // tabIndicatorHeight: 16,
                          // tabIndicatorSize: 16,
                          // tabController: tabController,
                          // curve: Curves.fastOutSlowIn,
                          // width: MediaQuery.of(context).size.width,
                          // height: 220,
                          // autoSlide: false,
                          // duration: new Duration(seconds: 6),
                          dragStartBehavior: DragStartBehavior.down,

                          // allowManualSlide: false,
                          physics: iw.gestureEnable
                              ? AlwaysScrollableScrollPhysics()
                              : NeverScrollableScrollPhysics(),
                          children: imageList.map((dynamic image) {
                            index = index + 1;
                            return InteractiveViewer(
                              scaleEnabled: true,
                              constrained: true,
                              maxScale: 2.5,
                              alignPanAxis: true,
                              boundaryMargin: EdgeInsets.all(5.0),
                              minScale: 1.05,
                              panEnabled: true,
                              child: EbookPage(
                                pageIndex: index,
                                image: image,
                                readerBloc: readerBloc,
                                // its a joke !!!
                              ),
                            );
                          }).toList(),
                        ),

                        // PhotoViewGallery.builder(
                        //   itemCount: imageList.length,
                        //   enableRotation: false,
                        //   builder: (context, index) {
                        // return PhotoViewGalleryPageOptions(
                        //   basePosition: Alignment.topCenter,
                        //   tightMode: true,
                        //   imageProvider: NetworkImage(
                        //       iw.hostName + imageList[index]["image"]["jpeg"]),
                        //   minScale: PhotoViewComputedScale.covered,
                        //   maxScale: PhotoViewComputedScale.covered * 2,
                        //   onTapUp: _onImagePointerUp,

                        //   disableGestures: false,
                        //   initialScale: PhotoViewComputedScale.contained,
                        //   // heroAttributes: PhotoViewHeroAttributes(

                        //   // ),
                        // );
                        // },
                        // scrollPhysics: BouncingScrollPhysics(),
                        //   backgroundDecoration: BoxDecoration(
                        //     color: Theme.of(context).canvasColor,
                        //   ),
                      ),
                      // !showMenu
                      //     ? Container()
                      //     : Positioned.fill(
                      //         child: GestureDetector(
                      //             onTapUp: (e) {
                      //               setState(() {
                      //                 showMenu = !showMenu;
                      //               });
                      //             },
                      //             child: Container(color: Colors.transparent)))

                      AnimatedPositioned(
                        bottom: (openToolbarIndex == 0) ? 0.0 : -300.0,
                        duration: Duration(milliseconds: 400),
                        curve: Curves.easeIn,
                        left: 0.0,
                        right: 0.0,
                        height: listMode
                            ? readerBloc.bookmarkList.length >= 4
                                ? 375.0
                                : 150.0 + 56.0 * readerBloc.bookmarkList.length
                            : 100.0,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                                topRight: Radius.circular(30)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.shade400,
                                blurRadius: 5,
                                offset: Offset(0, -1), // Shadow position
                              ),
                            ],
                          ),
                          padding: EdgeInsets.fromLTRB(20, 5, 20, 0),
                          child: Column(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width - 50,
                                child: SfSlider(
                                  min: 1.0,
                                  max: imageList.length > 0
                                      ? double.parse(
                                          (imageList.length).toString())
                                      : 100.0,

                                  value: _value,
                                  enableTooltip: true, // 1 dune off e

                                  stepSize: 1.0,
                                  // showTicks: true,

                                  // interval: 20,
                                  activeColor: Theme.of(context).accentColor,
                                  inactiveColor: Theme.of(context)
                                      .accentColor
                                      .withOpacity(0.3),
                                  // minorTicksPerInterval: 1,
                                  onChanged: (dynamic value) {
                                    setState(() {
                                      _value = value.floorToDouble();
                                    });
                                    goToPage(
                                        context, (value.floor() - 1), false);
                                  },
                                ),
                              ),
                              Stack(
                                children: [
                                  Center(
                                    child: Text(
                                      DemoLocalizations.of(context).transNumber(
                                          (iw.readerPageNumber + 1).toString()),
                                      style: TextStyle(fontSize: 20),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            if (!listMode) {
                                              listMode = !listMode;
                                              bookmarkOrAnnotation = false;
                                            } else if (!bookmarkOrAnnotation) {
                                              listMode = !listMode;
                                            } else {
                                              bookmarkOrAnnotation = false;
                                            }
                                            // _setListViewSize();
                                          });
                                        },
                                        child: PlatformSvgWidget.asset(
                                            'assets/images/annotation.svg',
                                            height: 28.0,
                                            color: !bookmarkOrAnnotation
                                                ? Theme.of(context).accentColor
                                                : Theme.of(context)
                                                    .primaryColorDark),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            if (iw.readerPageNumber > 0)
                                              goToPage(
                                                  context,
                                                  (iw.readerPageNumber - 1),
                                                  false);
                                          });
                                        },
                                        child: Icon(
                                          Entypo.chevron_right,
                                          size: 25,
                                          color: iw.readerPageNumber > 0
                                              ? Theme.of(context)
                                                  .primaryColorDark
                                              : Theme.of(context)
                                                  .primaryColorDark
                                                  .withOpacity(0.5),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            if (iw.readerPageNumber <
                                                (tabController.length - 1))
                                              goToPage(
                                                  context,
                                                  (iw.readerPageNumber + 1),
                                                  false);
                                          });
                                        },
                                        child: Icon(
                                          Entypo.chevron_left,
                                          size: 25,
                                          color: iw.readerPageNumber <
                                                  tabController.length
                                              ? Theme.of(context)
                                                  .primaryColorDark
                                              : Theme.of(context)
                                                  .primaryColorDark
                                                  .withOpacity(0.5),
                                        ),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            readerBloc.fillBookmarkList();
                                            if (!listMode) {
                                              listMode = !listMode;
                                              bookmarkOrAnnotation = true;
                                            } else if (bookmarkOrAnnotation) {
                                              listMode = !listMode;
                                            } else {
                                              bookmarkOrAnnotation = true;
                                            }
                                          });
                                        },
                                        child: Icon(
                                          MaterialCommunityIcons
                                              .bookmark_multiple_outline,
                                          size: 25,
                                          color: bookmarkOrAnnotation
                                              ? Theme.of(context).accentColor
                                              : Theme.of(context)
                                                  .primaryColorDark,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              listMode
                                  ? Card(
                                      elevation: 4.0,
                                      color: Theme.of(context).primaryColor,
                                      margin: const EdgeInsets.fromLTRB(
                                          10.0, 30.0, 10.0, 30.0),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(7.0)),
                                      child: ConstrainedBox(
                                        constraints: BoxConstraints(
                                          minHeight: 53,
                                          maxHeight: 227,
                                        ),
                                        child: StreamBuilder<dynamic>(
                                            stream:
                                                readerBloc.getSelectionCalls(),
                                            builder: (context, snapshot) {
                                              return ListView.builder(
                                                key: _listViewWidgetKey,
                                                shrinkWrap: true,
                                                itemCount: bookmarkOrAnnotation
                                                    ? readerBloc
                                                        .bookmarkList.length
                                                    : readerBloc
                                                        .bookmarkList.length,
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  return Column(
                                                    children: [
                                                      ListTile(
                                                        leading:
                                                            bookmarkOrAnnotation
                                                                ? Icon(
                                                                    FontAwesome
                                                                        .bookmark_o,
                                                                    color: Theme.of(
                                                                            context)
                                                                        .primaryColorDark,
                                                                  )
                                                                : PlatformSvgWidget
                                                                    .asset(
                                                                        'assets/images/highlighter.svg',
                                                                        height:
                                                                            28.0),
                                                        title: Text(
                                                            "صفحه " +
                                                                DemoLocalizations.of(
                                                                        context)
                                                                    .transNumber(
                                                                        (readerBloc.bookmarkList[index]["page"] +
                                                                                1)
                                                                            .toString()),
                                                            style: TextStyle(
                                                                fontSize:
                                                                    14.0)),
                                                        trailing: Icon(Icons
                                                            .keyboard_arrow_left),
                                                        onTap: () {
                                                          Navigator.of(context)
                                                              .pushNamed('/');
                                                        },
                                                      ),
                                                      (readerBloc.bookmarkList
                                                                      .length !=
                                                                  1 &&
                                                              index !=
                                                                  readerBloc
                                                                          .bookmarkList
                                                                          .length -
                                                                      1)
                                                          ? _buildDivider()
                                                          : Container()
                                                    ],
                                                  );
                                                },
                                              );
                                            }),
                                      ),
                                    )
                                  : Container()
                            ],
                          ),
                        ),
                      ),

                      AnimatedPositioned(
                        bottom: (openToolbarIndex == 3 ||
                                openToolbarIndex == 4 ||
                                openToolbarIndex == 5)
                            ? 0.0
                            : -300.0,
                        duration: Duration(milliseconds: 400),
                        curve: Curves.easeIn,
                        left: 0.0,
                        right: 0.0,
                        height: textMode
                            ? (_textFieldWidgetHeight > 97.0)
                                ? _textFieldWidgetHeight + 155
                                : 260.0
                            : 70.0,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                                topRight: Radius.circular(30)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.shade400,
                                blurRadius: 5,
                                offset: Offset(0, -1), // Shadow position
                              ),
                            ],
                          ),
                          padding: EdgeInsets.all(10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    width: 40,
                                    height: 40,
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          readerBloc.setCommand(
                                            {
                                              "color": readerBloc
                                                  .firstHighlightColor
                                                  .withOpacity(0.3),
                                              "page": iw.readerPageNumber
                                            },
                                          );
                                        });
                                        readerBloc.setSelectedColorIndex(1);
                                      },
                                      child: checkSelectedColor(1)
                                          ? Container(
                                              padding: EdgeInsets.all(4),
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 2,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                                shape: BoxShape.circle,
                                              ),
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: readerBloc
                                                      .firstHighlightColor,
                                                  shape: BoxShape.circle,
                                                ),
                                                child: Padding(
                                                  padding: EdgeInsets.all(2),
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: Theme.of(context)
                                                          .primaryColor
                                                          .withOpacity(0.4),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : Container(
                                              margin: EdgeInsets.all(4),
                                              decoration: BoxDecoration(
                                                color: readerBloc
                                                    .firstHighlightColor,
                                                shape: BoxShape.circle,
                                              ),
                                              child: Padding(
                                                padding: EdgeInsets.all(2),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Theme.of(context)
                                                        .primaryColor
                                                        .withOpacity(0.4),
                                                  ),
                                                ),
                                              ),
                                            ),
                                    ),
                                  ),
                                  SizedBox(width: 1),
                                  Container(
                                    width: 40,
                                    height: 40,
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          readerBloc.setCommand(
                                            {
                                              "color": readerBloc
                                                  .secondHighlightColor
                                                  .withOpacity(0.3),
                                              "page": iw.readerPageNumber
                                            },
                                          );
                                        });
                                        readerBloc.setSelectedColorIndex(2);
                                      },
                                      child: checkSelectedColor(2)
                                          ? Container(
                                              padding: EdgeInsets.all(4),
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  width: 2,
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                ),
                                                shape: BoxShape.circle,
                                              ),
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: readerBloc
                                                      .secondHighlightColor,
                                                  shape: BoxShape.circle,
                                                ),
                                                child: Padding(
                                                  padding: EdgeInsets.all(2),
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: Theme.of(context)
                                                          .primaryColor
                                                          .withOpacity(0.4),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : Container(
                                              margin: EdgeInsets.all(4),
                                              decoration: BoxDecoration(
                                                color: readerBloc
                                                    .secondHighlightColor,
                                                shape: BoxShape.circle,
                                              ),
                                              child: Padding(
                                                padding: EdgeInsets.all(2),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Theme.of(context)
                                                        .primaryColor
                                                        .withOpacity(0.4),
                                                  ),
                                                ),
                                              ),
                                            ),
                                    ),
                                  ),
                                  SizedBox(width: 1),
                                  Container(
                                    width: 40,
                                    height: 40,
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          readerBloc.setCommand(
                                            {
                                              "color": readerBloc
                                                  .thirdHighlightColor
                                                  .withOpacity(0.3),
                                              "page": iw.readerPageNumber
                                            },
                                          );
                                        });
                                        readerBloc.setSelectedColorIndex(3);
                                      },
                                      child: checkSelectedColor(3)
                                          ? Container(
                                              padding: EdgeInsets.all(4),
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                    width: 2,
                                                    color: Theme.of(context)
                                                        .primaryColorDark),
                                                shape: BoxShape.circle,
                                              ),
                                              child: Container(
                                                decoration: BoxDecoration(
                                                  color: readerBloc
                                                      .thirdHighlightColor,
                                                  shape: BoxShape.circle,
                                                ),
                                                child: Padding(
                                                  padding: EdgeInsets.all(2),
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      shape: BoxShape.circle,
                                                      color: Theme.of(context)
                                                          .primaryColor
                                                          .withOpacity(0.4),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : Container(
                                              margin: EdgeInsets.all(4),
                                              decoration: BoxDecoration(
                                                color: readerBloc
                                                    .thirdHighlightColor,
                                                shape: BoxShape.circle,
                                              ),
                                              child: Padding(
                                                padding: EdgeInsets.all(2),
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Theme.of(context)
                                                        .primaryColor
                                                        .withOpacity(0.4),
                                                  ),
                                                ),
                                              ),
                                            ),
                                    ),
                                  ),
                                  SizedBox(width: 1),
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _openFullMaterialColorPicker(iw);
                                      });
                                      readerBloc.setSelectedColorIndex(4);
                                    },
                                    child: (readerBloc.mainColor == null)
                                        ? Container(
                                            margin: EdgeInsets.all(4),
                                            width: 30,
                                            height: 30,
                                            child: PlatformSvgWidget.asset(
                                                'assets/images/circle_wheel2.svg'),
                                          )
                                        : Container(
                                            height: 40,
                                            width: 40,
                                            child: checkSelectedColor(4)
                                                ? Container(
                                                    padding: EdgeInsets.all(4),
                                                    decoration: BoxDecoration(
                                                      border: Border.all(
                                                          width: 2,
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColorDark),
                                                      shape: BoxShape.circle,
                                                    ),
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        color: readerBloc
                                                            .tempMainColor,
                                                        shape: BoxShape.circle,
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            EdgeInsets.all(2),
                                                        child: Container(
                                                          decoration:
                                                              BoxDecoration(
                                                            shape:
                                                                BoxShape.circle,
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColor
                                                                .withOpacity(
                                                                    0.4),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  )
                                                : Container(
                                                    margin: EdgeInsets.all(4),
                                                    decoration: BoxDecoration(
                                                      color: readerBloc
                                                          .tempMainColor,
                                                      shape: BoxShape.circle,
                                                    ),
                                                    child: Padding(
                                                      padding:
                                                          EdgeInsets.all(2),
                                                      child: Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          shape:
                                                              BoxShape.circle,
                                                          color: Theme.of(
                                                                  context)
                                                              .primaryColor
                                                              .withOpacity(0.4),
                                                        ),
                                                        child: Container(
                                                          child: Icon(
                                                            MaterialCommunityIcons
                                                                .eyedropper,
                                                            size: 15,
                                                            color: Theme.of(
                                                                    context)
                                                                .primaryColorDark,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                          ),
                                  ),
                                  Spacer(),
                                  StreamBuilder<dynamic>(
                                      stream: readerBloc.getSelectionCalls(),
                                      builder: (context, snapshot) {
                                        return AnimatedOpacity(
                                          opacity: (snapshot.hasData &&
                                                  (snapshot.data != null) &&
                                                  (snapshot.data["select"] !=
                                                      null) &&
                                                  (snapshot.data["select"]
                                                          ["end"] !=
                                                      null))
                                              ? (snapshot.data["select"]
                                                          ["touch"] !=
                                                      null)
                                                  ? 1
                                                  : 1 //  both cases
                                              : 0.3,
                                          duration: Duration(milliseconds: 300),
                                          child: IconButton(
                                            onPressed: () {
                                              if (snapshot.hasData &&
                                                  (snapshot.data != null) &&
                                                  (snapshot.data["select"] !=
                                                      null) &&
                                                  (snapshot.data["select"]
                                                          ["touch"] !=
                                                      null)) {
                                                // touch command save
                                                readerBloc.setCommand(
                                                  {
                                                    "click": "add",
                                                    "index": openToolbarIndex,
                                                    "menu": iw.selectionType,
                                                    "page": iw.readerPageNumber,
                                                  },
                                                );
                                              } else if (snapshot.hasData &&
                                                  (snapshot.data != null) &&
                                                  (snapshot.data["select"] !=
                                                      null) &&
                                                  (snapshot.data["select"]
                                                          ["end"] !=
                                                      null)) {
                                                readerBloc.setCommand(
                                                  {
                                                    "click": "add",
                                                    "index": openToolbarIndex,
                                                    "menu": iw.selectionType,
                                                    "page": iw.readerPageNumber,
                                                  },
                                                );
                                              }
                                            },
                                            icon: Icon(
                                              Feather.check_square,
                                              size: 30,
                                            ),
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                          ),
                                        );
                                      }),
                                  StreamBuilder<dynamic>(
                                      stream: readerBloc.getSelectionCalls(),
                                      builder: (context, snapshot) {
                                        return AnimatedOpacity(
                                            opacity: (snapshot.hasData &&
                                                    (snapshot.data != null) &&
                                                    (snapshot.data["select"] !=
                                                        null) &&
                                                    (snapshot.data["select"]
                                                            ["end"] !=
                                                        null))
                                                ? (snapshot.data["select"]
                                                            ["touch"] !=
                                                        null)
                                                    ? 1
                                                    : 1 //  both cases
                                                : 0.3,
                                            duration:
                                                Duration(milliseconds: 300),
                                            child: IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  textMode = !textMode;
                                                  _textFieldWidgetHeight = 0.0;
                                                });
                                              },
                                              icon: Icon(
                                                MaterialCommunityIcons
                                                    .card_text_outline,
                                                size: 30,
                                              ),
                                              color: Theme.of(context)
                                                  .primaryColorDark,
                                            ));
                                      }),
                                  StreamBuilder<dynamic>(
                                      stream: readerBloc.getSelectionCalls(),
                                      builder: (context, snapshot) {
                                        return AnimatedOpacity(
                                            opacity: (snapshot.hasData &&
                                                    (snapshot.data != null) &&
                                                    (snapshot.data["select"] !=
                                                        null) &&
                                                    (snapshot.data["select"]
                                                            ["end"] !=
                                                        null))
                                                ? (snapshot.data["select"]
                                                            ["touch"] !=
                                                        null)
                                                    ? 1
                                                    : 1 //  both cases
                                                : 0.3,
                                            duration:
                                                Duration(milliseconds: 300),
                                            child: IconButton(
                                              onPressed: () {
                                                if (snapshot.hasData &&
                                                    (snapshot.data != null) &&
                                                    (snapshot.data["select"] !=
                                                        null) &&
                                                    (snapshot.data["select"]
                                                            ["touch"] !=
                                                        null)) {
                                                  readerBloc.setCommand(
                                                    {
                                                      "click": "delete",
                                                      "index": openToolbarIndex,
                                                      "menu": "highlight",
                                                      "page":
                                                          iw.readerPageNumber
                                                    },
                                                  );
                                                }
                                              },
                                              icon: Icon(
                                                Feather.trash_2,
                                                size: 30,
                                              ),
                                              color: Colors.red,
                                            ));
                                      })
                                ],
                              ),
                              textMode
                                  ? Container(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        // mainAxisAlignment:
                                        //     MainAxisAlignment.start,
                                        children: [
                                          SizedBox(height: 15),
                                          Text(
                                            DemoLocalizations.of(context)
                                                .trans("Note"),
                                            style: TextStyle(fontSize: 20),
                                          ),
                                          SizedBox(height: 10),
                                          TextField(
                                            key: _textFieldWidgetKey,
                                            onChanged: (s) =>
                                                _setTextFieldSize(),
                                            autocorrect: true,
                                            maxLines: null,
                                            minLines: 2,
                                            cursorColor: Colors.red,
                                            decoration: InputDecoration(
                                              filled: true,
                                              hintText:
                                                  "یادداشت های خود را اینجا بنویسید ...",
                                              hintStyle: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                  fontSize: 14),
                                              fillColor: readerBloc
                                                  .selectColor()
                                                  .withOpacity(0.5),
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(12.0)),
                                                borderSide: BorderSide(
                                                    color: readerBloc
                                                        .selectColor(),
                                                    width: 2),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(10.0)),
                                                borderSide: BorderSide(
                                                    color: readerBloc
                                                        .selectColor(),
                                                    width: 2),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  : Container(),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0.0, 1.0), //(x,y)
                        blurRadius: 6.0,
                      ),
                    ], color: Theme.of(context).accentColor),
                    height: 60,
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 10,
                  right: 5,
                  height: 60,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.arrow_back,
                          color: Theme.of(context).primaryColor,
                          size: 25,
                        ),
                        onPressed: () {
                          setState(() {
                            Navigator.of(context).pop();
                          });
                        },
                      ),
                      IconButton(
                        icon: Icon(
                          FontAwesome.moon_o,
                          color: Theme.of(context).primaryColor,
                          size: 25,
                        ),
                        onPressed: () {
                          setState(() {
                            // _dark = !_dark;
                          });
                        },
                      )
                    ],
                  ),
                ),
                Positioned.fill(
                  top: 0,
                  left: 0,
                  right: 0,
                  // height: 60,
                  child: openToolbarIndex == 1
                      ? buildFloatingSearchBar(context)
                      : Container(
                          margin: EdgeInsets.only(
                            top: 15,
                            left: 80,
                            right: 80,
                          ),
                          alignment: Alignment.topCenter,
                          child: Text(
                            this.title,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 16,
                                fontWeight: FontWeight.normal),
                          ),
                        ),
                ),
              ],
            ),
            bottomNavigationBar: showMenu
                ? Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0.0, 1.0), //(x,y)
                        blurRadius: 6.0,
                      ),
                    ], color: Theme.of(context).primaryColor),
                    height: 60,
                    child: new TabBar(
                      indicatorColor: Colors.transparent,
                      // indicator: UnderlineTabIndicator(
                      //   borderSide: BorderSide(
                      //       color: Theme.of(context).accentColor, width: 2.0),
                      //   insets: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 58.0),
                      // ),
                      controller: _tabController,
                      onTap: (i) {
                        readerTools(i, context);
                      },
                      tabs: [
                        SizedBox(
                          height: 60,
                          child: Tab(
                            icon: Icon(
                              Ionicons.md_menu,
                              size: 28.0,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 60,
                          child: Tab(
                            icon: Icon(
                              Ionicons.md_search,
                              size: 28.0,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 60,
                          child: Tab(
                            icon: Icon(
                              (readerBloc.bookmarksMap.length > 0) &&
                                      ((readerBloc.bookmarksMap[
                                              (iw.readerPageNumber)
                                                  .toString()] is String) ||
                                          (readerBloc.bookmarksMap[
                                              (iw.readerPageNumber)
                                                  .toString()]))
                                  ? FontAwesome.bookmark
                                  : FontAwesome.bookmark_o,
                              size: 26.0,
                              color: (readerBloc.bookmarksMap.length > 0) &&
                                      ((readerBloc.bookmarksMap[
                                              (iw.readerPageNumber)
                                                  .toString()] is String) ||
                                          (readerBloc.bookmarksMap[
                                              (iw.readerPageNumber)
                                                  .toString()]))
                                  ? Theme.of(context).accentColor
                                  : Theme.of(context).primaryColorDark,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 60,
                          child: Tab(
                            icon: Icon(
                              MaterialCommunityIcons.format_color_highlight,
                              size: 30.0,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 60,
                          child: Tab(
                            icon: Icon(
                              MaterialCommunityIcons.comment_text_outline,
                              size: 26.0,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 60,
                          child: Tab(
                            icon: Icon(
                              Feather.underline,
                              size: 26.0,
                            ),
                          ),
                        ),
                      ],
                      labelColor: openToolbarIndex == -1
                          ? Theme.of(context).primaryColorDark
                          : Theme.of(context).accentColor,
                      unselectedLabelColor: Theme.of(context).primaryColorDark,
                      // indicatorSize: TabBarIndicatorSize.label,
                      // indicatorPadding: EdgeInsets.fromLTRB(2, 0, 2, 5),
                      // indicatorColor: Theme.of(context).accentColor,
                      // // indicator: CircleTabIndicator(
                      // //     color: Theme.of(context).accentColor, radius: 4),
                      // indicatorWeight: 3.0,
                    ),
                  )
                : null,
          ),
        ),
      ),
    );
  }

  bool checkSelectedColor(i) {
    if (readerBloc.selectedColorIndex == i) {
      return true;
    } else {
      return false;
    }
  }

  Container _buildDivider() {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 8.0,
      ),
      width: double.infinity,
      height: 1.0,
      color: Colors.grey.shade400,
    );
  }

  void _openFullMaterialColorPicker(iw) async {
    print("object");
    _openDialog(
      "انتخاب رنگ هایلایتر",
      Container(
        height: 230.0,
        child: MaterialColorPicker(
          colors: materialColors,
          allowShades: false,
          selectedColor: readerBloc.mainColor,
          onMainColorChange: (color) => setState(() {
            readerBloc.tempMainColor = color;
            readerBloc.setCommand(
              {
                "color": readerBloc.tempMainColor.withOpacity(0.3),
                "page": iw.readerPageNumber
              },
            );
          }),
        ),
      ),
    );
  }

  void _openDialog(String title, Widget content) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          contentPadding: const EdgeInsets.fromLTRB(8, 40, 8, 20),
          title: Text(title),
          content: content,
          actions: [
            Container(
              width: 100,
              child: TextButton(
                child: Text(
                  'بیخیال',
                  style: TextStyle(color: Theme.of(context).primaryColorDark),
                ),
                onPressed: Navigator.of(context).pop,
              ),
            ),
            Container(
              width: 100,
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                      Theme.of(context).primaryColorDark),
                ),
                child: Text('انتخاب',
                    style: TextStyle(color: Theme.of(context).primaryColor)),
                onPressed: () {
                  Navigator.of(context).pop();
                  setState(
                      () => readerBloc.mainColor = readerBloc.tempMainColor);
                  setState(
                      () => readerBloc.shadeColor = readerBloc.tempShadeColor);
                },
              ),
            ),
          ],
        );
      },
    );
  }

  Future<void> readerTools(index, context) {
    final iw = MainStateContainer.of(context);
    // showBottomToolbar(index);
    switch (index) {
      case 0: // menu
        showBottomToolbar(index);
        iw.setGestureEnable(true);
        break;
      case 1: // search
        showBottomToolbar(index);
        break;
      case 2: // bookmark
        if ((readerBloc.bookmarksMap[(iw.readerPageNumber).toString()]
                is String) ||
            readerBloc.bookmarksMap[(iw.readerPageNumber).toString()] == true) {
          readerBloc.removeBookmark(iw.readerPageNumber);
        } else {
          readerBloc.addToBookmarks(iw.readerPageNumber);
        }
        print("55555555555555" +
            readerBloc.bookmarksMap[(iw.readerPageNumber).toString()]
                .toString());

        setState(() {
          listMode = false;
          textMode = false;
          openToolbarIndex = index;
        });

        break;
      case 3: // highlight
        showBottomToolbar(index);
        selectionMode(context, "highlight");
        break;
      case 4: // anottate
        showBottomToolbar(index);
        selectionMode(context, "shape");
        // readerBloc.setCommand({"color": Colors.red.withOpacity(0.4)});
        break;
      case 5: // underline
        showBottomToolbar(index);
        selectionMode(context, "underline");
        break;
      default: // ozgal
    }
  }

  void showBottomToolbar(index) {
    if (openToolbarIndex == index) {
      setState(() {
        listMode = false;
        textMode = false;
        openToolbarIndex = -1;
      });
    } else {
      setState(() {
        listMode = false;
        textMode = false;
        openToolbarIndex = index;
      });
    }
  }

  void selectionMode(context, type) {
    final iw = MainStateContainer.of(context);

    if (iw.gestureEnable) {
      if ((ebookPages != null) && (ebookPages.length > 0)) {
        ebookPages.forEach((paragraph) {
          // print(paragraph.toString());
          // print("start : " +
          //     paragraph["type"] +
          //     " x1 : " +
          //     paragraph["boundry"][0].toString() +
          //     "  y1 : " +
          //     paragraph["boundry"][1].toString());

          // print("end : " +
          //     paragraph["type"] +
          //     " x2 : " +
          //     paragraph["boundry"][2].toString() +
          //     "  y2 : " +
          //     paragraph["boundry"][3].toString());
        });
      }
    }
    iw.setGestureEnable(false);
    iw.setSelectionType(type);
  }

  void _onImagePointerUp(
      BuildContext c, TapUpDetails d, PhotoViewControllerValue v) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    print('localPosition: ${d.localPosition}, globalPosition: ${d.globalPosition}, ' +
        'x: ${d.globalPosition?.dx}, y: ${d.globalPosition?.dy}, width: $width, height: $height');
    // Fluttertoast.showToast(
    //   msg: 'localPosition: ${d.localPosition}, globalPosition: ${d.globalPosition}, ' +
    //       'x: ${d.globalPosition?.dx}, y: ${d.globalPosition?.dy}, width: $width, height: $height',
    // );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    final iw = MainStateContainer.of(context);
    if (mounted) {
      readerBloc.getSelectionCalls().listen((command) {
        if ((command != null) &&
            (command["pageLanded"] != null) &&
            (command["pageLanded"] == iw.readerPageNumber))
          goToPage(context, command["pageLanded"], true);
      });
    }

    getBook(context);
  }

  Future<void> getBookAllAnnotations(BuildContext context) {
    final iw = MainStateContainer.of(context);
    accountsRepo
        .getAllAnnotation(iw.hostName, this.token, iw.token)
        .then((anootationsRes) {
      if (mounted) {
        readerBloc.setBookAnnotations(anootationsRes);
        // readerBloc.fillBookmarkList();
      }
    });
  }

  Future<void> getBook(context) async {
    final iw = MainStateContainer.of(context);

    accountsRepo
        .getEBook(iw.hostName, this.token, iw.token)
        .then((value) async {
      setState(() {
        book = value;
        imageList.addAll(value["pages"]);
        readerBloc.setPages(imageList.length);
        tabController = new TabController(
            length: imageList.length, vsync: this, initialIndex: 0);
      });
      getBookAllAnnotations(context);
      getBookPage(context, iw.readerPageNumber);
    });
  }

  Future<void> getBookPage(context, pageNum) async {
    final iw = MainStateContainer.of(context);
    if (readerBloc.ebookPagesRes[pageNum.toString()] != null) {
      readerBloc.setCommand({
        "ebookPageRes": readerBloc.ebookPagesRes[pageNum.toString()],
        "page": pageNum
      });
    } else {
      accountsRepo
          .getEbookPages(iw.hostName, this.token, pageNum, iw.token)
          .then((ebookPageRes) {
        readerBloc.setCommand({"ebookPageRes": ebookPageRes, "page": pageNum});
      });
    }
  }
  // accountsRepo
  //     .getEbookPages(iw.hostName, "19c0d9cf1dbf7297", pageIndex)
  //     .then((ebookPageRes) {
  //   if (ebookPageRes["elements_v2"] is List<dynamic>) {
}

Widget buildBody() {
  final time = DateTime.now();
  // print('BuildBody at ${time.second}:${time.millisecond}');
  return ClipRRect(
    borderRadius: BorderRadius.circular(8),
    child: Material(
      child: Column(
        children: List.generate(100, (index) => index.toString())
            .map((e) => ListTile(
                  title: Text(e),
                ))
            .toList(),
      ),
    ),
  );
}

Widget buildFloatingSearchBar(context) {
  final isPortrait = MediaQuery.of(context).orientation == Orientation.portrait;

  return FloatingSearchBar(
    margins: EdgeInsets.only(top: 9.8),
    elevation: 2,
    // accentColor: Colors.blue,
    automaticallyImplyBackButton: false,
    backgroundColor: Theme.of(context).primaryColor.withOpacity(0.6),
    backdropColor: Theme.of(context).primaryColorDark.withOpacity(0.7),
    iconColor: Theme.of(context).primaryColorDark,
    scrollPadding: const EdgeInsets.only(top: 20, bottom: 20),
    borderRadius: BorderRadius.circular(20),
    hint: DemoLocalizations.of(context).trans("Search"),
    hintStyle: TextStyle(fontSize: 14),
    height: 38.0,
    controller: FloatingSearchBarController(),
    transitionCurve: Curves.easeInOut,
    axisAlignment: isPortrait ? 0.0 : 0.0,
    openAxisAlignment: 0.0,
    openWidth: MediaQuery.of(context).size.width - 50,
    // width: isPortrait ? 200 : 500,
    width: MediaQuery.of(context).size.width / 2,
    debounceDelay: const Duration(milliseconds: 500),
    closeOnBackdropTap: true,
    clearQueryOnClose: true,
    // onQueryChanged: (query) {
    //   if (query == "")
    //     setState(() {
    //       searchList = null;
    //     });
    //   else {
    //     if (query != "") {
    //       print(query.toString());
    //       setState(() {
    //         searchList = [];
    //       });
    //       search(query.toString());
    //     } else {
    //       print("query : " + query);
    //     }
    //   }
    // },
    onFocusChanged: (f) {
      // setState(() {
      //   this.logoOpacity = !this.logoOpacity;
      //   open = f;
      // });
    },
    actions: [
      FloatingSearchBarAction.searchToClear(
        size: 25,
        showIfClosed: true,
        color: Theme.of(context).primaryColorDark,
      ),
    ],
    transition: CircularFloatingSearchBarTransition(),
    physics: const BouncingScrollPhysics(),
    builder: (context, _) => buildBody(),
  );
}

// Future<void> search(query) async {
//   final iw = MainStateContainer.of(context);
//   print(query);
//   setState(() {
//     lastQuery = query;
//   });
//   await accountsRepo
//       .search(
//     iw.hostName,
//     query,
//   )
//       .then((searchRes) {
//     if ((lastQuery == query) && (searchList != null)) {
//       setState(() {
//         if (searchRes["data"].isEmpty) {
//           searchList.add("empty");
//         } else {
//           searchList = searchRes["data"];
//           paginateUrl = searchRes["path"];
//         }
//       });
//     } else {}
//   });
// }

// Future<void> paginateRequest() {
//   // final iw = MainStateContainer.of(context);
//   accountsRepo
//       .getSearchPaginate(this.paginateUrl, (paginateIndex + 1).toString())
//       .then((value) {
//     setState(() {
//       searchList.addAll(value["data"]);
//       this.paginateUrl = value["path"];
//       paginateIndex++;
//       oneTime = true;
//     });
//   });
// }

// void _scrollListener() {
//   ScrollPosition sp;
//   sp = this.childCont.positions.elementAt(0);
//   if (sp != null && this.childCont.positions != null) {
//     if (sp.pixels <= (150) && !sp.outOfRange) {
//       if (goTop) {
//         if (mounted) {
//           setState(() {
//             goTop = false;
//           });
//           parentCont.animateTo(0.0,
//               duration: Duration(milliseconds: 200),
//               curve: Curves.easeInOutCubic);
//           // }
//         }
//       }
//     } else if (sp.pixels > (150) && !sp.outOfRange) {
//       if (!goTop) {
//         if (mounted) {
//           setState(() {
//             goTop = true;
//           });
//           parentCont.animateTo(225.0,
//               duration: Duration(milliseconds: 200),
//               curve: Curves.easeInOutCubic);
//         }
//       } else if (sp.pixels + 200 >= sp.maxScrollExtent &&
//           !sp.outOfRange &&
//           oneTime) {
//         setState(() {
//           oneTime = false;
//           paginateRequest();
//         });
//         print("reach the bottom");
//       }
//     }
//   } else {
//     print(sp.toString());
//   }
// }
