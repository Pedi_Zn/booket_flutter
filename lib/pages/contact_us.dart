import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:booket_flutter/widgets/alert_dialog.dart';
import 'package:booket_flutter/widgets/email_dialog.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactUs extends StatefulWidget {
  ContactUs({Key key}) : super(key: key);

  @override
  ContactUsState createState() => ContactUsState();
}

class ContactUsState extends State<ContactUs> {
  // AboutUs() {
  // }
  List<dynamic> logoList;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          pinned: true,
          iconTheme: IconThemeData(
            color: Theme.of(context).primaryColor, //change your color here
          ),
          centerTitle: true,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                DemoLocalizations.of(context).trans("ContactUs"),
                style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.normal,
                    fontSize: 18.0),
              ),
            ],
          ),
          expandedHeight: 230.0,
          backgroundColor: Theme.of(context).accentColor,
          flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              background: Column(
                children: <Widget>[
                  Container(
                    height: 80.0,
                  ),
                  PlatformSvgWidget.asset('assets/images/contact_us.svg',
                      height: 187.0),
                ],
              )),
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return Container(
                padding: EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 20.0),
                child: Column(
                  children: [
                    Text(
                      DemoLocalizations.of(context).trans("ContactUsText1"),
                      textAlign: TextAlign.justify,
                      style: TextStyle(fontSize: 14.0),
                    ),
                    Stack(
                      children: [
                        Positioned(
                            left: 0.0,
                            top: 25.0,
                            child: Icon(Icons.phone_android,
                                color: Colors.black, size: 25.0)),
                        Container(
                          alignment: Alignment.bottomLeft,
                          margin: EdgeInsets.fromLTRB(40.0, 0.0, 0, 0),
                          child: Column(
                            children: [
                              GestureDetector(
                                onTap: () => launch("tel://" +
                                    DemoLocalizations.of(context)
                                        .trans("ContactUsText2.1")),
                                child: Text(
                                  DemoLocalizations.of(context)
                                      .trans("ContactUsText2.1"),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 15.0),
                                ),
                              ),
                              GestureDetector(
                                onTap: () => launch("tel://" +
                                    DemoLocalizations.of(context)
                                        .trans("ContactUsText2.2")),
                                child: Text(
                                  DemoLocalizations.of(context)
                                      .trans("ContactUsText2.2"),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 15.0),
                                ),
                              ),
                              GestureDetector(
                                onTap: () => launch("tel://" +
                                    DemoLocalizations.of(context)
                                        .trans("ContactUsText2.3")),
                                child: Text(
                                  DemoLocalizations.of(context)
                                      .trans("ContactUsText2.3"),
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 15.0),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Text(
                      DemoLocalizations.of(context).trans("ContactUsText3"),
                      textAlign: TextAlign.justify,
                      style: TextStyle(fontSize: 14.0),
                    ),
                    Stack(
                      children: [
                        Positioned(
                            left: 0.0,
                            top: 25.0,
                            child: Icon(Icons.email_outlined,
                                color: Colors.black, size: 25.0)),
                        GestureDetector(
                          onTapDown: (s) {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return EmailDialog();
                                });
                            // showDialog(context: context,
                            // builder: (BuildContext context) {
                            //   return CustomDialog(title : DemoLocalizations.of(context).trans("LogoutText"));
                            // });
                            // setState(() {
                            //   _onAlertWithCustomImagePressed(context);
                            // });
                          },
                          child: Container(
                            alignment: Alignment.bottomLeft,
                            margin: EdgeInsets.fromLTRB(40.0, 0.0, 0, 0),
                            child: Text(
                              DemoLocalizations.of(context)
                                  .trans("ContactUsText4"),
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 15.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      DemoLocalizations.of(context).trans("ContactUsText5"),
                      textAlign: TextAlign.justify,
                      style: TextStyle(fontSize: 14.0),
                    ),
                    Stack(
                      children: [
                        Positioned(
                            left: 0.0,
                            top: 85.0,
                            child: Icon(Icons.location_on_outlined,
                                color: Colors.black, size: 25.0)),
                        Container(
                          alignment: Alignment.bottomLeft,
                          margin: EdgeInsets.fromLTRB(40.0, 0.0, 0, 0),
                          child: Text(
                            DemoLocalizations.of(context)
                                .trans("ContactUsText6"),
                            textAlign: TextAlign.left,
                            style: TextStyle(fontSize: 15.0),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              );
            },
            childCount: 1,
          ),
        ),
      ],
    ));
  }
}
