import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/pages/edit_profile.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:booket_flutter/widgets/email_or_phone_switch.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  const Login({Key key}) : super(key: key);

  @override
  LoginState createState() => new LoginState();
}

class LoginState extends State<Login> with SingleTickerProviderStateMixin {
  bool expandedSearch = false;
  bool loginType = true;
  bool loginResult = false;
  double logoOpacity = 1;
  double _width = 200;
  FocusNode searchFocus;

  final GlobalKey scaffoldKey = new GlobalKey<ScaffoldState>();
  final AccountsRepo accountsRepo = new AccountsRepo();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    this.searchFocus = new FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          elevation: 0,
          // backgroundColor: Theme.of(context).accentColor,
          iconTheme: IconThemeData(color: Colors.black),
          title: Text(
            DemoLocalizations.of(context).trans("Login"),
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w800),
          ),
        ),
        body: Builder(builder: (context) {
          return Stack(
            alignment: Alignment.center,
            children: [
              Positioned(
                  top: 100,
                  right: -50,
                  child: Container(
                    width: 100,
                    height: 100,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Theme.of(context).accentColor),
                  )),

              Positioned(
                  top: -50,
                  left: -50,
                  child: Container(
                    width: 200,
                    height: 200,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100),
                        color: Theme.of(context).accentColor),
                  )),
              // CustomPaint(
              //   child: Container(
              //     width: MediaQuery.of(context).size.width,
              //     height: MediaQuery.of(context).size.height,
              //   ),
              //   painter: HeaderCurvedContainer(context),
              // ),
              // Column(
              //   crossAxisAlignment: CrossAxisAlignment.center,
              //   children: [
              //     Container(
              //       padding: EdgeInsets.all(10.0),
              //       margin: EdgeInsets.only(top: 40.0),
              //       width: 120.0,
              //       height: 120.0,
              //       decoration: BoxDecoration(
              //         border: Border.all(color: Colors.white, width: 3),
              //         shape: BoxShape.circle,
              //         color: Colors.white,
              //         image: DecorationImage(
              //           fit: BoxFit.cover,
              //           image: AssetImage('assets/images/pedi.jpg'),
              //         ),
              //       ),
              //     ),
              //   ],
              // ),
              // Positioned(
              //   top: 130,
              //   right: 120,
              //   child: Container(
              //     height: 30.0,
              //     child: CircleAvatar(
              //       backgroundColor: Theme.of(context).accentColor,
              //       child: IconButton(
              //         icon: Icon(
              //           Icons.edit,
              //           color: Colors.white,
              //           size: 15.0,
              //         ),
              //         onPressed: () {},
              //       ),
              //     ),
              //   ),
              // ),
              Positioned(
                top: 200,
                left: 0,
                right: 0,
                child: Container(
                  margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
                  child: Column(
                    children: [
                      Container(
                        // height: 200.0,
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 5.0),
                              child: Center(
                                child: Text(
                                  !loginType
                                      ? DemoLocalizations.of(context)
                                          .trans("Email")
                                      : DemoLocalizations.of(context)
                                          .trans("Phone"),
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColorDark,
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.normal),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Container(
                              height: 40.0,
                              child: TextField(
                                onChanged: (s) {
                                  iw.authenticationBloc.updatePhoneNumber(s);
                                },
                                keyboardType: !loginType
                                    ? TextInputType.emailAddress
                                    : TextInputType.number,
                                textAlign: TextAlign.center,
                                controller: !loginType
                                    ? emailController
                                    : phoneController,
                                autofocus: false,
                                style: TextStyle(
                                    color: Theme.of(context).primaryColorDark,
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.normal),
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  contentPadding: const EdgeInsets.fromLTRB(
                                      0.0, 0.0, 0.0, 0.0),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(7.0),
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .primaryColorDark
                                              .withOpacity(0.3))),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(7.0),
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .primaryColorDark)),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
                              child: Center(
                                child: Text(
                                  DemoLocalizations.of(context)
                                      .trans("Password"),
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColorDark,
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.normal),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                            Container(
                              height: 40.0,
                              child: TextField(
                                onChanged: (s) {
                                  iw.authenticationBloc.updatePassword(s);
                                },
                                obscureText: true,
                                textAlign: TextAlign.center,
                                controller: passwordController,
                                autofocus: false,
                                style: TextStyle(
                                    color: Theme.of(context).primaryColorDark,
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.normal),
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  // hintText: DemoLocalizations.of(context).trans("Search"),
                                  //   hintStyle: TextStyle(
                                  //     color: Theme.of(context).primaryColor,
                                  //   ),
                                  contentPadding: const EdgeInsets.fromLTRB(
                                      0.0, 0.0, 0.0, 0.0),
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(7.0),
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .primaryColorDark
                                              .withOpacity(0.3))),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(7.0),
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .primaryColorDark)),
                                ),
                              ),
                            ),
                            Container(
                              height: 25.0,
                            )
                          ],
                        ),
                      ),
                      RaisedButton(
                        onPressed: () {
                          loginPressed(context);
                          // Navigator.of(context).pushNamed('/books/12343', arguments: RouteArgument(id:12, argumentsList: [true, "salam"]));
                        },
                        child: Container(
                            width: double.infinity,
                            height: 40.0,
                            child: Center(
                              child: !loginResult
                                  ? Text(
                                      DemoLocalizations.of(context)
                                          .trans("Login"),
                                      style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontSize: 15.0,
                                          fontWeight: FontWeight.normal),
                                      textAlign: TextAlign.center,
                                    )
                                  : Container(
                                      width: 50.0,
                                      child: Image.asset(
                                          "assets/images/loading_book_white.gif")),
                            )),
                        color: Theme.of(context).accentColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(7.0),
                        ),
                      ),
                      Container(
                        height: 55.0,
                        // color: Colors.blue,
                        margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 27.0),
                        child: Center(
                          child: GestureDetector(
                            onTapDown: (s) {
                              setState(() {
                                Navigator.of(context)
                                    .pushNamed('/ForgotPassword');
                              });
                            },
                            child: Text(
                              DemoLocalizations.of(context)
                                  .trans("ForgotPassword"),
                              style: TextStyle(
                                  color: Theme.of(context).primaryColorDark,
                                  fontSize: 13.0,
                                  fontWeight: FontWeight.normal),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        height: 2.0,
                      ),
                      RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed('/Register');
                        },
                        child: Container(
                            width: double.infinity,
                            height: 40.0,
                            child: Center(
                              child: Text(
                                DemoLocalizations.of(context)
                                    .trans("HaveNoAccount"),
                                style: TextStyle(
                                    color: Theme.of(context).primaryColorDark,
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.normal),
                                textAlign: TextAlign.center,
                              ),
                            )),
                        color: Theme.of(context).dividerColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(7.0),
                        ),
                      ),
                      Container(
                        height: 15.0,
                      ),
                    ],
                  ),
                ),
              )
            ],
          );
          // return Column(
          //   children: [
          //     // EmailOrPhoneSwitch(
          //     //     loginType: loginType,
          //     //     title: DemoLocalizations.of(context).trans("Login"),
          //     //     swich: (data) {
          //     //       setState(() {
          //     //         loginType = data;
          //     //       });
          //     //     }),
          //     Container(
          //       margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
          //       child: Column(
          //         children: [
          //           Container(
          //             // height: 200.0,
          //             child: Column(
          //               children: [
          //                 Container(
          //                   margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 5.0),
          //                   child: Center(
          //                     child: Text(
          //                       !loginType
          //                           ? DemoLocalizations.of(context)
          //                               .trans("Email")
          //                           : DemoLocalizations.of(context)
          //                               .trans("Phone"),
          //                       style: TextStyle(
          //                           color: Theme.of(context).primaryColorDark,
          //                           fontSize: 15.0,
          //                           fontWeight: FontWeight.normal),
          //                       textAlign: TextAlign.center,
          //                     ),
          //                   ),
          //                 ),
          //                 Container(
          //                   height: 40.0,
          //                   child: TextField(
          //                     onChanged: (s) {
          //                       iw.authenticationBloc.updatePhoneNumber(s);
          //                     },
          //                     keyboardType: !loginType
          //                         ? TextInputType.emailAddress
          //                         : TextInputType.number,
          //                     textAlign: TextAlign.center,
          //                     controller: !loginType
          //                         ? emailController
          //                         : phoneController,
          //                     autofocus: false,
          //                     style: TextStyle(
          //                         color: Theme.of(context).primaryColorDark,
          //                         fontSize: 18.0,
          //                         fontWeight: FontWeight.normal),
          //                     decoration: InputDecoration(
          //                       border: OutlineInputBorder(),
          //                       contentPadding: const EdgeInsets.fromLTRB(
          //                           0.0, 0.0, 0.0, 0.0),
          //                       enabledBorder: OutlineInputBorder(
          //                           borderRadius: BorderRadius.circular(7.0),
          //                           borderSide: BorderSide(
          //                               color: Theme.of(context)
          //                                   .primaryColorDark
          //                                   .withOpacity(0.3))),
          //                       focusedBorder: OutlineInputBorder(
          //                           borderRadius: BorderRadius.circular(7.0),
          //                           borderSide: BorderSide(
          //                               color: Theme.of(context)
          //                                   .primaryColorDark)),
          //                     ),
          //                   ),
          //                 ),
          //                 Container(
          //                   margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
          //                   child: Center(
          //                     child: Text(
          //                       DemoLocalizations.of(context).trans("Password"),
          //                       style: TextStyle(
          //                           color: Theme.of(context).primaryColorDark,
          //                           fontSize: 15.0,
          //                           fontWeight: FontWeight.normal),
          //                       textAlign: TextAlign.center,
          //                     ),
          //                   ),
          //                 ),
          //                 Container(
          //                   height: 40.0,
          //                   child: TextField(
          //                     onChanged: (s) {
          //                       iw.authenticationBloc.updatePassword(s);
          //                     },
          //                     obscureText: true,
          //                     textAlign: TextAlign.center,
          //                     controller: passwordController,
          //                     autofocus: false,
          //                     style: TextStyle(
          //                         color: Theme.of(context).primaryColorDark,
          //                         fontSize: 18.0,
          //                         fontWeight: FontWeight.normal),
          //                     decoration: InputDecoration(
          //                       border: OutlineInputBorder(),
          //                       // hintText: DemoLocalizations.of(context).trans("Search"),
          //                       //   hintStyle: TextStyle(
          //                       //     color: Theme.of(context).primaryColor,
          //                       //   ),
          //                       contentPadding: const EdgeInsets.fromLTRB(
          //                           0.0, 0.0, 0.0, 0.0),
          //                       enabledBorder: OutlineInputBorder(
          //                           borderRadius: BorderRadius.circular(7.0),
          //                           borderSide: BorderSide(
          //                               color: Theme.of(context)
          //                                   .primaryColorDark
          //                                   .withOpacity(0.3))),
          //                       focusedBorder: OutlineInputBorder(
          //                           borderRadius: BorderRadius.circular(7.0),
          //                           borderSide: BorderSide(
          //                               color: Theme.of(context)
          //                                   .primaryColorDark)),
          //                     ),
          //                   ),
          //                 ),
          //                 Container(
          //                   height: 25.0,
          //                 )
          //               ],
          //             ),
          //           ),
          //           RaisedButton(
          //             onPressed: () {
          //               loginPressed(context);
          //               // Navigator.of(context).pushNamed('/books/12343', arguments: RouteArgument(id:12, argumentsList: [true, "salam"]));
          //             },
          //             child: Container(
          //                 width: double.infinity,
          //                 height: 40.0,
          //                 child: Center(
          //                   child: !loginResult
          //                       ? Text(
          //                           DemoLocalizations.of(context)
          //                               .trans("Login"),
          //                           style: TextStyle(
          //                               color: Theme.of(context).primaryColor,
          //                               fontSize: 15.0,
          //                               fontWeight: FontWeight.normal),
          //                           textAlign: TextAlign.center,
          //                         )
          //                       : Container(
          //                           width: 50.0,
          //                           child: Image.asset(
          //                               "assets/images/loading_book_white.gif")),
          //                 )),
          //             color: Theme.of(context).accentColor,
          //             shape: RoundedRectangleBorder(
          //               borderRadius: BorderRadius.circular(7.0),
          //             ),
          //           ),
          //           Container(
          //             height: 55.0,
          //             // color: Colors.blue,
          //             margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 27.0),
          //             child: Center(
          //               child: GestureDetector(
          //                 onTapDown: (s) {
          //                   setState(() {
          //                     Navigator.of(context)
          //                         .pushNamed('/ForgotPassword');
          //                   });
          //                 },
          //                 child: Text(
          //                   DemoLocalizations.of(context)
          //                       .trans("ForgotPassword"),
          //                   style: TextStyle(
          //                       color: Theme.of(context).primaryColorDark,
          //                       fontSize: 13.0,
          //                       fontWeight: FontWeight.normal),
          //                   textAlign: TextAlign.center,
          //                 ),
          //               ),
          //             ),
          //           ),
          //           Container(
          //             height: 2.0,
          //           ),
          //           RaisedButton(
          //             onPressed: () {
          //               Navigator.of(context).pushNamed('/Register');
          //             },
          //             child: Container(
          //                 width: double.infinity,
          //                 height: 40.0,
          //                 child: Center(
          //                   child: Text(
          //                     DemoLocalizations.of(context)
          //                         .trans("HaveNoAccount"),
          //                     style: TextStyle(
          //                         color: Theme.of(context).primaryColorDark,
          //                         fontSize: 12.0,
          //                         fontWeight: FontWeight.normal),
          //                     textAlign: TextAlign.center,
          //                   ),
          //                 )),
          //             color: Theme.of(context).dividerColor,
          //             shape: RoundedRectangleBorder(
          //               borderRadius: BorderRadius.circular(7.0),
          //             ),
          //           ),
          //           Container(
          //             height: 15.0,
          //           ),
          //         ],
          //       ),
          //     ),
          //   ],
          // );
        }));
  }

  void login(BuildContext context) {
    Scaffold.of(context).showSnackBar(new SnackBar(
      content: Text("salam", style: TextStyle(color: Colors.white)),
      backgroundColor: Colors.red,
    ));
  }

  Future<void> loginPressed(context) async {
    final iw = MainStateContainer.of(context);
    if (!loginResult) {
      // 2 number refer the index of Home page
      setState(() {
        loginResult = true;
      });
      var response = await iw.authenticationBloc.login(iw.hostName, iw.locale);
      if (response.containsKey("message")) {
        Scaffold.of(context).showSnackBar(new SnackBar(
          content: Text("${response["message"]}",
              style: TextStyle(color: Colors.white, fontFamily: 'IranSans')),
          backgroundColor: Theme.of(context).accentColor,
        ));
        setState(() {
          loginResult = false;
        });
      } else {
        if (mounted) {
          iw.authenticationBloc.updateToken(response["auth"], 3);
          await Future.delayed(Duration(milliseconds: 10));
          iw.setToken(response["auth"]);
          dynamic me = response[
              "user"]; // = await iw.authenticationBloc.getProfile(iw.hostName);
          if ((me != null) && (!me.containsKey("title"))) {
            setState(() {
              loginResult = false;
            });
            Scaffold.of(scaffoldKey.currentContext).showSnackBar(new SnackBar(
              content: Text(
                "${me["message"]}",
                style: TextStyle(color: Colors.white),
              ),
              backgroundColor: Theme.of(context).accentColor,
            ));
          } else if ((me != null) && (me.containsKey("title"))) {
            setState(() {
              loginResult = false;
            });
            iw.updateUserInfo(me);
            var nextDestination = "/Profile";

            await accountsRepo
                .getProfile(
              iw.hostName,
              response["auth"],
            )
                .then((profile) {
              iw.updateUserInfo(profile);
              // print(iw.me.toString());
              // print(iw.me["hasBook"]);
              if (iw.me["hasbook"] != null) {
                Navigator.of(context).pushNamed('/Library');
              } else {
                Navigator.of(context).pushNamed('/Vitrine');
              }
            });
            await accountsRepo
                .getBasket(
              iw.hostName,
              response["auth"],
            )
                .then((basketReq) {
              setState(() {
                iw.setCart(basketReq);
              });
            });
            // Navigator.of(context)
            //     .popUntil((route) => !Navigator.of(context).canPop());

            // Navigator.of(context).pushNamed(nextDestination);

            iw.setLastPath("");
          }
        }
      }
    }
  }
}

class HeaderCurvedContainer extends CustomPainter {
  final BuildContext context;
  HeaderCurvedContainer(this.context);

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..color = Theme.of(context).accentColor;
    Path path = Path()
      ..relativeLineTo(0, 40)
      ..quadraticBezierTo(size.width / 2, 200, size.width, 40)
      ..relativeLineTo(0, -40)
      ..close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
