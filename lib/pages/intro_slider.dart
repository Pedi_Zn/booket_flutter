import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/tabs_page2.dart';
import 'package:booket_flutter/widgets/tabs_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:lottie/lottie.dart';

class IntroScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _IntroScreen();
  }
}

class _IntroScreen extends State<IntroScreen> {
  @override
  Widget build(BuildContext context) {
    PageDecoration pageDecoration = PageDecoration(
      titleTextStyle: TextStyle(
          fontSize: 26.0,
          fontWeight: FontWeight.w700,
          color: Theme.of(context).accentColor),
      titlePadding: EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 25.0),
      bodyTextStyle: TextStyle(
        fontSize: 18.0,
        color: Colors.black,
      ),
      descriptionPadding: EdgeInsets.fromLTRB(35.0, 0.0, 35.0, 20.0),
      imagePadding: EdgeInsets.all(40.0),
      pageColor: Theme.of(context).primaryColor,
      // boxDecoration:BoxDecoration(
      //   gradient: LinearGradient(
      //     begin: Alignment.topRight,
      //     end: Alignment.bottomLeft,
      //     stops: [0.1, 0.5, 0.7, 0.9],
      //     colors: [
      //       Colors.orange,
      //       Colors.deepOrangeAccent,
      //       Colors.red,
      //       Colors.redAccent,
      //     ],
      //   ),
      // ), //show linear gradient background of page
    );

    return IntroductionScreen(
      globalBackgroundColor: Colors.white,
      pages: [
        PageViewModel(
          title: "اپلیکیشن مطالعه و یادگیری الکترونیکی",
          body: "تمایز بوکت، امکان جستجوی کلیدواژه‌ها در محتوای کتاب هاست",
          image: introImage('assets/images/book.json'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "کتاب های درسی از ناشران دانشگاهی",
          body:
              "امکان کرایه کتاب الکترونیکی برای یک ترم و یا حتی خرید کتاب + ایبوک رایگان",
          image: introImage('assets/images/vitrine.json'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "کتابخانه و محیط مطالعه الکترونیکی",
          body:
              "ایبوک های خود را در کتابخانه الکترونیکی خود دریافت، مطالعه و از قابلیت‌های ویژه آن استفاده کنید.",
          image: introImage('assets/images/library.json'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "اطلاعات حساب کاربری",
          body:
              "با تکمیل اطلاعات خود در قسمت پروفایل از پیشنهادات شگفت انگیز ما با‌خبر شوید",
          image: introImage('assets/images/profile.json'),
          decoration: pageDecoration,
        ),

        //add more screen here
      ],

      onDone: () => goHomepage(context), //go to home page on done
      onSkip: () => goHomepage(context), // You can override on skip
      showSkipButton: false,
      skipFlex: 0,
      nextFlex: 0,
      skip: Text(
        'Skip',
        style: TextStyle(color: Colors.black),
      ),
      next: Icon(
        Icons.arrow_forward,
        color: Colors.black,
      ),
      done: Text(
        'ورود',
        style: TextStyle(
            fontWeight: FontWeight.w600, color: Colors.black, fontSize: 20.0),
      ),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0), //size of dots
        color: Colors.black, //color of dots
        activeColor: Colors.deepOrange,
        activeSize: Size(22.0, 10.0),
        //activeColor: Colors.white, //color of active dot
        activeShape: RoundedRectangleBorder(
          //shave of active dot
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
    );
  }

  void goHomepage(context) async {
    final iw = MainStateContainer.of(context);
    await iw.authenticationBloc.setFirstTimeOnStorage(false);
    Navigator.push(
        context,
        PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 500),
            transitionsBuilder: (context, animation, animationTime, child) {
              animation =
                  CurvedAnimation(parent: animation, curve: Curves.decelerate);
              return ScaleTransition(
                alignment: Alignment.center,
                child: child,
                scale: animation,
              );
            },
            pageBuilder: (context, animation, animationTime) {
              return TabsPage(pageIndex: 0);
            }));
  }

  Widget introImage(String assetName) {
    //widget to show intro image
    return Align(
      // precacheImage(AssetImage("images/splash_bg.png"), context);
      child: Lottie.asset('$assetName'),
      // alignment: Alignment.bottomCenter,
    );
  }
}
