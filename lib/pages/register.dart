import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/api/api.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/models/route_argument.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:booket_flutter/widgets/alert_dialog.dart';
import 'package:booket_flutter/widgets/email_or_phone_switch.dart';
import 'package:booket_flutter/widgets/privacy_dialog.dart';
import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  const Register({Key key}) : super(key: key);

  @override
  _RegisterState createState() => new _RegisterState();
}

class _RegisterState extends State<Register>
    with SingleTickerProviderStateMixin {
  bool expandedSearch = false;
  bool registerType = false;
  bool registerResult = false;
  double logoOpacity = 1;
  double _width = 200;
  FocusNode searchFocus;

  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();

  AccountsRepo accountsRepo = new AccountsRepo();

  @override
  void initState() {
    this.searchFocus = new FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orange[800],
          automaticallyImplyLeading: false,
          title: Container(
            width: MediaQuery.of(context).size.width,
            height: 60.0,
            child: Stack(
              children: [
                Positioned.fill(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    AnimatedContainer(
                      duration: const Duration(milliseconds: 250),
                      // width: this._width2,
                      // width: 60.0,
                      child: Center(
                        child: AnimatedOpacity(
                            duration: const Duration(milliseconds: 100),
                            opacity: logoOpacity,
                            child: IconButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              icon: Icon(
                                Icons.arrow_back_ios,
                                color: Colors.white,
                              ),
                            )),
                      ),
                    ),
                    AnimatedContainer(
                      duration: const Duration(milliseconds: 250),
                      // width: this._width2,
                      child: Center(
                          child: AnimatedOpacity(
                        duration: const Duration(milliseconds: 100),
                        opacity: logoOpacity,
                        // child: PlatformSvgWidget.asset('assets/images/booket_logo.svg',color: Colors.white, height: 18)
                      )),
                    ),
                  ],
                )),
                AnimatedPositioned(
                  width: this._width,
                  left: ((MediaQuery.of(context).size.width -
                          this._width -
                          30.0) /
                      2),
                  top: 10.5,
                  duration: const Duration(milliseconds: 250),
                  child: Container(
                    height: 38.0,
                    child: new TextField(
                      focusNode: searchFocus,
                      onEditingComplete: () {},
                      onSubmitted: (String s) {
                        searchFocus.unfocus();
                        setState(() {
                          this.logoOpacity = 1;
                          this.expandedSearch = false;
                          this._width = 200.0;
                        });
                      },
                      autofocus: false,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 15, color: Theme.of(context).primaryColor),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.orange,
                        hintText: DemoLocalizations.of(context).trans("Search"),
                        hintStyle: TextStyle(
                          color: Theme.of(context).primaryColor,
                        ),
                        contentPadding: EdgeInsets.only(left: 15, right: 10),
                        // border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0),
                            borderSide: BorderSide(
                                color: Theme.of(context)
                                    .accentColor
                                    .withOpacity(0.2))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0),
                            borderSide: BorderSide(
                                color: Theme.of(context).accentColor)),
                        prefixIcon: Icon(
                          Icons.search,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ),
                  ),
                ),
                AnimatedPositioned(
                    width: this._width,
                    height: 55.0,
                    left: ((MediaQuery.of(context).size.width -
                            this._width -
                            30.0) /
                        2),
                    top: 10.5,
                    duration: const Duration(milliseconds: 250),
                    child: GestureDetector(
                      onTapDown: (s) {
                        setState(() {
                          this.logoOpacity = 0;
                          this.expandedSearch = true;
                          this._width =
                              (MediaQuery.of(context).size.width) < 800.0
                                  ? (MediaQuery.of(context).size.width) * 9 / 11
                                  : 500.0;
                        });
                        FocusScope.of(context).requestFocus(searchFocus);
                      },
                    )),
              ],
            ),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: Column(
          children: [
            EmailOrPhoneSwitch(
                loginType: registerType,
                title: DemoLocalizations.of(context).trans("Register"),
                swich: (data) {
                  setState(() {
                    registerType = data;
                  });
                }),
            Container(
              margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
              child: Column(
                children: [
                  Container(
                    // height: 200.0,
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 5.0),
                          child: Center(
                            child: Text(
                              DemoLocalizations.of(context).trans("FullName"),
                              style: TextStyle(
                                  color: Theme.of(context).primaryColorDark,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Container(
                          height: 40.0,
                          child: TextField(
                            textAlign: TextAlign.center,
                            controller: nameController,
                            autofocus: false,
                            style: TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontSize: 18.0,
                                fontWeight: FontWeight.normal),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              // hintText: DemoLocalizations.of(context).trans("Search"),
                              // hintStyle: TextStyle(
                              //   fontSize: 15.0,
                              //   color: Theme.of(context).primaryColorDark,
                              // ),
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(7.0),
                                  borderSide: BorderSide(
                                      color: Theme.of(context)
                                          .primaryColorDark
                                          .withOpacity(0.3))),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(7.0),
                                  borderSide: BorderSide(
                                      color:
                                          Theme.of(context).primaryColorDark)),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
                          child: Center(
                            child: Text(
                              /*!registerType ? DemoLocalizations.of(context).trans("Email") : */ DemoLocalizations
                                      .of(context)
                                  .trans("Phone"),
                              style: TextStyle(
                                  color: Theme.of(context).primaryColorDark,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Container(
                          height: 40.0,
                          child: TextField(
                            textAlign: TextAlign.center,
                            controller: /*!registerType ? emailController : */ phoneController,
                            autofocus: false,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              // hintText: DemoLocalizations.of(context).trans("Search"),
                              //   hintStyle: TextStyle(
                              //     color: Theme.of(context).primaryColor,
                              //   ),
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(7.0),
                                  borderSide: BorderSide(
                                      color: Theme.of(context)
                                          .primaryColorDark
                                          .withOpacity(0.3))),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(7.0),
                                  borderSide: BorderSide(
                                      color:
                                          Theme.of(context).primaryColorDark)),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
                          child: Center(
                            child: Text(
                              DemoLocalizations.of(context).trans("Password"),
                              style: TextStyle(
                                  color: Theme.of(context).primaryColorDark,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Container(
                          height: 40.0,
                          child: TextField(
                            obscureText: true,
                            textAlign: TextAlign.center,
                            controller: passwordController,
                            autofocus: false,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              // hintText: DemoLocalizations.of(context).trans("Search"),
                              //   hintStyle: TextStyle(
                              //     color: Theme.of(context).primaryColor,
                              //   ),
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(7.0),
                                  borderSide: BorderSide(
                                      color: Theme.of(context)
                                          .primaryColorDark
                                          .withOpacity(0.3))),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(7.0),
                                  borderSide: BorderSide(
                                      color:
                                          Theme.of(context).primaryColorDark)),
                            ),
                          ),
                        ),
                        Container(
                          height: 25.0,
                        ),
                      ],
                    ),
                  ),
                  RaisedButton(
                    onPressed: () {
                      acceptPrivacy(context);
                      // Navigator.of(context).pushNamed('/books/12343', arguments: RouteArgument(id:12, argumentsList: [true, "salam"]));
                    },
                    child: Container(
                        width: double.infinity,
                        height: 40.0,
                        child: Center(
                          child: !registerResult
                              ? Text(
                                  DemoLocalizations.of(context)
                                      .trans("Register"),
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.normal),
                                  textAlign: TextAlign.center,
                                )
                              : Container(
                                  width: 50.0,
                                  child: Image.asset(
                                      "assets/images/loading_book_white.gif")),
                        )),
                    color: Theme.of(context).accentColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7.0),
                    ),
                  ),
                  Container(
                    height: 7.0,
                  ),
                  /*RaisedButton(
                    onPressed: () {
                      // Navigator.of(context).pushNamed('/books/12343', arguments: RouteArgument(id:12, argumentsList: [true, "salam"]));
                    },
                    child: Container(
                      width: double.infinity,
                      height: 40.0,
                      child: Center(
                        child: Text(
                          DemoLocalizations.of(context).trans("LoginWithGoogle"),
                          style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 15.0, fontWeight: FontWeight.normal),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ),
                    color: Theme.of(context).primaryColorDark,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7.0),
                    ),
                  ),
                  Container(height: 7.0,),
                  RaisedButton(
                    onPressed: () {
                      // Navigator.of(context).pushNamed('/books/12343', arguments: RouteArgument(id:12, argumentsList: [true, "salam"]));
                    },
                    child: Container(
                      width: double.infinity,
                      height: 40.0,
                      child: Center(
                        child: Text(
                          DemoLocalizations.of(context).trans("LoginWithApple"),
                          style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 15.0, fontWeight: FontWeight.normal),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ),
                    color: Theme.of(context).primaryColorDark,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7.0),
                    ),
                  ),*/
                  Container(
                    height: 7.0,
                  ),
                  RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed('/Login');
                    },
                    child: Container(
                        width: double.infinity,
                        height: 40.0,
                        child: Center(
                          child: Text(
                            DemoLocalizations.of(context).trans("HaveAccount"),
                            style: TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontSize: 12.0,
                                fontWeight: FontWeight.normal),
                            textAlign: TextAlign.center,
                          ),
                        )),
                    color: Theme.of(context).dividerColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7.0),
                    ),
                  ),
                  Container(
                    height: 15.0,
                  ),
                ],
              ),
            )
          ],
        )));
  }

  void register() {
    setState(() {
      registerResult = true;
    });
  }

  Future<void> acceptPrivacy(context) async {
    final iw = MainStateContainer.of(context);
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return PrivacyDialog(
            title: DemoLocalizations.of(context).trans("PrivacyText"),
            phoneNumber: phoneController.text,
            name: nameController.text,
            password: passwordController.text,
          );
        });
    setState(() {
      registerResult = true;
    });
    // print(iw.hostName);
  }
}
