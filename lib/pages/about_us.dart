import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:flutter_advanced_avatar/flutter_advanced_avatar.dart';

class AboutUs extends StatefulWidget {
  AboutUs({Key key}) : super(key: key);

  @override
  AboutUsState createState() => AboutUsState();
}

class AboutUsState extends State<AboutUs> {
  List<dynamic> logoList;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var personList = [
      {
        "img": "https://dev.mybooket.com/assets/team/dana.jpg",
        "name": "دانا خواجوی",
        "Roll": "بنیان‌گذار",
        "EnRoll": "CEO",
      },
      {
        "img": "https://dev.mybooket.com/assets/team/nami.png",
        "name": "نامی قرنی",
        "Roll": "بنیان‌گذار",
        "EnRoll": "COO",
      },
      {
        "img": "https://dev.mybooket.com/assets/team/erfan.jpg",
        "name": "عرفان فروغی",
        "Roll": "هم‌بنیان‌گذار",
        "EnRoll": "Co-Founder",
      },
      {
        "img": "https://dev.mybooket.com/assets/team/maziar.jpg",
        "name": "محمد ورمزیار",
        "Roll": "DevOps Engineer",
        "EnRoll": "-",
      },
      {
        "img": "https://dev.mybooket.com/assets/team/pedi.jpg",
        "name": "پدرام زین‌العابدین زادگان",
        "Roll": "برنامه نویس فرانت-اند",
        "EnRoll": "Front-End Developer",
      },
      {
        "img": "https://dev.mybooket.com/assets/team/safa.jpg",
        "name": "صفا مهدی زادگان",
        "Roll": "برنامه نویس بک-اند",
        "EnRoll": "Back-End Developer",
      },
      {
        "img": "https://dev.mybooket.com/assets/team/sabet.jpg",
        "name": "محمدرضا احسانی ثابت",
        "Roll": "برنامه نویس فول-استک",
        "EnRoll": "Full-Stack Developer",
      },
      {
        "img": "https://dev.mybooket.com/assets/team/alireza.jpg",
        "name": "علیرضا اکتفایی",
        "Roll": "طراح گرافیکی و رابط کاربری",
        "EnRoll": "UI/UX Designer",
      },
      {
        "img": "https://dev.mybooket.com/assets/team/ehsan.jpg",
        "name": "احسان سوری",
        "Roll": "متخصص بازاریابی دیجیتال",
        "EnRoll": "Digital Marketing Specialist",
      },
      {
        "img": "https://dev.mybooket.com/assets/team/fateme.jpg",
        "name": "فاطمه سجادیان",
        "Roll": "مدیر بازاریابی دیجیتال",
        "EnRoll": "Digital Marketing Manager",
      },
    ];

    var aboutUsText1 =
        '''این پلتفرم (بوکت | BOOKET) متعلق به شرکت"همراه علم" است که تحت شماره 144447 در اداره ثبت شرکت های استان تهران به ثبت رسیده است. شرکت همراه علم که از سال 1377 فعالیت‌ خود را مبتنی بر ارائه خدمات تخصصی نشر آغاز کرده، تا کنون خدمات بسیاری را در اختیار مشتریان خود قرار داده است. همکاری با تعداد زیادی از ناشران بین‌المللی و داخلی، کارگزاری نشریات چاپی و الکترونیکی بسیاری از دانشگاه ها، عرضه مستقیم کتاب در نمایشگاه های فصلی در ایران و... بخشی از سوابق و تجربیات شرکت همراه علم است.''';

    var aboutUsText2 =
        '''همراه علم یک شرکت دانش بنیان در حوزه فناوری مطالعه و یادگیری الکترونیکی است.
این مجموعه برای دانشجویان، اساتید دانشگاه ، مدیران و متخصصان شرایطی را فراهم کرده که با استفاده از روش‌هایی جذاب و سازگار با محیط بومی به نتایج بهتر علمی و تحقیقاتی دست یابند. با بهره‌گیری از بهترین فناوری‌ها، محتوای پژوهش‌ محور و اثبات‌ شده دانشگاه‌های بزرگ کشور را به شیوه‌ای ارائه کرده ایم تا علاقمندان به بهترین نحو ممکن از آن‌ها استفاده کرده و بهترین نتایج را کسب کنند.''';

    var aboutUsText3 = '''
 درک نیازها و تامین خواسته‌های مخاطبان بوکت با ارائه محصولات و کالاهای متنوع و به روز با هدف جلب رضایت مشتریان و نیز کسب سهم شایسته ای از فضای مجازی.
 ارتقای دانش، مهارت و توانایی تیم بوکت به عنوان منبع فکر و با ارزش‌ترین سرمایه شرکت با هدف دستیابی به اهداف والای بوکت.
 بالا بردن رقابت در بازار و ارائه خدمات بهتر به مشتریان از طریق بهینه‌سازی هزینه‌ها و استفاده از حداکثر ظرفیت‌های موجود در ارائه محصولات با کیفیت و مناسب
 جلب و توسعه مشارکت مخاطبان در ارائه روش‌های نوین آموزش و یادگیری الکترونیکی با هدف بهبود مستمر دانش در جامعه.
نقشه ی راه ماست''';

    var aboutUsText4 =
        '''در حال حاضر هدف ما جذب دانشجویانی هستند که ترجیح می دهند -یا نیاز دارند- که کتاب های خود را به صورت الکترونیکی مطالعه کنند. و همینطور جذب ناشرانی که به هر دلیل تمایل دارند کتاب های خود را به صورت الکترونیکی نیز منتشر کنند. برای لذت بردن بیشتر دانشجویان و حفاظ امنیت بیشتر ناشران، ما در هر بخش تلاش می کنیم تا تکنولوژی های پیشرفته تر را ارائه دهیم.
.امیدواریم با فراهم کردن اطلاعات مفید و کافی، بازارها و جوامع، بتوانند توانایی های بالقوه‌ خود را آشکار کرده و در راه رشد و پیشرفت کشور عزیزمان، ایران گام برداریم.''';
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            pinned: true,
            iconTheme: IconThemeData(
              color: Theme.of(context).primaryColor, //change your color here
            ),
            title: Text(
              DemoLocalizations.of(context).trans("AboutUs"),
              style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.normal,
                  fontSize: 18.0),
            ),
            centerTitle: true,
            expandedHeight: 230.0,
            backgroundColor: Theme.of(context).accentColor,
            flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                background: Column(
                  children: <Widget>[
                    Container(
                      height: 80.0,
                    ),
                    // Spacer(),
                    PlatformSvgWidget.asset('assets/images/about_us.svg',
                        height: 188.0),
                    // Spacer(),
                  ],
                )),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Container(
                  padding: EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 30.0),
                  child: Column(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.asset(
                          "assets/images/about1.png",
                          fit: BoxFit.fill,
                          // height: 120,
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.white,
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [fromHex("1c1cee"), fromHex('#1d1d69')]),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 2.0,
                              spreadRadius: 0.0,
                              offset: Offset(
                                0.0,
                                1.0,
                              ),
                            ),
                          ],
                        ),
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            aboutUsText1,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 12),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.asset(
                          "assets/images/about2.png",
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.white,
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [fromHex("ED6921"), fromHex('#EF9302')]),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 2.0,
                              spreadRadius: 0.0,
                              offset: Offset(
                                0.0,
                                1.0,
                              ),
                            ),
                          ],
                        ),
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            aboutUsText2,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 12),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.asset(
                          "assets/images/about4.png",
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.white,
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [fromHex("1c1cee"), fromHex('#1d1d69')]),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 2.0,
                              spreadRadius: 0.0,
                              offset: Offset(
                                0.0,
                                1.0,
                              ),
                            ),
                          ],
                        ),
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            aboutUsText3,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 12),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.asset(
                          "assets/images/about3.png",
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.white,
                          gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              fromHex("ED6921"),
                              fromHex('#1c1cee'),
                            ],
                          ),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 2.0,
                              spreadRadius: 0.0,
                              offset: Offset(
                                0.0,
                                1.0,
                              ),
                            ),
                          ],
                        ),
                        child: Container(
                          height: 1400,
                          child: Column(
                            // mainAxisSize: MainAxisSize.min,
                            children: [
                              Expanded(
                                child: AnimationLimiter(
                                  child: GridView.count(
                                    padding: EdgeInsets.all(10),
                                    crossAxisCount: 2,
                                    childAspectRatio: 0.7,
                                    shrinkWrap: true,
                                    crossAxisSpacing: 10,
                                    mainAxisSpacing: 10,
                                    children: List.generate(
                                      10,
                                      (int index) {
                                        return AnimationConfiguration
                                            .staggeredGrid(
                                          columnCount: 2,
                                          position: index,
                                          duration:
                                              const Duration(milliseconds: 375),
                                          child: ScaleAnimation(
                                            scale: 0.5,
                                            child: FadeInAnimation(
                                              child: _buildPersonCard(
                                                  personList[index]),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(15),
                                child: Text(
                                  "ما تاکنون تعداد زیادی از کتاب های درسی کشور را پوشش داده ایم و با شناسایی نیاز‌های آموزشی دانشجویان در تامین آنها، رو به جلو در حرکت هستیم تا آموزش و یادگیری از طریق کتاب‌های الکترونیکی بوکت را توسعه دهیم.",
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                  ),
                                ),
                              ),
                              SizedBox(height: 15),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: Colors.white,
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment.centerRight,
                              colors: [fromHex("ED6921"), fromHex('#EF9302')]),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 2.0,
                              spreadRadius: 0.0,
                              offset: Offset(
                                0.0,
                                1.0,
                              ),
                            ),
                          ],
                        ),
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: Text(
                            aboutUsText4,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 12),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.asset(
                          "assets/images/about5.png",
                        ),
                      ),
                      SizedBox(height: 20),
                    ],
                  ),
                );
              },
              childCount: 1,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPersonCard(person) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Theme.of(context).accentColor,
            fromHex('#EF9302'),
          ],
        ),
        borderRadius: BorderRadius.circular(10.0),
      ),
      height: 100,
      child: Column(
        children: [
          SizedBox(height: 10),
          AdvancedAvatar(
            statusSize: 0,
            size: 120.0,
            image: NetworkImage(person["img"]),
            decoration: BoxDecoration(shape: BoxShape.circle),
          ),
          SizedBox(height: 15),
          Text(
            person["name"],
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
          ),
          SizedBox(height: 5),
          Text(
            person["Roll"],
            style: TextStyle(
              color: Theme.of(context).primaryColorDark,
              fontSize: 13,
            ),
          ),
          SizedBox(height: 5),
          Text(
            person["EnRoll"],
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 13,
            ),
          )
        ],
      ),
    );
  }

  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
