import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:flutter/material.dart';

class Terms extends StatefulWidget {
  Terms({Key key}) : super(key: key);

  @override
  TermsState createState() => TermsState();
}

class TermsState extends State<Terms> {
  // AboutUs() {
  // }
  List<dynamic> descriptionList;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var termDescription1 =
        '''داشتن شماره همراه تایید شده الزامیست. همچنین لازم است نام و نام‌خانوادگی خود را اعلام بفرماید تا ما شما را بشناسیم.
رمز عبوری که در سایت ما تعیین می فرمایید قابل مشاهده و بازیابی توسط تیم فنی ما نیست و صرفا شما و پس از تایید از طریق شماره همراه از آن اطلاع خواهید داشت.
اپلیکیشن‌های ما برای دانلود رایگان‌اند و دارای دو بخش، یک بخش فروشگاهی برای خرید یا اجاره کتاب الکترونیکی (ایبوک | eBook) و یک بخش کتابخانه مجازی برای خواندن در بوکت است.
لطفا توجه داشته باشید که فرمت کتاب و محتوای الکترونیکی بر روی بوکت با PDF و ePub متفاوت است و یک فرمت (قالب) اختصاصی است که با انگیزه نیل به اهداف عالی ما (بهبود یادگیری الکترونیکی)، ایجاد شده است.
ما هیچ فایلی برای شما نمی‌فرستیم، بلکه می توانید از وب سایت ما یا اپلیکیشن‌های ما برای دسترسی به کتاب‌های الکترونیکی (ایبوک | eBook) خود استفاده کنید.
با توجه به قانون کپی رایت، مجاز نیستید متن و عکس ها را چاپ یا کپی کنید.
شما به کتاب‌های الکترونیکی (ایبوک | eBook) که از فروشگاه بوکت خریداری کنید دسترسی دائمی دارید. و اگر کرایه کنید دسترسی موقتی دارید و پس از پایان مدت ارائه از کتابخانه مجازی شما برداشته خواهد شد.
شما هم‌زمان می توانید با سه دستگاه به بوکت متصل باشید و از خدمات بوکت خود استفاده کنید. به محض اینکه از بیش از 3 دستگاه استفاده کنید، دستگاه اول از بوکت خارج خواهد شد.''';

    var termDescription2 =
        '''یک سری اطلاعات تکمیلی در قسمت اطلاعات کاربری به اختیار (مثل آدرس ایمیل) و در برخی موارد به ناچار (مثل آدرس پستی برای ارسال کتاب فیزیکی) از شما دریافت می‌شود. ما اطمینان می‌دهیم که این اطلاعات را در اختیار سایت‌ها و وب سرویس‌های دیگر قرار نمی‌دهیم. مگر اینکه با حکم قانونی مجبور باشیم در اختیار مراجع ذی‌صلاح قرار دهیم.
اگر شما اطلاعاتی نظیر مقطع تحصیلی، رشته و دانشگاه خود را به ما اعلام بفرمایید، یا مثلا اجازه دسترسی به لیست مخاطبان خود را بدهید، کسب این اطلاعات به ما امکان می‌دهد که با تحلیل آنها فرایندهای یادگیری الکترونیکی را برای افراد بهبود بخشیم و در غیر این صورت استفاده دیگری نخواهد شد.''';

    var termDescription3 =
        '''در صورت تایید و انتخاب شما یادداشت‌ها، هایلایت گذاری‌ها و دیگر انواع محتوایی که شما در صفحات کتاب الکترونیکی خود (ایبوک | Booket) ایجاد می‌کنید را با گروهی دیگر از اعضای بوکت به اشتراک می‌گذاریم.
هدف از اشتراک گذاری محتوای ایجاد شده توسط کاربر،کمک به سایر افراد برای ''';

    var termDescription4 =
        '''پس اگر شما به ما این اجازه را بدهید و تایید بفرمایید ما را در راه رسیدن به اهداف عالی بوکت که همانا بهبود آموزش و یادگیری الکترونیکی است، کمک کرده اید.
در مواردی بوکت ممکن است محتوای کاربران را در راستای رعایت قوانین و مقررات جاری کشور ویرایش کند. همچنین اگر محتوای ارسال شده توسط کاربر، مشمول مصادیق محتوای مجرمانه باشد، بوکت می‌تواند از اطلاعات ثبت شده برای پیگیری قانونی استفاده کند.''';

    var termDescription5 =
        '''امنیت محتوای ناشران به عنوان شرکای استراتژیک بوکت از اهمیت ویژه ای نزد ما برخوردارد است. رعایت مسائل مربوط به مالیکت محتوا و قوانین کپی‌رایت متناسب با قوانین جاری در کشور و با عقد قرارداد با ناشران میسر خواهد شد. با توجه به حساسیت هایی که ناشران جهت ارائه محتوا در فضای مجازی دارند ما یک فرمت اختصاصی از کتاب ایجاد کرده ایم که با فرمت های رایج کتاب الکترونیکی فرق دارد:
اولا فرمت اختصاصی بوکت (با پسوند BKT) متفاوت از فرمت های رایج PDF و ePub است و برای مطالعه فایل های BKT (مطالعه محتوا) لازم است فقط کتابخوان (Reader) بوکت به کار گرفته شود.
دوما فرمت بوکت (با پسوند BKT) بر پایه رایانش ابری (Cloud base) طراحی شده‌ است و برخلاف فرمت های PDF و ePub که به لحاظ فنی نیاز به تحویل فایل بر روی ابزار‌های الکترونیکی کاربران دارد، بوکت هیچ فایلی روی تلفن های هوشمند و سایر ابزارهای مطالعه‌ی کاربران ذخیره نمی‌کند.
این ایده و این نوع طراحی تاحد مطلوبی از نگرانی های ناشران مبنی بر درز فایل هایشان کاسته است و باعث شده است که ناشران بسیاری با اطمینان خاطر بیشتر به عرضه ی کتاب ها، مجلات، نشریات و... خود در بوکت بپردازند. همچنین فقط اطلاعات کابرانی در اختیار ناشر قرار می‌گیرد که از محتوای آن ناشر استفاده می‌کنند.''';
    return Scaffold(
        body: CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          pinned: true,
          iconTheme: IconThemeData(
            color: Theme.of(context).primaryColor, //change your color here
          ),
          title: Text(
            DemoLocalizations.of(context).trans("Terms"),
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.normal,
                fontSize: 18.0),
          ),
          centerTitle: true,
          expandedHeight: 230.0,
          backgroundColor: Theme.of(context).accentColor,
          flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              background: Column(
                children: <Widget>[
                  Container(
                    height: 80.0,
                  ),
                  // Spacer(),
                  PlatformSvgWidget.asset('assets/images/terms.svg',
                      height: 188.0),
                  // Spacer(),
                ],
              )),
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return Container(
                padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "ما در بوکت برای ارانه خدمات، اطلاعاتی را از شما دریافت می‌کنیم که جهت استفاده از این خدمات و محافظت از اطلاعات شما شرایط و خط مشی رازداری ما به شرح زیر است.",
                      textAlign: TextAlign.justify,
                      style:
                          TextStyle(fontSize: 14, color: Colors.grey.shade800),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "شرایط استفاده :‌",
                      style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                    SizedBox(height: 10),
                    RichText(
                      textAlign: TextAlign.start,
                      text: TextSpan(
                        style: TextStyle(fontFamily: 'IranSans'),
                        children: [
                          TextSpan(
                            text: "جهت ثبت نام در بوکت ",
                            style: TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                          TextSpan(
                            text: termDescription1,
                            style: TextStyle(
                                fontSize: 14, color: Colors.grey.shade800),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "خط مشی رازداری :‌",
                      style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                    SizedBox(height: 10),
                    RichText(
                      textAlign: TextAlign.start,
                      text: TextSpan(
                        style: TextStyle(fontFamily: 'IranSans'),
                        children: [
                          TextSpan(
                            text: "پس از ایجاد حساب کاربری و عضویت در بوکت ",
                            style: TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                          TextSpan(
                            text: termDescription2,
                            style: TextStyle(
                                fontSize: 14, color: Colors.grey.shade800),
                          ),
                          TextSpan(
                            text: " هنگام مطالعه کتاب و محتوا در بوکت ، ",
                            style: TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                          TextSpan(
                            text: termDescription3,
                            style: TextStyle(
                                fontSize: 14, color: Colors.grey.shade800),
                          ),
                          TextSpan(
                            text: "یادگیری بهتر، آسانتر و سریعتر است. ",
                            style: TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                          TextSpan(
                            text: termDescription4,
                            style: TextStyle(
                                fontSize: 14, color: Colors.grey.shade800),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10),
                    Text(
                      "شرایط استفاده :‌توضیح ویژه در مورد امنیت ناشران :",
                      style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                    SizedBox(height: 10),
                    RichText(
                      textAlign: TextAlign.start,
                      text: TextSpan(
                        style: TextStyle(fontFamily: 'IranSans'),
                        children: [
                          TextSpan(
                            text: termDescription5,
                            style: TextStyle(
                                fontSize: 14, color: Colors.grey.shade800),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
            childCount: 1,
          ),
        ),
      ],
    ));
  }
}
