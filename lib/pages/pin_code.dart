import 'dart:async';
import 'dart:io';

// import 'package:flare_flutter/flare_actor.dart';
import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/blocs/authentication_bloc.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:after_layout/after_layout.dart';
import 'package:booket_flutter/models/route_argument.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:quiver/async.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:sms_maintained/sms.dart';

class PinCodeVerificationScreen extends StatefulWidget {
  final RouteArgument routeArgument;

  PinCodeVerificationScreen(this.routeArgument);

  @override
  _PinCodeVerificationScreenState createState() =>
      _PinCodeVerificationScreenState();
}

class _PinCodeVerificationScreenState extends State<PinCodeVerificationScreen>
    with AfterLayoutMixin {
  var onTapRecognizer;

  TextEditingController textEditingController = TextEditingController()
    ..text = "";

  StreamController<ErrorAnimationType> errorController;
  FocusNode pinFocus;
  TextEditingController pinController;
  bool sendLoader = false;
  bool verifyLoader = false;

  // int _start;
  // int _current;

  String realPhoneNumber;
  String phoneNumber;
  String hostName;
  String fullName;
  String password;
  String trackingCode;

  AccountsRepo accountsRepo = new AccountsRepo();
  bool hasError = false;
  String currentText = "";
  bool isReset = false;
  // AuthenticationBloc authBloc;
  // int expires;
  var sub;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    this.fullName = this
        .widget
        .routeArgument
        .argumentsList
        .elementAt(0)["fullName"]
        .toString();
    this.trackingCode = this
        .widget
        .routeArgument
        .argumentsList
        .elementAt(0)["trackingCode"]
        .toString();
    this.phoneNumber = this
        .widget
        .routeArgument
        .argumentsList
        .elementAt(0)["phone"]
        .toString();
    this.password = this
        .widget
        .routeArgument
        .argumentsList
        .elementAt(0)["password"]
        .toString();

    this.pinFocus = new FocusNode();
    this.pinController = new TextEditingController();

//     onTapRecognizer = TapGestureRecognizer()
//       ..onTap = () {
//         setState(() {
//           sendLoader = true;
//         });
//         if (!isReset) {
//           authBloc.registerV2(this.hostName).then((value) {
//             print(value.toString());
//             setState(() {
//               sendLoader = false;
//               setCurrentTime(value["text"].split(":")[1]);
//             });
//           }).catchError((e) {});
//         } else {
//           _accountsRepo.resetPassword(this.hostName, {
//             "type": "phone",
//             "identification": this.phoneNumber
//           }).then((value) {
//             print(value.toString());
//             setState(() {
//               sendLoader = false;
//               setCurrentTime(value["expires"]);
//             });

//         }

//         _accountsRepo.sendPhoneVerification(this.hostName).then((value) {
//           print(value.toString());
//           setState(() {
//             sendLoader = false;
//           });
//         }).catchError((e) {
//           setState(() {
//             sendLoader = false;
//           });
//           print(e.toString());
//         });
// //        Navigator.pop(context);
//       };
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  @override
  void didUpdateWidget(PinCodeVerificationScreen oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    errorController.close();
    sub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () {
          Navigator.of(context).pop();
        },
      )),
      backgroundColor: Colors.blue.shade50,
      key: scaffoldKey,
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          LimitedBox(
            maxWidth: 500.0,
            child: Container(
              color: Colors.white,
              child: GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: ListView(
                    children: <Widget>[
                      SizedBox(height: 30),
                      Container(
                        height: MediaQuery.of(context).size.height / 3,
                        child: Lottie.asset("assets/images/verification.json"),
                      ),
                      SizedBox(height: 8),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Text(
                          DemoLocalizations.of(context).trans(this
                              .widget
                              .routeArgument
                              .argumentsList
                              .elementAt(0)["type"]
                              .toString()),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 22,
                              color: Colors.black),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30.0, vertical: 8),
                        child: RichText(
                          text: TextSpan(
                              text: "کد ارسالی به شماره ",
                              children: [
                                TextSpan(
                                    text: DemoLocalizations.of(context)
                                        .transNumber(
                                            phoneNumber.substring(8, 11)),
                                    style: TextStyle(
                                        textBaseline: TextBaseline.alphabetic,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'IranSans',
                                        fontSize: 15)),
                                TextSpan(
                                    text: " **** ",
                                    style: TextStyle(
                                        textBaseline: TextBaseline.alphabetic,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'IranSans',
                                        fontSize: 15)),
                                TextSpan(
                                    text: DemoLocalizations.of(context)
                                        .transNumber(
                                            phoneNumber.substring(0, 4)),
                                    style: TextStyle(
                                        textBaseline: TextBaseline.alphabetic,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'IranSans',
                                        fontSize: 15)),
                                TextSpan(
                                  text: " را وارد کنید",
                                )
                              ],
                              style: TextStyle(
                                color: Colors.black54,
                                fontSize: 14,
                                fontFamily: 'IranSans',
                              )),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8.0, horizontal: 30),
                            child: Directionality(
                              textDirection: TextDirection.ltr,
                              child: PinCodeTextField(
                                keyboardType: TextInputType.number,
                                length: 6,
                                autoFocus: true,
                                enabled: true,
                                obscureText: false,
                                autoDisposeControllers: false,
                                animationType: AnimationType.fade,
                                animationDuration: Duration(milliseconds: 300),
                                // backgroundColor: Colors.blue.withOpacity(0.001),
                                enableActiveFill: true,
                                errorAnimationController: errorController,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                controller: textEditingController,
                                pinTheme: PinTheme(
                                    shape: PinCodeFieldShape.box,
                                    activeColor: Theme.of(context).accentColor,
                                    selectedColor:
                                        Theme.of(context).accentColor,
                                    activeFillColor: Theme.of(context)
                                        .accentColor
                                        .withOpacity(0.3),
                                    inactiveColor: Theme.of(context)
                                        .accentColor
                                        .withOpacity(0.5),
                                    inactiveFillColor: Colors.white70,
                                    selectedFillColor: Theme.of(context)
                                        .accentColor
                                        .withOpacity(0.5),
                                    borderRadius: BorderRadius.circular(5),
                                    fieldHeight: 50,
                                    fieldWidth: 40,
                                    disabledColor: Colors.white),
                                onCompleted: (v) {
                                  // verifyPhone(context);
                                },
                                onChanged: (value) {
                                  print(value);
                                  setState(() {
                                    currentText = value;
                                  });
                                },
                                beforeTextPaste: (text) {
                                  print("Allowing to paste $text");
                                  //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                                  //but you can show anything you want here, like your pop up saying wrong paste format or etc
                                  return true;
                                },
                                appContext: context,
                              ),
                            )),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              child: Text(
                                hasError ? "لطفا کد را کامل وارد کنید" : "",
                                style: TextStyle(
                                    color: Colors.red.shade300, fontSize: 15),
                              ),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              width: 100,
                              child: FlatButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5)),
                                child: Text("پاک کن",
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 14)),
                                onPressed: () {
                                  textEditingController.clear();
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      // _current == null
                      //   ? Container()
                      //   : RichText(
                      //     textAlign: TextAlign.center,
                      //     text: TextSpan(
                      //       style: TextStyle(
                      //         color: Colors.black54, fontSize: 15),
                      //       children: [
                      //         _current == 0
                      //         ? TextSpan(text: "")
                      //         : TextSpan(
                      //           text: DemoLocalizations.of(context).transNumber("$_current"),
                      //           style: Theme.of(context).textTheme.headline6),
                      //         _current != 0
                      //         ? TextSpan(text: "")
                      //         : TextSpan(
                      //           text: "ارسال مجدد کد",
                      //           recognizer: onTapRecognizer,
                      //           style: TextStyle(
                      //             fontFamily: 'IranSans',
                      //             color: Colors.black,
                      //             fontWeight: FontWeight.bold,
                      //             fontSize: 16
                      //           )
                      //         )
                      //       ]
                      //     ),
                      //   ),
                      Container(
                          child: Center(
                        child: sendLoader
                            ? CircularProgressIndicator()
                            : Container(),
                      )),
                      SizedBox(
                        height: 14,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(
                            vertical: 16.0, horizontal: 30),
                        child: ButtonTheme(
                          height: 50,
                          child: FlatButton(
                            onPressed: () {
                              register(context);
                            },
                            child: Center(
                                child: Container(
                              height: 30,
                              child: verifyLoader
                                  ? CircularProgressIndicator(
                                      backgroundColor:
                                          Theme.of(context).primaryColor,
                                    )
                                  : Text(
                                      "تایید".toUpperCase(),
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                            )),
                          ),
                        ),
                        decoration: BoxDecoration(
                            color: Theme.of(context).accentColor,
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.blue.withOpacity(0.15),
                                  offset: Offset(1, -2),
                                  blurRadius: 8),
                              BoxShadow(
                                  color: Colors.blue.withOpacity(0.15),
                                  offset: Offset(-1, 2),
                                  blurRadius: 8)
                            ]),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> register(context) async {
    final iw = MainStateContainer.of(context);
    await accountsRepo.register(iw.hostName, {
      "firstname": this.fullName,
      "lastname": " ",
      "phone": "98" + int.parse(this.phoneNumber).toString(),
      "password": this.password,
      "code": this.textEditingController.text,
      "tracking_code": this.trackingCode,
      // "email": "",
      "inviter_code": "",
    }).then((value) {
      print(value.toString());
      Navigator.of(context).pushNamed("/Vitrine");
    });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // listenToSms();
    // if (!isReset) {
    //   authBloc.registerV2(this.hostName).then((value) {
    //     setState(() {
    //       sendLoader = false;
    //       setCurrentTime(value["text"].split(":")[1]);
    //     });
    //   }).catchError((e) {});
    // } else {
    //   _accountsRepo.resetPassword(this.hostName,
    //       {"type": "phone", "identification": this.phoneNumber}).then((value) {
    //     print(value.toString());
    //     setCurrentTime(value["expires"]);
    //   }).catchError((e) {
    //     e.toString();
    //   });
    // }
  }

//   void setCurrentTime(int oldTime) {
//     int newTime = int.parse(
//         ((oldTime - DateTime.now().millisecondsSinceEpoch) / 1000)
//             .toString()
//             .split(".")
//             .elementAt(0));
//     if (newTime <= 120) {
// //      setState(() {
//       _current = _start = newTime;
//       startTimer();
// //      });
//     } else {
//       _current = 0;
//     }
//   }

//   void startTimer() {
//     CountdownTimer countDownTimer = new CountdownTimer(
//       new Duration(seconds: _start),
//       new Duration(seconds: 1),
//     );

//     sub = countDownTimer.listen(null);
//     sub.onData((duration) {
//       setState(() {
//         _current = _start - duration.elapsed.inSeconds;
//       });
//     });

//     sub.onDone(() {
//       print("Done");
//       sub.cancel();
//     });
//   }

  // void verifyPhone(BuildContext context) {
  //   final iw = MainStateContainer.of(context);
  //   // conditions for validating
  //   if (currentText.length != 6) {
  //     errorController
  //         .add(ErrorAnimationType.shake); // Triggering error shake animation
  //     setState(() {
  //       hasError = true;
  //     });
  //   } else {
  //     if (!isReset) {
  //       setState(() {
  //         hasError = false;
  //         verifyLoader = true;
  //       });
  //       print("Real phone :" + realPhoneNumber.toString());
  //       _accountsRepo.verifyAndRegister(
  //         iw.hostName,
  //         {"code": currentText, "phone": realPhoneNumber},
  //       ).then((value) async {
  //         setState(() {
  //           verifyLoader = false;
  //         });
  //         if (value != null) {
  //           if (value.containsKey("message")) {
  //             scaffoldKey.currentState.showSnackBar(SnackBar(
  //               content: Text(value["message"]),
  //               duration: Duration(seconds: 3),
  //             ));
  //           } else {
  //             iw.authenticationBloc.updateToken(value["token"], 3);
  //             iw.setToken(value["token"]);
  //             await Future.delayed(Duration(milliseconds: 10));
  //             dynamic me = await iw.authenticationBloc.getProfile(iw.hostName);

  //             iw.updateUserInfo(me);
  //             iw.authenticationBloc.clearAll();
  //             await Future.delayed(Duration(milliseconds: 10));
  //             var nextDestination = "/Home";
  //             iw.updateUserInfo(me);

  //             if (iw.lastPathBeforeAuth != "") {
  //               nextDestination = iw.lastPathBeforeAuth;
  //             }

  //             Navigator.of(context)
  //                 .popUntil((route) => !Navigator.of(context).canPop());

  //             Navigator.of(context)
  //                 .popAndPushNamed(nextDestination, arguments: null);

  //             iw.setLastPath("");

  //             Navigator.of(context).popAndPushNamed(nextDestination,
  //                 arguments: new RouteArgument(
  //                     id: int.parse(
  //                         nextDestination.split("/Product/")[1].split("/")[0]),
  //                     argumentsList: null));
  //           }
  //         } else {
  //           scaffoldKey.currentState.showSnackBar(SnackBar(
  //             content: Text("Connection Timeout"),
  //             duration: Duration(seconds: 2),
  //           ));
  //         }
  //       });
  //     } else {
  //       Navigator.of(context).popAndPushNamed('/ResetPassword',
  //           arguments: new RouteArgument(argumentsList: [
  //             {"0": 1, "code": currentText, "timer": _current},
  //             {"0": iw.hostName},
  //             {"1": phoneNumber}
  //           ], id: "lll"));
  //     }
  //   }
  // }

  // void listenToSms() {
  //   if (!kIsWeb) {
  //     if (Platform.isAndroid) {
  //       // SmsReceiver receiver = new SmsReceiver();
  //       // receiver.onSmsReceived.listen((SmsMessage msg) {
  //       //   setState(() {
  //       //     textEditingController.text =
  //       //         msg.body.split("\n")[0].split(":")[1].trim();
  //       //     currentText = msg.body.split("\n")[0].split(":")[1].trim();
  //       //     verifyPhone(context);
  //       //   });
  //       // });
  //     } else {
  //       // print("not supported");
  //     }
  //   } else {
  //     // print("not supported");
  //   }
  // }
}
