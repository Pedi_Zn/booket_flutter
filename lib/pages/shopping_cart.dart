import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/blocs/grid_card_bloc.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:booket_flutter/widgets/books.dart';
import 'package:booket_flutter/widgets/expireDate.dart';
import 'package:booket_flutter/widgets/grid_or_list_switch.dart';
import 'package:booket_flutter/widgets/refresh_tab.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flash/flash.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:booket_flutter/models/route_argument.dart';
import 'package:dashed_container/dashed_container.dart';
import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

class ShoppingCart extends StatefulWidget {
  const ShoppingCart({Key key}) : super(key: key);

  @override
  ShoppingCartState createState() => new ShoppingCartState();
}

class ShoppingCartState extends State<ShoppingCart>
    with
        TickerProviderStateMixin,
        AfterLayoutMixin,
        AutomaticKeepAliveClientMixin {
  ScrollController parentCont, childCont;
  bool goTop = false;
  bool grid = true;
  double _gridPadding = 15;
  bool loader = false;
  bool couponLoader = false;
  bool couponResault = false;
  bool trashLoader = false;
  bool expandedSearch = false;
  double logoOpacity = 1;
  double _width = 200;
  FocusNode searchFocus;
  List<dynamic> basketList = [
    null,
  ];
  String token;
  // dynamic basket;

  TextEditingController couponController = TextEditingController();
  GridCardBloc gridCardBloc = new GridCardBloc(1);
  AccountsRepo accountsRepo = new AccountsRepo();

  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    this.searchFocus = new FocusNode();
    this.childCont = ScrollController(initialScrollOffset: 0.0);
    this.parentCont = ScrollController(initialScrollOffset: 0.0);
    this.childCont.addListener(_scrollListener);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final pageWidth = MediaQuery.of(context).size.width;

    int count = pageWidth < 350.0
        ? 1
        : pageWidth < 550.0
            ? 2
            : pageWidth < 900.0
                ? 3
                : pageWidth < 1000.0
                    ? 4
                    : pageWidth < 1300.0
                        ? 5
                        : 6;

    final bookHeight = (MediaQuery.of(context).size.width / count) * 8 / 5;
    final bookWidth = MediaQuery.of(context).size.width / count;

    final iw = MainStateContainer.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange[800],
        automaticallyImplyLeading: false,
        title: Container(
          width: MediaQuery.of(context).size.width,
          height: 60.0,
          child: Stack(
            children: [
              Positioned.fill(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 250),
                    // width: this._width2,
                    // width: 60.0,
                    child: Center(
                      child: AnimatedOpacity(
                          duration: const Duration(milliseconds: 100),
                          opacity: logoOpacity,
                          child: IconButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.white,
                            ),
                          )),
                    ),
                  ),
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 250),
                    // width: this._width2,
                    child: Center(
                        child: AnimatedOpacity(
                            duration: const Duration(milliseconds: 100),
                            opacity: logoOpacity,
                            child: PlatformSvgWidget.asset(
                                'assets/images/booket_logo.svg',
                                color: Colors.white,
                                height: 18))),
                  ),
                ],
              )),
              AnimatedPositioned(
                width: this._width,
                left:
                    ((MediaQuery.of(context).size.width - this._width - 30.0) /
                        2),
                top: 10.5,
                duration: const Duration(milliseconds: 250),
                child: Container(
                  height: 38.0,
                  child: new TextField(
                    focusNode: searchFocus,
                    onEditingComplete: () {},
                    onSubmitted: (String s) {
                      searchFocus.unfocus();
                      setState(() {
                        this.logoOpacity = 1;
                        this.expandedSearch = false;
                        this._width = 200.0;
                      });
                    },
                    autofocus: false,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 15, color: Theme.of(context).primaryColor),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.orange,
                      hintText: DemoLocalizations.of(context).trans("Search"),
                      hintStyle: TextStyle(
                        color: Theme.of(context).primaryColor,
                      ),
                      contentPadding: EdgeInsets.only(left: 15, right: 10),
                      // border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0),
                          borderSide: BorderSide(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.2))),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0),
                          borderSide:
                              BorderSide(color: Theme.of(context).accentColor)),
                      prefixIcon: Icon(
                        Icons.search,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                ),
              ),
              AnimatedPositioned(
                  width: this._width,
                  height: 55.0,
                  left: ((MediaQuery.of(context).size.width -
                          this._width -
                          30.0) /
                      2),
                  top: 10.5,
                  duration: const Duration(milliseconds: 250),
                  child: GestureDetector(
                    onTapDown: (s) {
                      setState(() {
                        this.logoOpacity = 0;
                        this.expandedSearch = true;
                        this._width =
                            (MediaQuery.of(context).size.width) < 800.0
                                ? (MediaQuery.of(context).size.width) * 9 / 11
                                : 500.0;
                      });
                      FocusScope.of(context).requestFocus(searchFocus);
                    },
                  )),
            ],
          ),
        ),
        centerTitle: true,
      ),
      body: LiquidPullToRefresh(
        key: _refreshIndicatorKey,
        onRefresh: _handleRefresh,
        showChildOpacityTransition: false,
        child: AutoRefresh(
            gridCardBloc: gridCardBloc,
            child: Scaffold(
              key: _scaffoldKey,
              floatingActionButton: (basketList.length > 0 &&
                      basketList[0] != null)
                  ? Container(
                      margin: EdgeInsets.only(right: 32.0),
                      // color: Colors.red,
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: RaisedButton(
                          onPressed: () {
                            pay(context);
                          },
                          child: Container(
                              width: double.infinity,
                              // width: 200.0,
                              height: 40.0,
                              child: Center(
                                child: !loader
                                    ? Text(
                                        DemoLocalizations.of(context)
                                            .trans("Continue"),
                                        style: TextStyle(
                                            color:
                                                Theme.of(context).primaryColor,
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.normal),
                                        textAlign: TextAlign.center,
                                      )
                                    : Container(
                                        width: 50.0,
                                        child: Image.asset(
                                            "assets/images/loading_book_white.gif")),
                              )),
                          color: Theme.of(context).accentColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(7.0),
                          ),
                        ),
                      ),
                    )
                  : Container(),
              // floatingActionButton: goTop ?
              //   FloatingActionButton(
              //     backgroundColor: Theme.of(context).accentColor,
              //     onPressed: () {
              //       parentCont.animateTo(0.0, duration: Duration(milliseconds: 200), curve: Curves.easeInCubic);
              //       childCont.animateTo(0.0, duration: Duration(milliseconds: 200), curve: Curves.easeInCubic);
              //       setState(() {
              //         goTop = true;
              //       });
              //     },
              //     child: Icon(Icons.arrow_upward, color: Colors.white, size: 25.0)
              //   ) : Container(),
              body: ListView(
                  physics:
                      AlwaysScrollableScrollPhysics(), // handle beshe ba triggere +300.0 scroll
                  controller: parentCont,
                  scrollDirection: Axis.vertical,
                  children: [
                    (basketList.length > 0 && basketList[0] != null)
                        ? Center(
                            child: Container(
                                padding: EdgeInsets.only(bottom: 40.0),
                                height: MediaQuery.of(context).size.height,
                                width: MediaQuery.of(context).size.width,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    LimitedBox(
                                      maxWidth:
                                          pageWidth > 700.0 ? 680.0 : pageWidth,
                                      child: AnimationLimiter(
                                        child: ListView.builder(
                                          padding: EdgeInsets.all(_gridPadding),
                                          controller: childCont,
                                          itemExtent: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                  (count + 1) +
                                              30,
                                          itemCount: basketList.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return Dismissible(
                                              key: UniqueKey(),
                                              background: Container(
                                                alignment: AlignmentDirectional
                                                    .centerEnd,
                                                color: Colors.red,
                                                child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      50.0, 0.0, 10.0, 0.0),
                                                  child: Icon(
                                                    Feather.trash_2,
                                                    color: Colors.white,
                                                    size: 33.0,
                                                  ),
                                                ),
                                              ),
                                              direction:
                                                  DismissDirection.endToStart,
                                              onDismissed: (direction) {
                                                setState(() {
                                                  removeFromBasket(
                                                      context, index);
                                                });
                                              },
                                              child: Container(
                                                padding: EdgeInsets.only(
                                                    bottom: 15.0),
                                                child: RepaintBoundary(
                                                    key: Key('str' +
                                                        index.toString()),
                                                    child:
                                                        AnimationConfiguration
                                                            .staggeredList(
                                                      position: index,
                                                      duration: const Duration(
                                                          milliseconds: 500),
                                                      delay: const Duration(
                                                          milliseconds: 100),
                                                      child: SlideAnimation(
                                                        verticalOffset: 50.0,
                                                        child: FadeInAnimation(
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceAround,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Container(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            10.0),
                                                                width: MediaQuery.of(context)
                                                                            .size
                                                                            .width /
                                                                        (count +
                                                                            1) -
                                                                    20,
                                                                child:
                                                                    GestureDetector(
                                                                  onTap: () {
                                                                    setState(
                                                                        () {
                                                                      Navigator.of(context).pushNamed(
                                                                          '/books/' +
                                                                              basketList[index]["book"][
                                                                                  "token"],
                                                                          arguments: RouteArgument(
                                                                              id: 12,
                                                                              argumentsList: [
                                                                                false,
                                                                                "salam"
                                                                              ]));
                                                                    });
                                                                  },
                                                                  child: Container(
                                                                      // color: Colors.blue,
                                                                      height: bookHeight,
                                                                      width: bookWidth,
                                                                      child: ClipRRect(
                                                                        borderRadius:
                                                                            BorderRadius.circular(16),
                                                                        child: FadeInImage
                                                                            .memoryNetwork(
                                                                          placeholder:
                                                                              kTransparentImage,
                                                                          imageErrorBuilder: (context, url, err) =>
                                                                              Container(child: Text("")),
                                                                          image:
                                                                              iw.hostName + basketList[index]["book"]["front_cover"]["medium"],
                                                                          fit: BoxFit
                                                                              .fill,
                                                                        ),
                                                                      )),
                                                                ),
                                                                // child: new Books(
                                                                //   basketList[index]["book"]["front_cover"]["medium"],
                                                                //   1.0,
                                                                //   null,
                                                                //   count
                                                                // ),
                                                              ),
                                                              // Container(width: 5.0), // spacer
                                                              Expanded(
                                                                  child:
                                                                      Container(
                                                                // color: Colors.blue,
                                                                padding: EdgeInsets
                                                                    .only(
                                                                        bottom:
                                                                            25.0),
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceBetween,
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    // Spacer(),
                                                                    Row(
                                                                      children: [
                                                                        Flexible(
                                                                            child:
                                                                                GestureDetector(
                                                                          onTap:
                                                                              () {
                                                                            setState(() {
                                                                              Navigator.of(context).pushNamed('/books/' + basketList[index]["book"]["token"],
                                                                                  arguments: RouteArgument(id: 12, argumentsList: [
                                                                                    false,
                                                                                    "salam"
                                                                                  ]));
                                                                            });
                                                                          },
                                                                          child:
                                                                              Text(
                                                                            basketList[index]["book"]["title"].toString(),
                                                                            maxLines:
                                                                                2,
                                                                            overflow:
                                                                                TextOverflow.ellipsis,
                                                                            textAlign:
                                                                                TextAlign.right,
                                                                            style:
                                                                                TextStyle(fontSize: 13.0, fontWeight: FontWeight.w600),
                                                                          ),
                                                                        )),
                                                                      ],
                                                                    ),
                                                                    Container(
                                                                      height:
                                                                          10.0,
                                                                    ),
                                                                    Text(
                                                                      DemoLocalizations.of(
                                                                              context)
                                                                          .trans(basketList[index]
                                                                              [
                                                                              "type"]),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .right,
                                                                      maxLines:
                                                                          1,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                      softWrap:
                                                                          false,
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              13.0),
                                                                    ),
                                                                    Row(
                                                                      children: [
                                                                        Row(
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.center,
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: [
                                                                              Text(
                                                                                DemoLocalizations.of(context).transPriceNumber(basketList[index]["price"].toString()),
                                                                                style: TextStyle(
                                                                                  color: Colors.black,
                                                                                  fontSize: 16.0,
                                                                                  fontStyle: FontStyle.normal,
                                                                                  fontWeight: FontWeight.w100,
                                                                                  decorationThickness: 1.0,
                                                                                ),
                                                                              ),
                                                                              SizedBox(width: 5),
                                                                              Text(
                                                                                DemoLocalizations.of(context).trans("unitBig"),
                                                                                style: TextStyle(
                                                                                  color: Colors.black,
                                                                                  fontSize: 16.0,
                                                                                  fontStyle: FontStyle.normal,
                                                                                  fontWeight: FontWeight.w100,
                                                                                  decorationThickness: 1.0,
                                                                                ),
                                                                              ),
                                                                            ]),
                                                                        Spacer(),
                                                                        GestureDetector(
                                                                            onTap:
                                                                                () {
                                                                              setState(() {
                                                                                removeFromBasket(context, index);
                                                                              });
                                                                            },
                                                                            child:
                                                                                Icon(
                                                                              Feather.trash_2,
                                                                              size: 25.0,
                                                                              color: Colors.red,
                                                                            )),
                                                                      ],
                                                                    ),
                                                                  ],
                                                                ),
                                                              )),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    )),
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                )))
                        : (basketList.length == 0)
                            ? Center(
                                child: Container(
                                margin: EdgeInsets.only(
                                    top: (MediaQuery.of(context).size.height /
                                            2) -
                                        230),
                                child: Column(
                                  children: [
                                    Image.asset(
                                      "assets/images/empty_library.png",
                                      width: 100.0,
                                    ),
                                    // PlatformSvgWidget.asset('assets/images/empty_library.svg'),
                                    Container(
                                      height: 15.0,
                                    ),
                                    FDottedLine(
                                      color: Colors.black,
                                      corner: FDottedLineCorner.all(10.0),

                                      /// add widget
                                      child: Container(
                                        padding: EdgeInsets.all(10.0),
                                        width:
                                            MediaQuery.of(context).size.width -
                                                80,
                                        // height: 110,
                                        alignment: Alignment.center,
                                        child: Text(
                                          "کتابخانه شما خالی است. کتاب‌های خریداری شده شما در این قسمت نمایش داده ‌می‌شوند\nبرای بارگذاری مجدد، صفحه را پایین بکشید",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(fontSize: 13.0),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ))
                            : Center(
                                child: Container(
                                    margin: EdgeInsets.only(
                                        top: (MediaQuery.of(context)
                                                    .size
                                                    .height -
                                                220) /
                                            2),
                                    height: 50.0,
                                    width: 50.0,
                                    child: CircularProgressIndicator()),
                              ),
                  ]),
            )),
      ),
      // bottomNavigationBar: Container(
      //   // padding: EdgeInsets.all(20.0),
      //   margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 20.0),
      //   height: 40.0,
      //   // color: Colors.blue,
      //   child: RaisedButton(
      //     onPressed: () {
      //       // Navigator.of(context).pushNamed('/Register');
      //     },
      //     child: Container(
      //       width: double.infinity,
      //       height: 40.0,
      //       child: Center(
      //         child: Text(
      //           DemoLocalizations.of(context).trans("Pay"),
      //           style: TextStyle(color: Theme.of(context).primaryColorDark, fontSize: 12.0, fontWeight: FontWeight.normal),
      //           textAlign: TextAlign.center,
      //         ),
      //       )
      //     ),
      //     color: Theme.of(context).accentColor,
      //     shape: RoundedRectangleBorder(
      //       borderRadius: BorderRadius.circular(7.0),
      //     ),
      //   ),
      // ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void afterFirstLayout(BuildContext context) async {
    final iw = MainStateContainer.of(context);
    await accountsRepo
        .getBasket(
      iw.hostName,
      iw.token,
    )
        .then((basketReq) {
      iw.setCart(basketReq);
      basketList.removeAt(0);
      for (int i = 0; i < basketReq["orders"].length; i++) {
        basketList.add(basketReq["orders"][i]);
      }
      setState(() {
        // basket = basketReq;
        basketList = basketList;
      });
    });
  }

  Future<void> removeFromBasket(context, index) async {
    setState(() {
      loader = true;
    });
    final iw = MainStateContainer.of(context);
    accountsRepo.removeFromBasket(
        iw.hostName, basketList[index]["token"], {}).then((basketReq) {
      iw.setCart(basketReq);
      setState(() {
        basketList.removeAt(index);
        loader = false;
      });
    });
  }

  Future<void> checkCoupon(context, setState) async {
    final iw = MainStateContainer.of(context);
    print(iw.token + couponController.text.toString());
    var string = couponController.text.toString();
    if (couponController.text.toString() == "") {
      string = "kose_batajrobeh";
    }
    if (!couponLoader) {
      setState(() {
        couponLoader = true;
      });

      accountsRepo.checkCoupon(
          iw.hostName, iw.token, {"coupon_code": string}).then((couponRes) {
        dynamic cart_temp = iw.cart;
        if (couponRes["statusCode"] == 422) {
          cart_temp["total_off"] = 0;
          iw.setCart(cart_temp);
          setState(() {
            couponResault = false;
          });
        } else {
          cart_temp["total_off"] = couponRes["total_off"];
          iw.setCart(cart_temp);
          setState(() {
            couponResault = true;
          });
        }
        setState(() {
          couponLoader = false;
        });
      });
    } else {
      print("loading");
    }
  }

  Future<void> pay(context) async {
    // setState(() {
    //   loader = true;
    // });

    final iw = MainStateContainer.of(context);
    showModalBottomSheet(
        context: context,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0), topRight: Radius.circular(15.0)),
        ),
        builder: (context) {
          return StatefulBuilder(builder: (context, setState) {
            return Container(
              padding: EdgeInsets.all(25.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisSize: MainAxisSize.cente,
                    children: [
                      Text(
                        DemoLocalizations.of(context).trans("City") + " : ",
                        // textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w100,
                          decorationThickness: 1.0,
                        ),
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            (iw.me["user"]["city"] != null) &&
                                    (iw.me["user"]["city"]["title"]
                                            .toString() !=
                                        "")
                                ? Text(
                                    iw.me["user"]["city"]["title"].toString(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15.0,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w100,
                                      decorationThickness: 1.0,
                                    ),
                                  )
                                : ElevatedButton(
                                    onPressed: () {
                                      Navigator.of(context)
                                          .popAndPushNamed("/EditProfile");
                                    },
                                    child: Container(
                                        width: 90.0,
                                        height: 30.0,
                                        child: Center(
                                          child: Text(
                                            DemoLocalizations.of(context)
                                                .trans("EditProfile"),
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                fontSize: 12.0,
                                                fontWeight: FontWeight.normal),
                                            textAlign: TextAlign.center,
                                          ),
                                        )),
                                    style: ElevatedButton.styleFrom(
                                      elevation: 6.0,
                                      primary:
                                          Theme.of(context).primaryColorDark,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(7.0),
                                      ),
                                    ),
                                  ),
                          ]),
                    ],
                  ),
                  Container(
                    height: 5.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisSize: MainAxisSize.cente,
                    children: [
                      Text(
                        DemoLocalizations.of(context).trans("Address") + " : ",
                        // textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w100,
                          decorationThickness: 1.0,
                        ),
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            (iw.me["address"] != null) &&
                                    (iw.me["address"].toString() != "")
                                ? Text(
                                    iw.me["address"].toString(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15.0,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w100,
                                      decorationThickness: 1.0,
                                    ),
                                  )
                                : ElevatedButton(
                                    onPressed: () {
                                      Navigator.of(context)
                                          .popAndPushNamed("/EditProfile");
                                    },
                                    child: Container(
                                        width: 90.0,
                                        height: 30.0,
                                        child: Center(
                                          child: Text(
                                            DemoLocalizations.of(context)
                                                .trans("EditProfile"),
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                fontSize: 12.0,
                                                fontWeight: FontWeight.normal),
                                            textAlign: TextAlign.center,
                                          ),
                                        )),
                                    style: ElevatedButton.styleFrom(
                                      elevation: 6.0,
                                      primary:
                                          Theme.of(context).primaryColorDark,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(7.0),
                                      ),
                                    ),
                                  ),
                          ]),
                    ],
                  ),
                  // Container(height: 5.0,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisSize: MainAxisSize.cente,
                    children: [
                      Text(
                        DemoLocalizations.of(context).trans("PostCode") + " : ",
                        // textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w100,
                          decorationThickness: 1.0,
                        ),
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            (iw.me["user"]["postal_code"] != null) &&
                                    (iw.me["user"]["postal_code"].toString() !=
                                        "")
                                ? Text(
                                    iw.me["user"]["postal_code"].toString(),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15.0,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w100,
                                      decorationThickness: 1.0,
                                    ),
                                  )
                                : ElevatedButton(
                                    onPressed: () {
                                      Navigator.of(context)
                                          .popAndPushNamed("/EditProfile");
                                    },
                                    child: Container(
                                        width: 90.0,
                                        height: 30.0,
                                        child: Center(
                                          child: Text(
                                            DemoLocalizations.of(context)
                                                .trans("EditProfile"),
                                            style: TextStyle(
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                fontSize: 12.0,
                                                fontWeight: FontWeight.normal),
                                            textAlign: TextAlign.center,
                                          ),
                                        )),
                                    style: ElevatedButton.styleFrom(
                                      elevation: 6.0,
                                      primary:
                                          Theme.of(context).primaryColorDark,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(7.0),
                                      ),
                                    ),
                                  ),
                          ]),
                    ],
                  ),
                  Container(
                    height: 5.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisSize: MainAxisSize.cente,
                    children: [
                      Text(
                        DemoLocalizations.of(context).trans("Total") + " : ",
                        // textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w100,
                          decorationThickness: 1.0,
                        ),
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            RichText(
                              text: new TextSpan(
                                text: '',
                                children: <TextSpan>[
                                  ((iw.cart["total_off"] != 0) &&
                                          (iw.cart["total_off"] != null))
                                      ? TextSpan(
                                          text: DemoLocalizations.of(context)
                                              .transPriceNumber(
                                                  (iw.cart["total"] -
                                                          iw.cart["total_off"])
                                                      .toString()),
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1
                                              .merge(new TextStyle(
                                                color: Colors.black,
                                                fontSize: 15.0,
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w100,
                                                decorationThickness: 1.0,
                                                // decoration: TextDecoration.lineThrough
                                              )),
                                        )
                                      : TextSpan(text: ""),
                                  TextSpan(text: "  "),
                                  TextSpan(
                                    text: DemoLocalizations.of(context)
                                        .transPriceNumber(
                                            (iw.cart["total"]).toString()),
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1
                                        .merge(new TextStyle(
                                            color: Colors.black,
                                            fontSize: 15.0,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w100,
                                            decorationThickness: 4.0,
                                            decoration:
                                                iw.cart["total_off"] != 0
                                                    ? TextDecoration.lineThrough
                                                    : TextDecoration.none,
                                            decorationColor:
                                                Theme.of(context).accentColor)),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(width: 5),
                            Text(
                              DemoLocalizations.of(context).trans("unitBig"),
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 15.0,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w100,
                                decorationThickness: 1.0,
                              ),
                            ),
                          ]),
                    ],
                  ),
                  Container(
                    height: 13.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisSize: MainAxisSize.cente,
                    children: [
                      Text(
                        DemoLocalizations.of(context).trans("TransportCost") +
                            " : ",
                        // textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w100,
                          decorationThickness: 1.0,
                        ),
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              DemoLocalizations.of(context).transPriceNumber(
                                  iw.cart["transport_price"].toString()),
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 15.0,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w100,
                                decorationThickness: 1.0,
                              ),
                            ),
                            SizedBox(width: 5),
                            Text(
                              DemoLocalizations.of(context).trans("unitBig"),
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 15.0,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w100,
                                decorationThickness: 1.0,
                              ),
                            ),
                          ]),
                    ],
                  ),
                  Container(
                    height: 10.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisSize: MainAxisSize.cente,
                    children: [
                      Text(
                        DemoLocalizations.of(context).trans("DiscountCoupon") +
                            " : ",
                        // textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w100,
                          decorationThickness: 1.0,
                        ),
                      ),
                      Container(
                        transform: Matrix4.translationValues(
                          -8.0,
                          1.5,
                          0.0,
                        ),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(top: 3.0),
                                child: DashedContainer(
                                  child: Container(
                                    height: 30.0,
                                    width: 125.0,
                                    padding: EdgeInsets.all(3.0),
                                    decoration: BoxDecoration(
                                        color: Colors.transparent,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0))),
                                    child: TextField(
                                      controller: couponController,
                                      onEditingComplete: () {},
                                      onSubmitted: (String s) {},
                                      autofocus: false,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Theme.of(context)
                                              .primaryColorDark),
                                      decoration: InputDecoration(
                                          enabledBorder:
                                              const OutlineInputBorder(
                                            borderSide: const BorderSide(
                                                color: Colors.white,
                                                width: 0.0),
                                          ),
                                          filled: true,
                                          fillColor: Colors.white,
                                          focusedBorder:
                                              const OutlineInputBorder(
                                                  borderSide: const BorderSide(
                                                      color: Colors.white,
                                                      width: 0.0)),
                                          // disabledBorder: InputBorder.none,
                                          hintText:
                                              DemoLocalizations.of(context)
                                                  .trans("Code"),
                                          hintStyle: TextStyle(
                                            color: Theme.of(context)
                                                .primaryColorDark,
                                          ),
                                          contentPadding: EdgeInsets.only(
                                              left: 15, right: 10),
                                          border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0)),
                                          focusColor: Colors.white,
                                          hoverColor: Colors.white),
                                    ),
                                  ),
                                  dashColor: !couponResault
                                      ? Theme.of(context).primaryColorDark
                                      : Theme.of(context).accentColor,
                                  borderRadius: 10.0,
                                  dashedLength: 5.0,
                                  blankLength: 2.0,
                                  strokeWidth: 2.0,
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  checkCoupon(context, setState);
                                },
                                child: Container(
                                  height: 32.0,
                                  width: 40.0,
                                  color: Colors.transparent,
                                  child: Container(
                                      transform: Matrix4.translationValues(
                                        8.0,
                                        1.5,
                                        0.0,
                                      ),
                                      decoration: BoxDecoration(
                                          color: !couponResault
                                              ? Theme.of(context)
                                                  .primaryColorDark
                                              : Theme.of(context).accentColor,
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(10.0),
                                              bottomLeft:
                                                  Radius.circular(10.0))),
                                      child: !couponLoader
                                          ? Icon(Feather.check,
                                              color: Theme.of(context)
                                                  .primaryColor)
                                          : Container(
                                              margin: EdgeInsets.fromLTRB(
                                                  12, 8, 12, 8),
                                              height: 20.0,
                                              width: 10.0,
                                              child: CircularProgressIndicator(
                                                strokeWidth: 2.0,
                                                valueColor:
                                                    new AlwaysStoppedAnimation<
                                                            Color>(
                                                        Theme.of(context)
                                                            .primaryColor),
                                              ))),
                                ),
                              ),
                            ]),
                      ),
                    ],
                  ),
                  Container(
                    height: 20.0,
                  ),
                  RaisedButton(
                    onPressed: () {
                      _showBasicsFlash();
                    },
                    child: Container(
                        width: double.infinity,
                        height: 40.0,
                        child: Center(
                          child: Text(
                            DemoLocalizations.of(context).trans("Pay"),
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 14.0,
                                fontWeight: FontWeight.normal),
                            textAlign: TextAlign.center,
                          ),
                        )),
                    color: Theme.of(context).accentColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7.0),
                    ),
                  ),
                ],
              ),
            );
          });
        });
  }

  String validateAddress() {
    final iw = MainStateContainer.of(context);
    if ((iw.me["user"]["city"] != null) &&
        (iw.me["user"]["city"]["title"].toString() != "")) {
      if ((iw.me["address"] != null) && (iw.me["address"].toString() != "")) {
        if ((iw.me["postal_code"] != null) &&
            (iw.me["user"]["postal_code"].toString() != "")) {
          return "true";
        } else {
          return "کدپستی را وارد کنید";
        }
      } else if ((iw.me["postal_code"] != null) &&
          (iw.me["user"]["postal_code"].toString() != "")) {
        return "ادرس را وارد کنید";
      } else {
        return "ادرس و کدپستی را وارد کنید";
      }
    } else {
      if ((iw.me["address"] != null) && (iw.me["address"].toString() != "")) {
        if ((iw.me["postal_code"] != null) &&
            (iw.me["user"]["postal_code"].toString() != "")) {
          return "شهر را وارد کنید";
        } else {
          return "شهر و کدپستی را وارد کنید";
        }
      } else if ((iw.me["postal_code"] != null) &&
          (iw.me["user"]["postal_code"].toString() != "")) {
        return "شهر و ادرس را وارد کنید";
      } else {
        return "شهر، ادرس و کدپستی را وارد کنید";
      }
    }
  }

  void _showBasicsFlash({
    Duration duration,
    flashStyle = FlashBehavior.floating,
  }) {
    showFlash(
      context: context,
      duration: Duration(milliseconds: 2000),
      builder: (context, controller) {
        return Flash(
          controller: controller,
          barrierDismissible: false,
          behavior: flashStyle,
          position: FlashPosition.bottom,
          margin: EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 29.0),
          borderRadius: BorderRadius.circular(7.0),
          boxShadows: kElevationToShadow[4],
          backgroundColor: Theme.of(context).accentColor,
          horizontalDismissDirection: HorizontalDismissDirection.horizontal,
          child: Container(
            height: 40.0,
            // padding: EdgeInsets.only(bottom: 20.0),
            // margin: EdgeInsets.only(bottom: 5.0),
            child: Center(
              child: Text(
                validateAddress(),
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 14.0),
              ),
            ),
          ),
        );
      },
    );
  }

  void _showMessage(String message) {
    if (!mounted) return;
    showFlash(
        context: context,
        duration: Duration(seconds: 3),
        builder: (_, controller) {
          return Flash(
            controller: controller,
            position: FlashPosition.top,
            behavior: FlashBehavior.fixed,
            child: FlashBar(
              icon: Icon(
                Icons.face,
                size: 36.0,
                color: Colors.black,
              ),
              content: Text(message),
            ),
          );
        });
  }

  Future<void> _handleRefresh() {
    final Completer<void> completer = Completer<void>();
    Timer(const Duration(seconds: 1), () {
      completer.complete();
      setState(() {
        Navigator.of(context).pushNamed("/ShoppingCart");
      });
    });
  }

  void _scrollListener() {
    ScrollPosition sp;
    sp = this.childCont.positions.elementAt(0);
    if (sp.pixels <= (100) && !sp.outOfRange) {
      if (goTop) {
        if (mounted) {
          setState(() {
            goTop = false;
          });
          parentCont.animateTo(0.0,
              duration: Duration(milliseconds: 200),
              curve: Curves.easeInOutCubic);
          // }
        }
      }
    } else if (sp.pixels > (100) && !sp.outOfRange) {
      if (!goTop) {
        if (mounted) {
          setState(() {
            goTop = true;
          });
          parentCont.animateTo(225.0,
              duration: Duration(milliseconds: 200),
              curve: Curves.easeInOutCubic);
        }
      }
    }
  }
}
