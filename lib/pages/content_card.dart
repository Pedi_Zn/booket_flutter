import 'package:booket_flutter/pages/address_content.dart';
import 'package:booket_flutter/pages/profile_content.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

import 'graduate_content.dart';

class ContentCard extends StatefulWidget {
  @override
  _ContentCardState createState() => _ContentCardState();
}

class _ContentCardState extends State<ContentCard> {
  bool showInput = true;
  bool showInputTabOptions = true;

  @override
  Widget build(BuildContext context) {
    return new Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
      ),
      elevation: 4.0,
      color: Colors.white,
      margin: const EdgeInsets.all(20.0),
      child: DefaultTabController(
        child: new LayoutBuilder(
          builder: (BuildContext context, BoxConstraints viewportConstraints) {
            return Column(
              children: <Widget>[
                _buildTabBar(),
                _buildContentContainer(viewportConstraints),
              ],
            );
          },
        ),
        length: 3,
      ),
    );
  }

  Widget _buildTabBar({bool showFirstOption}) {
    return Stack(
      children: <Widget>[
        new Positioned.fill(
          top: null,
          child: new Container(
            height: 2.0,
            color: new Color(0xFFEEEEEE),
          ),
        ),
        new TabBar(
          tabs: [
            Tab(
              icon: Icon(SimpleLineIcons.user, size: 25),
            ),
            Tab(
              icon: Icon(Entypo.location, size: 25),
            ),
            Tab(
              icon: Icon(FontAwesome.graduation_cap, size: 25),
            ),
            // Tab(text: showInputTabOptions ? "Bus" : "Stops"),
          ],
          labelColor: Colors.black,
          unselectedLabelColor: Colors.grey,
        ),
      ],
    );
  }

  Widget _buildContentContainer(BoxConstraints viewportConstraints) {
    return Expanded(
      child: TabBarView(
        // controller: tabController,
        children: [
          SingleChildScrollView(
            child: new ConstrainedBox(
              constraints: new BoxConstraints(
                minHeight: viewportConstraints.maxHeight - 48.0,
              ),
              child: new IntrinsicHeight(
                child: ProfileContent(),
              ),
            ),
          ),
          SingleChildScrollView(
            child: new ConstrainedBox(
              constraints: new BoxConstraints(
                minHeight: viewportConstraints.maxHeight - 48.0,
              ),
              child: new IntrinsicHeight(
                child: AddressContent(),
              ),
            ),
          ),
          SingleChildScrollView(
            child: new ConstrainedBox(
              constraints: new BoxConstraints(
                minHeight: viewportConstraints.maxHeight - 48.0,
              ),
              child: new IntrinsicHeight(
                child: GraduateContent(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildProfileTab() {
    return Column(
      children: <Widget>[
        ProfileContent(),
        // AddressContent(),
        // GraduateContent()
        // Expanded(child: Container()),
        // Padding(
        //   padding: const EdgeInsets.only(bottom: 16.0, top: 8.0),
        //   child: FloatingActionButton(
        //     onPressed: () => setState(() => showInput = false),
        //     child: Icon(Icons.timeline, size: 36.0),
        //   ),
        // ),
      ],
    );
  }

  Widget _buildAddressTab() {
    return Column(
      children: <Widget>[
        // ProfileContent(),
        AddressContent(),
        // GraduateContent()
        // Expanded(child: Container()),
        // Padding(
        //   padding: const EdgeInsets.only(bottom: 16.0, top: 8.0),
        //   child: FloatingActionButton(
        //     onPressed: () => setState(() => showInput = false),
        //     child: Icon(Icons.timeline, size: 36.0),
        //   ),
        // ),
      ],
    );
  }

  Widget _buildGraduateTab() {
    return Column(
      children: <Widget>[
        // ProfileContent(),
        // AddressContent(),
        GraduateContent()
        // Expanded(child: Container()),
        // Padding(
        //   padding: const EdgeInsets.only(bottom: 16.0, top: 8.0),
        //   child: FloatingActionButton(
        //     onPressed: () => setState(() => showInput = false),
        //     child: Icon(Icons.timeline, size: 36.0),
        //   ),
        // ),
      ],
    );
  }
}
