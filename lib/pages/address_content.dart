import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/widgets/drop_down_text_field.dart';
import 'package:booket_flutter/widgets/text_field_widget.dart';
import 'package:flutter/material.dart';

class AddressContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    return Form(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Row(
              children: [
                Container(
                  width: (MediaQuery.of(context).size.width - 80) * 2 / 5,
                  child: TextFieldWidget(
                    label: DemoLocalizations.of(context).trans("PostCode"),
                    maxLength: 10,
                    text: iw.me["postal_code"],
                    onChanged: (s) {
                      iw.authenticationBloc.updatePostCode(s);
                    },
                  ),
                ),
                const SizedBox(width: 10),
                Container(
                  width: (MediaQuery.of(context).size.width - 85) * 3 / 5,
                  child: DropDownTextFieldWidget(
                    label: DemoLocalizations.of(context).trans("City"),
                    contentList: iw.citiesList,
                    text: iw.me["user"]["city"] != null
                        ? iw.me["user"]["city"]["title"]
                        : "",
                    onChanged: (s) {
                      iw.authenticationBloc.updateCity(s);
                    },
                  ),
                ),
              ],
            ),
            const SizedBox(height: 5),
            // Container(
            //   height: 40.0,
            //   width: 200.0,
            //   child: TextField(
            //     style: TextStyle(fontSize: 14.0),
            //     keyboardType: TextInputType.multiline,
            //     textAlignVertical: TextAlignVertical.top,
            //     decoration: InputDecoration(
            //       // contentPadding: EdgeInsets.zero,
            //       isDense: true,
            //       contentPadding: EdgeInsets.all(7),
            //       border: OutlineInputBorder(
            //         borderRadius: BorderRadius.circular(7),
            //       ),
            //     ),
            //     maxLines: null,
            //   ),
            // ),
            TextFieldWidget(
              label: DemoLocalizations.of(context).trans("Address"),
              text: iw.me["address"],
              maxLines: 3,
              onChanged: (s) {
                iw.authenticationBloc.updateAddress(s);
              },
            ),
          ],
        ),
      ),
    );
  }
}
