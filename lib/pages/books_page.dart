import 'dart:convert';

import 'package:after_layout/after_layout.dart';
import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/widgets/books2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:expand_widget/expand_widget.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:booket_flutter/models/route_argument.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:fontisto_flutter/fontisto_flutter.dart';
import 'package:transparent_image/transparent_image.dart';

class BooksPage extends StatefulWidget {
  // final bool hasBook;
  final String token;

  final RouteArgument args;
  const BooksPage({Key key, this.args, this.token}) : super(key: key);

  @override
  BooksPageState createState() => new BooksPageState(this.args, this.token);
}

class BooksPageState extends State<BooksPage>
    with
        SingleTickerProviderStateMixin,
        AfterLayoutMixin,
        AutomaticKeepAliveClientMixin {
  RouteArgument args;
  String token;
  String rentType = "";
  bool hasBook;
  bool expandedSearch = false;
  bool loader = false;
  bool isSimilarBooksEmpty = false;
  bool isAuthorBooksEmpty = false;
  double logoOpacity = 1;
  double _width = 200;
  FocusNode searchFocus;
  dynamic book;
  ScrollController parentCont;
  bool marked = false;

  List<dynamic> similarBooksList = [];
  List<dynamic> authorBooksList = [];

  AccountsRepo accountsRepo = new AccountsRepo();

  BooksPageState(this.args, this.token) {
    this.hasBook = args.argumentsList[0];
    // print(args.argumentsList[1]);
    // this.token = this.widget.token;
  }

  @override
  void initState() {
    this.parentCont = ScrollController(initialScrollOffset: 0.0);
    this.searchFocus = new FocusNode();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    final height = (MediaQuery.of(context).size.width / 2.5) * 8 / 5;
    final width = MediaQuery.of(context).size.width / 2.5;

    final pageWidth = MediaQuery.of(context).size.width;

    int count2 = pageWidth < 350.0
        ? 2
        : pageWidth < 550.0
            ? 3
            : pageWidth < 750.0
                ? 4
                : pageWidth < 950.0
                    ? 5
                    : pageWidth < 1150.0
                        ? 6
                        : pageWidth < 1300.0
                            ? 7
                            : 8;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange[800],
        automaticallyImplyLeading: false,
        title: Container(
          width: MediaQuery.of(context).size.width,
          height: 60.0,
          child: Stack(
            children: [
              Positioned.fill(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 250),
                    // width: this._width2,
                    // width: 60.0,
                    child: Center(
                      child: AnimatedOpacity(
                          duration: const Duration(milliseconds: 100),
                          opacity: logoOpacity,
                          child: IconButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.white,
                            ),
                          )),
                    ),
                  ),
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 250),
                    // width: this._width2,
                    child: Center(
                        child: AnimatedOpacity(
                            duration: const Duration(milliseconds: 100),
                            opacity: logoOpacity,
                            child: PlatformSvgWidget.asset(
                                'assets/images/booket_logo.svg',
                                color: Colors.white,
                                height: 18))),
                  ),
                ],
              )),
              AnimatedPositioned(
                width: this._width,
                left:
                    ((MediaQuery.of(context).size.width - this._width - 30.0) /
                        2),
                top: 10.5,
                duration: const Duration(milliseconds: 250),
                child: Container(
                  height: 38.0,
                  child: new TextField(
                    focusNode: searchFocus,
                    onEditingComplete: () {},
                    onSubmitted: (String s) {
                      searchFocus.unfocus();
                      setState(() {
                        this.logoOpacity = 1;
                        this.expandedSearch = false;
                        this._width = 200.0;
                      });
                    },
                    autofocus: false,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 15, color: Theme.of(context).primaryColor),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.orange,
                      hintText: DemoLocalizations.of(context).trans("Search"),
                      hintStyle: TextStyle(
                        color: Theme.of(context).primaryColor,
                      ),
                      contentPadding: EdgeInsets.only(left: 15, right: 10),
                      // border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0),
                          borderSide: BorderSide(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.2))),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0),
                          borderSide:
                              BorderSide(color: Theme.of(context).accentColor)),
                      prefixIcon: Icon(
                        Icons.search,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                ),
              ),
              AnimatedPositioned(
                  width: this._width,
                  height: 55.0,
                  left: ((MediaQuery.of(context).size.width -
                          this._width -
                          30.0) /
                      2),
                  top: 10.5,
                  duration: const Duration(milliseconds: 250),
                  child: GestureDetector(
                    onTapDown: (s) {
                      setState(() {
                        this.logoOpacity = 0;
                        this.expandedSearch = true;
                        this._width =
                            (MediaQuery.of(context).size.width) < 800.0
                                ? (MediaQuery.of(context).size.width) * 9 / 11
                                : 500.0;
                      });
                      FocusScope.of(context).requestFocus(searchFocus);
                    },
                  )),
            ],
          ),
        ),
        centerTitle: true,
      ),
      body: book != null
          ? SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(15.0),
                      child: Column(
                        children: [
                          Container(
                            // color: Colors.blue,
                            height: height,
                            padding: EdgeInsets.only(bottom: 10.0, top: 10.0),
                            child: Row(
                              children: [
                                Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Container(
                                      height: height,
                                      width: width,
                                      child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(16),
                                          child: Container(
                                              color: Theme.of(context)
                                                  .dividerColor)),
                                    ),
                                    CircularProgressIndicator(),
                                    book != null
                                        ? Container(
                                            height: height,
                                            width: width,
                                            child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(16),
                                                child:
                                                    FadeInImage.memoryNetwork(
                                                  placeholder:
                                                      kTransparentImage,
                                                  imageErrorBuilder: (context,
                                                          url, err) =>
                                                      Container(
                                                          child: Text("Err")),
                                                  image: iw.hostName +
                                                      book["front_cover"]
                                                          ["medium"],
                                                  fit: BoxFit.fill,
                                                )),
                                          )
                                        : Container(),
                                  ],
                                ),
                                Flexible(
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.only(
                                            top: 15.0, right: 15.0),
                                        child: Column(
                                            // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                book["title"],
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 14.0,
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.w800,
                                                  // decorationThickness: 1.0,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 10.0,
                                              ),
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: List.generate(
                                                    book["authors"].length,
                                                    (i) {
                                                  return Container(
                                                      child: i == 0
                                                          ? Text(
                                                              book["authors"][i]
                                                                      ["title"]
                                                                  .toString(),
                                                              maxLines: 1,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                              softWrap: false,
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      12.0),
                                                            )
                                                          : Text(
                                                              i < 2
                                                                  ? ", " +
                                                                      book["authors"][i]
                                                                              [
                                                                              "title"]
                                                                          .toString()
                                                                  : i == 2
                                                                      ? ", ..."
                                                                      : "",
                                                              maxLines: 1,
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                              softWrap: false,
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      12.0),
                                                            ));
                                                }),
                                              ),
                                            ]),
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(bottom: 20.0),
                                        child: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              marked = !marked;
                                            });
                                            // Navigator.of(context).pop();
                                          },
                                          icon: !marked
                                              ? Icon(
                                                  Istos.bookmark,
                                                  size: 35.0,
                                                )
                                              : Icon(
                                                  Istos.bookmark_alt,
                                                  size: 35.0,
                                                  color: Theme.of(context)
                                                      .accentColor,
                                                ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          RaisedButton(
                            onPressed: () {
                              !this.hasBook
                                  ? Navigator.of(context)
                                      .pushNamed('/BooksPage')
                                  : Navigator.of(context).pushNamed('/Reader',
                                      arguments:
                                          new RouteArgument(argumentsList: [
                                        {
                                          "bookToken": book["token"],
                                          "bookTitle": book["title"],
                                          "type": "Read"
                                        }
                                      ], id: "Read"));
                            },
                            child: Container(
                                height: 45.0,
                                width: MediaQuery.of(context).size.width,
                                child: Center(
                                  child: Text(
                                    !this.hasBook
                                        ? DemoLocalizations.of(context)
                                            .trans("Preview")
                                        : DemoLocalizations.of(context)
                                            .trans("Read"),
                                    style: TextStyle(
                                        color: Colors.orange[800],
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.normal),
                                    textAlign: TextAlign.center,
                                  ),
                                )),
                            color: Colors.black,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                          ),
                          Container(
                            height: 10.0,
                          ),
                          !this.hasBook
                              ? Container(
                                  // height: 250.0,
                                  child: Column(
                                    children: [
                                      book["rent_1_week_price"] != -1
                                          ? Container(
                                              padding:
                                                  EdgeInsets.only(bottom: 5.0),
                                              child: RaisedButton(
                                                onPressed: () {
                                                  setState(() {
                                                    rentType = "rent_1_week";
                                                  });
                                                  addToBasket(
                                                      context,
                                                      "rent_1_week",
                                                      book["token"],
                                                      book["rent_1_week_price"],
                                                      book[
                                                          "rent_1_week_price"]);
                                                },
                                                child: Container(
                                                  height: 45.0,
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: Row(
                                                    children: [
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            !checkInBasket(
                                                                    context,
                                                                    book[
                                                                        "token"],
                                                                    "rent_1_week")
                                                                ? !checkLoader(
                                                                        "rent_1_week")
                                                                    ? Icon(
                                                                        MaterialCommunityIcons
                                                                            .shopping,
                                                                        color: Colors
                                                                            .black)
                                                                    : Container(
                                                                        margin: EdgeInsets.only(
                                                                            top:
                                                                                5.0),
                                                                        height:
                                                                            20.0,
                                                                        width:
                                                                            20.0,
                                                                        child:
                                                                            CircularProgressIndicator(
                                                                          strokeWidth:
                                                                              3.0,
                                                                          valueColor:
                                                                              new AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColorDark),
                                                                        ))
                                                                : Icon(
                                                                    MaterialCommunityIcons
                                                                        .check,
                                                                    color: Theme.of(
                                                                            context)
                                                                        .primaryColor),
                                                            SizedBox(width: 10),
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .trans(
                                                                      "rent_1"),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "rent_1_week")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                          ]),
                                                      Spacer(),
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .transPriceNumber(
                                                                      book["rent_1_week_price"]
                                                                          .toString()),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "rent_1_week")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                            SizedBox(width: 5),
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .trans(
                                                                      "unitBig"),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "rent_1_week")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                          ]),
                                                    ],
                                                  ),
                                                ),
                                                color: !checkInBasket(
                                                        context,
                                                        book["token"],
                                                        "rent_1_week")
                                                    ? Theme.of(context)
                                                        .primaryColor
                                                    : Theme.of(context)
                                                        .accentColor,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                ),
                                              ),
                                            )
                                          : Container(),
                                      book["rent_3_price"] != -1
                                          ? Container(
                                              padding:
                                                  EdgeInsets.only(bottom: 5.0),
                                              child: RaisedButton(
                                                onPressed: () {
                                                  setState(() {
                                                    rentType = "rent_3";
                                                  });
                                                  addToBasket(
                                                      context,
                                                      "rent_3",
                                                      book["token"],
                                                      book["rent_3_price"],
                                                      book["rent_3_price"]);
                                                },
                                                child: Container(
                                                  height: 45.0,
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: Row(
                                                    children: [
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            !checkInBasket(
                                                                    context,
                                                                    book[
                                                                        "token"],
                                                                    "rent_3")
                                                                ? !checkLoader(
                                                                        "rent_3")
                                                                    ? Icon(
                                                                        MaterialCommunityIcons
                                                                            .shopping,
                                                                        color: Theme.of(context)
                                                                            .primaryColorDark)
                                                                    : Container(
                                                                        margin: EdgeInsets.only(
                                                                            top:
                                                                                5.0),
                                                                        height:
                                                                            20.0,
                                                                        width:
                                                                            20.0,
                                                                        child:
                                                                            CircularProgressIndicator(
                                                                          strokeWidth:
                                                                              3.0,
                                                                          valueColor:
                                                                              new AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColorDark),
                                                                        ))
                                                                : Icon(
                                                                    MaterialCommunityIcons
                                                                        .check,
                                                                    color: Theme.of(
                                                                            context)
                                                                        .primaryColor),
                                                            SizedBox(width: 10),
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .trans(
                                                                      "rent_3"),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "rent_3")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                          ]),
                                                      Spacer(),
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .transPriceNumber(
                                                                      book["rent_3_price"]
                                                                          .toString()),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "rent_3")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                            SizedBox(width: 5),
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .trans(
                                                                      "unitBig"),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "rent_3")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                          ]),
                                                    ],
                                                  ),
                                                ),
                                                color: !checkInBasket(context,
                                                        book["token"], "rent_3")
                                                    ? Theme.of(context)
                                                        .primaryColor
                                                    : Theme.of(context)
                                                        .accentColor,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                ),
                                              ),
                                            )
                                          : Container(),
                                      book["rent_6_price"] != -1
                                          ? Container(
                                              padding:
                                                  EdgeInsets.only(bottom: 5.0),
                                              child: RaisedButton(
                                                onPressed: () {
                                                  setState(() {
                                                    rentType = "rent_6";
                                                  });
                                                  addToBasket(
                                                      context,
                                                      "rent_6",
                                                      book["token"],
                                                      book["rent_6_price"],
                                                      book["rent_6_price"]);
                                                },
                                                child: Container(
                                                  height: 45.0,
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: Row(
                                                    children: [
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            !checkInBasket(
                                                                    context,
                                                                    book[
                                                                        "token"],
                                                                    "rent_6")
                                                                ? !checkLoader(
                                                                        "rent_6")
                                                                    ? Icon(
                                                                        MaterialCommunityIcons
                                                                            .shopping,
                                                                        color: Theme.of(context)
                                                                            .primaryColorDark)
                                                                    : Container(
                                                                        margin: EdgeInsets.only(
                                                                            top:
                                                                                5.0),
                                                                        height:
                                                                            20.0,
                                                                        width:
                                                                            20.0,
                                                                        child:
                                                                            CircularProgressIndicator(
                                                                          strokeWidth:
                                                                              3.0,
                                                                          valueColor:
                                                                              new AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColorDark),
                                                                        ))
                                                                : Icon(
                                                                    MaterialCommunityIcons
                                                                        .check,
                                                                    color: Theme.of(
                                                                            context)
                                                                        .primaryColor),
                                                            SizedBox(width: 10),
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .trans(
                                                                      "rent_6"),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "rent_6")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                          ]),
                                                      Spacer(),
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .transPriceNumber(
                                                                      book["rent_6_price"]
                                                                          .toString()),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "rent_6")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                            SizedBox(width: 5),
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .trans(
                                                                      "unitBig"),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "rent_6")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                          ]),
                                                    ],
                                                  ),
                                                ),
                                                color: !checkInBasket(context,
                                                        book["token"], "rent_6")
                                                    ? Theme.of(context)
                                                        .primaryColor
                                                    : Theme.of(context)
                                                        .accentColor,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                ),
                                              ),
                                            )
                                          : Container(),
                                      book["rent_12_price"] != -1
                                          ? Container(
                                              padding:
                                                  EdgeInsets.only(bottom: 5.0),
                                              child: RaisedButton(
                                                onPressed: () {
                                                  setState(() {
                                                    rentType = "rent_12";
                                                  });
                                                  addToBasket(
                                                      context,
                                                      "rent_12",
                                                      book["token"],
                                                      book["rent_12_price"],
                                                      book["rent_12_price"]);
                                                },
                                                child: Container(
                                                  height: 45.0,
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: Row(
                                                    children: [
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            !checkInBasket(
                                                                    context,
                                                                    book[
                                                                        "token"],
                                                                    "rent_12")
                                                                ? !checkLoader(
                                                                        "rent_12")
                                                                    ? Icon(
                                                                        MaterialCommunityIcons
                                                                            .shopping,
                                                                        color: Colors
                                                                            .black)
                                                                    : Container(
                                                                        margin: EdgeInsets.only(
                                                                            top:
                                                                                5.0),
                                                                        height:
                                                                            20.0,
                                                                        width:
                                                                            20.0,
                                                                        child:
                                                                            CircularProgressIndicator(
                                                                          strokeWidth:
                                                                              3.0,
                                                                          valueColor:
                                                                              new AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColorDark),
                                                                        ))
                                                                : Icon(
                                                                    MaterialCommunityIcons
                                                                        .check,
                                                                    color: Theme.of(
                                                                            context)
                                                                        .primaryColor),
                                                            SizedBox(width: 10),
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .trans(
                                                                      "rent_12"),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "rent_12")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                          ]),
                                                      Spacer(),
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .transPriceNumber(
                                                                      book["rent_12_price"]
                                                                          .toString()),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "rent_12")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                            SizedBox(width: 5),
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .trans(
                                                                      "unitBig"),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "rent_12")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                          ]),
                                                    ],
                                                  ),
                                                ),
                                                color: !checkInBasket(
                                                        context,
                                                        book["token"],
                                                        "rent_12")
                                                    ? Theme.of(context)
                                                        .primaryColor
                                                    : Theme.of(context)
                                                        .accentColor,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                ),
                                              ),
                                            )
                                          : Container(),
                                      book["ebook_price"] != -1
                                          ? Container(
                                              padding:
                                                  EdgeInsets.only(bottom: 5.0),
                                              child: RaisedButton(
                                                onPressed: () {
                                                  setState(() {
                                                    rentType = "ebook";
                                                  });
                                                  addToBasket(
                                                      context,
                                                      "ebook",
                                                      book["token"],
                                                      book["ebook_price"],
                                                      book["ebook_price"]);
                                                },
                                                child: Container(
                                                  height: 45.0,
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: Row(
                                                    children: [
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            !checkInBasket(
                                                                    context,
                                                                    book[
                                                                        "token"],
                                                                    "ebook")
                                                                ? !checkLoader(
                                                                        "ebook")
                                                                    ? Icon(
                                                                        MaterialCommunityIcons
                                                                            .shopping,
                                                                        color: Colors
                                                                            .black)
                                                                    : Container(
                                                                        margin: EdgeInsets.only(
                                                                            top:
                                                                                5.0),
                                                                        height:
                                                                            20.0,
                                                                        width:
                                                                            20.0,
                                                                        child:
                                                                            CircularProgressIndicator(
                                                                          strokeWidth:
                                                                              3.0,
                                                                          valueColor:
                                                                              new AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColorDark),
                                                                        ))
                                                                : Icon(
                                                                    MaterialCommunityIcons
                                                                        .check,
                                                                    color: Theme.of(
                                                                            context)
                                                                        .primaryColor),
                                                            SizedBox(width: 10),
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .trans(
                                                                      "ebook"),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "ebook")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                          ]),
                                                      Spacer(),
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .transPriceNumber(
                                                                      book["ebook_price"]
                                                                          .toString()),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "ebook")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                            SizedBox(width: 5),
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .trans(
                                                                      "unitBig"),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "ebook")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                          ]),
                                                    ],
                                                  ),
                                                ),
                                                color: !checkInBasket(context,
                                                        book["token"], "ebook")
                                                    ? Theme.of(context)
                                                        .primaryColor
                                                    : Theme.of(context)
                                                        .accentColor,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                ),
                                              ),
                                            )
                                          : Container(),
                                      book["print_price"] != -1
                                          ? Container(
                                              padding:
                                                  EdgeInsets.only(bottom: 5.0),
                                              child: RaisedButton(
                                                onPressed: () {
                                                  setState(() {
                                                    rentType = "print";
                                                  });
                                                  addToBasket(
                                                      context,
                                                      "print",
                                                      book["token"],
                                                      book["price"],
                                                      book["price"]);
                                                },
                                                child: Container(
                                                  height: 45.0,
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  child: Row(
                                                    children: [
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            !checkInBasket(
                                                                    context,
                                                                    book[
                                                                        "token"],
                                                                    "print")
                                                                ? !checkLoader(
                                                                        "print")
                                                                    ? Icon(
                                                                        MaterialCommunityIcons
                                                                            .shopping,
                                                                        color: Colors
                                                                            .black)
                                                                    : Container(
                                                                        margin: EdgeInsets.only(
                                                                            top:
                                                                                5.0),
                                                                        height:
                                                                            20.0,
                                                                        width:
                                                                            20.0,
                                                                        child:
                                                                            CircularProgressIndicator(
                                                                          strokeWidth:
                                                                              3.0,
                                                                          valueColor:
                                                                              new AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColorDark),
                                                                        ))
                                                                : Icon(
                                                                    MaterialCommunityIcons
                                                                        .check,
                                                                    color: Theme.of(
                                                                            context)
                                                                        .primaryColor),
                                                            SizedBox(width: 10),
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .trans(
                                                                      "print"),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "print")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                          ]),
                                                      Spacer(),
                                                      Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .transPriceNumber(
                                                                      book["price"]
                                                                          .toString()),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "print")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                            SizedBox(width: 5),
                                                            Text(
                                                              DemoLocalizations
                                                                      .of(
                                                                          context)
                                                                  .trans(
                                                                      "unitBig"),
                                                              style: TextStyle(
                                                                color: !checkInBasket(
                                                                        context,
                                                                        book[
                                                                            "token"],
                                                                        "print")
                                                                    ? Colors
                                                                        .black
                                                                    : Theme.of(
                                                                            context)
                                                                        .primaryColor,
                                                                fontSize: 16.0,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .normal,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w100,
                                                                decorationThickness:
                                                                    1.0,
                                                              ),
                                                            ),
                                                          ]),
                                                    ],
                                                  ),
                                                ),
                                                color: !checkInBasket(context,
                                                        book["token"], "print")
                                                    ? Theme.of(context)
                                                        .primaryColor
                                                    : Theme.of(context)
                                                        .accentColor,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                ),
                                              ),
                                            )
                                          : Container(),
                                      Container(
                                        height: 10.0,
                                      ),
                                    ],
                                  ),
                                )
                              : Container(),
                          Container(
                            padding: EdgeInsets.only(right: 5.0, left: 5.0),
                            child: ExpandText(
                              book["description"],
                              style: TextStyle(
                                fontSize: 13.0,
                              ),
                              textAlign: TextAlign.justify,
                              maxLines: 6,
                            ),
                          ),
                          // Container(height: 10.0,),
                        ],
                      ),
                    ),
                    similarBooksList.length > 0 && similarBooksList != null
                        ? Column(
                            children: [
                              Container(
                                padding:
                                    EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 15.0),
                                child: Row(
                                  children: [
                                    Icon(Feather.book, size: 30.0),
                                    Container(
                                      width: 10.0,
                                    ),
                                    Text(
                                      DemoLocalizations.of(context)
                                          .trans("SimilarBooks"),
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.bold,
                                        decorationThickness: 1.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: (MediaQuery.of(context).size.width /
                                            count2) *
                                        8 /
                                        5 -
                                    35,
                                color: Colors.transparent,
                                alignment: Alignment.topRight,
                                child: AnimationLimiter(
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    shrinkWrap: true,
                                    padding: EdgeInsets.only(
                                        right: 7.5, left: 7.5, top: 5.0),
                                    addRepaintBoundaries: true,
                                    itemCount: similarBooksList.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return RepaintBoundary(
                                          key: Key('str' + index.toString()),
                                          child: AnimationConfiguration
                                              .staggeredList(
                                            position: index,
                                            duration: const Duration(
                                                milliseconds: 500),
                                            delay: const Duration(
                                                milliseconds: 100),
                                            child: SlideAnimation(
                                              verticalOffset: 50.0,
                                              child: FadeInAnimation(
                                                child: GestureDetector(
                                                  onTapDown: (s) {
                                                    setState(() {
                                                      // selectedBooksList.removeAt(0);
                                                      Navigator.of(context).pushNamed(
                                                          '/books/' +
                                                              similarBooksList[
                                                                      index]
                                                                  ["token"],
                                                          arguments:
                                                              RouteArgument(
                                                                  id: 12,
                                                                  argumentsList: [
                                                                false,
                                                                "salam"
                                                              ]));
                                                    });
                                                  },
                                                  child: new Books2(
                                                      similarBooksList[index]
                                                              ["front_cover"]
                                                          ["medium"],
                                                      8.9,
                                                      null,
                                                      count2),
                                                ),
                                              ),
                                            ),
                                          ));
                                    },
                                  ),
                                ),
                              ),
                            ],
                          )
                        : isSimilarBooksEmpty
                            ? Container()
                            : CircularProgressIndicator(),
                    authorBooksList.length > 0 && authorBooksList != null
                        ? Column(
                            children: [
                              Container(
                                padding: EdgeInsets.all(15.0),
                                child: Row(
                                  children: [
                                    Icon(Feather.book_open, size: 30.0),
                                    Container(
                                      width: 10.0,
                                    ),
                                    Text(
                                      DemoLocalizations.of(context)
                                          .trans("AuthorOtherBooks"),
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 17.0,
                                        fontWeight: FontWeight.bold,
                                        decorationThickness: 1.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: (MediaQuery.of(context).size.width /
                                            count2) *
                                        8 /
                                        5 -
                                    35,
                                color: Colors.transparent,
                                alignment: Alignment.topRight,
                                child: AnimationLimiter(
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    shrinkWrap: true,
                                    padding: EdgeInsets.only(
                                        right: 7.5, left: 7.5, top: 5.0),
                                    addRepaintBoundaries: true,
                                    itemCount: authorBooksList.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return RepaintBoundary(
                                          key: Key('str' + index.toString()),
                                          child: AnimationConfiguration
                                              .staggeredList(
                                            position: index,
                                            duration: const Duration(
                                                milliseconds: 500),
                                            delay: const Duration(
                                                milliseconds: 100),
                                            child: SlideAnimation(
                                              verticalOffset: 50.0,
                                              child: FadeInAnimation(
                                                child: GestureDetector(
                                                  onTapDown: (s) {
                                                    setState(() {
                                                      // selectedBooksList.removeAt(0);
                                                      Navigator.of(context).pushNamed(
                                                          '/books/' +
                                                              authorBooksList[
                                                                      index]
                                                                  ["token"],
                                                          arguments:
                                                              RouteArgument(
                                                                  id: 12,
                                                                  argumentsList: [
                                                                false,
                                                                "salam"
                                                              ]));
                                                    });
                                                  },
                                                  child: new Books2(
                                                      authorBooksList[index]
                                                              ["front_cover"]
                                                          ["medium"],
                                                      8.9,
                                                      null,
                                                      count2),
                                                ),
                                              ),
                                            ),
                                          ));
                                    },
                                  ),
                                ),
                              ),
                            ],
                          )
                        : Container(),
                    Container(
                      height: 20.0,
                    )
                  ],
                ),
              ),
            )
          : Center(
              child: Container(
                  height: 50.0,
                  width: 50.0,
                  child: CircularProgressIndicator()),
            ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void afterFirstLayout(BuildContext context) {
    final iw = MainStateContainer.of(context);
    accountsRepo
        .getBook(
      iw.hostName,
      token,
    )
        .then((value) {
      setState(() {
        book = value;
      });
    });
    accountsRepo
        .getMoreBook(
      iw.hostName,
      token,
    )
        .then((value) {
      setState(() {
        if (value[0] != null && value[0]["books"].length > 0) {
          similarBooksList = value[0]["books"];
        }
        if (value[0]["books"].length == 0) {
          isSimilarBooksEmpty = true;
        }
        if (value[1] != null && value[1]["books"].length > 0) {
          authorBooksList = value[1]["books"];
        }
        if (value[1]["books"].length == 0) {
          isAuthorBooksEmpty = true;
        }
      });
    });
  }

  bool checkLoader(type) {
    if (type == rentType && loader == true) {
      return true;
    } else {
      return false;
    }
  }

  bool checkInBasket(context, bookToken, type) {
    final iw = MainStateContainer.of(context);
    // print(bookToken);
    // print(type);
    if (iw.cart != null) {
      if (iw.cart["orders"] != null) {
        for (int i = 0; i < iw.cart["orders"].length; i++) {
          if (iw.cart["orders"][i]["type"] == type &&
              iw.cart["orders"][i]["book"]["token"] == bookToken) {
            return true;
          }
        }
      }
      return false;
    } else
      return false;
  }

  Future<void> addToBasket(
      context, rentType, bookToken, price, originalPrice) async {
    final iw = MainStateContainer.of(context);
    Map data = {
      "orders": [
        {
          // "token": null,
          "type": rentType,
          "book": {"token": bookToken},
          // "price": price,
          "original_price": null
        }
      ]
    };
    String body = json.encode(data);
    setState(() {
      loader = true;
    });

    await accountsRepo
        .addToBasket(
      iw.hostName,
      iw.token,
      body,
    )
        .then((basketReq) {
      iw.setCart(basketReq);
      setState(() {
        loader = false;
        // print(basketReq.toString());
        // book = basketReq;
        Navigator.of(context).pushNamed('/ShoppingCart');
      });
    });
  }
}
