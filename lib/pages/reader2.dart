import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class InlineExample extends StatelessWidget {
  final imageList = [
    'https://www.mybooket.com/storage/books/d70b75d09d7ca4fb/pages/4ef8bb2288f8656ab0a3c00f78c7d8f0.jpg',
    'https://image.shutterstock.com/image-photo/3d-wallpaper-design-waterfall-sea-260nw-1380925703.jpg',
    'https://m.economictimes.com/thumb/msid-68721417,width-1200,height-900,resizemode-4,imgsize-1016106/nature1_gettyimages.jpg',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQus9rtXGG06QBn-Q13zk5oWKr-ryngXbTW-g&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTyP2vktrvo9rlhz4jQDnSsI-D-WL92iX36Ig&usqp=CAU',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.blueGrey,
      // add this body tag with container and photoview widget
      body: PhotoViewGallery.builder(
        itemCount: imageList.length,
        builder: (context, index) {
          return PhotoViewGalleryPageOptions(
            // basePosition: Alignment.center,
            imageProvider: NetworkImage(imageList[index]),
            minScale: PhotoViewComputedScale.contained * 0.8,
            maxScale: PhotoViewComputedScale.covered * 2,
            initialScale: PhotoViewComputedScale.contained,
            // heroAttributes: PhotoViewHeroAttributes(

            // ),
          );
        },
        scrollPhysics: BouncingScrollPhysics(),
        backgroundDecoration: BoxDecoration(
          color: Theme.of(context).canvasColor,
        ),
      ),
    );
  }
}
