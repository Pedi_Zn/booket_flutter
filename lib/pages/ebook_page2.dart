import 'dart:async';
import 'dart:io';

import 'package:after_layout/after_layout.dart';
import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/blocs/reader_bloc.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:hovering/hovering.dart';

typedef DoubleClickAnimationListener = void Function();

class EbookPage extends StatefulWidget {
  final dynamic image;
  final ReaderBloc readerBloc;
  final int pageIndex;

  const EbookPage({Key key, this.image, this.pageIndex, this.readerBloc})
      : super(key: key);
  @override
  _ebookPagetate createState() =>
      _ebookPagetate(this.image, this.pageIndex, this.readerBloc);
}

class _ebookPagetate extends State<EbookPage>
    with SingleTickerProviderStateMixin, AfterLayoutMixin
// , AutomaticKeepAliveClientMixin
{
  final AccountsRepo accountsRepo = new AccountsRepo();
  dynamic image;
  final int pageIndex;
  final ReaderBloc readerBloc;
  Comparator<dynamic> xComparator =
      (a, b) => (a["boundary"][0].compareTo(b["boundary"][0]));
  Animation<double> _doubleClickAnimation;
  List<double> doubleTapScales = <double>[1.0, 2.0];
  List<bool> hiddens = [];
  DoubleClickAnimationListener _doubleClickAnimationListener;
  AnimationController _doubleClickAnimationController;
  List<dynamic> words = [];
  List<dynamic> showWords = [];
  List<dynamic> paragraphs = [];
  Color highlightColor = Colors.amberAccent.withOpacity(0.3);
  // Color highlightColor;
  dynamic firstTouch;
  dynamic endTouch;
  dynamic lineWords = {};
  List<dynamic> annotations = [];
  String lastSelectedLine;
  String lastSelectedWordKey;
  String lastEndLine;
  String endWordKey;
  String endWordLine;
  int startIndex;
  bool saved = false;
  int endIndex;
  var topExtraPadding = 60.0;
  var topExtraMargin = 0.0;
  List<String> wordsKeysList = [];
  dynamic wordsMap = {};
  _ebookPagetate(this.image, this.pageIndex, this.readerBloc);
  @override
  void initState() {
    _doubleClickAnimationController = AnimationController(
        duration: const Duration(milliseconds: 150), vsync: this);

    super.initState();
  }

  @override
  void dispose() {
    _doubleClickAnimationController.dispose();
    clearGestureDetailsCache();
    //cancelToken?.cancel();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant EbookPage oldWidget) {
    // TODO: implement didUpdateWidget
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    var x = -1;
    var t = 0.0;
    // left: 200.0,
    var fromTopX;
    var fromBottomX;
    var fromLeftX;
    var fromRightX;

    final pageSize = MediaQuery.of(context).size;
    final pageHeight = MediaQuery.of(context).size.height - 120.0;
    final imageWidth = image["info"]["width"];
    final imageHeight = image["info"]["height"];
    final iw = MainStateContainer.of(context);
    var imgProcessedWidth, imgProcessedHeight;
    var boxWidth, boxHeight;
    var ratio = imageWidth / imageHeight;
    var zarib;

    if (Platform.isAndroid) {
      topExtraMargin = -11.5;
      topExtraPadding = 90.0;
    }
    var condition = (pageSize.width / pageHeight) > ratio;
    if (condition) {
      // zarib = (imageHeight / pageSize.height);
      boxWidth = (pageHeight * ratio);
      boxHeight = pageHeight;
      imgProcessedWidth = boxWidth;
      boxWidth -= 17.0;
      fromLeftX = ((pageSize.width - boxWidth) / 2);
      fromRightX = ((pageSize.width - boxWidth) / 2);
      fromTopX = 0.0;
      fromBottomX = 0.0;
    } else {
      // zarib = (imageWidth / imageHeight);
      imgProcessedWidth = pageSize.width;
      boxHeight = (imgProcessedWidth / ratio);
      fromLeftX = 0.0;
      fromRightX = 0.0;
      fromTopX = ((pageHeight - boxHeight) / 2) - 2.0;
      fromBottomX = (pageHeight - boxHeight) / 2;
      if (Platform.isAndroid) {
        fromBottomX = fromBottomX - 11.5;
      }
    }

    // print("pagwWidth : " +
    //     pageSize.width.toString() +
    //     "  /   pageHeight : " +
    //     pageSize.height.toString());
    // print("image Width : " +
    //     imageWidth.toString() +
    //     "  /   image Height : " +
    //     imageHeight.toString());
    // print("width : " + imgProcessedWidth.toString());
    // print("height : " + imgProcessedHeight.toString());
    return Container(
      child: Stack(
        children: [
          // Positioned.fill(
          //     child: Center(
          //         child: Container(
          //             width: 40.0,
          //             height: 40.0,
          //             child: CircularProgressIndicator()))),
          Positioned(
            child: Center(
              child: Center(
                child: ExtendedImage.network(
                  iw.hostName + image["image"]["webp"],
                  fit: (pageSize.width > pageHeight)
                      ? BoxFit.fill
                      : BoxFit.cover,
                  clearMemoryCacheWhenDispose: true,
                  retries: 3,
                  cache: true,
                  afterPaintImage: (a, v, s, e) {},
                  onDoubleTap: (ExtendedImageGestureState state) {
                    final Offset pointerDownPosition =
                        state.pointerDownPosition;
                    final double begin = state.gestureDetails.totalScale;
                    double end;
                    _doubleClickAnimation
                        ?.removeListener(_doubleClickAnimationListener);
                    _doubleClickAnimationController.stop();
                    _doubleClickAnimationController.reset();

                    if (begin == doubleTapScales[0]) {
                      end = doubleTapScales[1];
                    } else {
                      end = doubleTapScales[0];
                    }

                    _doubleClickAnimationListener = () {
                      state.handleDoubleTap(
                          scale: _doubleClickAnimation.value,
                          doubleTapPosition: pointerDownPosition);
                    };
                    _doubleClickAnimation = _doubleClickAnimationController
                        .drive(Tween<double>(begin: begin, end: end));

                    _doubleClickAnimation
                        .addListener(_doubleClickAnimationListener);

                    _doubleClickAnimationController.forward();
                  },
                  mode: iw.gestureEnable
                      ? ExtendedImageMode.gesture
                      : ExtendedImageMode.none,
                  initGestureConfigHandler: (state) {
                    return GestureConfig(
                      minScale: 1,
                      animationMinScale: 0.7,
                      maxScale: 3.0,
                      animationMaxScale: 3.5,
                      speed: 1.0,
                      cacheGesture: true,
                      inertialSpeed: 100.0,
                      initialScale: 1.0,
                      inPageView: true,
                      initialAlignment: InitialAlignment.center,
                    );
                  },
                ),
              ),
            ),
          ),

          // Paragraphs ocr
          Positioned(
            // left: 200.0,
            top: fromTopX + topExtraMargin,
            bottom: condition ? null : fromBottomX,
            height: condition ? (boxHeight) : null,
            left: fromLeftX,
            right: condition ? null : fromRightX,
            width: condition ? boxWidth : null,
            // height: (pageSize.width / pageHeight) > ratio
            //     ? pageHeight
            //     : imgProcessedHeight,
            // height: imageHeight,
            child:
                //  Material(
                //   // color: Colors.blue.withOpacity(0.2),
                //   color: Colors.transparent,
                //   child:
                GestureDetector(
              onTapDown: (detail) {
                if (!iw.gestureEnable) {
                  var lineKey = findTouchedLine(detail, boxHeight, fromTopX,
                      imgProcessedWidth, imgProcessedHeight);
                  setState(() {
                    lastSelectedLine = lineKey;
                  });
                  if (lineKey != null) {
                    var startLine = lineWords[lineKey];

                    // print(startLine.toString());
                    var startWordKey = findStartWord(
                        detail,
                        boxHeight,
                        fromTopX,
                        imgProcessedWidth,
                        imgProcessedHeight,
                        lineKey);
                    if (startWordKey != null) {
                      if (wordsMap[lineKey + "-" + startWordKey] != null) {
                        setState(() {
                          lastSelectedWordKey = startWordKey;
                          endWordKey = startWordKey;
                          endWordLine = lastSelectedLine;
                          firstTouch = {
                            "x": detail.globalPosition.dx,
                            "y": detail.globalPosition.dy
                          };
                        });
                        var index =
                            wordsMap[lineKey + "-" + startWordKey]["index"];

                        repaintColorSelected(index, index, false);
                      }
                    } else {
                      setState(() {
                        lastSelectedWordKey = null;
                        endWordKey = null;
                        endWordLine = null;
                        firstTouch = {
                          "x": detail.globalPosition.dx,
                          "y": detail.globalPosition.dy
                        };
                      });
                    }
                  } else {
                    setState(() {
                      firstTouch = {
                        "x": detail.globalPosition.dx,
                        "y": detail.globalPosition.dy
                      };
                    });
                  }
                }
              },
              onPanStart: (detail) {
                if (!iw.gestureEnable) {
                  var lineKey = findTouchedLine(detail, boxHeight, fromTopX,
                      imgProcessedWidth, imgProcessedHeight);
                  setState(() {
                    lastSelectedLine = lineKey;
                  });
                  if (lineKey != null) {
                    var startLine = lineWords[lineKey];

                    // print(startLine.toString());
                    var startWordKey = findStartWord(
                        detail,
                        boxHeight,
                        fromTopX,
                        imgProcessedWidth,
                        imgProcessedHeight,
                        lineKey);
                    if (startWordKey != null) {
                      if (wordsMap[lineKey + "-" + startWordKey] != null) {
                        setState(() {
                          lastSelectedWordKey = startWordKey;
                          endWordKey = startWordKey;
                          endWordLine = lastSelectedLine;
                          firstTouch = {
                            "x": detail.globalPosition.dx,
                            "y": detail.globalPosition.dy
                          };
                        });
                        var index =
                            wordsMap[lineKey + "-" + startWordKey]["index"];

                        repaintColorSelected(index, index, true);
                      }
                    } else {
                      setState(() {
                        lastSelectedWordKey = null;
                        endWordKey = null;
                        endWordLine = null;
                        firstTouch = {
                          "x": detail.globalPosition.dx,
                          "y": detail.globalPosition.dy
                        };
                      });
                    }
                  } else {
                    setState(() {
                      firstTouch = {
                        "x": detail.globalPosition.dx,
                        "y": detail.globalPosition.dy
                      };
                    });
                  }
                }
                print(" endLine : " +
                    endWordLine.toString() +
                    " / endWord :" +
                    endWordKey.toString() +
                    " / firstLine : " +
                    lastSelectedLine.toString() +
                    " / firstWord : " +
                    lastSelectedWordKey.toString());
              },
              onPanUpdate: (detail) {
                if (!iw.gestureEnable) {
                  // if (firstTouch == null) {
                  if (firstTouch == null) {
                    var lineKey = findTouchedLine(detail, boxHeight, fromTopX,
                        imgProcessedWidth, imgProcessedHeight);
                    if (lineKey != null) {
                      var startLine = lineWords[lineKey];
                      setState(() {
                        lastSelectedLine = lineKey;
                      });
                      // print(startLine.toString());
                      var startWordKey = findStartWord(
                          detail,
                          boxHeight,
                          fromTopX,
                          imgProcessedWidth,
                          imgProcessedHeight,
                          lastSelectedLine);
                      if (startWordKey != null) {
                        if (wordsMap[lastSelectedLine + "-" + startWordKey] !=
                            null) {
                          setState(() {
                            lastSelectedWordKey = startWordKey;
                            endWordKey = startWordKey;
                            endWordLine = lastSelectedLine;
                            firstTouch = {
                              "x": detail.globalPosition.dx,
                              "y": detail.globalPosition.dy
                            };
                          });
                          var index = wordsMap[lastSelectedLine +
                              "-" +
                              lastSelectedWordKey]["index"];
                          showWords[index]["color"] = highlightColor;
                        }
                      }
                      setState(() {
                        firstTouch = {
                          "x": detail.globalPosition.dx,
                          "y": detail.globalPosition.dy
                        };
                      });
                    } else {
                      setState(() {
                        firstTouch = {
                          "x": detail.globalPosition.dx,
                          "y": detail.globalPosition.dy
                        };
                      });
                    }
                  } else {
                    // already has start point
                    // firstTouch!=null

                    var lineKey = findTouchedLine(detail, boxHeight, fromTopX,
                        imgProcessedWidth, imgProcessedHeight);
                    setState(() {
                      endWordLine = lineKey;
                    });
                    if (lineKey != null) {
                      var endLine = lineWords[lineKey];

                      // print(endLine.toString());
                      var endWordK = findStartWord(detail, boxHeight, fromTopX,
                          imgProcessedWidth, imgProcessedHeight, lineKey);
                      setState(() {
                        endWordKey = endWordK;
                      });
                      if (endWordKey != null) {
                        // second touch on word inside the line
                        setState(() {
                          endTouch = {
                            "x": detail.globalPosition.dx,
                            "y": detail.globalPosition.dy
                          };
                        });
                      } else {
                        setState(() {
                          endTouch = {
                            "x": detail.globalPosition.dx,
                            "y": detail.globalPosition.dy
                          };
                        });
                      }
                    } else {
                      setState(() {
                        endWordKey = null;
                        endWordLine = null;
                        endTouch = {
                          "x": detail.globalPosition.dx,
                          "y": detail.globalPosition.dy
                        };
                      });
                    }

                    // find and track endPoint allways
                    // wordsMap[lastSelectedLine + "-" + lastSelectedWordKey]
                    //     ["color"] = Colors.amberAccent.withOpacity(0.9);
                  }

                  // check all conditions and pass variants to checkerr function

                  if ((firstTouch["x"] != null) &&
                      (firstTouch["y"] != null) &&
                      (endTouch["x"] != null) &&
                      (endTouch["y"] != null) &&
                      (lineWords["1"] != null)) {
                    print(":update");
                    print("x'1 :" +
                        firstTouch["x"].toString() +
                        " / y'1 :" +
                        firstTouch["y"].toString() +
                        " / x'2 :" +
                        endTouch["x"].toString() +
                        " / y'2 :" +
                        endTouch["y"].toString());

                    double y22 =
                        ((lineWords["1"][0]["boundary"][3] * boxHeight) +
                            fromTopX +
                            topExtraPadding);
                    double y11 =
                        ((lineWords["1"][0]["boundary"][3] * boxHeight) +
                            fromTopX +
                            topExtraPadding);

                    print("y''1 :" + y11.toString());
                    print("y''2 :" + y22.toString());
                    // var endIndex =
                    //     wordsMap[endWordLine + "-" + endWordKey]["index"];
                    // var startIndex =
                    //     wordsMap[lastSelectedLine + "-" + lastSelectedWordKey]
                    //         ["index"];
                    // if (startIndex > endIndex) {
                    //   var tmp = startIndex;
                    //   startIndex = endIndex;
                    //   endIndex = tmp;
                    // }
                    // for (var counter = 0;
                    //     counter < showWords.length;
                    //     counter++) {
                    //   showWords[counter]["color"] = Colors.transparent;
                    // }
                    // for (var counter = startIndex;
                    //     counter < endIndex;
                    //     counter++) {
                    //   showWords[counter]["color"] = highlightColor;
                    // }

                    if (((lastSelectedLine != null) &&
                            (endWordLine != null) &&
                            (endWordKey != null) &&
                            (lastSelectedWordKey != null)) &&
                        ((endWordKey != lastSelectedWordKey) ||
                            (endWordLine != lastSelectedLine))) {
                      // L1 , Ln
                      // touches are on  different valid words
                      print(":update");
                      var endIndex =
                          wordsMap[endWordLine + "-" + endWordKey]["index"];
                      var startIndex =
                          wordsMap[lastSelectedLine + "-" + lastSelectedWordKey]
                              ["index"];
                      if (startIndex > endIndex) {
                        var tmp = startIndex;
                        startIndex = endIndex;
                        endIndex = tmp;
                      }
                      repaintColorSelected(startIndex, endIndex, true);
                    } else if ((endWordKey != null) &&
                        (lastSelectedWordKey == null)) {
                      // N1 , L2
                      var firstLine = null;
                      lineWords.forEach((key, element) {
                        List<dynamic> line = element;
                        int lineNumber = int.tryParse(key);

                        var firstWordOfLnBoundaries = element[0]["boundary"];
                        double middleOfTheLineY =
                            (((firstWordOfLnBoundaries[1] +
                                            firstWordOfLnBoundaries[3]) /
                                        2) *
                                    boxHeight) +
                                fromTopX +
                                topExtraPadding;
                        // start word = null;
                        if ((firstLine == null) &&
                            (middleOfTheLineY >= firstTouch["y"])) {
                          firstLine = lineNumber;
                        }
                      });
                      var endIndex, startIndex;
                      if (firstLine != null) {
                        if (firstLine == 0) {
                          firstLine = 2;
                        }
                        if (endTouch["y"] < firstTouch["y"]) {
                          print("Ssssss");
                          endIndex = lineWords[(firstLine - 1).toString()][
                              lineWords[(firstLine - 1).toString()].length -
                                  1]["index"];

                          startIndex = lineWords[(firstLine - 1).toString()][
                              lineWords[(firstLine - 1).toString()].length -
                                  1]["index"];
                          // wordsMap[endWordLine + "-" + endWordKey]["index"];
                        } else {
                          endIndex =
                              wordsMap[endWordLine + "-" + endWordKey]["index"];
                          startIndex =
                              lineWords[firstLine.toString()][0]["index"];
                        }
                        print("startIndex  : " +
                            startIndex.toString() +
                            " , endIndex  : " +
                            endIndex.toString());
                        repaintColorSelected(startIndex, endIndex, true);
                      }
                    } else if ((lastSelectedLine != null) &&
                        (lastSelectedWordKey != null) &&
                        (endWordKey == null)) {
                      // L1 , N2
                      var endLine = null;
                      lineWords.forEach((key, element) {
                        List<dynamic> line = element;
                        int lineNumber = int.tryParse(key);

                        var endWordOfLnBoundaries = element[0]["boundary"];
                        double middleOfTheLineY = (((endWordOfLnBoundaries[1] +
                                        endWordOfLnBoundaries[3]) /
                                    2) *
                                boxHeight) +
                            fromTopX +
                            topExtraPadding;

                        // start word = null;
                        if ((endLine == null) &&
                            (middleOfTheLineY >= endTouch["y"])) {
                          if (firstTouch["y"] > endTouch["y"]) {
                            endLine = lineNumber;
                          } else {
                            endLine = lineNumber - 1;
                          }
                        }
                      });

                      if (endLine != null) {
                        if (endLine == 0) {
                          endLine = 1;
                        }
                        var endIndex;

                        if (firstTouch["y"] > endTouch["y"]) {
                          if ((lastEndLine != null) &&
                              (lastEndLine != endLine)) {
                            endIndex = lineWords[lastEndLine]
                                [(lineWords[lastEndLine].length - 1)];
                          } else {
                            endIndex =
                                wordsMap[lastSelectedLine + "-0"]["index"];
                          }
                        } else {
                          endIndex = lineWords[endLine.toString()]
                                  [(lineWords[endLine.toString()].length - 1)]
                              ["index"];
                        }

                        var startIndex = wordsMap[lastSelectedLine +
                            "-" +
                            lastSelectedWordKey]["index"];
                        print("startIndex  : " +
                            startIndex.toString() +
                            " , endIndex  : " +
                            endIndex.toString());
                        repaintColorSelected(startIndex, endIndex, true);
                      }
                    } else if ((lastSelectedWordKey == null) &&
                        (endWordKey == null)) {
                      // N1 , N2
                      var endLine = null;
                      var firstLine = null;
                      lineWords.forEach((key, element) {
                        List<dynamic> line = element;
                        int lineNumber = int.tryParse(key);

                        var endWordOfLnBoundaries = element[0]["boundary"];
                        double middleOfTheLineY = (((endWordOfLnBoundaries[1] +
                                        endWordOfLnBoundaries[3]) /
                                    2) *
                                boxHeight) +
                            fromTopX +
                            topExtraPadding;

                        // start word = null;
                        if (firstTouch["y"] > endTouch["y"]) {
                          var tmp = firstTouch;
                          firstTouch = endTouch;
                          endTouch = tmp;
                        }
                        if (((firstLine == null) && (endLine == null)) &&
                            (middleOfTheLineY > firstTouch["y"]) &&
                            (endTouch["y"] >= middleOfTheLineY)) {
                          firstLine = lineNumber;
                        } else if ((endLine == null) &&
                            (middleOfTheLineY >= endTouch["y"])) {
                          if (firstTouch["y"] > endTouch["y"]) {
                            endLine = lineNumber;
                          } else {
                            endLine = lineNumber - 1;
                          }
                        }
                      });
                      var endIndex, startIndex;
                      if ((firstLine == null)) {
                      } else {
                        if ((endLine == null) || (endLine == 0)) {
                          endLine = lineWords.length;
                        }
                        if ((firstLine == null) || (firstLine == 0)) {
                          firstLine = 1;
                        }
                        // print("last line length : " +
                        //     lineWords[endLine.toString()].length.toString());
                        if (lastEndLine == null)
                          setState(() {
                            lastEndLine = endLine.toString();
                          });
                        if (firstTouch["y"] > endTouch["y"]) {
                          endIndex = lineWords[lastEndLine.toString()][
                              (lineWords[lastEndLine.toString()].length -
                                  1)]["index"];
                          startIndex =
                              lineWords[firstLine.toString()][0]["index"];
                        } else {
                          endIndex = lineWords[endLine.toString()]
                                  [(lineWords[endLine.toString()].length - 1)]
                              ["index"];
                          startIndex =
                              lineWords[firstLine.toString()][0]["index"];
                        }

                        print("startIndex  : " +
                            startIndex.toString() +
                            " , endIndex  : " +
                            endIndex.toString());

                        repaintColorSelected(startIndex, endIndex, true);
                        // print("shit");
                      }
                    }
                  }
                }
              },
              onPanEnd: (e) {
                //
                if (!iw.gestureEnable) {
                  // print("clean");

                  saveSelect();
                }
              },
              onPanCancel: () {
                // color Selected Line Whole
                if (!iw.gestureEnable) {
                  print("clean");

                  cleanSelect();
                }
              },
              child: Material(
                  color: Colors.blue.withOpacity(0.0),
                  child: Stack(
                    children: [
                      Positioned.fill(
                          child: StreamBuilder<dynamic>(
                              stream: readerBloc
                                  .getSelectionCallsForPage(pageIndex),
                              initialData: {
                                "page": pageIndex,
                                "annotations": []
                              },
                              builder: (context, snapshot) {
                                var contidion = ((paragraphsMapToWords.length >
                                        0) &&
                                    (showWords.length > 0) &&
                                    (snapshot.hasData &&
                                        snapshot.data != null) &&
                                    (readerBloc.pagedAnnotations.length - 1 >=
                                        pageIndex) &&
                                    (readerBloc.pagedAnnotations[pageIndex] !=
                                        null) &&
                                    (readerBloc.pagedAnnotations[pageIndex]
                                            .length >
                                        0));

                                return Stack(
                                    children: !contidion
                                        ? [
                                            Positioned(
                                                left: 0,
                                                top: 0,
                                                child: Container(),
                                                height: 0,
                                                width: 0)
                                          ]
                                        : readerBloc.pagedAnnotations[pageIndex]
                                            .map<Widget>((annotation) {
                                            if (annotation["type"] == "shape") {
                                              var pointX =
                                                  annotation["points"][0]["x"];
                                              var pointY =
                                                  annotation["points"][0]["y"];
                                              var yy = (pointY * pageHeight);
                                              var xx = (pointX * boxWidth);
                                              return Container();
                                            } else {
                                              var startParagraph =
                                                  annotation["properties"]
                                                      ["start"]["paragraph"];
                                              var startWord =
                                                  annotation["properties"]
                                                      ["start"]["word"];
                                              var endParagraph =
                                                  annotation["properties"]
                                                      ["end"]["paragraph"];
                                              var endWord =
                                                  annotation["properties"]
                                                      ["end"]["word"];

                                              var startIndexM =
                                                  paragraphsMapToWords[
                                                              startParagraph
                                                                  .toString()]
                                                          ["start"] +
                                                      startWord;
                                              var endIndexM =
                                                  paragraphsMapToWords[
                                                              endParagraph
                                                                  .toString()]
                                                          ["end"] +
                                                      endWord;
                                              var endT = endIndexM >
                                                      showWords.length - 1
                                                  ? showWords.length - 1
                                                  : endIndexM;
                                              for (var ix = startIndexM;
                                                  ix < endT;
                                                  ix++) {
                                                dynamic word = showWords[ix];

                                                var leftX;
                                                var rightX;
                                                var width;
                                                var height;
                                                var bottomY;
                                                var fromTop;
                                                var topY;
                                                var fromLeft;

                                                dynamic data =
                                                    calculatePositions(
                                                        pageSize,
                                                        pageHeight,
                                                        imgProcessedWidth,
                                                        imgProcessedHeight,
                                                        fromLeft,
                                                        fromTop,
                                                        leftX,
                                                        topY,
                                                        rightX,
                                                        bottomY,
                                                        width,
                                                        height,
                                                        ratio,
                                                        boxWidth,
                                                        boxHeight,
                                                        word);

                                                leftX = data["left"];
                                                topY = data["top"];
                                                height = data["height"];
                                                width = data["width"];
                                                rightX = data["right"];
                                                bottomY = data["bottom"];

                                                annotation['type'] ==
                                                        "highlight"
                                                    ? Positioned(
                                                        left: leftX,
                                                        top: topY,
                                                        height: height,
                                                        width: width,
                                                        // bottom: bottomY,
                                                        child: Container(
                                                          color: getColorObjectFromHex(
                                                              "#55" +
                                                                  annotation[
                                                                          "color"]
                                                                      .split(
                                                                          "#")[1]),
                                                        ))
                                                    : Positioned(
                                                        left: leftX,
                                                        top:
                                                            topY + height + 3.0,
                                                        height: 2,
                                                        width: width,
                                                        child: Container(
                                                          color: getColorObjectFromHex(
                                                              "#55" +
                                                                  annotation[
                                                                          "color"]
                                                                      .split(
                                                                          "#")[1]),
                                                        ),
                                                      );
                                              }
                                              // return new Positioned(
                                              //     left: 0,
                                              //     top: 0,
                                              //     height: 100.0,
                                              //     width: 100,
                                              //     child: Container(
                                              //         color:
                                              //             getColorObjectFromHex(
                                              //                 "#55" +
                                              //                     annotation[
                                              //                             "start"]
                                              //                         ["x"]),
                                              //         child: Text(contidion
                                              //             ? annotation[
                                              //                     "page_index"]
                                              //                 .toString()
                                              //             : condition
                                              //                 .toString())));
                                            }
                                          }).toList());
                              })),
                      Positioned.fill(
                        child: StreamBuilder<dynamic>(
                            stream:
                                readerBloc.getSelectionCallsForPage(pageIndex),
                            initialData: {
                              "color": readerBloc.selectedColor.withOpacity(0.3)
                            },
                            builder: (context, snapshot) {
                              return Stack(
                                  children: (((paragraphs != null) &&
                                          (paragraphs.length > 0)))
                                      ? iw.gestureEnable
                                          ? [
                                              Positioned(
                                                  left: 0.0,
                                                  top: 0.0,
                                                  width: 0.0,
                                                  height: 0.0,
                                                  child: Container())
                                            ]
                                          // paragraphs.map((dynamic paragraph) {
                                          //     x = x + 1;
                                          //     // final pageSize = MediaQuery.of(context).size;
                                          //     // final pageHeight = MediaQuery.of(context).size.height - 120.0;
                                          //     // final imageWidth = image["info"]["width"];
                                          //     // final imageHeight = image["info"]["height"];

                                          //     var leftX;
                                          //     var rightX;
                                          //     var width;
                                          //     var height;
                                          //     var bottomY;
                                          //     var fromTop;
                                          //     var topY;
                                          //     var fromLeft;

                                          //     dynamic data = calculatePositions(
                                          //         pageSize,
                                          //         pageHeight,
                                          //         imgProcessedWidth,
                                          //         imgProcessedHeight,
                                          //         fromLeft,
                                          //         fromTop,
                                          //         leftX,
                                          //         topY,
                                          //         rightX,
                                          //         bottomY,
                                          //         width,
                                          //         height,
                                          //         ratio,
                                          //         boxWidth,
                                          //         boxHeight,
                                          //         paragraph);

                                          //     leftX = data["left"];
                                          //     topY = data["top"];
                                          //     height = data["height"];
                                          //     width = data["width"];

                                          //     return Positioned(
                                          //       // duration: Duration(milliseconds: 10),
                                          //       left: leftX,
                                          //       top: topY,
                                          //       height: height,
                                          //       width: width,
                                          //       // bottom: bottomY,
                                          //       child: Material(
                                          //           color: x < 200.0
                                          //               ? Colors.amber.withOpacity(0.3)
                                          //               : Colors.red.withOpacity(0.3)),
                                          //     );
                                          //   }).toList()

                                          // WORDS SHOW

                                          : wordsKeysList
                                              .asMap()
                                              .entries
                                              .map((entry) {
                                              dynamic word = showWords[
                                                  wordsMap[entry.value]
                                                      ["index"]];
                                              var color = word["color"];
                                              int x = entry.key;
                                              var y1 = (((word["boundary"][1] *
                                                          boxHeight) +
                                                      fromTopX) +
                                                  topExtraPadding);
                                              var y2 = (((word["boundary"][3] *
                                                          boxHeight) +
                                                      fromTopX) +
                                                  topExtraPadding);

                                              var x1 = word["boundary"][0] *
                                                  imgProcessedWidth;

                                              var x2 = word["boundary"][2] *
                                                  imgProcessedWidth;

                                              var leftX;
                                              var rightX;
                                              var width;
                                              var height;
                                              var bottomY;
                                              var fromTop;
                                              var topY;
                                              var fromLeft;

                                              dynamic data = calculatePositions(
                                                  pageSize,
                                                  pageHeight,
                                                  imgProcessedWidth,
                                                  imgProcessedHeight,
                                                  fromLeft,
                                                  fromTop,
                                                  leftX,
                                                  topY,
                                                  rightX,
                                                  bottomY,
                                                  width,
                                                  height,
                                                  ratio,
                                                  boxWidth,
                                                  boxHeight,
                                                  word);

                                              leftX = data["left"];
                                              topY = data["top"];
                                              height = data["height"];
                                              width = data["width"];
                                              rightX = data["right"];
                                              bottomY = data["bottom"];
                                              // if type = highlight

                                              return iw.selectionType ==
                                                      "highlight"
                                                  ? Positioned(
                                                      left: leftX,
                                                      top: topY,
                                                      height: height,
                                                      width: width,
                                                      // bottom: bottomY,
                                                      child: Container(
                                                          color: (word[
                                                                      "color"] !=
                                                                  Colors
                                                                      .transparent)
                                                              ? (!snapshot
                                                                      .hasData
                                                                  ? highlightColor
                                                                  : snapshot
                                                                          .data[
                                                                      "color"])
                                                              : Colors
                                                                  .transparent))
                                                  : iw.selectionType ==
                                                          "underline"
                                                      ? Positioned(
                                                          left: leftX,
                                                          top: topY +
                                                              height +
                                                              3.0,
                                                          height: 2,
                                                          width: width,
                                                          child: Container(
                                                            color: (word[
                                                                        "color"] !=
                                                                    Colors
                                                                        .transparent)
                                                                ? (!snapshot
                                                                        .hasData
                                                                    ? highlightColor
                                                                    : snapshot
                                                                            .data[
                                                                        "color"])
                                                                : Colors
                                                                    .transparent,
                                                          ),
                                                        )
                                                      // masalan noghtas in type== shape
                                                      : Positioned(
                                                          left: 100,
                                                          top: 100,
                                                          child: Container(
                                                            color: (word[
                                                                        "color"] !=
                                                                    Colors
                                                                        .transparent)
                                                                ? (!snapshot
                                                                        .hasData
                                                                    ? highlightColor
                                                                    : snapshot
                                                                            .data[
                                                                        "color"])
                                                                : Colors
                                                                    .transparent,
                                                          ),
                                                        );
                                            }).toList()
                                      : [
                                          Positioned(
                                              left: 10,
                                              top: 10,
                                              child:
                                                  Container(color: Colors.blue),
                                              width: 0.0,
                                              height: 0.0)
                                        ]);
                            }),
                      ),
                    ],
                  )),
            ),
            // ),
          ),
        ],
      ),
    );
  }

  void repaintColorSelected(startIndex, endIndex, clear) {
    var tmp;
    if (startIndex > endIndex) {
      tmp = endIndex;
      endIndex = startIndex;
      startIndex = tmp;
    }
    setState(() {
      startIndex = startIndex;
      endIndex = endIndex;
    });

    if (clear) clearHighlight();
    for (var counter = startIndex; counter < endIndex + 1; counter++) {
      showWords[counter]["color"] = readerBloc.selectColor().withOpacity(0.3);
      readerBloc.setCommand(
        {"color": readerBloc.selectColor().withOpacity(0.3), "page": pageIndex},
      );
    }
  }

  void clearHighlight() {
    for (var counter = 0; counter < showWords.length; counter++) {
      showWords[counter]["color"] = Colors.transparent;
    }
  }

  @override
  void afterFirstLayout(BuildContext context) {
    final iw = MainStateContainer.of(context);

    final threshold = 0.05;
    var yThreshold = 0.01;
    List<dynamic> sortedWordsInLinesWithWhiteSpaces = [];
    readerBloc.getSelectionCallsForPage(pageIndex).listen((command) {
      // print(command.toString());
      if (command["click"] != null) {
        print("clicked yes from " + command["menu"] + " menu");
        if (command["click"] == "delete") clearHighlight();
      } else if (command["ebookPageRes"] != null) {
        // yes , no

      }
    });
    iw.setReaderPage(pageIndex);
    readerBloc.setParentRequest({"pageLanded": pageIndex});
    readerBloc.getPageAnnotations(pageIndex);
    if (mounted) {
      readerBloc.getSelectionCallsForPage(pageIndex).listen((command) {
        if (command == null) {
          // print("herew");
        } else {
          if (mounted) {
            if (command["ebookPageRes"] != null) {
              var ebookPageRes;

              if (command["page"] == iw.readerPageNumber) {
                print(command["page"].toString() +
                    " = " +
                    iw.readerPageNumber.toString());
                ebookPageRes = command["ebookPageRes"];
                //  run another isolate
                print("computation for page " +
                    pageIndex.toString() +
                    " started");
                runHeavyShit(ebookPageRes, readerBloc, context);
              }
            }
          }
        }
      });
    }
    // accountsRepo
    //     .getEbookPages(iw.hostName, "19c0d9cf1dbf7297", pageIndex)
    //     .then((ebookPageRes) {
    //   if (ebookPageRes["elements_v2"] is List<dynamic>) {
    //     if (mounted) {
    //       paragraphs.addAll(ebookPageRes["elements_v2"]);
    //     }
    //     // marginsList.add({
    //     //   "left": ebookPageRes["info"]["margins"][0],
    //     //   "top": ebookPageRes["info"]["margins"][1],
    //     //   "right": ebookPageRes["info"]["margins"][2],
    //     //   "bottom": ebookPageRes["info"]["margins"][3]
    //     // });

    //     paragraphs.forEach((paragraph) {
    //       if (paragraph["words"] != null) {
    //         if (mounted) {
    //           setState(() {
    //             words.addAll(paragraph["words"]);
    //           });
    //         }
    //       }
    //     });
    //     dynamic lastWord;
    //     // print(words.length);
    //     // print("words :: " + words.length.toString());
    //     List<dynamic> sortedWordsInLines = getSortedWordsInLines();
    //     // print("sortedWordsInLines :: " + sortedWordsInLines.length.toString());
    //     sortedWordsInLines.forEach((dynamic word) {
    //       // print("lWord : " + lastWord.toString());
    //       // print("word : " + word.toString());

    //       if (lastWord != null) {
    //         if (lastWord["boundary"] != null) {
    //           // print("lastWord X Right : " +
    //           //     lastWord["boundary"][0].toString());
    //           // if ((lastWord["boundary"][0] > word["boundary"][2])) {
    //           var delta = (lastWord["boundary"][0] - word["boundary"][2]);

    //           if (delta < 0) {
    //             delta = -1 * delta;
    //           }
    //           var deltaY = (lastWord["boundary"][1] - word["boundary"][1]);
    //           if (deltaY < 0) {
    //             deltaY = -1 * deltaY;
    //           }

    //           if ((deltaY < yThreshold)) {
    //             // print("deltaY : " +
    //             //     sortedWordsInLinesWithWhiteSpaces.length.toString() +
    //             //     ") " +
    //             //     deltaY.toString());
    //             if ((delta > 0) && (delta < threshold)) {
    //               // print("deltaX : " +
    //               //     sortedWordsInLinesWithWhiteSpaces.length.toString() +
    //               //     ") " +
    //               //     delta.toString());
    //               var tmp = word;
    //               var whiteSpaceFillerWord = word;
    //               // whiteSpaceFillerWord["boundary"][0] = word["boundary"][2];
    //               word["boundary"][2] = lastWord["boundary"][0];
    //               // print("word With white space : " + tmp.toString());
    //               // print("================================");

    //               tmp["whiteSpace"] = true;
    //               sortedWordsInLinesWithWhiteSpaces.add(tmp);
    //               lastWord = tmp;
    //             } else {
    //               sortedWordsInLinesWithWhiteSpaces.add(word);
    //               lastWord = word;
    //             }
    //             // }
    //           } else {
    //             // line change detect
    //             sortedWordsInLinesWithWhiteSpaces.add(word);
    //             lastWord = word;
    //           }
    //         } else {
    //           lastWord = word;
    //           sortedWordsInLinesWithWhiteSpaces.add(word);
    //         }
    //       } else {
    //         sortedWordsInLinesWithWhiteSpaces.add(word);
    //         lastWord = word;
    //       }
    //     });
    //     if (mounted) {
    //       setState(() {
    //         showWords = sortedWordsInLinesWithWhiteSpaces;
    //       });
    //       var z = 0;
    //       sortedWordsInLinesWithWhiteSpaces.forEach((element) {
    //         hiddens.add(false);
    //       });
    //       print("hiddens : " + hiddens.length.toString());
    //     }
    //   } else if (mounted) {
    //     setState(() {
    //       paragraphs.addAll(ebookPageRes["elements"]);
    //     });
    //   }
    //   // print(ebookPageRes);
    // });
  }

  Future<void> runHeavyShit(
      ebookPageRes, ReaderBloc readerBloc, BuildContext contex) async {
    final iw = MainStateContainer.of(context);
    String str = "";
    await runOcrDataComputations(
      ebookPage: ebookPageRes,
      readerBloc: readerBloc,
      execute: (data) {
        print("heavy computation end for " + iw.readerPageNumber.toString());
      },
    );

    // print(str);
  }

  dynamic paragraphsMapToWords = {};
  Future<dynamic> runOcrDataComputations({
    dynamic ebookPage,
    ReaderBloc readerBloc,
    Function(dynamic ebookPageRes) execute,
    int chunkLength = 25,
  }) {
    final threshold = 0.05;
    var yThreshold = 0.01;
    List<dynamic> sortedWordsInLinesWithWhiteSpaces = [];
    final completer = new Completer<dynamic>();
    Function(dynamic i) _exec;
    _exec = (ebookPageRes) {
      if (ebookPageRes == null) {
        print("loading");
      } else {
        if (ebookPageRes["elements_v2"] is List<dynamic>) {
          if (mounted) {
            paragraphs.addAll(ebookPageRes["elements_v2"]);
          }
          // marginsList.add({
          //   "left": ebookPageRes["info"]["margins"][0],
          //   "top": ebookPageRes["info"]["margins"][1],
          //   "right": ebookPageRes["info"]["margins"][2],
          //   "bottom": ebookPageRes["info"]["margins"][3]
          // });= 0
          var loghata = [];
          var loghatIndex = 0;
          var paraNumber = 0;
          if (mounted) {
            paragraphs.forEach((dynamic paragraph) {
              if (paragraph["words"] != null) {
                var wrdIndex = 0;
                paragraph["words"].forEach((wrd) {
                  wrd["parNumber"] = paraNumber;
                  wrd["indexInParNumber"] = wrdIndex;

                  if (wrdIndex == 0) {
                    paragraphsMapToWords[paraNumber.toString()] = {
                      "start": loghatIndex
                    };
                  } else if (wrdIndex == paragraph["words"].length - 1) {
                    paragraphsMapToWords[paraNumber.toString()]["end"] =
                        loghatIndex;
                  }
                  loghata.add(wrd);
                  wrdIndex++;
                  loghatIndex++;
                });

                paraNumber++;
              }
            });
          }

          dynamic lastWord;
          // print(words.length);
          // print("words :: " + words.length.toString());
          List<dynamic> sortedWordsInLines = getSortedWordsInLines(loghata);
          // print("sortedWordsInLines :: " + sortedWordsInLines.length.toString());
          sortedWordsInLines.forEach((dynamic word) {
            // print("lWord : " + lastWord.toString());
            // print("word : " + word.toString());

            if (lastWord != null) {
              if (lastWord["boundary"] != null) {
                // print("lastWord X Right : " +
                //     lastWord["boundary"][0].toString());
                // if ((lastWord["boundary"][0] > word["boundary"][2])) {
                var delta = (lastWord["boundary"][0] - word["boundary"][2]);

                if (delta < 0) {
                  delta = -1 * delta;
                }
                var deltaY = (lastWord["boundary"][1] - word["boundary"][1]);
                if (deltaY < 0) {
                  deltaY = -1 * deltaY;
                }

                if ((deltaY < yThreshold)) {
                  // print("deltaY : " +
                  //     sortedWordsInLinesWithWhiteSpaces.length.toString() +
                  //     ") " +
                  //     deltaY.toString());
                  if ((delta > 0) && (delta < threshold)) {
                    // print("deltaX : " +
                    //     sortedWordsInLinesWithWhiteSpaces.length.toString() +
                    //     ") " +
                    //     delta.toString());
                    var tmp = word;
                    var whiteSpaceFillerWord = word;
                    // whiteSpaceFillerWord["boundary"][0] = word["boundary"][2];
                    word["boundary"][2] = lastWord["boundary"][0];
                    // print("word With white space : " + tmp.toString());
                    // print("================================");

                    tmp["whiteSpace"] = true;
                    sortedWordsInLinesWithWhiteSpaces.add(tmp);
                    lastWord = tmp;
                  } else {
                    sortedWordsInLinesWithWhiteSpaces.add(word);
                    lastWord = word;
                  }
                  // }
                } else {
                  // line change detect
                  sortedWordsInLinesWithWhiteSpaces.add(word);
                  lastWord = word;
                }
              } else {
                lastWord = word;
                sortedWordsInLinesWithWhiteSpaces.add(word);
              }
            } else {
              sortedWordsInLinesWithWhiteSpaces.add(word);
              lastWord = word;
            }
          });
          if (mounted) {
            setState(() {
              showWords = sortedWordsInLinesWithWhiteSpaces;
            });
            var z = 0;
            sortedWordsInLinesWithWhiteSpaces.forEach((element) {
              hiddens.add(false);
            });
            print("hiddens : " + hiddens.length.toString());
          }
        } else if (mounted) {
          setState(() {
            paragraphs.addAll(ebookPageRes["elements"]);
          });
        }
        // print(ebookPageRes);
      }
    };
    _exec(ebookPage);
    return completer.future;
  }

  String findStartWord(detail, boxHeight, fromTopX, imgProcessedWidth,
      imgProcessedHeight, lineKey) {
    var selectedWord;
    var choseX = 2.0;
    var choseY = 2.0;
    if ((lineKey != null) && (lineWords[lineKey] != null)) {
      var x = 0;
      lineWords[lineKey].forEach((word) {
        var y1 =
            (((word["boundary"][1] * boxHeight) + fromTopX) + topExtraPadding);
        var y2 =
            (((word["boundary"][3] * boxHeight) + fromTopX) + topExtraPadding);

        var x1 = word["boundary"][0] * imgProcessedWidth;

        var x2 = word["boundary"][2] * imgProcessedWidth;

        if ((detail.globalPosition.dx > (x1 - choseX)) &&
            (detail.globalPosition.dx < (x2 + choseX)) &&
            ((detail.globalPosition.dy) > (y1 - choseY)) &&
            ((detail.globalPosition.dy) < (y2 + choseY))) {
          selectedWord = x.toString();

          // if ((lastSelectedLine != null) && (key != lastSelectedLine)) {
          //   // clean last
          //   var zz = lineWords[lastSelectedLine].length;
          //   for (var z = 0; z < zz; z++) {
          //     if (wordsMap[lastSelectedLine + "-" + z.toString()] != null) {
          //       wordsMap[lastSelectedLine + "-" + z.toString()]["color"] =
          //           Colors.blue.withOpacity(0.2);
          //     }
          //   }
          // }
          // setState(() {
          //   lastSelectedLine = key;
          // });
          // // color Selected Line Whole
          // var zz = lineWords[key].length;
          // for (var z = 0; z < zz; z++) {
          //   if (wordsMap[key + "-" + z.toString()] != null) {
          //     wordsMap[key + "-" + z.toString()]["color"] =
          //         Colors.amberAccent.withOpacity(0.3);
          //   }
          // }
        }
        x++;
      });
      return selectedWord;
    }
  }

  String findTouchedLine(
      detail, boxHeight, fromTopX, imgProcessedWidth, imgProcessedHeight) {
    String lineKey = null;
    lineWords.forEach((key, element) {
      List<dynamic> line = element;
      // print(element.toString());

      // print(detail.globalPosition.toString());
      if (line.length > 0) {
        var word = line[0];
        var y1 =
            (((word["boundary"][1] * boxHeight) + fromTopX) + topExtraPadding);
        var y2 =
            (((word["boundary"][3] * boxHeight) + fromTopX) + topExtraPadding);

        var x1 = word["boundary"][0] * imgProcessedWidth;

        var x2 = word["boundary"][2] * imgProcessedWidth;
        // print(y1.toString());
        // print(y2.toString());
        if (((detail.globalPosition.dy) < y2 + 2.0) &&
            ((detail.globalPosition.dy) > y1 - 2.0)) {
          print(key);
          lineKey = key;
        }
      } else {
        // print("line with no words");
      }
    });
    return lineKey;
  }

  void cleanSelect() {
    setState(() {
      lastSelectedLine = null;
      lastSelectedWordKey = null;
      endWordLine = null;
      endWordKey = null;
      firstTouch = null;
      endTouch = null;
      lastEndLine = null;
      endIndex = null;
      startIndex = null;
      saved = false;
    });
    for (var t = 0; t < showWords.length; t++) {
      if (showWords[t] != null) {
        setState(() {
          showWords[t]["color"] = Colors.transparent;
          readerBloc.setCommand(
            {"color": readerBloc.selectedColor, "page": pageIndex},
          );
        });
      }
    }
  }

  void saveSelect() {
    setState(() {
      saved = true;
      if ((endIndex != null) && (startIndex != null)) {
        readerBloc.setCommand({
          "select": {"end": endIndex, "start": startIndex}
        });
      }
    });
    // for (var t = 0; t < showWords.length; t++) {
    //   if (showWords[t] != null) {
    //     setState(() {
    //       showWords[t]["color"] = Colors.transparent;
    //     });
    //   }
    // }
  }

  List<dynamic> getSortedWordsInLines(wordsx) {
    var lineNumber = 0;
    dynamic linedWords = {};
    var yThreshold = 0.01;
    var xThreshold = 0.03;
    var lastWord;
    bool goNextLine = false;
    wordsx.forEach((word) {
      // print("lWord : " + lastWord.toString());
      // print("word : " + word.toString());

      if (lastWord != null) {
        if (lastWord["boundary"] != null) {
          // print("lastWord X Right : " +
          //     lastWord["boundary"][0].toString());
          // if ((lastWord["boundary"][0] > word["boundary"][2])) {
          var delta = (lastWord["boundary"][0] - word["boundary"][2]);
          if (delta < 0) {
            delta = -1 * delta;
          }
          var deltaY = (lastWord["boundary"][1] - word["boundary"][1]);
          if (deltaY < 0) {
            deltaY = -1 * deltaY;
          }
          // print("deltaY : " +
          //     linedWords.length.toString() +
          //     ") " +
          //     deltaY.toString());
          // print("================================");

          if (!(deltaY < yThreshold)) {
            lineNumber = lineNumber + 1;
            goNextLine = true;
          } else if (lastWord["boundary"][3] < word["boundary"][1]) {
            lineNumber = lineNumber + 1;
            goNextLine = true;
          }
        }
      }
      if (lineNumber == 0) {
        // print(":::::::");
        lineNumber++;
        linedWords[lineNumber.toString()] = [word];
        lastWord = word;
      } else {
        if (goNextLine) {
          lastWord = null;
          goNextLine = false;
        } else {
          lastWord = word;
        }
        if (linedWords[lineNumber.toString()] == null) {
          linedWords[lineNumber.toString()] = [word];
        } else {
          linedWords[lineNumber.toString()].add(word);
        }
      }
    });
    var tempWords = [];
    var lineIndex = -1;
    var ttm = 0;
    linedWords.forEach((s, d) {
      List t = d;
      // List<dynamic> x = d.sort(xComparator);
      var minTopY = 1.0;
      var maxBottomY = 0.0;
      var maxRightX = 0.0;
      var minLeftx = 1.0;
      var lst = [];
      t.forEach((element) {
        if (element["boundary"][0] < minLeftx) {
          minLeftx = element["boundary"][0];
        }
        if (element["boundary"][1] < minTopY) {
          minTopY = element["boundary"][1];
        }
        if (element["boundary"][2] > maxRightX) {
          maxRightX = element["boundary"][2];
        }
        if (element["boundary"][3] > maxBottomY) {
          maxBottomY = element["boundary"][3];
        }
      });
      var i = 0;
      t.forEach((element) {
        if (element["boundary"][1] > minTopY) {
          element["boundary"][1] = minTopY;
        }

        if (element["boundary"][3] < maxBottomY) {
          element["boundary"][3] = maxBottomY;
        }

        element["color"] = Colors.transparent;
        element["id"] = (s.toString() + "-" + i.toString());

        i++;
        // ttm++;
      });
      // y haro yeki mikonim tu wordaye ye line vase kaheshe mohasebe va khati shodan
      lineIndex = lineIndex + 1;

      t.sort((b, a) => a["boundary"][0].compareTo(b["boundary"][0]));
      if (t is List) {
        t.forEach((element) {
          wordsKeysList.add(element["id"]);

          element["index"] = (ttm);
          wordsMap[element["id"]] = element;
          tempWords.add(wordsMap[element["id"]]);
          // print("ttm * i) = " + ttm.toString());
          ttm++;
        });
      }
      // print(wordsMap.toString());
      // else
      // print("FFF");
      // print(t.toString());
    });
    if (mounted) {
      setState(() {
        lineWords = linedWords;
        showWords = tempWords;
      });
    }
    tempWords.forEach((item) {
      // print("sortedInanes : " + item.toString());
    });
    // print(linedWords.toString());
    return tempWords;
  }

  dynamic calculatePositions(
      pageSize,
      pageHeight,
      imgProcessedWidth,
      imgProcessedHeight,
      fromLeft,
      fromTop,
      leftX,
      topY,
      rightX,
      bottomY,
      width,
      height,
      ratio,
      boxWidth,
      boxHeight,
      paragraph) {
    if ((pageSize.width / pageHeight) > ratio) {
      fromLeft = (pageSize.width / 2 - boxWidth / 2);
      leftX = (paragraph["boundary"][0] * boxWidth);
      rightX = imgProcessedWidth - (paragraph["boundary"][2] * boxWidth);
      topY = (paragraph["boundary"][1] * pageHeight);
      width = (paragraph["boundary"][2] - paragraph["boundary"][0]) * boxWidth;
      height =
          (paragraph["boundary"][3] - paragraph["boundary"][1]) * pageHeight;
      bottomY = (pageHeight - (paragraph["boundary"][3] * pageHeight));
    } else {
      leftX = (paragraph["boundary"][0] * pageSize.width);
      fromTop = (pageHeight / 2 - boxHeight / 2);
      rightX = pageSize.width - (paragraph["boundary"][2] * pageSize.width);
      topY = (paragraph["boundary"][1] * boxHeight);
      width = ((paragraph["boundary"][2] - paragraph["boundary"][0]) *
          pageSize.width);
      height =
          (paragraph["boundary"][3] - paragraph["boundary"][1]) * boxHeight;
      bottomY = (pageHeight - (paragraph["boundary"][3] * boxHeight) + fromTop);
    }
    return {
      "left": leftX,
      "top": topY,
      "height": height,
      "width": width,
      "right": rightX,
      "bottom": bottomY,
    };
  }

  // @override
  // TODO: implement wantKeepAlive
  // bool get wantKeepAlive => true; // 12
}

const double _kRadius = 10;
const double _kBorderWidth = 3;

class MyPainter extends CustomPainter {
  MyPainter();

  @override
  void paint(Canvas canvas, Size size) {
    final rrectBorder =
        RRect.fromRectAndRadius(Offset.zero & size, Radius.circular(_kRadius));
    final rrectShadow =
        RRect.fromRectAndRadius(Offset(0, 3) & size, Radius.circular(_kRadius));

    final shadowPaint = Paint()
      ..strokeWidth = _kBorderWidth
      ..color = Colors.black
      ..style = PaintingStyle.stroke
      ..maskFilter = MaskFilter.blur(BlurStyle.normal, 2);
    final borderPaint = Paint()
      ..strokeWidth = _kBorderWidth
      ..color = Colors.green
      ..style = PaintingStyle.stroke;

    canvas.drawRRect(rrectShadow, shadowPaint);
    canvas.drawRRect(rrectBorder, borderPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

Color getColorObjectFromHex(hexString) {
  var l = hexString.split("#").join("");
  int colorHex = int.parse(l, radix: 16);
  return (new Color(colorHex));
}
