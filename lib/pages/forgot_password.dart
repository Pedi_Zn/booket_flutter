import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:booket_flutter/widgets/email_or_phone_switch.dart';
import 'package:flutter/material.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({Key key}) : super(key: key);

  @override
  ForgotPasswordState createState() => new ForgotPasswordState();
}

class ForgotPasswordState extends State<ForgotPassword>
    with SingleTickerProviderStateMixin {
  bool expandedSearch = false;
  bool forgotPasswordType = false;
  double logoOpacity = 1;
  double _width = 200;
  FocusNode searchFocus;
  bool loginType = false;

  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    this.searchFocus = new FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.orange[800],
          automaticallyImplyLeading: false,
          title: Container(
            width: MediaQuery.of(context).size.width,
            height: 60.0,
            child: Stack(
              children: [
                Positioned.fill(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    AnimatedContainer(
                      duration: const Duration(milliseconds: 250),
                      // width: this._width2,
                      // width: 60.0,
                      child: Center(
                        child: AnimatedOpacity(
                            duration: const Duration(milliseconds: 100),
                            opacity: logoOpacity,
                            child: IconButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              icon: Icon(
                                Icons.arrow_back_ios,
                                color: Colors.white,
                              ),
                            )),
                      ),
                    ),
                    AnimatedContainer(
                      duration: const Duration(milliseconds: 250),
                      // width: this._width2,
                      child: Center(
                          child: AnimatedOpacity(
                        duration: const Duration(milliseconds: 100),
                        opacity: logoOpacity,
                        // child: PlatformSvgWidget.asset('assets/images/booket_logo.svg',color: Colors.white, height: 18)
                      )),
                    ),
                  ],
                )),
                AnimatedPositioned(
                  width: this._width,
                  left: ((MediaQuery.of(context).size.width -
                          this._width -
                          30.0) /
                      2),
                  top: 10.5,
                  duration: const Duration(milliseconds: 250),
                  child: Container(
                    height: 38.0,
                    child: new TextField(
                      focusNode: searchFocus,
                      onEditingComplete: () {},
                      onSubmitted: (String s) {
                        searchFocus.unfocus();
                        setState(() {
                          this.logoOpacity = 1;
                          this.expandedSearch = false;
                          this._width = 200.0;
                        });
                      },
                      autofocus: false,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 15, color: Theme.of(context).primaryColor),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.orange,
                        hintText: DemoLocalizations.of(context).trans("Search"),
                        hintStyle: TextStyle(
                          color: Theme.of(context).primaryColor,
                        ),
                        contentPadding: EdgeInsets.only(left: 15, right: 10),
                        // border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0),
                            borderSide: BorderSide(
                                color: Theme.of(context)
                                    .accentColor
                                    .withOpacity(0.2))),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0),
                            borderSide: BorderSide(
                                color: Theme.of(context).accentColor)),
                        prefixIcon: Icon(
                          Icons.search,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ),
                  ),
                ),
                AnimatedPositioned(
                    width: this._width,
                    height: 55.0,
                    left: ((MediaQuery.of(context).size.width -
                            this._width -
                            30.0) /
                        2),
                    top: 10.5,
                    duration: const Duration(milliseconds: 250),
                    child: GestureDetector(
                      onTapDown: (s) {
                        setState(() {
                          this.logoOpacity = 0;
                          this.expandedSearch = true;
                          this._width =
                              (MediaQuery.of(context).size.width) < 800.0
                                  ? (MediaQuery.of(context).size.width) * 9 / 11
                                  : 500.0;
                        });
                        FocusScope.of(context).requestFocus(searchFocus);
                      },
                    )),
              ],
            ),
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: Column(
          children: [
            EmailOrPhoneSwitch(
                loginType: forgotPasswordType,
                title: DemoLocalizations.of(context).trans("ForgotPassword"),
                swich: (data) {
                  setState(() {
                    forgotPasswordType = data;
                  });
                }),
            Container(
              margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
              child: Column(
                children: [
                  Container(
                    // height: 200.0,
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 5.0),
                          child: Center(
                            child: Text(
                              !loginType
                                  ? DemoLocalizations.of(context).trans("Phone")
                                  : DemoLocalizations.of(context)
                                      .trans("Phone"),
                              style: TextStyle(
                                  color: Theme.of(context).primaryColorDark,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Container(
                          height: 40.0,
                          child: TextField(
                            keyboardType: TextInputType.number,
                            textAlign: TextAlign.center,
                            controller: phoneController,
                            autofocus: false,
                            style: TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontSize: 18.0,
                                fontWeight: FontWeight.normal),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(7.0),
                                  borderSide: BorderSide(
                                      color: Theme.of(context)
                                          .primaryColorDark
                                          .withOpacity(0.3))),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(7.0),
                                  borderSide: BorderSide(
                                      color:
                                          Theme.of(context).primaryColorDark)),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
                          child: Center(
                            child: Text(
                              DemoLocalizations.of(context).trans("Password"),
                              style: TextStyle(
                                  color: Theme.of(context).primaryColorDark,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Container(
                          height: 40.0,
                          child: TextField(
                            obscureText: true,
                            textAlign: TextAlign.center,
                            controller: passwordController,
                            autofocus: false,
                            style: TextStyle(
                                color: Theme.of(context).primaryColorDark,
                                fontSize: 18.0,
                                fontWeight: FontWeight.normal),
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              // hintText: DemoLocalizations.of(context).trans("Search"),
                              //   hintStyle: TextStyle(
                              //     color: Theme.of(context).primaryColor,
                              //   ),
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(7.0),
                                  borderSide: BorderSide(
                                      color: Theme.of(context)
                                          .primaryColorDark
                                          .withOpacity(0.3))),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(7.0),
                                  borderSide: BorderSide(
                                      color:
                                          Theme.of(context).primaryColorDark)),
                            ),
                          ),
                        ),
                        Container(
                          height: 25.0,
                        )
                      ],
                    ),
                  ),
                  RaisedButton(
                    onPressed: () {
                      // Navigator.of(context).pushNamed('/books/12343', arguments: RouteArgument(id:12, argumentsList: [true, "salam"]));
                    },
                    child: Container(
                        width: double.infinity,
                        height: 40.0,
                        child: Center(
                          child: Text(
                            DemoLocalizations.of(context).trans("Submit"),
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 15.0,
                                fontWeight: FontWeight.normal),
                            textAlign: TextAlign.center,
                          ),
                        )),
                    color: Theme.of(context).accentColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7.0),
                    ),
                  ),
                  /*Container(
                    height: 55.0,
                    // color: Colors.blue,
                    margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 27.0),
                    child: Center(
                      child: GestureDetector(
                              onTapDown: (s) {
                                setState(() {
                                  Navigator.of(context).pushNamed('/Login');
                                });
                              }, 
                              child: Text(
                                DemoLocalizations.of(context).trans("ForgotPassword"),
                                style: TextStyle(color: Theme.of(context).primaryColorDark, fontSize: 13.0, fontWeight: FontWeight.normal),
                                textAlign: TextAlign.center,
                              ),
                          ),
                    ),
                  ),
                  RaisedButton(
                    onPressed: () {
                      // Navigator.of(context).pushNamed('/books/12343', arguments: RouteArgument(id:12, argumentsList: [true, "salam"]));
                    },
                    child: Container(
                      width: double.infinity,
                      height: 40.0,
                      child: Center(
                        child: Text(
                          DemoLocalizations.of(context).trans("LoginWithGoogle"),
                          style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 15.0, fontWeight: FontWeight.normal),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ),
                    color: Theme.of(context).primaryColorDark,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7.0),
                    ),
                  ),
                  Container(height: 7.0,),
                  RaisedButton(
                    onPressed: () {
                      // Navigator.of(context).pushNamed('/books/12343', arguments: RouteArgument(id:12, argumentsList: [true, "salam"]));
                    },
                    child: Container(
                      width: double.infinity,
                      height: 40.0,
                      child: Center(
                        child: Text(
                          DemoLocalizations.of(context).trans("LoginWithApple"),
                          style: TextStyle(color: Theme.of(context).primaryColor, fontSize: 15.0, fontWeight: FontWeight.normal),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ),
                    color: Theme.of(context).primaryColorDark,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7.0),
                    ),
                  ),
                  Container(height: 7.0,),
                  RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed('/Register');
                    },
                    child: Container(
                      width: double.infinity,
                      height: 40.0,
                      child: Center(
                        child: Text(
                          DemoLocalizations.of(context).trans("HaveNoAccount"),
                          style: TextStyle(color: Theme.of(context).primaryColorDark, fontSize: 12.0, fontWeight: FontWeight.normal),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ),
                    color: Theme.of(context).dividerColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7.0),
                    ),
                  ),*/
                  Container(
                    height: 15.0,
                  ),
                ],
              ),
            )
          ],
        )));
  }
}
