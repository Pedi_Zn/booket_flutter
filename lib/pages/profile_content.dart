import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/widgets/text_field_widget.dart';
import 'package:flutter/material.dart';

import '../main_state_container.dart';

class ProfileContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    return Form(
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: [
                Container(
                  width: (MediaQuery.of(context).size.width - 80) * 1 / 3,
                  child: TextFieldWidget(
                    label: DemoLocalizations.of(context).trans("FirstName"),
                    text: iw.me["user"]["firstname"] != null
                        ? iw.me["user"]["firstname"]
                        : "",
                    onChanged: (s) {
                      iw.authenticationBloc.updateFirstname(s);
                    },
                  ),
                ),
                const SizedBox(width: 10),
                Container(
                  width: (MediaQuery.of(context).size.width - 80) * 2 / 3,
                  child: TextFieldWidget(
                    label: DemoLocalizations.of(context).trans("LastName"),
                    text: iw.me["user"]["lastname"] != null
                        ? iw.me["user"]["lastname"]
                        : "",
                    onChanged: (s) {
                      iw.authenticationBloc.updateLastName(s);
                    },
                  ),
                ),
              ],
            ),
            const SizedBox(height: 5),
            TextFieldWidget(
              label: DemoLocalizations.of(context).trans("Email"),
              text: iw.me["email"] != null ? iw.me["email"] : "",
              onChanged: (s) {
                iw.authenticationBloc.updateEmail(s);
              },
            ),
            const SizedBox(height: 5),
            TextFieldWidget(
              label: DemoLocalizations.of(context).trans("Phone"),
              enabled: false,
              filled: true,
              text: "09912037674",
              // onChanged: (email) {},
            ),
          ],
        ),
      ),
    );
  }
}
