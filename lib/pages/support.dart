import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';

class Support extends StatefulWidget {
  Support({Key key}) : super(key: key);

  @override
  SupportState createState() => SupportState();
}

class SupportState extends State<Support> {
  // AboutUs() {
  // }
  List<dynamic> logoList;

  @override
  void initState() {
    logoList = [
      {
        "img": "assets/images/call.svg",
        "title": "تماس باما",
      },
      {
        "img": "assets/images/whatsapp.svg",
        "title": "واتس اپ",
      },
      {
        "img": "assets/images/email.svg",
        "title": "ایمیل",
      },
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          pinned: true,
          centerTitle: true,
          iconTheme: IconThemeData(
            color: Theme.of(context).primaryColor, //change your color here
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                DemoLocalizations.of(context).trans("Support"),
                style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontWeight: FontWeight.normal,
                    fontSize: 18.0),
              ),
            ],
          ),
          expandedHeight: 230.0,
          backgroundColor: Theme.of(context).accentColor,
          flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              background: Column(
                children: <Widget>[
                  Container(
                    height: 80.0,
                  ),
                  PlatformSvgWidget.asset('assets/images/support.svg',
                      height: 188.0),
                ],
              )),
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return Container(
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 20.0),
                      alignment: Alignment.center,
                      child: Text(
                        DemoLocalizations.of(context).trans("SupportText1"),
                        textAlign: TextAlign.justify,
                        style: TextStyle(fontSize: 14.0),
                      ),
                    ),
                    Container(
                      height: 400.0,
                      color: Colors.transparent,
                      padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 20.0),
                      child: AnimationLimiter(
                        child: ListView.builder(
                          addAutomaticKeepAlives: true,
                          itemCount: logoList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return AnimationConfiguration.staggeredList(
                              position: index,
                              duration: const Duration(milliseconds: 375),
                              child: SlideAnimation(
                                child: FadeInAnimation(
                                  child: _buildSearchItems(logoList[index]),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    )
                  ],
                ),
              );
            },
            childCount: 1,
          ),
        ),
      ],
    ));
  }
}

Widget _buildSearchItems(item) {
  return ListTile(
    title: Text(
      item["title"],
      style: TextStyle(
        fontSize: 14.0,
      ),
    ),
    leading: PlatformSvgWidget.asset(
      item["img"],
      width: 35,
      height: 35,
    ),
    // trailing: Text(
    //   item["title"],
    //   style: TextStyle(
    //     fontSize: 14.0,
    //   ),
    // ),
    // trailing: PlatformSvgWidget.asset(
    //   item["img"],
    //   width: 35,
    //   height: 35,
    // ),
    onTap: () {},
  );
}
