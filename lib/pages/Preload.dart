import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/blocs/grid_card_bloc.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/models/route_argument.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:booket_flutter/tabs/library.dart';
import 'package:booket_flutter/tabs/profile.dart';
import 'package:booket_flutter/tabs_page2.dart';
import 'package:booket_flutter/widgets/expireDate.dart';
import 'package:booket_flutter/widgets/grid_or_list_switch.dart';
import 'package:booket_flutter/widgets/refresh_tab.dart';
import 'package:booket_flutter/widgets/tabs_page.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:booket_flutter/widgets/books.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:intl/intl.dart';
import 'package:flare_flutter/flare_actor.dart';

class Preload extends StatefulWidget {
  Preload({Key key}) : super(key: key);

  @override
  _PreloadState createState() => _PreloadState();
}

class _PreloadState extends State<Preload>
    with SingleTickerProviderStateMixin, AfterLayoutMixin {
  AccountsRepo accountsRepo = new AccountsRepo();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 500.0,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [fromHex('#FF7C09'), fromHex('#F88B2D')]),
      ),
      child: Center(
        child: PlatformSvgWidget.asset('assets/images/booket.svg',
            color: Colors.white, height: 100.0),
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    final iw = MainStateContainer.of(context);
    await Future.delayed(Duration(seconds: 3));
    final String token = await iw.authenticationBloc.getTokenFromStorage();

    if (token != null) {
      iw.setToken(token);
      iw.authenticationBloc.updateToken(token, 3);
      await accountsRepo
          .getProfile(
        iw.hostName,
        token,
      )
          .then((profile) async {
        if ((profile["statusCode"] != null) && (profile["statusCode"] >= 380)) {
          // accountsRepo.logout();
          iw.removeUser();
          iw.setToken(null);
          await iw.authenticationBloc.removeTokenFromStorage();
          Navigator.of(context).pushNamed('/Vitrine');
        } else {
          iw.updateUserInfo(profile);
          if (iw.me["hasbook"] != null) {
            Navigator.of(context).pushNamed('/Library');
          } else {
            Navigator.of(context).pushNamed('/Vitrine');
          }
        }
      });
      await accountsRepo
          .getBasket(
        iw.hostName,
        token,
      )
          .then((basketReq) {
        setState(() {
          iw.setCart(basketReq);
        });
      });
      await accountsRepo.getDegrees(iw.hostName).then((degreesRes) {
        iw.setDegreesList(degreesRes);
      });
      await accountsRepo.getUniversities(iw.hostName).then((universitiesRes) {
        iw.setUniversitiesList(universitiesRes);
      });
      await accountsRepo.getStudyFields(iw.hostName).then((studyFieldRes) {
        iw.setStudyFieldsList(studyFieldRes);
      });
      await accountsRepo.getCities(iw.hostName).then((citiesRes) {
        iw.setCitiesList(citiesRes);
      });
    } else {
      if (await iw.authenticationBloc.getFirstTimeFromStorage() == "false") {
        Navigator.push(
            context,
            PageRouteBuilder(
                transitionDuration: Duration(milliseconds: 500),
                transitionsBuilder: (context, animation, animationTime, child) {
                  animation = CurvedAnimation(
                      parent: animation, curve: Curves.decelerate);
                  return ScaleTransition(
                    alignment: Alignment.center,
                    child: child,
                    scale: animation,
                  );
                },
                pageBuilder: (context, animation, animationTime) {
                  return TabsPage(pageIndex: 1);
                }));
      } else {
        Navigator.of(context).popAndPushNamed('/Intro');
      }
    }
  }

  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
