import 'package:after_layout/after_layout.dart';
import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/pages/content_card.dart';
import 'package:flutter/material.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> with AfterLayoutMixin {
  bool loader = false;
  AccountsRepo accountsRepo = new AccountsRepo();

  Widget textfield({@required hintText}) {
    return Material(
      elevation: 4,
      shadowColor: Colors.grey,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextField(
        decoration: InputDecoration(
            hintText: hintText,
            hintStyle: TextStyle(
              letterSpacing: 2,
              color: Colors.black54,
              fontWeight: FontWeight.bold,
            ),
            fillColor: Colors.white30,
            filled: true,
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide.none)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColorDark, //change your color here
        ),
        backgroundColor: Theme.of(context).accentColor,
        elevation: 0.0,
        // centerTitle: true,
        title: new Text(
          DemoLocalizations.of(context).trans("EditProfile"),
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ),
      body: Stack(
        alignment: Alignment.center,
        children: [
          CustomPaint(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
            painter: HeaderCurvedContainer(context),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.all(10.0),
                margin: EdgeInsets.only(top: 40.0),
                width: 120.0,
                height: 120.0,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: 3),
                  shape: BoxShape.circle,
                  color: Colors.white,
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage('assets/images/pedi.jpg'),
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            top: 150.0,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Text(
                iw.me["title"],
                style: TextStyle(
                  fontSize: 20,
                  // letterSpacing: 1.5,
                  color: Theme.of(context).primaryColorDark,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
          Positioned.fill(
            child: Padding(
              padding: EdgeInsets.only(
                  bottom: 60.0,
                  top: MediaQuery.of(context).padding.top + 200.0),
              child: new Column(
                children: <Widget>[
                  Expanded(child: ContentCard()),
                ],
              ),
            ),
          ),
          Positioned(
            top: 130,
            right: 120,
            child: Container(
              height: 30.0,
              child: CircleAvatar(
                backgroundColor: Colors.orange[600],
                child: IconButton(
                  icon: Icon(
                    Icons.edit,
                    color: Colors.white,
                    size: 15.0,
                  ),
                  onPressed: () {},
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 15.0,
            // left: 0.0,
            child: RaisedButton(
              onPressed: () async {
                editProfile(context);
                // print(iw.me.toString());
                // _showBasicsFlash();
              },
              child: Container(
                  width: MediaQuery.of(context).size.width - 70,
                  height: 40.0,
                  child: Center(
                    child: !loader
                        ? Text(
                            DemoLocalizations.of(context).trans("Save"),
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 15.0,
                                fontWeight: FontWeight.normal),
                            textAlign: TextAlign.center,
                          )
                        : Container(
                            width: 50.0,
                            child: Image.asset(
                                "assets/images/loading_book_white.gif")),
                  )),
              color: Theme.of(context).accentColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(7.0),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    final iw = MainStateContainer.of(context);
    iw.authenticationBloc.updateFirstname(iw.me["user"]["firstname"]);
    iw.authenticationBloc.updateLastName(iw.me["user"]["lastname"]);
    iw.authenticationBloc.updateEmail(iw.me["email"]);
    iw.authenticationBloc.updateAddress(iw.me["address"]);
    iw.authenticationBloc.updateCity(iw.me["user"]["city"]);
    iw.authenticationBloc.updatePostCode(iw.me["postal_code"].toString());
    iw.authenticationBloc.updateUniversity(iw.me["user"]["university"]);
    iw.authenticationBloc.updateDegree(iw.me["user"]["degree"]);
    iw.authenticationBloc.updateStudyField(iw.me["user"]["study_field"]);
  }

  Future<void> editProfile(context) async {
    setState(() {
      loader = true;
    });
    final iw = MainStateContainer.of(context);
    var res = await iw.authenticationBloc.updateProfile(iw.hostName, iw.token);
    iw.updateUserInfo(res);
    setState(() {
      loader = false;
    });
  }
}

class HeaderCurvedContainer extends CustomPainter {
  final BuildContext context;
  HeaderCurvedContainer(this.context);

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..color = Theme.of(context).accentColor;
    Path path = Path()
      ..relativeLineTo(0, 40)
      ..quadraticBezierTo(size.width / 2, 200, size.width, 40)
      ..relativeLineTo(0, -40)
      ..close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
