import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/models/route_argument.dart';
import 'package:booket_flutter/pages/Preload.dart';
import 'package:booket_flutter/pages/about_us.dart';
import 'package:booket_flutter/pages/books_page.dart';
import 'package:booket_flutter/pages/contact_us.dart';
import 'package:booket_flutter/pages/edit_profile.dart';
import 'package:booket_flutter/pages/intro_slider.dart';
import 'package:booket_flutter/pages/pin_code.dart';
import 'package:booket_flutter/pages/reader.dart';
import 'package:booket_flutter/pages/reader2.dart';
import 'package:booket_flutter/pages/terms.dart';
import 'package:booket_flutter/pages/shopping_cart.dart';
import 'package:booket_flutter/pages/support.dart';
import 'package:booket_flutter/pages/forgot_password.dart';
import 'package:booket_flutter/pages/login.dart';
import 'package:booket_flutter/pages/register.dart';
import 'package:booket_flutter/pages/test.dart';
import 'package:booket_flutter/pages/settings_page.dart';
import 'package:booket_flutter/widgets/tabs_page.dart';

import 'package:booket_flutter/widgets/widget_test.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'Authorization/login/login.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(
    RouteSettings settings,
  ) {
    // Getting arguments passed in while calling Navigator.pushNamed
    var args = settings.arguments;
    final AccountsRepo accountRepository = AccountsRepo();

    switch (settings.name) {
      case '/':
        return MyCustomRoute(builder: (_) => Preload(), settings: settings);
      // case '/':
      //   return MyCustomRoute(builder: (_) => InlineExample());
      case '/Reader':
        return MyCustomRoute(builder: (_) => Reader(args as RouteArgument));
      case '/Test':
        return MyCustomRoute(builder: (_) => HomePagee());
      case '/Setting':
        return MyCustomRoute(builder: (_) => SettingsPage());
      case '/Test3':
        return MyCustomRoute(builder: (_) => LoginScreen());
      case '/Testt':
        return MyCustomRoute(builder: (_) => XGestureExample());
      case '/Intro':
        return MyCustomRoute(builder: (_) => IntroScreen());
      case '/Login':
        return MyCustomRoute(builder: (_) => Login());
      case '/Pincode':
        return MyCustomRoute(
            builder: (_) => PinCodeVerificationScreen(args as RouteArgument));
      case '/Register':
        return MyCustomRoute(builder: (_) => Register());
      case '/ForgotPassword':
        return MyCustomRoute(builder: (_) => ForgotPassword());
      case '/EditProfile':
        return MyCustomRoute(builder: (_) => EditProfile());
      case '/ShoppingCart':
        return MyCustomRoute(builder: (_) => ShoppingCart());
      case '/Support':
        return MyCustomRoute(builder: (_) => Support());
      case '/Terms':
        return MyCustomRoute(builder: (_) => Terms());
      case '/ContactUs':
        return MyCustomRoute(builder: (_) => ContactUs());
      case '/AboutUs':
        return MyCustomRoute(builder: (_) => AboutUs());
      case '/ResetPassword':
      // return MyCustomRoute(
      //     builder: (_) => ResetPasswordWidget(args as RouteArgument),
      //     settings: settings);

      default:
        if (settings.name.startsWith("/Vitrine") ||
            settings.name.startsWith("/Library") ||
            settings.name.startsWith("/Profile")) {
          var tab = settings.name.startsWith("/Vitrine")
              ? 2
              : (settings.name.startsWith("/Library")
                  ? 1
                  : (settings.name.startsWith("/Profile") ? 0 : 2));
          if (tab > -1) {
            return MyCustomRoute(
                builder: (_) => TabsPage(
                      pageIndex: tab,
                    ),
                settings: settings);
            // builder: (_) => Salam(
            //       pageIndex: tab,
            //     ),
            // settings: settings);
          }
        } else {
          if (settings.name.startsWith("/books/")) {
            var token = settings.name.split("/books/")[1];

            return MyCustomRoute(
              builder: (_) =>
                  BooksPage(args: args as RouteArgument, token: token),
              settings: settings,
            );
          }
          // If there is no such named route in the switch statement, e.g. /third
          return _errorRoute();
        }
    }
  }

  static Route<dynamic> _errorRoute() {
    return MyCustomRoute(
        builder: (_) {
          return Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: true,
              leading: IconButton(
                onPressed: () {
                  Navigator.of(_).popAndPushNamed("/Vitrine", arguments: 1);
                },
                icon: Icon(Icons.arrow_back),
              ),
              title: Text('Error'),
            ),
            body: Center(
              child: Text('Not Found 404'),
            ),
          );
        },
        settings: RouteSettings(name: "404"));
  }
}

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    // if (settings.isInitialRoute)
    if (kIsWeb) {
      return child;
    } else {
      // Fades between routes. (If you don't want any animation,
      // just return child.)
      return new FadeTransition(
        child: child,
        opacity: animation,
        alwaysIncludeSemantics: true,
      );
    }
  }
}
