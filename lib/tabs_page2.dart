// import 'package:badges/badges.dart';
import 'package:booket_flutter/blocs/tabs_bloc.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/pages/login.dart';
import 'package:booket_flutter/pages/register.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:icon_badge/icon_badge.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

import 'tabs/library.dart';
import 'tabs/profile.dart';
import 'tabs/vitrine.dart';

// import 'package:badge/badge.dart';

class TabsPage2 extends StatefulWidget {
  final int pageIndex;
  const TabsPage2({Key key, this.pageIndex}) : super(key: key);

  @override
  TabsPageState createState() => new TabsPageState(this.pageIndex);
}

class TabsPageState extends State<TabsPage2>
    with SingleTickerProviderStateMixin {
  TabsBloc tabsBloc;
  bool expandedSearch = false;
  double logoOpacity = 1;
  double _width = 200;
  FocusNode searchFocus;
  TabController _tabController;
  int pageIndex;

  TabsPageState(this.pageIndex);

  @override
  void initState() {
    this.searchFocus = new FocusNode();
    tabsBloc = new TabsBloc(this.pageIndex);
    _tabController =
        TabController(length: 3, initialIndex: this.pageIndex, vsync: this);
    _tabController.addListener(() {
      if (_tabController.indexIsChanging)
        ;
      // Tab Changed tapping on new tab
      else if (_tabController.index != _tabController.previousIndex)
        tabsBloc.setIndex(_tabController.index);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange[800],
        automaticallyImplyLeading: false,
        title: Container(
          width: MediaQuery.of(context).size.width,
          height: 60.0,
          child: Stack(
            children: [
              Positioned.fill(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 250),
                    // width: this._width2,
                    // width: 60.0,
                    child: Center(
                      child: AnimatedOpacity(
                        duration: const Duration(milliseconds: 100),
                        opacity: logoOpacity,
                        child: StreamBuilder<int>(
                            stream: tabsBloc.getTabsStream(),
                            builder: (context, snapshot) {
                              if (snapshot.data != null) {
                                return Text(setTitle(snapshot.data),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.w200));
                              } else {
                                return Container();
                              }
                            }),
                      ),
                    ),
                  ),
                  // SizedBox(width: 30.0),

                  // SizedBox(width: 30.0),
                  AnimatedContainer(
                    duration: const Duration(milliseconds: 250),
                    // width: this._width2,
                    child: Center(
                        child: AnimatedOpacity(
                      duration: const Duration(milliseconds: 100),
                      opacity: logoOpacity,
                      // child: PlatformSvgWidget.asset('assets/images/booket_logo.svg',
                      //   color: Colors.white,
                      //   height: 18
                      // )
                    )),
                  ),
                ],
              )),
              AnimatedPositioned(
                width: this._width,
                left:
                    ((MediaQuery.of(context).size.width - this._width - 30.0) /
                        2),
                top: 10.5,
                duration: const Duration(milliseconds: 250),
                child: Container(
                  height: 38.0,
                  child: new TextField(
                    focusNode: searchFocus,
                    onEditingComplete: () {},
                    onSubmitted: (String s) {
                      searchFocus.unfocus();
                      setState(() {
                        this.logoOpacity = 1;
                        this.expandedSearch = false;
                        this._width = 200.0;
                      });
                    },
                    autofocus: false,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 15, color: Theme.of(context).primaryColor),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.orange,
                      hintText: DemoLocalizations.of(context).trans("Search"),
                      hintStyle: TextStyle(
                        color: Theme.of(context).primaryColor,
                      ),
                      contentPadding: EdgeInsets.only(left: 15, right: 10),
                      // border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0),
                          borderSide: BorderSide(
                              color: Theme.of(context)
                                  .accentColor
                                  .withOpacity(0.2))),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0),
                          borderSide:
                              BorderSide(color: Theme.of(context).accentColor)),
                      prefixIcon: Icon(
                        Icons.search,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                ),
              ),
              AnimatedPositioned(
                  width: this._width,
                  height: 55.0,
                  left: ((MediaQuery.of(context).size.width -
                          this._width -
                          30.0) /
                      2),
                  top: 10.5,
                  duration: const Duration(milliseconds: 250),
                  child: GestureDetector(
                    onTapDown: (s) {
                      setState(() {
                        this.logoOpacity = 0;
                        this.expandedSearch = true;
                        this._width =
                            (MediaQuery.of(context).size.width) < 800.0
                                ? (MediaQuery.of(context).size.width) * 9 / 11
                                : 500.0;
                      });
                      FocusScope.of(context).requestFocus(searchFocus);
                    },
                  )),
            ],
          ),
        ),
        centerTitle: true,
      ),
      body: TabBarView(
        dragStartBehavior: DragStartBehavior.start,
        controller: _tabController,
        physics: NeverScrollableScrollPhysics(),
        children: [
          Profile(),
          Library(),
          ///////////////////////////////////////////////////////////////////
          Vitrine(),
          ///////////////////////////////////////////////////////////////////
        ],
      ),
      bottomNavigationBar: SizedBox(
        child: new TabBar(
          controller: _tabController,
          onTap: (i) {
            tabsBloc.setIndex(i);
            setState(() {
              pageIndex = i;
            });
          },
          tabs: [
            Container(
              // color: Colors.red,
              width: 50.0,
              child: Stack(
                children: [
                  Container(
                    padding: EdgeInsets.only(right: 8.0),
                    height: 60,
                    child: Tab(
                      icon: Icon(Octicons.person, size: 33),
                    ),
                  ),
                  pageIndex != 0 &&
                          iw.cart != null &&
                          iw.cart["orders"].length > 0
                      ? Positioned(
                          bottom: 2,
                          left: 0,
                          child: Stack(
                            children: [
                              Icon(
                                MaterialIcons.work,
                                size: 24,
                                color: Colors.orange,
                              ),
                              Positioned.fill(
                                // bottom: 4,
                                // left: 0,
                                top: 2.0,
                                right: 0.5,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      DemoLocalizations.of(context).transNumber(
                                          iw.cart["orders"].length.toString()),
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: Colors.white,
                                          fontWeight: FontWeight.normal),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        )
                      : Container(height: 0),
                ],
              ),
            ),
            SizedBox(
              height: 60,
              child: Tab(
                icon: Icon(
                  Ionicons.ios_book,
                  size: 32.0,
                ),
              ),
            ),
            SizedBox(
              height: 60,
              child: Tab(
                icon: Icon(MaterialCommunityIcons.store, size: 32.0),
              ),
            ),
          ],
          labelColor: Theme.of(context).accentColor,
          unselectedLabelColor: Colors.black,
          indicatorSize: TabBarIndicatorSize.label,
          indicatorPadding: EdgeInsets.fromLTRB(2, 0, 2, 10),
          indicatorColor: Theme.of(context).accentColor,
          indicatorWeight: 5.0,
        ),
      ),
    );
  }

  Widget buildFloatingSearchBar(context) {
    final isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;

    return FloatingSearchBar(
      iconColor: Theme.of(context).primaryColor,
      scrollPadding: const EdgeInsets.only(top: 20, bottom: 56),
      borderRadius: BorderRadius.circular(50),
      title: Container(
        child: Text(
          DemoLocalizations.of(context).trans("Search"),
          textAlign: TextAlign.center,
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
      ),
      height: 45.0,
      controller: FloatingSearchBarController(),
      transitionCurve: Curves.easeInOut,
      backgroundColor: Colors.orange,
      // physics: const BouncingScrollPhysics(),
      axisAlignment: isPortrait ? 0.0 : 0.0,
      openAxisAlignment: 0.0,
      openWidth: 300,
      width: isPortrait ? 200 : 500,
      debounceDelay: const Duration(milliseconds: 200),
      onQueryChanged: (query) {},

      // Specify a custom transition to be used for
      // animating between opened and closed stated.
      transition: CircularFloatingSearchBarTransition(),
      actions: [
        // FloatingSearchBarAction(
        //   showIfOpened: false,
        //   child: CircularButton(
        //     icon: const Icon(Icons.place),
        //     onPressed: () {},
        //   ),
        // ),
        FloatingSearchBarAction.searchToClear(
          showIfClosed: false,
          color: Theme.of(context).primaryColor,
        ),
      ],
      builder: (context, transition) {
        return ClipRRect(
          borderRadius: BorderRadius.circular(7),
          child: Material(
            color: Colors.white,
            elevation: 4.0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: Colors.accents.map((color) {
                return Container(height: 112, color: color);
              }).toList(),
            ),
          ),
        );
      },
    );
  }

  String setTitle(int index) {
    if (index == 0)
      return DemoLocalizations.of(context).trans("Profile");
    else if (index == 1)
      return DemoLocalizations.of(context).trans("Library");
    else
      return DemoLocalizations.of(context).trans("Home");
  }
}
