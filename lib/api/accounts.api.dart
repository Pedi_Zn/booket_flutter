import 'dart:convert';
import 'dart:io';
import 'package:booket_flutter/api/api.dart';

class AccountsRepo {
  AccountsRepo();

  Future<dynamic> login(url, body) async {
    // print(url + "/api/auth/login/");
    var res = await Api.post(url + "/api/auth/login/", body, 20);
    if (res != null) {
      try {
        var jsonResponse = json.decode(res.body);
        if (jsonResponse[0] == null &&
            jsonResponse.containsKey("auth") != null) {
          // jsonResponse;
          // if (jsonResponse["access_token"]!=null) {
          return jsonResponse;
          // } else {
          //   print("Request failed with status: ${res.statusCode}.");
          //   return token;
          // }
        } else if (jsonResponse[0]["message"] != null) {
          return jsonResponse[0];
        }
      } catch (exeption) {
        print("shit");
        return {
          "message": "خطایی رخ داد " + res.statusCode.toString(),
          "statusCode": res.statusCode
        };
      }
    } else {
      return null;
    }

// Add in username and password to requestvar url = 'http://example.com/whatsit/create';
  }

  Future<dynamic> register(url, body) async {
    Api.removeCookie("Content-Type");
    Api.setHeader("Content-Type", "application/json;charset=utf-8");
    // Api.setHeader('Accept', '*/*');
    // Api.setHeader('Accept-Encoding', 'gzip, deflate, br');
    // Api.setHeader('Connection', 'keep-alive');
    // Api.removeCookie("Platform");
    Api.setHeader("MacId", "mac-id-test");
    var res = await Api.post(url + "/api/auth/register", jsonEncode(body), 20);
    if (res != null) {
      try {
        var jsonResponse = json.decode(res.body);
        if (jsonResponse[0] == null &&
            jsonResponse.containsKey("auth") != null) {
          // jsonResponse;
          // if (jsonResponse["access_token"]!=null) {
          return jsonResponse;
          // } else {
          //   print("Request failed with status: ${res.statusCode}.");
          //   return token;
          // }
        } else if (jsonResponse[0]["message"] != null) {
          return jsonResponse[0];
        }
      } catch (exeption) {
        print("shit");
        return {
          "message": "خطایی رخ داد " + res.statusCode.toString(),
          "statusCode": res.statusCode
        };
      }
    } else {
      return null;
    }
  }

  Future<dynamic> registerV2(url, body) async {
    var res = await Api.post(url + "/api/v1/register/v2", body, 20);
    if (res != null) {
      try {
        var jsonResponse = json.decode(res.body);
        if (jsonResponse[0] == null &&
            jsonResponse.containsKey("token") != null) {
          return jsonResponse;
        } else if (jsonResponse[0]["message"] != null) {
          return jsonResponse[0];
        }
      } catch (exeption) {
        return {
          "message": "خطایی رخ داد " + res.statusCode.toString(),
          "statusCode": res.statusCode
        };
      }
    } else {
      return null;
    }
  }

  Future<dynamic> verifyAndRegister(url, body) async {
    var res = await Api.post(url + "/api/v1/users/verify", body, 20);
    var jsonResponse = json.decode(res.body);
    try {
      var jsonResponse = json.decode(res.body);
      if (jsonResponse[0] == null &&
          jsonResponse.containsKey("token") != null) {
        Api.setHeader("Authorization", "Bearer " + jsonResponse["token"]);
        // jsonResponse;
        // if (jsonResponse["access_token"]!=null) {
        if (jsonResponse.containsKey("token")) {
          Api.setCookie(jsonResponse["token"]);
        }
        return jsonResponse;
        // } else {
        //   print("Request failed with status: ${res.statusCode}.");
        //   return token;
        // }
      } else if (jsonResponse[0]["message"] != null) {
        return jsonResponse[0];
      }
    } catch (exeption) {
      return {
        "message": res.body.toString().toLowerCase().contains("invalid verif")
            ? "کد ارسالی اشتباه است"
            : "خطایی رخ داد " + res.statusCode.toString(),
        "statusCode": res.statusCode
      };
    }
    return jsonResponse;
  }

  Future<dynamic> verifyPhone(url, body) async {
    var res = await Api.post(url + "/api/v1/users/verifyPhone", body, 10);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getVitrine(url) async {
    var res = await Api.get(url + "/api/bookstore/vitrines");
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getVitrinePaginate(url, page) async {
    var res = await Api.get(url + "?pagination=12&page=" + page);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getSearchPaginate(url, page) async {
    var res = await Api.get(url + "?pagination=12&page=" + page);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getProfile(url, token) async {
    Api.setCookie(token);
    var res = await Api.get(url + "/api/profile");
    dynamic parsedJson = Api.parseResponseBody(res);
    if (res.statusCode == 401) {
      Api.removeCookie("Authorization");
    }
    return parsedJson;
  }

  Future<dynamic> getBasket(url, token) async {
    Api.setCookie(token);
    var res = await Api.get(url + "/api/shopping-cart");
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> search(url, query) async {
    var res = await Api.get(url + "/api/books/search?q=" + query);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getDegrees(url) async {
    var res = await Api.get(url + "/api/degrees");
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getUniversities(url) async {
    var res = await Api.get(url + "/api/universities");
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getStudyFields(url) async {
    var res = await Api.get(url + "/api/study-fields");
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getCities(url) async {
    var res = await Api.get(url + "/api/cities");
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getLibrary(url, token) async {
    Api.setCookie2(token);
    Api.setHeader("MacId", "WEB-dd09bc1aebdc4e94339e4e8a022b258c");
    var res = await Api.get(url + "/api/library");
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> checkCoupon(url, token, body) async {
    Api.setCookie(token);
    Api.setHeader("Platform", Platform.operatingSystem);
    Api.removeCookie('Content-Type');
    // Api.setHeader('Accept-Encoding', 'gzip, deflate, br');
    // Api.setHeader('Connection', 'keep-alive');
    Api.setHeader("MacId", "WEB-dd09bc1aebdc4e94339e4e8a022b258c");
    print(Api.headers);
    var res =
        await Api.put(url + "/api/shopping-cart/checkout-coupon", body, 30);
    dynamic parsedJson = Api.parseResponseBody(res);
    Api.setHeader('Content-Type', 'application/json');
    return parsedJson;
  }

  Future<dynamic> getBook(url, token) async {
    var res = await Api.get(url + "/api/books/" + token);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getEBook(url, bookToken, token) async {
    Api.setHeader("MacId", "mac-id-test");
    Api.setCookie(token);
    var res = await Api.get(url + "/api/e-books/" + bookToken + "/v2");
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getAllAnnotation(url, bookToken, token) async {
    Api.setHeader("MacId", "mac-id-test");
    Api.setCookie(token);
    var res = await Api.get(url + "/api/e-books/" + bookToken + "/annotations");
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getAnnotation(url, bookToken, pageIndex, token) async {
    Api.setHeader("MacId", "mac-id-test");
    Api.setCookie(token);
    var res = await Api.get(url +
        "/api/e-books/" +
        bookToken +
        "/pages/" +
        pageIndex.toString() +
        "/annotations");
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getEbookPages(url, bookToken, pageIndex, token) async {
    Api.setHeader("MacId", "mac-id-test");
    Api.setCookie(token);
    var res = await Api.get(url +
        "/api/e-books/" +
        bookToken +
        "/pages/v2/" +
        pageIndex.toString());
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getMoreBook(url, token) async {
    var res = await Api.get(url + "/api/books/vitrine/" + token);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> addToBasket(url, token, body) async {
    // Api.setCookie(token);
    Api.setHeader('Content-Type', 'application/json;charset=UTF-8');
    // Api.setHeader('Accept-Encoding', 'gzip, deflate, br');
    // Api.setHeader('Connection', 'keep-alive');
    print(Api.headers.toString());
    var res = await Api.post(url + "/api/shopping-cart/add", body, 30);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> removeFromBasket(url, token, body) async {
    // Api.setCookie(token);
    Api.setHeader("MacId", "WEB-dd09bc1aebdc4e94339e4e8a022b258c");

    // Api.setHeader('Accept-Encoding', 'gzip, deflate, br');
    // Api.setHeader('Connection', 'keep-alive');
    print(Api.headers.toString());
    Api.removeCookie("Content-Type");
    var res =
        await Api.post(url + "/api/shopping-cart/remove/" + token, body, 30);
    Api.setHeader('Content-Type', 'application/json;charset=UTF-8');
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> resetPassword(url, body) async {
    var res = await Api.post(url + "/api/v1/users/resetPassword", body, 30);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> forgotPassword(url, body) async {
    var res = await Api.post(url + "/api/v1/users/resetPassword", body, 30);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> sendPhoneVerification(url, body) async {
    var res = await Api.post(url + "/api/auth/phone/verify", body, 30);
    // Api.setHeader("Content-Type", "application/json;charset=UTF-8");
    // Api.setHeader("MacId", "mac-id-test");
    // Api.setHeader("Platform", "android");
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> getShipments(url, filter) async {
    String reqUrl = url + "/api/v1/users/shipment?";
    if (filter["type"] != null) {
      reqUrl += ("&status=" + filter["type"]);
    }
    if (filter["page"] != null) {
      reqUrl += "&page=" + filter["page"].toString();
    }
    if (filter["limit"] != null) {
      reqUrl += "&limit=" + filter["limit"].toString();
    }
    var res = await Api.get(reqUrl);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> addToWishList(url, id) async {
    var res = await Api.post(
        url + "/api/v1/users/addToWishList", {"productId": id}, 30);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> removeFromWishList(url, id) async {
    var res = await Api.post(
        url + "/api/v1/users/removeFromWishList", {"productId": id}, 30);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> clearWishList(url) async {
    var res = await Api.post(url + "/api/v1/users/clearWishList", {}, 30);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> addAddress(url, dynamic d) async {
    var res = await Api.post(url + "/api/v1/users/address", d, 30);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> updateProfile(url, token, dynamic d) async {
    Api.setCookie(token);
    Api.setHeader('Content-Type', 'application/json;charset=UTF-8');
    Api.setHeader('Accept-Encoding', 'gzip, deflate, br');
    Api.setHeader('Content-Type', 'application/json');
    var res = await Api.put(url + "/api/profile", d, 30);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> removeAddress(url, id) async {
    var res = await Api.post(
        url + "/api/v1/users/address/remove", {"address_id": id}, 30);
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<dynamic> uploadProfilePic(url, base64Avatar, imageFormat) async {
    if (imageFormat == "jpg") {
      base64Avatar = "data:image/jpeg;base64," + base64Avatar;
    } else {
      base64Avatar = "data:image/$imageFormat;base64," + base64Avatar;
    }
    var res = await Api.post(
      url + "/api/v1/users/upload/profPics",
      {"avatar": base64Avatar, "format": imageFormat},
      100,
    );
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  //  Future<dynamic> clearAddreses(url) async{
  //    var res = await Api.post(url+"/api/v1/users/clearWishList",{},30);
  //    dynamic parsedJson = Api.parseResponseBody(res);
  //    return parsedJson;
  //  }

  Future<Map> getMe(url) async {
    var res = await Api.get(url + "/api/v1/users/profile");
    dynamic parsedJson = Api.parseResponseBody(res);
    return parsedJson;
  }

  Future<Map> checkIsTaken(url, body) async {
    var res = await Api.post(url, body, 30);
    var jsonResponse;
    var result = (res != null && res.body == "true");
    if (!result) {
      if (res != null) {
        jsonResponse = json.decode(res.body);
        if (jsonResponse['result'] == false) {
          print("key : $body.");
          return jsonResponse;
        } else {
          return jsonResponse;
        }
      } else {
        // Future not completed (timeout)
        return {"result": true};
      }
    } else {
      return {"result": true};
    }
  }

  Future<void> logout() async {
    Api.removeCookies();
  }
}
