import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class Api {
  static Map<String, String> headers = {
    // "MacId": "WEB-dd09bc1aebdc4e94339e4e8a022b258c",
    "MacId": "mac-id-test",
    // "Accept-Encoding": "gzip, deflate, br",
    // "Accept":"application/json, text/plain, */*",
    // "Host": "192.168.1.55:8081",
  };
  static Map<String, String> headers2 = {
    // "MacId": "WEB-dd09bc1aebdc4e94339e4e8a022b258c",
    // "Accept-Encoding": "gzip, deflate, br",
    // "Accept":"application/json, text/plain, */*",
    // "Host": "192.168.1.55:8081",
  };
  static int tokenExpire;
  static dynamic fa;

  static Future changeHeader() async {
    headers2 = headers;
    headers = {};
  }

  static Future returnHeader() async {
    headers = headers2;
    headers2 = {};
  }

  static Future removeCookie(s) async {
    headers[s] = null;
    return true;
  }

  static Future get(String url) async {
    try {
      final http.Response response =
          await http.get(Uri.parse(url), headers: headers).catchError((err) {
        print(err);
        // err.message="Conntection Error";
        // return err;
      }).timeout(Duration(seconds: 60));

      if (response.statusCode == 500) {
        return response;
      } else {
        if ((response.body is String))
          // updateCookie(response);
          return response;
      }
    } catch (exception) {
      print(exception);
      // exception.message="Conntection Error";
      // exception._arguments = "Conntection Error";

      dynamic err = {"message": "Connection Error", "statusCode": 600};
      return err;
    }
    // updateCookie(response);
  }

  static dynamic getDicJson() {
    return fa;
  }

  static void setDicJson(dic) {
    fa = dic;
  }

  static Future<Response> post(
      String url, dynamic data, int timeoutSeconds) async {
    var response;
    try {
      response = await http
          .post(Uri.parse(url), body: data, headers: headers)
          .timeout(Duration(seconds: timeoutSeconds));
      if ((response.body is String)) {
        try {
          dynamic responseBody = json.decode(response.body);
          if (responseBody.containsKey("auth")) {
            // setCookie(responseBody["auth"]);
            updateCookie(response);
          }
          return response;
        } catch (exception) {
          return response;
        }
      } else {
        return null;
      }
    } catch (exception) {
      print(exception);
      return null;
    }
  }

  static Future<Response> put(
      String url, dynamic data, int timeoutSeconds) async {
    var response;
    try {
      response = await http
          .put(Uri.parse(url), body: data, headers: headers)
          .timeout(Duration(seconds: timeoutSeconds));
      if ((response.body is String)) {
        try {
          dynamic responseBody = json.decode(response.body);
          if (responseBody.containsKey("auth")) {
            // setCookie(responseBody["auth"]);
            updateCookie(response);
          }
          return response;
        } catch (exception) {
          return response;
        }
      } else {
        return null;
      }
    } catch (exception) {
      print(exception);
      return null;
    }
  }

  static void updateCookie(
    http.Response response,
  ) {
    if (response.statusCode == 200) {
      if (response.body != "true") {
        json.decode(response.body);
        String token = json.decode(response.body)['auth'];
        // int expire = json.decode(response.body)['expires_in'];
        if (token != null) {
          setCookie(token);
          // tokenExpire = (DateTime.now().millisecondsSinceEpoch + expire);
        }
      }
    } else {
      if (response.statusCode == 401) {
        removeCookies();
      }
      print("POST : " + response.statusCode.toString());
    }
  }

  static void setCookie(String token) {
    if (token != null) {
      // headers['cookie'] = token;
      headers['Authorization'] = token;
    }
    // print(headers['cookie']);
  }

  static void setCookie2(String token) {
    if (token != null) {
      // headers['cookie'] = token;
      headers['authorization'] = token;
    }
    // print(headers['cookie']);
  }

  static void setHeader(String key, val) {
    headers[key] = val;
  }

  static void removeCookies() {
    headers.remove('Authorization');
    tokenExpire = 0;
    headers['cookie'] = null;
  }

  static dynamic parseResponseBody(res) {
    if (res is Response) {
      if ((res != null) &&
          (((res.statusCode != null) &&
              (res.statusCode >= 200) &&
              (res.statusCode <= 310)))) {
        if (res.statusCode == 200) {
          var jsonResponse = json.decode(res.body);
          // print("key : $jsonResponse['email'].");
          return jsonResponse;
        } else {
          return res;
        }
      } else {
        return {
          "error":
              (res.statusCode != null) ? res.reasonPhrase : "Connection Error",
          "statusCode": (res.statusCode != null) ? res.statusCode : 600
        };
      }
    } else {
      return {
        "error": (res != null) ? res["message"] : "Connection Error",
        "statusCode": ((res != null) && (res["statusCode"] != null))
            ? res["statusCode"]
            : 600
      };
    }
  }
}
