import 'package:rxdart/rxdart.dart';

class RouterBloc {
  String path = '/';
  String lastPath = '/';
  String lastLastPath = '';
  String appTitle = "بوکت";

  BehaviorSubject<String> _subjectRoute;
  BehaviorSubject<String> _appTitleSubject;
  RouterBloc({this.path,this.lastPath}){
    print(this.path);
    _subjectRoute = new BehaviorSubject<String>(); //initializes the subject with element already
    _appTitleSubject = new BehaviorSubject<String>(); 
  }

  // Observable<String> get routeObservable => _subjectRoute.stream;
  // Observable<String> get appTitleObservable => _appTitleSubject.stream;

  void route(newPath) {
    if(newPath!="/switchLang" || newPath!="/closeEnd" || newPath!="/Switch") {
      lastLastPath = lastPath;
      lastPath = path;
    }
    path = newPath;
    print(path);
    _subjectRoute.sink.add(path);
  }

  String get() {
    return path;
  }

  String getLastPath() {
    return lastPath;
  }

  void setAppTitle(newName) {
    appTitle = newName;
    _appTitleSubject.sink.add(appTitle);
  }

  void dispose() {
    _subjectRoute.close();
    _appTitleSubject.close();
  }
}