import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;
import 'dart:async';
import 'package:hive/hive.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class DatabaseBloc {
  Box box;

  DatabaseBloc() {
    if (kIsWeb) {
      Hive.init("/hive");
    } else {
      // initAsync();
      initialAsync();
    }
  }

  // Future<void> initAsync() async {
  //   final dir = await getApplicationDocumentsDirectory();
  //   print("kirimagham");
  //   try {
  //     print("db instance initial success = " + dir.path.toString());

  //     Hive.init(dir.path);

  //     box = await Hive.openBox('myBox');
  //   } catch (e) {
  //     print(e.toString());
  //   }
  // }

  Future<void> initialAsync() async {
    try {
      Directory directory =
          await pathProvider.getApplicationDocumentsDirectory();
      Hive.init(directory.path);
      box = await Hive.openBox("myBooket");
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> setItem(key, item) async {
    if (box.isOpen) {
      await box.put(key, item.toString());
    } else {
      print("box is not open");
    }
  }

  Future<String> getItem(key) async {
    if (this.box == null) {
      this.box = await Hive.openBox("myBooket");
    }
    if (this.box.isOpen) {
      final val = await box.get(key);
      return val;
    } else {
      return "";
    }
  }

  Future<void> removeItem(key) async {
    if (this.box.isOpen) {
      await this.box.delete(key);
    } else {}
  }
}
