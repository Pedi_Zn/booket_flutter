import 'dart:convert';

import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/api/api.dart';
import 'package:booket_flutter/blocs/database_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';

class AuthenticationBloc {
  String firstname;
  String lastname;
  String username;
  bool usernameStatus;
  bool emailStatus;
  bool phoneNumberStatus;
  bool oldPasswordStatus;
  bool passwordStatus;
  bool confPasswordStatus;
  DatabaseBloc databaseBloc;
  String usernameError;
  String emailError;
  String oldPasswordError;
  String passwordError;
  String confPasswordError;
  String phoneNumberError;
  String phoneNumber;
  String email;
  String oldPassword;
  String password;
  String confPass;
  String token; // access token
  String address;
  String postCode;
  dynamic city;
  dynamic degree;
  dynamic university;
  dynamic studyField;
  int tokenExpire;

  String key; // login/reg key

  Api api = new Api();

  // double pageHeight;
  BehaviorSubject<String> _subjectToken;
  BehaviorSubject<bool> _usernameStat;
  BehaviorSubject<bool> _emailStat;
  BehaviorSubject<bool> _phoneNumberStat;
  BehaviorSubject<bool> _oldPasswordStat;
  BehaviorSubject<bool> _passwordStat;
  BehaviorSubject<bool> _confPasswordStat;

  BehaviorSubject<String> _usernameError;
  BehaviorSubject<String> _emailError;
  BehaviorSubject<String> _phoneError;
  BehaviorSubject<String> _oldPasswordError;
  BehaviorSubject<String> _passwordError;
  BehaviorSubject<String> _confPasswordError;

  AccountsRepo accountsRepo = new AccountsRepo();

  AuthenticationBloc(this.databaseBloc) {
    _subjectToken = new BehaviorSubject<String>();

    _usernameStat = new BehaviorSubject<bool>();
    _emailStat = new BehaviorSubject<bool>();
    _phoneNumberStat = new BehaviorSubject<bool>();
    _oldPasswordStat = new BehaviorSubject<bool>();
    _passwordStat = new BehaviorSubject<bool>();
    _confPasswordStat = new BehaviorSubject<bool>();

    _usernameError = new BehaviorSubject<String>();
    _emailError = new BehaviorSubject<String>();
    _phoneError = new BehaviorSubject<String>();
    _oldPasswordError = new BehaviorSubject<String>();
    _passwordError = new BehaviorSubject<String>();
    _confPasswordError = new BehaviorSubject<String>();
    // initializes the subject with element already
  }

  // Observable<String> get tokenObservable => _subjectToken.stream;
  // Observable<bool> get usernameStatObservable => _usernameStat.stream;
  // Observable<bool> get emailStatObservable => _emailStat.stream;
  // Observable<bool> get phoneNumberStatObservable => _phoneNumberStat.stream;
  // Observable<bool> get passwordStatObservable => _passwordStat.stream;
  // Observable<bool> get confPasswordStatObservable => _confPasswordStat.stream;

  //  Observable<String> get usernameErrorObserver => _usernameError.stream;
  //   Observable<String> get emailErrorObserver => _emailError.stream;
  //   Observable<String> get phoneNumberErrorObserver => _phoneError.stream;
  //   Observable<String> get passwordErrorObserver => _passwordError.stream;
  //   Observable<String> get confPasswordErrorObserver => _confPasswordError.stream;

  void initialize() {
    username = "";
    phoneNumber = "";
    usernameStatus = null;
    emailStatus = null;
    phoneNumberStatus = null;
    passwordStatus = null;
    confPasswordStatus = null;
    usernameError = null;
    emailError = null;
    oldPasswordError = null;
    passwordError = null;
    confPasswordError = null;
    phoneNumberError = null;
    email = "";
    password = "";
    token = "";
    confPass = "";
    postCode = "";
    city = "";
    address = "";
    studyField = "";
    university = "";
    degree = "";
    databaseBloc = DatabaseBloc();
  }

  Future<String> getFirstTimeFromStorage() async {
    return await databaseBloc.getItem("FIRSTTIME");
  }

  Future<void> setFirstTimeOnStorage(bool firstTime) async {
    return await databaseBloc.setItem("FIRSTTIME", firstTime);
  }

  Future<String> getTokenFromStorage() async {
    return await databaseBloc.getItem("TOKEN");
  }

  Future<void> setTokenOnStorage(String token, expiresAt) async {
    return await databaseBloc.setItem("TOKEN", token);
  }

  Future<void> removeTokenFromStorage() async {
    return await databaseBloc.removeItem("TOKEN");
  }

  Future<void> updateToken(inputToken, expiresAt) async {
    if (expiresAt == 0) {
      token = null;
      tokenExpire = expiresAt;
      await removeTokenFromStorage();
    } else {
      token = inputToken;
      Api.setCookie(token);
      tokenExpire =
          DateTime.now().millisecondsSinceEpoch + (expiresAt * 24 * 3600);
      await setTokenOnStorage(token, tokenExpire);
      _subjectToken.sink.add(token);
    }

    return;
  }

  void updateKey(inputKey) {
    key = inputKey;
  }

  String getError(type) {
    if (type == "username") {
      return usernameError;
    } else if (type == "email") {
      return emailError;
    } else if (type == "phoneNumber") {
      return phoneNumberError;
    } else if (type == "oldPassword") {
      return oldPasswordError;
    } else if (type == "password") {
      return passwordError;
    } else if (type == "confPassword") {
      return confPasswordError;
    } else {
      return null;
    }
  }

  void setError(type, err) {
    if (type == "username") {
      usernameError = err;
      _usernameError.sink.add(err);
    } else if (type == "email") {
      emailError = err;
      _emailError.sink.add(err);
    } else if (type == "phone") {
      phoneNumberError = err;
      _phoneError.sink.add(err);
    } else if (type == "oldPassword") {
      oldPasswordError = err;
      _oldPasswordError.sink.add(err);
    } else if (type == "password") {
      passwordError = err;
      _passwordError.sink.add(err);
    } else if (type == "confPassword") {
      confPasswordError = err;
      _confPasswordError.sink.add(err);
    } else {}
    return;
  }

  BehaviorSubject<String> getUsernameErrorSim() {
    return _usernameError;
  }

  BehaviorSubject<String> getEmailErrorSim() {
    return _emailError;
  }

  BehaviorSubject<String> getPhoneErrorSim() {
    return _phoneError;
  }

  BehaviorSubject<String> getOldPasswordErrorSim() {
    return _oldPasswordError;
  }

  BehaviorSubject<String> getPasswordErrorSim() {
    return _passwordError;
  }

  BehaviorSubject<String> getConfPasswordErrorSim() {
    return _confPasswordError;
  }

  void setInputStatus(type, stat) {
    if (type == "email") {
      emailStatus = stat;
      _emailStat.sink.add(emailStatus);
    } else if (type == "phoneNumber") {
      phoneNumberStatus = stat;
      _phoneNumberStat.sink.add(phoneNumberStatus);
    } else if (type == "username") {
      usernameStatus = stat;
      _usernameStat.sink.add(usernameStatus);
    } else if (type == "oldPassword") {
      oldPasswordStatus = stat;
      _oldPasswordStat.sink.add(oldPasswordStatus);
    } else if (type == "password") {
      passwordStatus = stat;
      _passwordStat.sink.add(passwordStatus);
    } else if (type == "confPassword") {
      confPasswordStatus = stat;
      _confPasswordStat.sink.add(confPasswordStatus);
    } else {}
    return;
  }

  void updateUsername(s) {
    username = s;
  }

  void updateFirstname(s) {
    firstname = s;
  }

  void updateLastName(s) {
    lastname = s;
  }

  void updateOldPassword(p) {
    oldPassword = p;
  }

  void updatePassword(p) {
    password = p;
  }

  void updateConfPass(c) {
    confPass = c;
  }

  void updateEmail(e) {
    email = e;
  }

  void updatePhoneNumber(p) {
    phoneNumber = p;
  }

  void updatePostCode(p) {
    postCode = p;
  }

  void updateCity(c) {
    city = c;
  }

  void updateAddress(a) {
    address = a;
  }

  void updateUniversity(u) {
    university = u;
  }

  void updateStudyField(s) {
    studyField = s;
  }

  void updateDegree(d) {
    degree = d;
  }

  String getOldPassword() {
    return oldPassword;
  }

  String getPassword() {
    return password;
  }

  String getConfPass() {
    return confPass;
  }

  String getFirstName() {
    return firstname;
  }

  String getLastName() {
    return lastname;
  }

  String getUsername() {
    return username;
  }

  String getEmail() {
    return email;
  }

  String getKey() {
    return key;
  }

  String getToken() {
    return token;
  }

  Future<dynamic> login(url, locale) async {
    if ((this.phoneNumber != null) &&
        (this.password != null) &&
        (this.phoneNumber.length > 3) &&
        (this.password.length > 5)) {
      var username = this.phoneNumber;
      try {
        var phone = int.parse(phoneNumber);
        username = "98" + phone.toString();
      } catch (e) {
        username = this.phoneNumber;
      }
      print("username      : " + username);
      print("password      : " + this.password);
      final res = await accountsRepo
          .login(url, {"username": username, "password": this.password});
      if (res.containsKey("auth")) {
        Api.setCookie(res["auth"]);
        clearAll();
      }
      return res;
    } else {
      return {
        "message": "فرم ورود را پر کنید"
        // "message": locale.languageCode == "fa"
        //     ? "فرم ورود را پر کنید"
        //     : "Please fill your login infoqqq"
      };
    }
  }

  Future<dynamic> forgotPassword(url) async {
    if ((this.username != null) && (this.username.length > 3)) {
      var username = this.username;
      try {
        var idef = 0;
        try {
          idef = int.parse(username);
          username = "98/" + idef.toString();
        } catch (e) {
          username = this.username;
        }
        // if(phone.toString().startsWith("0")){

        // }
        final res = await accountsRepo.forgotPassword(url, {
          "type": "phone",
          "identification": username
        }); // can handle both phone and username , assigne dto the phone
//        clearAll();
        return res;
      } catch (e) {
//        username = this.username;
        return {"message": "شماره موبایل خود را وارد کنید"};
      }
    } else {
      return {"message": "شماره موبایل خود را وارد کنید"};
    }
  }

  Future<dynamic> register(url) async {
    if ((this.username != null) &&
        (this.password != null) &&
        (this.username.length > 0) &&
        (this.password.length > 0)) {
      if (this.confPass == this.password) {
        final res = await accountsRepo.register(url, {
          "username": this.username,
          "password": this.password,
          "email": "",
          "phone":
              "98/" + this.phoneNumber.substring(1, this.phoneNumber.length)
        });
        if (res != null) {
          if (res.containsKey("token")) {
            Api.setCookie(res["token"]);
            clearAll();
          }
        }
        return res;
      } else {
        return {"message": "رمز های عبور برابر نیستند"};
      }
    } else {
      return {"message": "!"};
    }
  }

  Future<dynamic> registerV2(url) async {
    if ((this.username != null) &&
        (this.password != null) &&
        (this.username.length > 0) &&
        (this.password.length > 0)) {
      if (this.confPass == this.password) {
        final res = await accountsRepo.registerV2(url, {
          "username": this.username,
          "password": this.password,
          "email": "",
          "phone":
              "98/" + this.phoneNumber.substring(1, this.phoneNumber.length)
        });
        // print(res.toString());
        return res;
      } else {
        return {"message": "رمز های عبور برابر نیستند"};
      }
    } else {
      return {"message": "!"};
    }
  }

  Future<dynamic> updateProfile(url, token) async {
    var profileData = jsonEncode({
      "address": this.address,
      "email": this.email,
      "postal_code": this.postCode,
      "user": {
        "city": this.city,
        "degree": this.degree,
        "firstname": this.firstname,
        "lastname": this.lastname,
        "study_field": this.studyField,
        "university": this.university
      }
    });
    final res = await accountsRepo.updateProfile(url, token, profileData);
    print(res.toString());
    return res;
  }

  Future<dynamic> resetPassword(url, code) async {
    if ((((this.oldPassword != null) && (code == "")) || (code != "")) &&
        (this.password != null) &&
        (((this.oldPassword != null) &&
                (this.oldPassword.length > 0) &&
                (code == "")) ||
            (code != "")) &&
        (this.password.length > 0)) {
      var dt;
      if (this.confPass == this.password) {
        if (code != "") {
          dt = {
            "key": code,
            "phone": "98/" + int.parse(this.username).toString(),
            "newPass": this.password,
            "confirm": this.confPass
          };
        } else {
          dt = {
            "old": this.oldPassword,
            "newPass": this.password,
            "confirm": this.confPass
          };
        }
        final res = await accountsRepo.resetPassword(
            url, {"type": "change", "identification": jsonEncode(dt)});
        if (res == null) {
          return {"message": "connection timeout "};
        } else {
//          clearAll();
          if (res.containsKey("token")) {
            Api.setCookie(res["token"]);
//            clearAll();

          }
          return res;
        }
      } else {
        return {"message": "رمز های عبور برابر نیستند"};
      }
    } else {
      return {"message": "!"};
    }
  }

  void clearAll() {
    this.username = "";
//    this.phoneNumber = "";
    this.email = "";
    this.oldPassword = "";
    this.password = "";
//    this.token = "";
    this.confPass = "";
    this.postCode = "";
    this.city = "";
    this.address = "";
    this.studyField = "";
    this.university = "";
    this.degree = "";
    this.usernameStatus = null;
    this.emailStatus = null;
    this.phoneNumberStatus = null;
    this.oldPasswordStatus = null;
    this.passwordStatus = null;
    this.confPasswordStatus = null;
    this.usernameError = null;
    this.emailError = null;
    this.oldPasswordError = null;
    this.passwordError = null;
    this.confPasswordError = null;
    this.phoneNumberError = null;
  }

  Future<dynamic> getProfile(url) async {
    return await accountsRepo.getMe(url);
  }

  void dispose() {
    _subjectToken.close();

    _usernameStat.close();
    _oldPasswordStat.close();
    _passwordStat.close();
    _emailStat.close();
    _phoneNumberStat.close();
    _confPasswordStat.close();

    _usernameError.close();
    _emailError.close();
    _phoneError.close();
    _oldPasswordError.close();
    _passwordError.close();
    _confPasswordError.close();

    username = "";
    phoneNumber = "";
    email = "";
    postCode = "";
    city = "";
    address = "";
    studyField = "";
    university = "";
    degree = "";
    oldPassword = "";
    password = "";
    token = "";
    confPass = "";

    usernameStatus = null;
    emailStatus = null;
    phoneNumberStatus = null;
    oldPasswordStatus = null;
    passwordStatus = null;
    confPasswordStatus = null;

    usernameError = null;
    emailError = null;
    oldPasswordError = null;
    passwordError = null;
    confPasswordError = null;
    phoneNumberError = null;
  }
}
