import 'package:rxdart/rxdart.dart';

class GridCardBloc {

  int index;
  BehaviorSubject<int> _tabsStream;

  GridCardBloc(this.index){
    _tabsStream = new BehaviorSubject<int>(); 
    _tabsStream.sink.add(this.index);
    // initializes the subject with element already
  }

  void initialize(){

  }

  void setIndex(int i){
    index = i;
    _tabsStream.sink.add(index);
  }

   BehaviorSubject<int> getTabsStream(){
    return _tabsStream;
  }

  void dispose(){
    _tabsStream.close();

  }
}

