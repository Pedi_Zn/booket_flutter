import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';

class ReaderBloc {
  String selectionMode;
  dynamic behaviorCommand;
  Color selectedColor;
  BehaviorSubject<dynamic> selectionCalls;
  dynamic ebookPagesRes = {};
  List<dynamic> allAnnotations;
  List<dynamic> pagedAnnotations = [];
  dynamic bookmarkList = [];
  int selectedColorIndex = 1;
  int pages = 1;
  Color firstHighlightColor = Colors.amberAccent;
  Color secondHighlightColor = Colors.greenAccent;
  Color thirdHighlightColor = Colors.redAccent;
  ColorSwatch tempMainColor;
  Color tempShadeColor;
  dynamic bookmarksMap = {};
  ColorSwatch mainColor;
  Color shadeColor = Colors.blue[800];
  List<BehaviorSubject<dynamic>> selectionCallsList;
  ReaderBloc(this.selectionMode, this.pages) {
    selectedColor = firstHighlightColor.withOpacity(0.3);
    selectionCalls = new BehaviorSubject<dynamic>();
    selectionCallsList = [];

    selectionCalls.sink.add(this.behaviorCommand);
    // initializes the subject with element already
  }

  void initialize() {}

  void setCommand(dynamic s) {
    behaviorCommand = s;
    if ((s != null) &&
        (s["page"] != null) &&
        (selectionCallsList.length > 0) &&
        (selectionCallsList[s["page"]] != null)) {
      selectionCallsList[s["page"]].sink.add(behaviorCommand);
    }
    if ((s["ebookPageRes"] != null) && (s["ebookPageRes"].length > 0)) {
      ebookPagesRes[s["page"].toString()] = s["ebookPageRes"];
    } else if (behaviorCommand["color"] != null) {
      selectedColor = behaviorCommand["color"];
    } else if (behaviorCommand["select"] != null) {
      selectionCalls.sink.add(behaviorCommand);
    }
  }

  void setBookAnnotations(anns) {
    allAnnotations = anns;
    // start paging annotations
    var res = createPagedAnnotations();
  }

  void fillBookmarkList() {
    bookmarkList = [];
    for (var i = 0; i < bookmarksMap.length; i++) {
      if (bookmarksMap[i.toString()] != false)
        bookmarkList
            .add({"page": i, "bookmarkToken": bookmarksMap[i.toString()]});
    }
    // bookmarkList =
    //     readerBloc.bookmarksMap.where((element) => (element != false));
    print("00000000000" + bookmarkList.toString());
  }

  dynamic createPagedAnnotations() {
    if ((pages > 1) && (allAnnotations != null)) {
      for (var z = 0; z < pages; z++) {
        pagedAnnotations.add([]);
        bookmarksMap[z.toString()] = false;
      }
      for (var z = 0; z < allAnnotations.length; z++) {
        if (allAnnotations[z]["type"] == "bookmark") {
          bookmarksMap[allAnnotations[z]["page_index"].toString()] =
              allAnnotations[z]["token"];
        } else {
          pagedAnnotations[allAnnotations[z]["page_index"]]
              .add(allAnnotations[z]);
          selectionCallsList[allAnnotations[z]["page_index"]].sink.add({
            "page": allAnnotations[z]["page_index"],
            "annotations": pagedAnnotations[allAnnotations[z]["page_index"]]
          });
        }
      }
      return pagedAnnotations;
    } else
      return null;
  }

  void setParentRequest(s) {
    selectionCalls.sink.add(s);
  }

  void setSelectedColorIndex(i) {
    selectedColorIndex = i;
  }

  void setPages(i) {
    pages = i;
    createPagedAnnotations();
    for (var z = 0; z < this.pages; z++) {
      selectionCallsList.add(new BehaviorSubject<dynamic>());
    }
  }

  void addToAnnotations(dynamic annotation, pageNumber) {
    pagedAnnotations[pageNumber].add(annotation);
    allAnnotations.add(annotation);
    selectionCallsList[pageNumber]
        .sink
        .add({"page": pageNumber, "annotations": pagedAnnotations[pageNumber]});
  }

  void addToBookmarks(pageNumber) {
    bookmarksMap[pageNumber.toString()] = true;
    selectionCallsList[pageNumber]
        .sink
        .add({"page": pageNumber, "bookmarkToken": true});
  }

  void removeBookmark(pageNumber) {
    bookmarksMap[pageNumber.toString()] = false;
    selectionCallsList[pageNumber]
        .sink
        .add({"page": pageNumber, "bookmarkToken": false});
  }

  void removeAnnotation(token, pageNumber) {
    pagedAnnotations.removeWhere((ann) => ann["token"] == token);
    allAnnotations.removeWhere((ann) => ann["token"] == token);
    selectionCallsList[pageNumber]
        .sink
        .add({"page": pageNumber, "annotations": pagedAnnotations[pageNumber]});
  }

  dynamic getEbookPageRes(pageNumber) {
    return ebookPagesRes[pageNumber.toString()];
  }

  List<dynamic> getPageAnnotations(pageNumber) {
    if (((pagedAnnotations.length - 1) >= pageNumber) && (pageNumber >= 0)) {
      // selectionCallsList[pageNumber].sink.add(
      //     {"page": pageNumber, "annotations": pagedAnnotations[pageNumber]});
      return pagedAnnotations[pageNumber];
    } else {
      return [];
    }
  }

  Color selectColor() {
    switch (selectedColorIndex) {
      case 1:
        return firstHighlightColor;
        break;
      case 2:
        return secondHighlightColor;
        break;
      case 3:
        return thirdHighlightColor;
        break;
      case 4:
        if (mainColor != null)
          return mainColor;
        else
          return Colors.red;
        break;
      default:
        return Colors.grey[400];
        break;
    }
  }

  BehaviorSubject<dynamic> getSelectionCalls() {
    return selectionCalls;
  }

  BehaviorSubject<dynamic> getSelectionCallsForPage(pageNumber) {
    return selectionCallsList[pageNumber];
  }

  void dispose() {
    selectionCalls.close();
  }
}
