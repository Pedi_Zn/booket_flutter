import 'dart:io';
import 'dart:math';

import 'package:booket_flutter/blocs/authentication_bloc.dart';
import 'package:booket_flutter/blocs/database_bloc.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/route_generator.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:booket_flutter/localization/localization_delegate.dart';
import 'package:flutter/services.dart';
import 'package:flutter\_localizations/flutter\_localizations.dart';
// import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    systemNavigationBarColor:
        Color.fromRGBO(200, 200, 200, 0.08), // navigation bar color
    statusBarColor: Colors.black12, // status bar color
    statusBarBrightness: Brightness.light, //status bar brigtness
    statusBarIconBrightness: Brightness.light, //status barIcon Brightness
    systemNavigationBarDividerColor:
        Color.fromRGBO(200, 200, 200, 0.1), //Navigation bar divider color
    systemNavigationBarIconBrightness: Brightness.dark, //navigation bar icon
  ));
  _enablePlatformOverrideForDesktop();
  runApp(MyApp());
}

void _enablePlatformOverrideForDesktop() {
  if (!kIsWeb) {
    if ((Platform.isWindows || Platform.isLinux || Platform.isMacOS)) {
      debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
    }
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  // final prefrencesBloc = PrefrencesBloc({"langCode": "fa", "themeCode": 2});
  final rg = RouteGenerator.generateRoute;
  final int randomShit = Random().nextInt(1000);

  @override
  Widget build(BuildContext context) {
    var initialError;
    // if((user==null) || user["error"]!=null){
    //     initialError = user;
    //     // user = null;
    // }
    // FlutterStatusbarcolor.setStatusBarColor(
    //     Colors.white); //this change the status bar color to white
    // FlutterStatusbarcolor.setNavigationBarColor(
    //     Colors.green); //this sets the navigation bar color to green
    var localeFa = new Locale('fa', 'FA');

    var localeEn = new Locale('en', 'EN');
    var me;
    final localizationDel = new DemoLocalizationsDelegate();
    final DatabaseBloc dbBloc = new DatabaseBloc();
    final AuthenticationBloc authenticationBloc = AuthenticationBloc(dbBloc);
    if (!kIsWeb) {
      print(Platform.operatingSystem.toString());
      print(Platform.operatingSystemVersion.toString());
      print(Platform.numberOfProcessors.toString());
    }

    // final DemoLocalizationsDelegate localizationDel = new DemoLocalizationsDelegate();
    return MainStateContainer(
        hostName:
            // kIsWeb
            // ? html.window.location.href
            // ? html.window.location.href.split("/web")[0]
            "https://mybooket.com",
        // hostName: "http://localhost:8081",
        authenticationBloc: authenticationBloc,
        me: me,
        randomShit: randomShit,
        // mainRouter: routerBloc,
        locale: kIsWeb ? localeFa : localeEn,
        // prefrencesBloc: prefrencesBloc,
        // token:token,
        themeCode: 2,
        config: {"brands": []},
        initialError: initialError,
        booksList: [],
        connectionsMap: {},
        localizationDelegate: localizationDel,
        child: MaterialApp(
          supportedLocales: [
            const Locale('en', 'EN'),
            const Locale('fa', 'FA'),
          ],
          locale: const Locale('fa', 'FA'),
          initialRoute: "/",
          onGenerateRoute: RouteGenerator.generateRoute,
          localizationsDelegates: [
            localizationDel,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate
          ],
          debugShowCheckedModeBanner: false,
          title: "title",
          themeMode: ThemeMode.light,
          darkTheme: ThemeData(
            fontFamily: 'IranSans',
            primarySwatch: Colors.red,
            primaryColor: Color(0xFF252525), //Colors.grey.shade200
            brightness: Brightness.dark,
            indicatorColor: fromHex('#FF7C09'),
            dividerColor: Color(0xFFEEEEEE),
            // primaryColorDark: config.Colors().secondDarkColor(1),
            scaffoldBackgroundColor: Color(0xFF2C2C2C),
            accentColor: fromHex('#FF7C09'),
            primaryColorLight: fromHex('#FF7C09'),
            appBarTheme: AppBarTheme(
                iconTheme: IconThemeData(color: fromHex('#FF7C09'))),
            backgroundColor: fromHex('#FF7C09'),
            // hintColor: config.Colors().secondDarkColor(1),
            // focusColor: config.Colors().accentDarkColor(1),
            textTheme: TextTheme(
              button: TextStyle(color: Color(0xFF252525)),
            ),
          ),
          theme: ThemeData(
            fontFamily: 'IranSans',
            appBarTheme: AppBarTheme(
                iconTheme: IconThemeData(color: fromHex('#FF7C09'))),
            colorScheme: ColorScheme(
              primary: fromHex('#FF7C09'),
              primaryVariant: Colors.black,
              secondary: Colors.black,
              secondaryVariant: Colors.black,
              surface: Colors.black,
              background: Colors.black,
              error: Colors.black,
              onPrimary: Colors.black,
              onSecondary: Colors.black,
              onSurface: Colors.black,
              onBackground: Colors.black,
              onError: Colors.black,
              brightness: Brightness.light,
            ),
            backgroundColor: fromHex('#FF7C09'),
            dividerColor: Color(0xFFEEEEEE),
            shadowColor: Color(0XFFE5E5E5),
            cardColor: Color(0x00FFFFFF),
            primaryColor: Colors.white,
            brightness: Brightness.light,
            accentColor: fromHex('#FF7C09'),
            indicatorColor: fromHex('#FF7C09'),
            primaryColorDark: Colors.black,
            primaryColorLight: fromHex('#FF7C09'),
            // focusColor: config.Colors().accentColor(1),
            // hintColor: config.Colors().secondColor(1),
            iconTheme: IconThemeData(
                // color: config.Colors().accentColor(1),
                ),
            textTheme: TextTheme(
              button: TextStyle(color: Colors.white),
            ),
          ),
        ));
  }

  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}
