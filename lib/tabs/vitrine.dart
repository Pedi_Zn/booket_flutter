import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/blocs/grid_card_bloc.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/widgets/books.dart';
import 'package:booket_flutter/widgets/expireDate.dart';
import 'package:booket_flutter/widgets/grid_or_list_switch.dart';
import 'package:booket_flutter/widgets/refresh_tab.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:booket_flutter/models/route_argument.dart';
import 'package:flutter/material.dart';

class Vitrine extends StatefulWidget {
  const Vitrine({Key key}) : super(key: key);

  @override
  VitrineState createState() => new VitrineState();
}

class VitrineState extends State<Vitrine>
    with
        TickerProviderStateMixin,
        AfterLayoutMixin,
        AutomaticKeepAliveClientMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();

  double _gridPadding = 15;
  bool grid = true;
  GridCardBloc gridCardBloc = new GridCardBloc(1);
  List<List<dynamic>> booksListList;
  ScrollController parentCont, childCont;
  TabController tabController;
  bool goTop = false;
  bool loader = true;
  bool oneTime = true;
  int paginateIndex = 1;
  AccountsRepo accountsRepo = new AccountsRepo();

  List<dynamic> test = [
    {
      "front_cover": {
        "thumbnail":
            "/storage/files/styles/thumbnail/books/92f79f0a16446af6/front.jpg",
        "medium": "",
        "medium_rounded":
            "/storage/files/styles/medium_rounded/books/92f79f0a16446af6/front.png",
        "large": "/storage/files/styles/large/books/92f79f0a16446af6/front.jpg",
        "medium_trans":
            "/storage/files/styles/medium_trans/books/92f79f0a16446af6/front.png",
        "medium_zoom_fit":
            "/storage/files/styles/medium_zoom_fit/books/92f79f0a16446af6/front.png"
      },
    },
  ];

  List<dynamic> title = [];
  List<dynamic> vitrineList = [];
  List<dynamic> booksList = [];
  List<dynamic> categoryList = [];
  List<List<dynamic>> vitrineBooksList = [
    [],
  ];

  List<dynamic> selectedBooksList = [];

  @override
  void initState() {
    this.childCont = ScrollController(initialScrollOffset: 0.0);
    this.parentCont = ScrollController(initialScrollOffset: 0.0);
    this.childCont.addListener(_scrollListener);

    this.tabController = TabController(initialIndex: 0, length: 0, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final pageWidth = MediaQuery.of(context).size.width;

    int count = pageWidth < 350.0
        ? 1
        : pageWidth < 650.0
            ? 2
            : pageWidth < 1100.0
                ? 3
                : pageWidth < 1000.0
                    ? 4
                    : pageWidth < 1300.0
                        ? 5
                        : 6;

    final itemExtentList = pageWidth < 350.0
        ? 160.0
        : pageWidth < 550.0
            ? 290.0
            : pageWidth < 900.0
                ? 210.0
                : pageWidth < 1000.0
                    ? 250.0
                    : pageWidth < 1300.0
                        ? 280.0
                        : 300.0;

    return LiquidPullToRefresh(
      key: _refreshIndicatorKey,
      onRefresh: _handleRefresh,
      showChildOpacityTransition: false,
      child: AutoRefresh(
        gridCardBloc: gridCardBloc,
        child: Scaffold(
          floatingActionButton: goTop
              ? FloatingActionButton(
                  backgroundColor: Theme.of(context).accentColor,
                  onPressed: () {
                    parentCont.animateTo(0.0,
                        duration: Duration(milliseconds: 200),
                        curve: Curves.easeInCubic);
                    childCont.animateTo(0.0,
                        duration: Duration(milliseconds: 200),
                        curve: Curves.easeInCubic);
                    setState(() {
                      goTop = true;
                    });
                  },
                  child:
                      Icon(Icons.arrow_upward, color: Colors.white, size: 25.0))
              : Container(),
          body: Container(
            child: ListView(
                physics: AlwaysScrollableScrollPhysics(),
                controller: parentCont,
                addRepaintBoundaries: true,
                scrollDirection: Axis.vertical,
                children: [
                  DefaultTabController(
                    length: 2,
                    initialIndex: 0,
                    child: Center(
                      child: Padding(
                          padding: const EdgeInsets.all(0),
                          child: GridOrListSwitch(
                              grid: grid,
                              swich: (data) {
                                setState(() {
                                  grid = data;
                                });
                              })),
                    ),
                  ),
                  Container(
                    color: Color.fromRGBO(0, 0, 0, 0.03),
                    child: Container(
                      height: 45.0,
                      width: MediaQuery.of(context).size.width,
                      child: Center(
                        child: TabBar(
                          controller: tabController,
                          onTap: (i) {
                            if ((i > 0) && (i < tabController.length - 1)) {
                              setState(() {
                                paginateIndex = 1;
                                gridCardBloc.setIndex(i);
                                selectedBooksList = vitrineBooksList[i];
                              });
                            }
                          },
                          isScrollable: true,
                          unselectedLabelStyle: TextStyle(
                            fontWeight: FontWeight.normal,
                            fontFamily: 'IranSans',
                          ),
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: 'IranSans',
                          ),
                          labelPadding: EdgeInsets.only(right: 7.5, left: 7.5),
                          tabs: title
                              .map((t) =>
                                  Tab(child: (t is String) ? Text(t) : t))
                              .toList(),
                          labelColor: Colors.black,
                          indicatorColor: Colors.transparent,
                        ),
                      ),
                    ),
                  ),
                  Container(),
                  (selectedBooksList.length > 0)
                      ? Center(
                          child: Container(
                              height: MediaQuery.of(context).size.height,
                              width: MediaQuery.of(context).size.width,
                              child: !grid
                                  ? Column(
                                      children: [
                                        Container(
                                          height: !oneTime
                                              ? MediaQuery.of(context)
                                                      .size
                                                      .height -
                                                  50.0
                                              : MediaQuery.of(context)
                                                  .size
                                                  .height,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              LimitedBox(
                                                maxWidth: pageWidth > 700.0
                                                    ? 680.0
                                                    : pageWidth,
                                                child: AnimationLimiter(
                                                  child: ListView.builder(
                                                    padding: EdgeInsets.all(
                                                        _gridPadding),
                                                    controller: childCont,
                                                    itemExtent:
                                                        MediaQuery.of(context)
                                                                    .size
                                                                    .width /
                                                                (count + 1) +
                                                            30,
                                                    itemCount: vitrineBooksList[
                                                            tabController.index]
                                                        .length,
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            int index) {
                                                      return Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                bottom: 15.0),
                                                        child: RepaintBoundary(
                                                            key: Key('str' +
                                                                index
                                                                    .toString()),
                                                            child: AnimationConfiguration
                                                                .staggeredList(
                                                              position: index,
                                                              duration:
                                                                  const Duration(
                                                                      milliseconds:
                                                                          500),
                                                              delay: const Duration(
                                                                  milliseconds:
                                                                      100),
                                                              child:
                                                                  SlideAnimation(
                                                                verticalOffset:
                                                                    50.0,
                                                                child:
                                                                    FadeInAnimation(
                                                                  child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .spaceAround,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Container(
                                                                        padding:
                                                                            const EdgeInsets.only(left: 10.0),
                                                                        width: MediaQuery.of(context).size.width /
                                                                                (count + 1) -
                                                                            20,
                                                                        child: GestureDetector(
                                                                            onTapDown: (s) {
                                                                              setState(() {
                                                                                // selectedBooksList.removeAt(0);
                                                                                Navigator.of(context).pushNamed('/books/' + selectedBooksList[index]["token"],
                                                                                    arguments: RouteArgument(id: 12, argumentsList: [
                                                                                      false,
                                                                                      "salam"
                                                                                    ]));
                                                                              });
                                                                            },
                                                                            child: new Books(selectedBooksList[index]["front_cover"]["medium"], 0.0, null, count)),
                                                                      ),
                                                                      // Container(width: 5.0), // spacer
                                                                      Expanded(
                                                                          child:
                                                                              Container(
                                                                        child:
                                                                            Column(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.end,
                                                                          children: [
                                                                            // Spacer(),
                                                                            Container(
                                                                              padding: EdgeInsets.only(
                                                                                left: 10.0,
                                                                                top: 10.0,
                                                                              ),
                                                                              child: Column(children: [
                                                                                Row(
                                                                                  children: [
                                                                                    Flexible(
                                                                                        child: GestureDetector(
                                                                                      onTapDown: (s) {
                                                                                        setState(() {
                                                                                          // selectedBooksList.removeAt(0);
                                                                                          Navigator.of(context).pushNamed('/books/' + selectedBooksList[index]["token"], arguments: RouteArgument(id: 12, argumentsList: [false, "salam"]));
                                                                                        });
                                                                                      },
                                                                                      child: Text(
                                                                                        vitrineBooksList[tabController.index][index]["title"].toString(),
                                                                                        maxLines: 2,
                                                                                        overflow: TextOverflow.ellipsis,
                                                                                        textAlign: TextAlign.right,
                                                                                        style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w600),
                                                                                      ),
                                                                                    )),
                                                                                  ],
                                                                                ),
                                                                                Container(
                                                                                  height: 10.0,
                                                                                ),
                                                                                Container(
                                                                                  padding: EdgeInsets.only(left: 15.0),
                                                                                  child: Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                                                    children: List.generate(vitrineBooksList[tabController.index][index]["authors"].length, (i) {
                                                                                      return Container(
                                                                                          child: i == 0
                                                                                              ? Text(
                                                                                                  vitrineBooksList[tabController.index][index]["authors"][i]["title"].toString(),
                                                                                                  maxLines: 1,
                                                                                                  overflow: TextOverflow.ellipsis,
                                                                                                  softWrap: false,
                                                                                                  style: TextStyle(fontSize: 12.0),
                                                                                                )
                                                                                              : Text(
                                                                                                  i < 2
                                                                                                      ? ", " + vitrineBooksList[tabController.index][index]["authors"][i]["title"].toString()
                                                                                                      : i == 2
                                                                                                          ? ", ..."
                                                                                                          : "",
                                                                                                  maxLines: 1,
                                                                                                  overflow: TextOverflow.ellipsis,
                                                                                                  softWrap: false,
                                                                                                  style: TextStyle(fontSize: 12.0),
                                                                                                ));
                                                                                    }),
                                                                                  ),
                                                                                ),
                                                                              ]),
                                                                            ),
                                                                            Spacer(),
                                                                            // vitrineBooksList[tabController.index].length != null
                                                                            // ? Padding(
                                                                            //     padding: const EdgeInsets.all(0.0),
                                                                            //     child: Row(
                                                                            //       mainAxisAlignment: MainAxisAlignment.end,
                                                                            //       children: [
                                                                            //         Container(
                                                                            //           width: 80.0,
                                                                            //           height: 35.0,
                                                                            //           child: new ExpireDate(
                                                                            //             DemoLocalizations.of(context).transNumber("0"),
                                                                            //             50.0,
                                                                            //             100.0,
                                                                            //           )
                                                                            //         )
                                                                            //       ],
                                                                            //     ),
                                                                            //   )
                                                                            // : Container()
                                                                          ],
                                                                        ),
                                                                      )),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                            )),
                                                      );
                                                    },
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        !oneTime
                                            ? Center(
                                                child: Container(
                                                    // margin: EdgeInsets.only(bottom: 350.0),
                                                    height: 30.0,
                                                    width: 30.0,
                                                    child:
                                                        CircularProgressIndicator()),
                                              )
                                            : Container(),
                                      ],
                                    )
                                  : Column(
                                      children: [
                                        Container(
                                            height: !oneTime
                                                ? MediaQuery.of(context)
                                                        .size
                                                        .height -
                                                    50.0
                                                : MediaQuery.of(context)
                                                    .size
                                                    .height,
                                            color: Colors.transparent,
                                            child: AnimationLimiter(
                                              child: GridView.count(
                                                padding: EdgeInsets.all(
                                                    _gridPadding),
                                                controller: childCont,
                                                crossAxisCount: count,
                                                childAspectRatio: 0.7,
                                                shrinkWrap: true,
                                                crossAxisSpacing: _gridPadding,
                                                mainAxisSpacing: _gridPadding,
                                                children: List.generate(
                                                  selectedBooksList.length,
                                                  (int index) {
                                                    return AnimationConfiguration
                                                        .staggeredGrid(
                                                      columnCount: count,
                                                      position: index,
                                                      duration: const Duration(
                                                          milliseconds: 375),
                                                      child: ScaleAnimation(
                                                        scale: 0.5,
                                                        child: FadeInAnimation(
                                                          child:
                                                              GestureDetector(
                                                                  onTapDown:
                                                                      (s) {
                                                                    setState(
                                                                        () {
                                                                      // selectedBooksList.removeAt(0);
                                                                      Navigator.of(context).pushNamed(
                                                                          '/books/' +
                                                                              selectedBooksList[index][
                                                                                  "token"],
                                                                          arguments: RouteArgument(
                                                                              id: 12,
                                                                              argumentsList: [
                                                                                false,
                                                                                "salam"
                                                                              ]));
                                                                    });
                                                                  },
                                                                  child: new Books(
                                                                      selectedBooksList[index]
                                                                              [
                                                                              "front_cover"]
                                                                          [
                                                                          "medium"],
                                                                      0.0,
                                                                      null,
                                                                      count)),
                                                        ),
                                                      ),
                                                    );
                                                  },
                                                ),
                                              ),
                                            )),
                                        !oneTime
                                            ? Center(
                                                child: Container(
                                                    // margin: EdgeInsets.only(bottom: 350.0),
                                                    height: 30.0,
                                                    width: 30.0,
                                                    child:
                                                        CircularProgressIndicator()),
                                              )
                                            : Container(),
                                      ],
                                    )))
                      : Center(
                          child: Container(
                              margin: EdgeInsets.only(
                                  top: (MediaQuery.of(context).size.height -
                                          220) /
                                      2),
                              height: 50.0,
                              width: 50.0,
                              child: CircularProgressIndicator()),
                        ),
                ]),
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void afterFirstLayout(BuildContext context) {
    final iw = MainStateContainer.of(context);
    accountsRepo.getVitrine(iw.hostName).then((vitrine) {
      loader = false;
      title.clear();
      title.add(' ');
      for (int i = 0; i < vitrine.length; i++) {
        if (vitrine[i]["title"] != null && vitrine[i]["title"] != "")
          this.title.add(vitrine[i]["title"]);
        if (vitrine[i] != null && vitrine[i] != "")
          this.vitrineList.add(vitrine[i]);
        if (vitrine[i]["books"] != null && vitrine[i]["books"] != "")
          this.categoryList.add(vitrine[i]["books"]);
      }
      for (int i = 0; i < (title.length) - 1; i++) {
        for (int j = 0; j < categoryList[i].length; j++) {
          this.booksList.add(categoryList[i][j]);
        }
        if (mounted) {
          setState(() {
            vitrineBooksList.add(booksList.toList());
            booksList.clear();
            selectedBooksList = vitrineBooksList[1];
          });
        }
      }
      title.add(' ');
      this.tabController.dispose();
      if (mounted) {
        setState(() {
          this.tabController = TabController(
              initialIndex: 1, length: (title.length), vsync: this);
        });
      }
    });
  }

  Future<void> paginateRequest() {
    // selectedBooksList.addAll(test);
    final iw = MainStateContainer.of(context);
    accountsRepo
        .getVitrinePaginate(
            iw.hostName + this.vitrineList[tabController.index]["url"],
            (paginateIndex + 1).toString())
        .then((value) {
      setState(() {
        // selectedBooksList.removeAt(12);
        selectedBooksList.addAll(value["data"]);
        // Timer(const Duration(seconds: 3), () {
        //   selectedBooksList.addAll(value["data"]);
        // });

        paginateIndex++;
        oneTime = true;
      });
    });
  }

  Future<void> _handleRefresh() {
    final Completer<void> completer = Completer<void>();
    Timer(const Duration(seconds: 1), () {
      completer.complete();
    });
    setState(() {
      Navigator.of(context).pushNamed("/Vitrine");
    });
  }

  void _scrollListener() {
    ScrollPosition sp;
    sp = this.childCont.positions.elementAt(0);
    if (sp.pixels <= (150) && !sp.outOfRange) {
      if (goTop) {
        if (mounted) {
          setState(() {
            goTop = false;
          });
          parentCont.animateTo(0.0,
              duration: Duration(milliseconds: 200),
              curve: Curves.easeInOutCubic);
          // }
        }
      }
    } else if (sp.pixels > (150) && !sp.outOfRange) {
      if (!goTop) {
        if (mounted) {
          setState(() {
            goTop = true;
          });
          parentCont.animateTo(225.0,
              duration: Duration(milliseconds: 200),
              curve: Curves.easeInOutCubic);
        }
      } else if (sp.pixels + 200 >= sp.maxScrollExtent &&
          !sp.outOfRange &&
          oneTime) {
        setState(() {
          oneTime = false;
          paginateRequest();
        });
        print("reach the bottom");
      }
    }
  }
}
