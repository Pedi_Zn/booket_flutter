import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/models/route_argument.dart';
import 'package:booket_flutter/widgets/books2.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:booket_flutter/widgets/alert_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:booket_flutter/widgets/books.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class Profile extends StatefulWidget {
  Profile({Key key}) : super(key: key);

  @override
  ProfileState createState() => ProfileState();
}

class ProfileState extends State<Profile>
    with
        SingleTickerProviderStateMixin,
        AfterLayoutMixin,
        AutomaticKeepAliveClientMixin {
  int _numberOfBooks;
  bool isLogedin = false;
  bool logoutResult = false;
  bool emptyImage = false;
  List<dynamic> basketList = [];

  final AccountsRepo accountsRepo = new AccountsRepo();
  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Profile(int _numberOfBooks) {
    this._numberOfBooks = _numberOfBooks;
  }

  @override
  Widget build(BuildContext context) {
    final iw = MainStateContainer.of(context);
    if (iw.token != null && iw.token != "") {
      // iw.me ham bayad handel she
      isLogedin = true;
    }

    List<dynamic> booksList = [];

    booksList = [
      // {
      //   "img": "assets/images/pretty_sky.jpg",
      //   "title": "برای یک روز دیگر",
      //   "author": "زهره زاهدی",
      //   "expireDays": 14
      // },
      // {
      //   "img": "assets/images/home.jpg",
      //   "title": "برای یک روز دیگر",
      //   "author": "زهره زاهدی",
      //   "expireDays": 7
      // },
      // {
      //   "img": "assets/images/pretty_sky.jpg",
      //   "title": "برای یک روز دیگر",
      //   "author": "زهره زاهدی",
      //   "expireDays": 14
      // },
    ];
    final pageWidth = MediaQuery.of(context).size.width;

    int count2 = pageWidth < 350.0
        ? 2
        : pageWidth < 550.0
            ? 3
            : pageWidth < 750.0
                ? 4
                : pageWidth < 950.0
                    ? 5
                    : pageWidth < 1150.0
                        ? 6
                        : pageWidth < 1300.0
                            ? 7
                            : 8;

    final devicePadding =
        MediaQuery.of(context).size.width < 350.0 ? 10.0 : 15.0;
    return LiquidPullToRefresh(
      key: _refreshIndicatorKey,
      onRefresh: _handleRefresh,
      showChildOpacityTransition: false,
      child: Scaffold(
        key: _scaffoldKey,
        body: ListView(
          children: [
            SingleChildScrollView(
              child: this.isLogedin
                  ? Column(
                      children: [
                        Container(
                          color: Colors.white70,
                          padding: EdgeInsets.all(25.0),
                          child: Row(children: [
                            Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          iw.me["title"],
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 15.0,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w800,
                                            decorationThickness: 1.0,
                                          ),
                                        ),
                                      ]),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          _numberOfBooks.toString() +
                                              DemoLocalizations.of(context)
                                                  .trans("Books_In_Library"),
                                          // textAlign: TextAlign.left,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 13.0,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w100,
                                            decorationThickness: 1.0,
                                          ),
                                        ),
                                      ])
                                ]),
                            Spacer(),
                            Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Icon(Icons.open_in_new,
                                            color: Colors.black),
                                        SizedBox(width: 5),
                                        GestureDetector(
                                          onTapDown: (s) {
                                            showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return CustomDialog(
                                                      title: DemoLocalizations
                                                              .of(context)
                                                          .trans("LogoutText"));
                                                });
                                            // setState(() {
                                            //   _onAlertWithCustomImagePressed(context);
                                            // });
                                          },
                                          child: Text(
                                            DemoLocalizations.of(context)
                                                .trans("Exit"),
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 13.0,
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w100,
                                              decorationThickness: 1.0,
                                            ),
                                          ),
                                        ),
                                      ]),
                                  SizedBox(height: 10),
                                  Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Icon(Icons.settings,
                                            color: Colors.black),
                                        SizedBox(width: 5),
                                        GestureDetector(
                                          onTapDown: (s) {
                                            setState(() {
                                              Navigator.of(context)
                                                  .pushNamed('/Setting');
                                            });
                                          },
                                          child: Text(
                                            DemoLocalizations.of(context)
                                                .trans("Settings"),
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 13.0,
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.w100,
                                              decorationThickness: 1.0,
                                            ),
                                          ),
                                        ),
                                      ])
                                ]),
                          ]),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              left: devicePadding, right: devicePadding),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Icon(
                                    AntDesign.shoppingcart,
                                    size: 33.0,
                                  ),
                                  Text(
                                    DemoLocalizations.of(context)
                                        .trans("Basket"),
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 17.0,
                                      fontWeight: FontWeight.bold,
                                      decorationThickness: 1.0,
                                    ),
                                  ),
                                ],
                              ),
                              basketList.length > 0
                                  ? RaisedButton(
                                      onPressed: () {
                                        Navigator.of(context)
                                            .pushNamed('/ShoppingCart');
                                      },
                                      child: Container(
                                          width: 90.0,
                                          child: Text(
                                            DemoLocalizations.of(context)
                                                .trans("Factor"),
                                            style:
                                                TextStyle(color: Colors.white),
                                            textAlign: TextAlign.center,
                                          )),
                                      color: Colors.black,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(7.0),
                                      ),
                                    )
                                  : Container()
                            ],
                          ),
                        ),
                        basketList.length > 0
                            ? Container(
                                height: (MediaQuery.of(context).size.width /
                                            count2) *
                                        8 /
                                        5 -
                                    35,
                                color: Colors.transparent,
                                alignment: Alignment.topRight,
                                child: AnimationLimiter(
                                  child: ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    shrinkWrap: true,
                                    padding: EdgeInsets.only(
                                        right: 7.5, left: 7.5, top: 5.0),
                                    addRepaintBoundaries: true,
                                    itemCount: basketList.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return RepaintBoundary(
                                          key: Key('str' + index.toString()),
                                          child: AnimationConfiguration
                                              .staggeredList(
                                            position: index,
                                            duration: const Duration(
                                                milliseconds: 500),
                                            delay: const Duration(
                                                milliseconds: 100),
                                            child: SlideAnimation(
                                              verticalOffset: 50.0,
                                              child: FadeInAnimation(
                                                child: GestureDetector(
                                                  onTap: () {
                                                    setState(() {
                                                      Navigator.of(context).pushNamed(
                                                          '/books/' +
                                                              basketList[index]
                                                                      ["book"]
                                                                  ["token"],
                                                          arguments:
                                                              RouteArgument(
                                                                  id: 12,
                                                                  argumentsList: [
                                                                false,
                                                                "salam"
                                                              ]));
                                                    });
                                                  },
                                                  child: new Books2(
                                                      basketList[index]["book"]
                                                              ["front_cover"]
                                                          ["medium"],
                                                      8.9,
                                                      null,
                                                      count2),
                                                ),
                                              ),
                                            ),
                                          ));
                                    },
                                  ),
                                ),
                              )
                            : Container(
                                padding: EdgeInsets.all(20.0),
                                child: !emptyImage
                                    ? CircularProgressIndicator()
                                    : Container(
                                        child: Column(
                                          children: [
                                            Image.asset(
                                              "assets/images/empty_cart.png",
                                              width: 170.0,
                                            ),
                                            Text("سبد خرید شما خالی است"),
                                          ],
                                        ),
                                      )),
                        Container(
                          padding: EdgeInsets.all(15.0),
                          child: Row(
                            children: [
                              Icon(AntDesign.book, size: 33.0),
                              Container(
                                width: 5.0,
                              ),
                              Text(
                                DemoLocalizations.of(context)
                                    .trans("Bookmarks"),
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.bold,
                                  decorationThickness: 1.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                        booksList.length > 0
                            ? Container(
                                color: Colors.transparent,
                                child: AnimationLimiter(
                                  child: GridView.count(
                                    physics: NeverScrollableScrollPhysics(),
                                    padding: EdgeInsets.only(
                                        right: 15.0, left: 15.0),
                                    crossAxisCount: count2,
                                    childAspectRatio: 0.7,
                                    shrinkWrap: true,
                                    crossAxisSpacing: 15.0,
                                    mainAxisSpacing: 15.0,
                                    children: List.generate(
                                      booksList.length,
                                      (int index) {
                                        return AnimationConfiguration
                                            .staggeredGrid(
                                          columnCount: count2,
                                          position: index,
                                          duration:
                                              const Duration(milliseconds: 375),
                                          child: ScaleAnimation(
                                            scale: 0.5,
                                            child: FadeInAnimation(
                                              child: new Books(
                                                  booksList[index]["img"],
                                                  0.0,
                                                  null,
                                                  count2),
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ),
                              )
                            : Container(
                                child: Column(
                                  children: [
                                    Image.asset(
                                      "assets/images/empty_bookmark.png",
                                      width: 90.0,
                                    ),
                                    Container(
                                      height: 10.0,
                                    ),
                                    Text("کتاب نشان شده‌ای ندارید"),
                                  ],
                                ),
                              ),
                      ],
                    )
                  : Column(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(30.0, 40.0, 30.0, 20.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Theme.of(context).accentColor,
                          ),
                          height: 420,
                          child: Align(
                            child: Container(
                              width: MediaQuery.of(context).size.width - 60,
                              color: Colors.transparent,
                              child: Stack(children: [
                                Positioned(
                                    right: -10,
                                    bottom: -10,
                                    child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(45.0),
                                        child: Image.asset(
                                          "assets/images/profile_book.png",
                                          width: 200.0,
                                        ))),
                                Column(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.fromLTRB(
                                          25.0, 40.0, 25.0, 40.0),
                                      width: MediaQuery.of(context).size.width -
                                          70,
                                      child: Text(
                                        DemoLocalizations.of(context)
                                            .trans("ShoAr"),
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                          fontSize: 15.0,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w800,
                                          decorationThickness: 1.0,
                                        ),
                                      ),
                                    ),
                                    Text(
                                      DemoLocalizations.of(context)
                                          .trans("Login"),
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                        decorationThickness: 1.0,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.fromLTRB(
                                          20.0, 10.0, 20.0, 0.0),
                                      child: RaisedButton(
                                        onPressed: () {
                                          Navigator.of(context)
                                              .pushNamed('/Test3');
                                        },
                                        child: Container(
                                            width: double.infinity,
                                            height: 40.0,
                                            child: Center(
                                                child: Text(
                                              "با " +
                                                  DemoLocalizations.of(context)
                                                      .trans("Phone"),
                                              style: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                  fontSize: 15.0,
                                                  fontWeight:
                                                      FontWeight.normal),
                                              textAlign: TextAlign.center,
                                            ))),
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(7.0),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.fromLTRB(
                                          20.0, 0.0, 20.0, 0.0),
                                      child: RaisedButton(
                                        onPressed: () {
                                          // Navigator.of(context)
                                          //     .pushNamed('/Test3');
                                          iw.authenticationBloc
                                              .setFirstTimeOnStorage(true);
                                          verifyPhone(context);
                                          // Navigator.of(context).pushNamed(
                                          //   "/Test",
                                          //   arguments: new RouteArgument(
                                          //     argumentsList: [
                                          //       {
                                          //         "0": "989912037674"
                                          //         // "0": "98" +
                                          //         // "/" +
                                          //         // iw.authenticationBloc.phoneNumber.substring(1, iw.authenticationBloc.phoneNumber.length).toString()
                                          //       },
                                          //       // {
                                          //       //   "0": iw.hostName
                                          //       // },
                                          //       // {
                                          //       //   "authenticationBloc": iw.authenticationBloc
                                          //       // },
                                          //       // {
                                          //       //   // "expire": response["text"].split(":")[1]
                                          //       // }
                                          //     ],
                                          //   id: "register")
                                          // );
                                        },
                                        child: Container(
                                            width: double.infinity,
                                            height: 40.0,
                                            child: Center(
                                                child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                  DemoLocalizations.of(context)
                                                      .trans("LoginWithGoogle"),
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                          .primaryColor,
                                                      fontSize: 15.0,
                                                      fontWeight:
                                                          FontWeight.normal),
                                                  textAlign: TextAlign.center,
                                                ),
                                                Container(
                                                  width: 15.0,
                                                ),
                                                Icon(
                                                  Zocial.googleplus,
                                                  color: Colors.white,
                                                  size: 18.0,
                                                ),
                                              ],
                                            ))),
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(7.0),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.fromLTRB(
                                          20.0, 0.0, 20.0, 10.0),
                                      child: RaisedButton(
                                        onPressed: () {
                                          Navigator.of(context)
                                              .pushNamed("/Testt");
                                          // loginPressed(context);
                                          // Navigator.of(context).pushNamed('/books/12343', arguments: RouteArgument(id:12, argumentsList: [true, "salam"]));
                                        },
                                        child: Container(
                                            width: double.infinity,
                                            height: 40.0,
                                            child: Center(
                                                child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                  DemoLocalizations.of(context)
                                                          .trans(
                                                              "LoginWithApple") +
                                                      "  ",
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                          .primaryColor,
                                                      fontSize: 15.0,
                                                      fontWeight:
                                                          FontWeight.normal),
                                                  textAlign: TextAlign.center,
                                                ),
                                                Container(
                                                  width: 24.0,
                                                ),
                                                Icon(
                                                  AntDesign.apple_o,
                                                  color: Colors.white,
                                                  size: 20.0,
                                                ),
                                              ],
                                            ))),
                                        color:
                                            Theme.of(context).primaryColorDark,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(7.0),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      DemoLocalizations.of(context)
                                          .trans("Register"),
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                        decorationThickness: 1.0,
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.fromLTRB(
                                          20.0, 10.0, 20.0, 0.0),
                                      child: RaisedButton(
                                        onPressed: () {
                                          Navigator.of(context)
                                              .pushNamed('/Register');
                                        },
                                        child: Container(
                                            width: double.infinity,
                                            height: 40.0,
                                            child: Center(
                                                child: Text(
                                              "با " +
                                                  DemoLocalizations.of(context)
                                                      .trans("Phone"),
                                              style: TextStyle(
                                                  color: Theme.of(context)
                                                      .primaryColorDark,
                                                  fontSize: 15.0,
                                                  fontWeight:
                                                      FontWeight.normal),
                                              textAlign: TextAlign.center,
                                            ))),
                                        color: Theme.of(context).primaryColor,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(7.0),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ]),
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 30.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: OutlineButton(
                                      onPressed: () {
                                        Navigator.of(context)
                                            .pushNamed('/Terms');
                                      },
                                      highlightedBorderColor:
                                          Theme.of(context).primaryColorDark,
                                      child: Container(
                                          height: 40.0,
                                          child: Center(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Icon(
                                                  MaterialCommunityIcons
                                                      .script_text_outline,
                                                  color: Colors.black,
                                                  size: 20,
                                                ),
                                                Container(
                                                  width: 3.0,
                                                ),
                                                Text(
                                                  DemoLocalizations.of(context)
                                                      .trans("Terms"),
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                          .primaryColorDark,
                                                      fontSize: 11.0,
                                                      fontWeight:
                                                          FontWeight.normal),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ],
                                            ),
                                          )),
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(7.0),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: 30.0,
                                  ),
                                  Expanded(
                                    child: OutlineButton(
                                      onPressed: () {
                                        Navigator.of(context)
                                            .pushNamed('/AboutUs');
                                      },
                                      highlightedBorderColor:
                                          Theme.of(context).primaryColorDark,
                                      child: Container(
                                          height: 40.0,
                                          child: Center(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                PlatformSvgWidget.asset(
                                                    'assets/images/booket_logo.svg',
                                                    color: Theme.of(context)
                                                        .primaryColorDark,
                                                    height: 10),
                                                Container(
                                                  width: 3.0,
                                                ),
                                                Text(
                                                  DemoLocalizations.of(context)
                                                      .trans("AboutUs"),
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                          .primaryColorDark,
                                                      fontSize: 11.0,
                                                      fontWeight:
                                                          FontWeight.normal),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ],
                                            ),
                                          )),
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(7.0),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Container(
                                height: 5.0,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: OutlineButton(
                                      onPressed: () {
                                        Navigator.of(context)
                                            .pushNamed('/Support');
                                      },
                                      highlightedBorderColor:
                                          Theme.of(context).primaryColorDark,
                                      child: Container(
                                          height: 40.0,
                                          child: Center(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Icon(
                                                  Icons.support_agent_outlined,
                                                  color: Colors.black,
                                                  size: 20,
                                                ),
                                                Container(
                                                  width: 3.0,
                                                ),
                                                Text(
                                                  DemoLocalizations.of(context)
                                                      .trans("Support"),
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                          .primaryColorDark,
                                                      fontSize: 11.0,
                                                      fontWeight:
                                                          FontWeight.normal),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ],
                                            ),
                                          )),
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(7.0),
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: 30.0,
                                  ),
                                  Expanded(
                                    child: OutlineButton(
                                      onPressed: () {
                                        Navigator.of(context)
                                            .pushNamed('/ContactUs');
                                      },
                                      highlightedBorderColor:
                                          Theme.of(context).primaryColorDark,
                                      child: Container(
                                          height: 40.0,
                                          child: Center(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Icon(
                                                  Icons.call,
                                                  color: Colors.black,
                                                  size: 20,
                                                ),
                                                Container(
                                                  width: 3.0,
                                                ),
                                                Text(
                                                  DemoLocalizations.of(context)
                                                      .trans("ContactUs"),
                                                  style: TextStyle(
                                                      color: Theme.of(context)
                                                          .primaryColorDark,
                                                      fontSize: 11.0,
                                                      fontWeight:
                                                          FontWeight.normal),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ],
                                            ),
                                          )),
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(7.0),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        // Text(
                        //   "Version 1.0.0",
                        //   style: TextStyle(
                        //     color: Theme.of(context).primaryColorDark,
                        //     fontSize: 13.0,
                        //     fontStyle: FontStyle.normal,
                        //     fontWeight: FontWeight.w800,
                        //     decorationThickness: 1.0,
                        //   ),
                        // ),
                        // Row(
                        //   mainAxisAlignment: MainAxisAlignment.center,
                        //   children: [
                        //     Text(
                        //       "by Pedram",
                        //       style: TextStyle(
                        //         color: Theme.of(context).primaryColorDark,
                        //         fontSize: 12.0,
                        //         fontStyle: FontStyle.normal,
                        //         fontWeight: FontWeight.w800,
                        //         decorationThickness: 1.0,
                        //       ),
                        //     ),
                        //     Container(
                        //         padding: EdgeInsets.all(2.0),
                        //         child: Icon(
                        //           Icons.favorite,
                        //           color: Colors.red,
                        //           size: 17,
                        //         )),
                        //     Text(
                        //       "Made with",
                        //       style: TextStyle(
                        //         color: Theme.of(context).primaryColorDark,
                        //         fontSize: 12.0,
                        //         fontStyle: FontStyle.normal,
                        //         fontWeight: FontWeight.w800,
                        //         decorationThickness: 1.0,
                        //       ),
                        //     ),
                        //   ],
                        // ),
                        Container(
                          height: 10.0,
                        )
                      ],
                    ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => false;

  @override
  void afterFirstLayout(BuildContext context) {
    final iw = MainStateContainer.of(context);
    accountsRepo
        .getBasket(
      iw.hostName,
      iw.token,
    )
        .then((basket) {
      iw.setCart(basket);
      for (int i = 0; i < basket["orders"].length; i++) {
        basketList.add(basket["orders"][i]);
      }
      if (basketList.length == 0) {
        setState(() {
          emptyImage = true;
        });
      } else {
        setState(() {
          emptyImage = false;
        });
      }
      setState(() {
        basketList = basketList;
      });
    });
  }

  Future<void> _handleRefresh() {
    final Completer<void> completer = Completer<void>();
    Timer(const Duration(seconds: 1), () {
      completer.complete();
      setState(() {
        Navigator.of(context).pushNamed("/Profile");
      });
    });
  }

  Future<void> verifyPhone(context) async {
    String salam = "09912037674";
  }

  Future<void> logoutPressed(context) async {
    final iw = MainStateContainer.of(context);
    accountsRepo.logout();
    iw.removeUser();
    iw.setToken(null);
    await iw.authenticationBloc.removeTokenFromStorage();
    logoutResult = false;
    if (iw.lastPathBeforeAuth != "") {
      Navigator.of(context).pop();
    } else {
      Navigator.of(context).pushReplacementNamed("/Login");
    }
  }
}
