import 'dart:async';

import 'package:after_layout/after_layout.dart';
import 'package:booket_flutter/api/accounts.api.dart';
import 'package:booket_flutter/blocs/grid_card_bloc.dart';
import 'package:booket_flutter/localization/localization.dart';
import 'package:booket_flutter/main_state_container.dart';
import 'package:booket_flutter/models/route_argument.dart';
import 'package:booket_flutter/platforms/PlatformSvgWidget.dart';
import 'package:booket_flutter/widgets/expireDate.dart';
import 'package:booket_flutter/widgets/grid_or_list_switch.dart';
import 'package:booket_flutter/widgets/refresh_tab.dart';
import 'package:fdottedline/fdottedline.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:booket_flutter/widgets/books.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:intl/intl.dart';

class Library extends StatefulWidget {
  Library({Key key}) : super(key: key);

  @override
  _LibraryState createState() => _LibraryState();
}

class _LibraryState extends State<Library>
    with
        SingleTickerProviderStateMixin,
        AfterLayoutMixin,
        AutomaticKeepAliveClientMixin {
  ScrollController parentCont, childCont;
  bool goTop = false;
  bool grid = true;
  double _gridPadding = 15;
  List<dynamic> imageList = [];
  List<dynamic> booksList = [];
  List<dynamic> authorsList = [];
  String token;

  GridCardBloc gridCardBloc = new GridCardBloc(1);
  AccountsRepo accountsRepo = new AccountsRepo();

  final GlobalKey<LiquidPullToRefreshState> _refreshIndicatorKey =
      GlobalKey<LiquidPullToRefreshState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    this.childCont = ScrollController(initialScrollOffset: 0.0);
    this.parentCont = ScrollController(initialScrollOffset: 0.0);
    this.childCont.addListener(_scrollListener);
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final pageWidth = MediaQuery.of(context).size.width;

    int count = pageWidth < 350.0
        ? 1
        : pageWidth < 550.0
            ? 2
            : pageWidth < 900.0
                ? 3
                : pageWidth < 1000.0
                    ? 4
                    : pageWidth < 1300.0
                        ? 5
                        : 6;

    return LiquidPullToRefresh(
      key: _refreshIndicatorKey,
      onRefresh: _handleRefresh,
      showChildOpacityTransition: false,
      child: AutoRefresh(
          gridCardBloc: gridCardBloc,
          child: Scaffold(
            key: _scaffoldKey,
            floatingActionButton: goTop
                ? FloatingActionButton(
                    backgroundColor: Theme.of(context).accentColor,
                    onPressed: () {
                      parentCont.animateTo(0.0,
                          duration: Duration(milliseconds: 200),
                          curve: Curves.easeInCubic);
                      childCont.animateTo(0.0,
                          duration: Duration(milliseconds: 200),
                          curve: Curves.easeInCubic);
                      setState(() {
                        goTop = true;
                      });
                    },
                    child: Icon(Icons.arrow_upward,
                        color: Colors.white, size: 25.0))
                : Container(),
            body: ListView(
                physics:
                    AlwaysScrollableScrollPhysics(), // handle beshe ba triggere +300.0 scroll
                controller: parentCont,
                scrollDirection: Axis.vertical,
                children: [
                  DefaultTabController(
                    length: 2,
                    initialIndex: 0,
                    child: Center(
                      child: Padding(
                          padding: const EdgeInsets.all(0),
                          child: GridOrListSwitch(
                              grid: grid,
                              swich: (data) {
                                setState(() {
                                  grid = data;
                                });
                              })),
                    ),
                  ),
                  (booksList.length > 0 && booksList[0] != null)
                      ? Center(
                          child: Container(
                            height: MediaQuery.of(context).size.height,
                            width: MediaQuery.of(context).size.width,
                            child: !grid
                                ? Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      LimitedBox(
                                        maxWidth: pageWidth > 700.0
                                            ? 680.0
                                            : pageWidth,
                                        child: AnimationLimiter(
                                          child: ListView.builder(
                                            padding:
                                                EdgeInsets.all(_gridPadding),
                                            controller: childCont,
                                            itemExtent: MediaQuery.of(context)
                                                        .size
                                                        .width /
                                                    (count + 1) +
                                                30,
                                            itemCount: booksList.length,
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              return Container(
                                                padding: EdgeInsets.only(
                                                    bottom: 15.0),
                                                child: RepaintBoundary(
                                                    key: Key('str' +
                                                        index.toString()),
                                                    child:
                                                        AnimationConfiguration
                                                            .staggeredList(
                                                      position: index,
                                                      duration: const Duration(
                                                          milliseconds: 500),
                                                      delay: const Duration(
                                                          milliseconds: 100),
                                                      child: SlideAnimation(
                                                        verticalOffset: 50.0,
                                                        child: FadeInAnimation(
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceAround,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Container(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            10.0),
                                                                width: MediaQuery.of(context)
                                                                            .size
                                                                            .width /
                                                                        (count +
                                                                            1) -
                                                                    20,
                                                                child:
                                                                    GestureDetector(
                                                                  onTapDown:
                                                                      (s) {
                                                                    setState(
                                                                        () {
                                                                      // selectedBooksList.removeAt(0);
                                                                      Navigator.of(context).pushNamed(
                                                                          '/books/' +
                                                                              booksList[index]["book"][
                                                                                  "token"],
                                                                          arguments: RouteArgument(
                                                                              id: 12,
                                                                              argumentsList: [
                                                                                true,
                                                                                "salam"
                                                                              ]));
                                                                    });
                                                                  },
                                                                  child: new Books(
                                                                      booksList[index]["book"]
                                                                              [
                                                                              "front_cover"]
                                                                          [
                                                                          "medium"],
                                                                      1.0,
                                                                      null,
                                                                      count),
                                                                ),
                                                              ),
                                                              // Container(width: 5.0), // spacer
                                                              Expanded(
                                                                  child:
                                                                      Container(
                                                                child: Column(
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .end,
                                                                  children: [
                                                                    // Spacer(),
                                                                    Container(
                                                                      padding: EdgeInsets.only(
                                                                          left:
                                                                              10.0,
                                                                          top:
                                                                              10.0),
                                                                      child: Column(
                                                                          children: [
                                                                            Row(
                                                                              children: [
                                                                                Flexible(
                                                                                    child: GestureDetector(
                                                                                  onTapDown: (s) {
                                                                                    setState(() {
                                                                                      // selectedBooksList.removeAt(0);
                                                                                      Navigator.of(context).pushNamed('/books/' + booksList[index]["book"]["token"], arguments: RouteArgument(id: 12, argumentsList: [true, "salam"]));
                                                                                    });
                                                                                  },
                                                                                  child: Text(
                                                                                    booksList[index]["book"]["title"].toString(),
                                                                                    maxLines: 2,
                                                                                    overflow: TextOverflow.ellipsis,
                                                                                    textAlign: TextAlign.right,
                                                                                    style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w600),
                                                                                  ),
                                                                                )),
                                                                              ],
                                                                            ),
                                                                            Container(
                                                                              height: 10.0,
                                                                            ),
                                                                            Container(
                                                                              padding: EdgeInsets.only(left: 15.0),
                                                                              child: Row(
                                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                                children: List.generate(booksList[index]["book"]["authors"].length, (i) {
                                                                                  return Container(
                                                                                      child: i == 0
                                                                                          ? Text(
                                                                                              booksList[index]["book"]["authors"][i]["title"].toString(),
                                                                                              maxLines: 1,
                                                                                              overflow: TextOverflow.ellipsis,
                                                                                              softWrap: false,
                                                                                              style: TextStyle(fontSize: 12.0),
                                                                                            )
                                                                                          : Text(
                                                                                              i < 2
                                                                                                  ? ", " + booksList[index]["book"]["authors"][i]["title"].toString()
                                                                                                  : i == 2
                                                                                                      ? ", ..."
                                                                                                      : "",
                                                                                              maxLines: 1,
                                                                                              overflow: TextOverflow.ellipsis,
                                                                                              softWrap: false,
                                                                                              style: TextStyle(fontSize: 12.0),
                                                                                            ));
                                                                                }),
                                                                              ),
                                                                            ),
                                                                          ]),
                                                                    ),
                                                                    Spacer(),

                                                                    Padding(
                                                                      padding:
                                                                          const EdgeInsets.all(
                                                                              0.0),
                                                                      child:
                                                                          Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.end,
                                                                        children: [
                                                                          Container(
                                                                              width: 80.0,
                                                                              height: 35.0,
                                                                              child: readTimestamp(booksList[index]["expire_at"]) != null
                                                                                  ? new ExpireDate(
                                                                                      readTimestamp(booksList[index]["expire_at"]),
                                                                                      50.0,
                                                                                      100.0,
                                                                                    )
                                                                                  : Container())
                                                                        ],
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              )),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    )),
                                              );
                                            },
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                : Container(
                                    height: MediaQuery.of(context)
                                        .size
                                        .height, // chos mesghal
                                    color: Colors.transparent,
                                    child: AnimationLimiter(
                                      child: GridView.count(
                                        padding: EdgeInsets.all(_gridPadding),
                                        controller: childCont,
                                        crossAxisCount: count,
                                        childAspectRatio: 0.7,
                                        shrinkWrap: true,
                                        crossAxisSpacing: _gridPadding,
                                        mainAxisSpacing: _gridPadding,
                                        children: List.generate(
                                          booksList.length,
                                          (int index) {
                                            return AnimationConfiguration
                                                .staggeredGrid(
                                              columnCount: count,
                                              position: index,
                                              duration: const Duration(
                                                  milliseconds: 375),
                                              child: ScaleAnimation(
                                                scale: 0.5,
                                                child: FadeInAnimation(
                                                  child: GestureDetector(
                                                      onTapDown: (s) {
                                                        setState(() {
                                                          Navigator.of(context).pushNamed(
                                                              '/books/' +
                                                                  booksList[index]
                                                                          [
                                                                          "book"]
                                                                      ["token"],
                                                              arguments:
                                                                  RouteArgument(
                                                                      id: 12,
                                                                      argumentsList: [
                                                                    true,
                                                                    "salam"
                                                                  ]));
                                                        });
                                                      },
                                                      child: new Books(
                                                          booksList[index]
                                                                      ["book"][
                                                                  "front_cover"]
                                                              ["medium"],
                                                          0.0,
                                                          readTimestamp(
                                                              booksList[index][
                                                                  "expire_at"]),
                                                          count)),
                                                ),
                                              ),
                                            );
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                          ),
                        )
                      : (((booksList.length > 0) && booksList[0] == null) ||
                              token == null)
                          ? Center(
                              child: Container(
                              margin: EdgeInsets.only(
                                  top:
                                      (MediaQuery.of(context).size.height / 2) -
                                          230),
                              child: Column(
                                children: [
                                  Image.asset(
                                    "assets/images/empty_library.png",
                                    width: 100.0,
                                  ),
                                  // PlatformSvgWidget.asset('assets/images/empty_library.svg'),
                                  Container(
                                    height: 15.0,
                                  ),
                                  FDottedLine(
                                    color: Colors.black,
                                    corner: FDottedLineCorner.all(10.0),

                                    /// add widget
                                    child: Container(
                                      padding: EdgeInsets.all(10.0),
                                      width: MediaQuery.of(context).size.width -
                                          80,
                                      // height: 110,
                                      alignment: Alignment.center,
                                      child: Text(
                                        "کتابخانه شما خالی است. کتاب‌های خریداری شده شما در این قسمت نمایش داده ‌می‌شوند\nبرای بارگذاری مجدد، صفحه را پایین بکشید",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 13.0),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ))
                          : Center(
                              child: Container(
                                  margin: EdgeInsets.only(
                                      top: (MediaQuery.of(context).size.height -
                                              220) /
                                          2),
                                  height: 50.0,
                                  width: 50.0,
                                  child: CircularProgressIndicator()),
                            ),
                ]),
          )),
    );
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void afterFirstLayout(BuildContext context) async {
    final iw = MainStateContainer.of(context);
    token = await iw.authenticationBloc.getTokenFromStorage();
    setState(() {
      imageList = imageList;
    });
    if (token != null) {
      accountsRepo
          .getLibrary(iw.hostName, token
              // "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIwNCwiaXNzIjoiaHR0cHM6Ly9teWJvb2tldC5jb20vYXBpL2F1dGgvbG9naW4iLCJpYXQiOjE2MTQ1MjM2MzUsImV4cCI6MTYyMDU2ODAzNSwibmJmIjoxNjE0NTIzNjM1LCJqdGkiOiJiYzd1WTBIZjR0UlBkVVQ3In0.xUF82WWk1nZepr_wnWsLWPgAbWJlpy87YdWKOnK-ozc",
              )
          .then((value) {
        if (value["data"].length == 0) booksList.add(null);
        for (int i = 0; i < value["data"].length; i++) {
          if (value["data"][i] != null && value["data"][i] != "")
            booksList.add(value["data"][i]);
        }
        setState(() {
          imageList = imageList;
        });
      });
    } else {
      // Navigator.of(context).popAndPushNamed('/Profile');
    }
  }

  String readTimestamp(int timestamp) {
    if (timestamp != null) {
      var now = new DateTime.now();
      var format = new DateFormat('HH:mm a');
      var date = new DateTime.fromMicrosecondsSinceEpoch(timestamp * 1000);
      var diff = date.difference(now);
      var time = '';
      if (diff.inSeconds <= 0 ||
          diff.inSeconds > 0 && diff.inMinutes == 0 ||
          diff.inMinutes > 0 && diff.inHours == 0 ||
          diff.inHours > 0 && diff.inDays == 0) {
        time = format.format(date);
      } else {
        if (diff.inDays <= 14) {
          time = diff.inDays.toString();
        } else {
          time = null;
        }
      }
      return time;
    } else
      return null;
  }

  Future<void> _handleRefresh() {
    final Completer<void> completer = Completer<void>();
    Timer(const Duration(seconds: 1), () {
      completer.complete();
      setState(() {
        Navigator.of(context).pushNamed("/Library");
      });
    });
  }

  void _scrollListener() {
    ScrollPosition sp;
    sp = this.childCont.positions.elementAt(0);
    if (sp.pixels <= (150) && !sp.outOfRange) {
      if (goTop) {
        if (mounted) {
          setState(() {
            goTop = false;
          });
          parentCont.animateTo(0.0,
              duration: Duration(milliseconds: 200),
              curve: Curves.easeInOutCubic);
        }
      }
    } else if (sp.pixels > (150) && !sp.outOfRange) {
      if (!goTop) {
        if (mounted) {
          setState(() {
            goTop = true;
          });
          parentCont.animateTo(225.0,
              duration: Duration(milliseconds: 200),
              curve: Curves.easeInOutCubic);
        }
      }
    }
  }
}
