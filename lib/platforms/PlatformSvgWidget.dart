import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PlatformSvgWidget {
  static Widget asset(String assetName,
      {double width,
      double height,
      BoxFit fit = BoxFit.contain,
      Color color,
      alignment = Alignment.center,
      placeholderBuilder,
      String semanticsLabel}) {
    List<String> splitedPath = assetName.split("/");
    var fileName = splitedPath.last;
    if (kIsWeb) {
      if (fileName.split("svg").length > 1) {
        splitedPath.removeLast();
        fileName = splitedPath.join("/") +
            "/orange/" +
            fileName.split(".")[0] +
            ".png";

        return Image.network("/$fileName",
            width: width,
            height: height,
            fit: fit,
            color: color,
            alignment: alignment,
            // loadingBuilder: placeholderBuilder,
            semanticLabel: semanticsLabel);
      }
    } else {
      fileName = assetName;

      return SvgPicture.asset(fileName,
          width: width,
          height: height,
          fit: fit,
          color: color,
          alignment: alignment,
          placeholderBuilder: placeholderBuilder,
          semanticsLabel: semanticsLabel);
    }
  }

  static Widget network(String url,
      {double width,
      double height,
      BoxFit fit = BoxFit.contain,
      Color color,
      alignment = Alignment.center,
      placeholderBuilder,
      String semanticsLabel}) {
    if (kIsWeb) {
      return Image.network(url,
          width: width,
          height: height,
          fit: fit,
          color: color,
          // loadingBuilder: placeholderBuilder,
          alignment: alignment,
          semanticLabel: semanticsLabel);
    } else {
      return SvgPicture.network(url,
          width: width,
          height: height,
          fit: fit,
          color: color,
          alignment: alignment,
          placeholderBuilder: placeholderBuilder,
          semanticsLabel: semanticsLabel);
    }
  }
}
