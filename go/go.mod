module booket_flutter/go

go 1.13

require (
	github.com/go-flutter-desktop/go-flutter v0.43.0
	github.com/go-flutter-desktop/plugins/path_provider v0.4.0
	github.com/go-gl/gl v0.0.0-20210501111010-69f74958bac0 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20210410170116-ea3d685f79fb // indirect
	github.com/go-ole/go-ole v1.2.4 // indirect
	github.com/pkg/errors v0.9.1
	golang.org/x/sys v0.0.0-20210514084401-e8d321eab015 // indirect
	golang.org/x/text v0.3.6 // indirect
)
